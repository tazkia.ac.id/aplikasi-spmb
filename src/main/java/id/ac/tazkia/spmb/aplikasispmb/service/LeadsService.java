package id.ac.tazkia.spmb.aplikasispmb.service;

import id.ac.tazkia.spmb.aplikasispmb.constants.AppConstants;
import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.dto.LeadsDto;
import id.ac.tazkia.spmb.aplikasispmb.dto.NotifRegistrasiReferalDto;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;

@Service @Transactional
public class LeadsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(LeadsService.class);


    @Autowired private LeadsDao leadsDao;
    @Autowired private UserDao userDao;
    @Autowired private RoleDao roleDao;
    @Autowired private UserPasswordDao userPasswordDao;
    @Autowired private TahunAjaranDao tahunAjaranDao;
    @Autowired private PasswordEncoder passwordEncoder;
    @Autowired private ApiService apiService;
    @Value("${group.leads}") private String insertGroupLeads;

    @Autowired private ReferalDao referalDao;
    @Autowired private NotifikasiService notifikasiService;



    public String registrasiLeads(LeadsDto leadsDto){
        Leads leads = new Leads();

//        Leads cekLeads = leadsDao.cariUsername("%"+leadsDto.getUsername().toLowerCase()+"%");
        User cekLeads = userDao.findByUsername(leadsDto.getEmail());

        if (cekLeads == null) {
            //CreateUser&Password
            User user = new User();
            Role role = roleDao.findById(AppConstants.ROLE_PENDAFTAR).get();

            user.setUsername(leadsDto.getEmail());
            user.setActive(true);
            user.setRole(role);
            userDao.save(user);

            UserPassword password = new UserPassword();
            password.setUser(user);
            password.setPassword(passwordEncoder.encode(leadsDto.getPassword()));
            userPasswordDao.save(password);
            //

            TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
            leads.setTahunAjaran(ta);
            leads.setUser(user);

            Referal referal = referalDao.findAllByKodeReferal(leadsDto.getKode());
            if (referal == null) {
                LOGGER.info("Tidak menggunakan kode referal");
            } else {
                LOGGER.info("Kode Referal ditemukan {}", referal);
                leads.setKode(referal);
            }

            BeanUtils.copyProperties(leadsDto, leads);
            leadsDao.save(leads);

//            apiService.insertSubcriber(leads);
//            apiService.insertGroup(leads, insertGroupLeads);

            if (referal != null) {
                NotifRegistrasiReferalDto nrf = new NotifRegistrasiReferalDto();
                nrf.setEmail(leadsDto.getEmail());
                nrf.setNama(leadsDto.getNama());
                nrf.setNoHp(leadsDto.getNoHp());
                nrf.setEmailRef(referal.getEmail());
                nrf.setKodeRef(referal.getKodeReferal());

                notifikasiService.NotifikasiReferal(nrf);
            } else {
                LOGGER.info("Kode referal tidak memiliki email");
            }

            LOGGER.info("Leads a.n {} Berhasiil di simpan", leads.getNama());
        } else {
            LOGGER.info("Username a.n {} sudah ada", cekLeads.getUsername());

        }


        return "redirect:/selesai";

    }

}

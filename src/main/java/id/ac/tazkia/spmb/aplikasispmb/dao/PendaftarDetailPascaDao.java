package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PendaftarDetailPascaDao extends PagingAndSortingRepository<PendaftarDetailPasca, String>, CrudRepository<PendaftarDetailPasca, String> {

    PendaftarDetailPasca findByPendaftar(Pendaftar pendaftar);

    Iterable<PendaftarDetailPasca> findByPendaftarTahunAjaranAktif(Status aktif);

    Iterable<PendaftarDetailPasca> findByPendaftarTahunAjaranAktifAndPendaftarLeadsJenjang(Status aktif, Jenjang s2);

    Page<PendaftarDetailPasca> findByNimNotNullAndPendaftarLeadsNamaContainingIgnoreCaseOrNimContainingIgnoreCaseAndPendaftarTahunAjaranOrderByNimAsc(String search, String search1, TahunAjaran ta, Pageable page);

    Page<PendaftarDetailPasca> findByNimNotNullAndPendaftarTahunAjaran(TahunAjaran ta, Pageable page);
    Iterable<PendaftarDetailPasca> findByNimNotNullAndPendaftarTahunAjaran(TahunAjaran ta);
    Iterable<PendaftarDetailPasca> findByNimNotNullAndPendaftarTahunAjaranAndNim(TahunAjaran ta, String nim);

    PendaftarDetailPasca findByNim(String nim);

    @Query(value = "select count(*) as jumlah from  (select count(t.id) from tagihan t " +
            "inner join pendaftar p on p.id = t.id_pendaftar\n" +
            "            inner join pendaftar_detail_pasca pd on pd.id_pendaftar = p.id " +
            "            inner join leads l on l.id = p.id_leads where t.id_jenisbiaya = 002  and  p.id_tahun = :ta " +
            "            and l.jenjang = 'S2' and t.lunas = true group by t.id_pendaftar ) aa", nativeQuery = true)
    Long countByPendaftarTahunAjaran(String ta);
}

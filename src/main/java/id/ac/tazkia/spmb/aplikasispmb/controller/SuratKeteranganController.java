package id.ac.tazkia.spmb.aplikasispmb.controller;

import fr.opensagres.xdocreport.converter.ConverterTypeTo;
import fr.opensagres.xdocreport.converter.Options;
import fr.opensagres.xdocreport.core.document.DocumentKind;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import id.ac.tazkia.spmb.aplikasispmb.dao.PendaftarDetailDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.PendaftarDetailPascaDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.PendaftarOrangTuaDao;
import id.ac.tazkia.spmb.aplikasispmb.entity.Pendaftar;
import id.ac.tazkia.spmb.aplikasispmb.entity.PendaftarDetail;
import id.ac.tazkia.spmb.aplikasispmb.entity.PendaftarDetailPasca;
import id.ac.tazkia.spmb.aplikasispmb.entity.PendaftarOrangtua;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Controller
public class SuratKeteranganController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SuratKeteranganController.class);
    @Value("classpath:keterangan-lulus.odt")
    private Resource templateSuratKeterangan;

    @Value("classpath:keterangan-lulus-pasca.odt")
    private Resource templateSuratKeteranganPasca;

    @Autowired
    private PendaftarDetailDao pendaftarDetailDao;
    @Autowired
    private PendaftarOrangTuaDao pendaftarOrangTuaDao;
    @Autowired
    private PendaftarDetailPascaDao pendaftarDetailPascaDao;

    @GetMapping("/suratKeterangan")
    public void suratKeterangan(@RequestParam(name = "pendaftar") Pendaftar pendaftar,
                                HttpServletResponse response) {
        try {
            // 0. Setup converter
            Options options = Options.getFrom(DocumentKind.ODT).to(ConverterTypeTo.PDF);

            // 1. Load template dari file
            InputStream in = templateSuratKeterangan.getInputStream();

            // 2. Inisialisasi template engine, menentukan sintaks penulisan variabel
            IXDocReport report = XDocReportRegistry.getRegistry().
                    loadReport(in, TemplateEngineKind.Freemarker);

            // 3. Context object, untuk mengisi variabel
            PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pendaftar);
            PendaftarOrangtua pendaftarOrangtua = pendaftarOrangTuaDao.findByPendaftar(pendaftar);

            IContext ctx = report.createContext();
            if (pendaftarDetail != null && pendaftarOrangtua != null) {
                ctx.put("nama", pendaftarDetail.getPendaftar().getLeads().getNama());
                ctx.put("ttl", pendaftarDetail.getTempatLahir() + ',' + pendaftarDetail.getTanggalLahir());
                ctx.put("asalSekolah", pendaftarDetail.getPendaftar().getNamaAsalSekolah());
                ctx.put("namaAyah", pendaftarOrangtua.getNamaAyah());
                ctx.put("alamat", pendaftarDetail.getAlamatRumah());
                ctx.put("programStudi", pendaftarDetail.getPendaftar().getProgramStudi().getNama());
            } else if (pendaftarDetail == null && pendaftarOrangtua == null) {
                ctx.put("nama",pendaftar.getLeads().getNama());
                ctx.put("ttl", "-");
                ctx.put("asalSekolah", pendaftar.getNamaAsalSekolah());
                ctx.put("namaAyah", "-");
                ctx.put("alamat", "-");
                ctx.put("programStudi", pendaftar.getProgramStudi().getNama());
            }

            Locale indonesia = new Locale("id", "id");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEEE, dd MMMM yyyy", indonesia);

            LocalDate tanggal =
                    LocalDate.now(ZoneId.systemDefault());


            String tanggalSekarang = tanggal.format(formatter);

            ctx.put("tanggalSekarang", tanggalSekarang);
            ctx.put("bulan", tanggal.getMonthValue());


            response.setHeader("Content-Disposition", "attachment;filename=Surat-Keterangan.pdf");
            OutputStream out = response.getOutputStream();
            report.convert(ctx, options, out);
            out.flush();
        } catch (Exception err) {
            LOGGER.error(err.getMessage(), err);
        }
    }

    @GetMapping("/suratKeteranganPasca")
    public void suratKeteranganPasca(@RequestParam(name = "pendaftar") Pendaftar pendaftar,
                                HttpServletResponse response) {
        try {
            // 0. Setup converter
            Options options = Options.getFrom(DocumentKind.ODT).to(ConverterTypeTo.PDF);

            // 1. Load template dari file
            InputStream in = templateSuratKeteranganPasca.getInputStream();

            // 2. Inisialisasi template engine, menentukan sintaks penulisan variabel
            IXDocReport report = XDocReportRegistry.getRegistry().
                    loadReport(in, TemplateEngineKind.Freemarker);

            // 3. Context object, untuk mengisi variabel
            PendaftarDetailPasca pendaftarDetail = pendaftarDetailPascaDao.findByPendaftar(pendaftar);

            IContext ctx = report.createContext();
            if (pendaftarDetail != null ) {
                ctx.put("nama", pendaftarDetail.getPendaftar().getLeads().getNama());
                ctx.put("ttl", pendaftarDetail.getTempatLahir() + ',' + pendaftarDetail.getTanggalLahir());
                ctx.put("asalSekolah", pendaftarDetail.getPendaftar().getNamaAsalSekolah());
                ctx.put("namaAyah", pendaftarDetail.getNamaAyah());
                ctx.put("alamat", pendaftarDetail.getAlamatRumah());
                ctx.put("programStudi", pendaftarDetail.getPendaftar().getProgramStudi().getNama());
            } else if (pendaftarDetail == null) {
                ctx.put("nama",pendaftar.getLeads().getNama());
                ctx.put("ttl", "-");
                ctx.put("asalSekolah", pendaftar.getNamaAsalSekolah());
                ctx.put("namaAyah", "-");
                ctx.put("alamat", "-");
                ctx.put("programStudi", pendaftar.getProgramStudi().getNama());
            }

            Locale indonesia = new Locale("id", "id");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEEE, dd MMMM yyyy", indonesia);

            LocalDate tanggal =
                    LocalDate.now(ZoneId.systemDefault());


            String tanggalSekarang = tanggal.format(formatter);

            ctx.put("tanggalSekarang", tanggalSekarang);
            ctx.put("bulan", tanggal.getMonthValue());


            response.setHeader("Content-Disposition", "attachment;filename=Surat-Keterangan-Pasca.pdf");
            OutputStream out = response.getOutputStream();
            report.convert(ctx, options, out);
            out.flush();
        } catch (Exception err) {
            LOGGER.error(err.getMessage(), err);
        }
    }
}

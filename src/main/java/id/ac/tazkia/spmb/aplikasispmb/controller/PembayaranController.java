package id.ac.tazkia.spmb.aplikasispmb.controller;

import fr.opensagres.xdocreport.converter.ConverterTypeTo;
import fr.opensagres.xdocreport.converter.Options;
import fr.opensagres.xdocreport.core.document.DocumentKind;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import id.ac.tazkia.spmb.aplikasispmb.constants.AppConstants;
import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import id.ac.tazkia.spmb.aplikasispmb.service.ApiService;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.flywaydb.core.internal.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

@Controller
public class PembayaranController {
    private static final Logger logger = LoggerFactory.getLogger(PembayaranController.class);



    @Autowired private PembayaranDao pembayaranDao;
    @Autowired private TagihanDao tagihanDao;
    @Autowired private JenisBiayaDao jenisBiayaDao;
    @Autowired private HasilTestDao hasilTestDao;
    @Autowired private BankDao bankDao;
    @Autowired private ProgramStudiDao programStudiDao;
    @Autowired private PendaftarDetailDao pendaftarDetailDao;
    @Autowired private PendaftarOrangTuaDao pendaftarOrangTuaDao;
    @Autowired private TahunAjaranDao tahunAjaranDao;
    @Value("classpath:kwitansi.odt")
    private Resource templateKwitansi;

    @Value("${upload.folder.bukti}")
    private String uploadFolder;
    @Autowired
    private TotalDiskonDao totalDiskonDao;

    @Autowired
    private ApiService apiService;

    @ModelAttribute("bank")
    public Iterable<Bank> bank() {
        return bankDao.findAll(Sort.by(Sort.Direction.ASC, "id"));
//        return bankDao.findAll(new Sort(Sort.Direction.ASC, "id"));
    }
    @GetMapping("/pembayaran/form")
    public void formPembayaran(@RequestParam(value = "id", required = true) String id,
                               @RequestParam(required = false) String error,
                               Model m) {
        //defaultnya, isi dengan object baru
        Pembayaran p = new Pembayaran();
        m.addAttribute("bayar", p);
        m.addAttribute("error", error);

        Tagihan t = tagihanDao.findById(id).get();
        if (t != null) {
            p.setNilai(t.getNilai());
            p.setTagihan(t);
        }
    }

    @Value("${group.pendaftar}") private String  deleteGroupPendaftar;
    @Value("${group.lunas.pendaftar}") private String insertGroupLunasPendaftar;
    @Value("${group.lunas.du}") private String insertGroupLunasDaftarUlang;
    @Value("${group.lulus.test}") private String deleteGroupHasilTest;
    @PostMapping("/pembayaran/form")
    public String prosesForm(@ModelAttribute @Valid Pembayaran pembayaran, BindingResult errors,
                             MultipartFile fileBukti) throws Exception {

//        if (errors.hasErrors()) {
//            logger.debug("Error upload bukti pembayaran : {}", errors.toString());
//            return "redirect:/pembayaran/form?error=Invalid&id=" + pembayaran.getTagihan().getId();
//        }

        String idPeserta = pembayaran.getTagihan().getPendaftar().getId();

        String namaFile = fileBukti.getName();
        String jenisFile = fileBukti.getContentType();
        String namaAsli = fileBukti.getOriginalFilename();
        Long ukuran = fileBukti.getSize();

        logger.debug("Nama File : {}", namaFile);
        logger.debug("Jenis File : {}", jenisFile);
        logger.debug("Nama Asli File : {}", namaAsli);
        logger.debug("Ukuran File : {}", ukuran);

//        memisahkan extensi
        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }

        String idFile = UUID.randomUUID().toString();
        String lokasiUpload = uploadFolder + File.separator + idPeserta;
        logger.debug("Lokasi upload : {}", lokasiUpload);
        new File(lokasiUpload).mkdirs();
        File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
        pembayaran.setBuktiPembayaran(idFile + "." + extension);
        fileBukti.transferTo(tujuan);
        logger.debug("File sudah dicopy ke : {}", tujuan.getAbsolutePath());

        Tagihan tagihan = pembayaran.getTagihan();

//Cek Prodi
//        ProgramStudi pr07 = programStudiDao.findById("007").get();
        ProgramStudi pr09 = programStudiDao.findById("009").get();
        ProgramStudi pr08 = programStudiDao.findById("008").get();
//        Boolean pen1 = tagihan.getPendaftar().getProgramStudi().equals(pr07);
        Boolean pen2 = tagihan.getPendaftar().getProgramStudi().equals(pr08);
        Boolean pen3 = tagihan.getPendaftar().getProgramStudi().equals(pr09);
        Boolean cekPodi =  pen2 == true || pen3 == true;
        if (tagihan.getJenisBiaya().getId().equals(AppConstants.JENIS_BIAYA_DAFTAR_ULANG)) {
            PendaftarDetail dp = pendaftarDetailDao.findByPendaftar(tagihan.getPendaftar());
            if (dp == null) {
                logger.warn("Tagihan dengan nomor {} tidak memiliki data detail pendaftar", tagihan.getNomorTagihan());
            }
//suketPasca
            if (cekPodi == true) {
                logger.debug("Program Studi Pasca :" + tagihan.getPendaftar().getProgramStudi().getNama());
//                notifikasiService.kirimNotifikasiKeteranganLulusPasca(dp);
            }else{
//suketS1
                logger.debug("Program Studi S1  :" + tagihan.getPendaftar().getProgramStudi().getNama());
//                notifikasiService.kirimNotifikasiKeteranganLulus(dp);
            }
        }

        logger.debug("Bank : {}", pembayaran.getBank());
        pembayaran.setReferensi(pembayaran.getBuktiPembayaran());
        pembayaran.setWaktuPembayaran(LocalDateTime.now());
        pembayaran.setNilai(tagihan.getNilai());
        pembayaranDao.save(pembayaran);

        tagihan.setLunas(true);
        tagihanDao.save(tagihan);

//        if (tagihan.getJenisBiaya().equals(AppConstants.JENIS_BIAYA_PENDAFTARAN)) {
//            apiService.insertGroup(tagihan.getPendaftar().getLeads(), insertGroupLunasPendaftar);
//            apiService.deleteGroup(tagihan.getPendaftar().getLeads(), deleteGroupPendaftar);
//
//        }
//        if (tagihan.getJenisBiaya().getId().equals(AppConstants.JENIS_BIAYA_DAFTAR_ULANG)) {
//            hitungTotalDiskon(tagihan);
//            apiService.deleteGroup(tagihan.getPendaftar().getLeads(), deleteGroupHasilTest);
//            apiService.insertGroup(tagihan.getPendaftar().getLeads(), insertGroupLunasDaftarUlang);
//        }
        return "redirect:/tagihan/list";
    }



    public Pembayaran cekPembayaran (Pendaftar pendaftar){

        JenisBiaya jenisBiaya = new JenisBiaya();
        jenisBiaya.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);

        Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiaya(pendaftar, jenisBiaya);
        if (tagihan == null){
            logger.warn("Tagihan dengan pendaftar {} tidak ada dalam database", pendaftar.getLeads().getNama());
        }

        Pembayaran pembayaran = pembayaranDao.findByTagihan(tagihan);
        if (pembayaran == null || tagihan == null) {
            logger.warn("Tagihan {} belum dilunasi", tagihan.getNomorTagihan());
        }
        return pembayaran;
    }

    public Pembayaran cekPembayaranDU(Pendaftar pendaftar){

        JenisBiaya jenisBiaya = new JenisBiaya();
        jenisBiaya.setId(AppConstants.JENIS_BIAYA_DAFTAR_ULANG);

        Tagihan tagihan = tagihanDao.cekPendaftarDanJenisBiaya(pendaftar.getId(), jenisBiaya.getId());
        System.out.println("Tagihan" + tagihan
        );
        if (tagihan == null){
            logger.warn("Tagihan dengan pendaftar {} tidak ada dalam database", pendaftar.getLeads().getNama());
        }

        Pembayaran pembayaran = pembayaranDao.findByTagihan(tagihan);
        if (pembayaran == null || tagihan == null) {
            logger.warn("Tagihan {} belum dilunasi");
            System.out.println("Tagihannya : " + pembayaran);
        }
        return pembayaran;
    }

    @ModelAttribute("listJenisTagihan")
    public Iterable<JenisBiaya> listJenisTagihan() {
        return jenisBiayaDao.findAll();
    }

    @GetMapping("/pembayaran/list")
    public void daftarPembayaran(@RequestParam(required = false) String pendaftar, @RequestParam(required = false) JenisBiaya jenisBiaya, Jenjang jenjang, Model m, Pageable page) {
        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
        if (jenjang != null && pendaftar != null && jenisBiaya != null) {
            m.addAttribute("pendaftar", pendaftar);
            m.addAttribute("jenisBiaya", jenisBiaya);
            m.addAttribute("jenjang", jenjang);
            m.addAttribute("daftarPembayaran", pembayaranDao.findByTagihanPendaftarLeadsJenjangAndTagihanJenisBiayaAndTagihanPendaftarLeadsNamaContainingIgnoreCaseAndTagihanPendaftarTahunAjaran(jenjang,jenisBiaya,pendaftar,ta, page));
        }else if (jenjang == null && pendaftar != null && jenisBiaya != null) {
            m.addAttribute("pendaftar", pendaftar);
            m.addAttribute("jenisBiaya", jenisBiaya);
            m.addAttribute("daftarPembayaran", pembayaranDao.findByTagihanJenisBiayaAndTagihanPendaftarLeadsNamaContainingIgnoreCaseAndTagihanPendaftarTahunAjaran(jenisBiaya,pendaftar,ta, page));
        }else if (jenjang != null && jenisBiaya != null && pendaftar == null){
            m.addAttribute("jenisBiaya",jenisBiaya);
            m.addAttribute("jenjang",jenjang);
            m.addAttribute("daftarPembayaran", pembayaranDao.findByTagihanPendaftarLeadsJenjangAndTagihanJenisBiayaAndTagihanPendaftarTahunAjaran(jenjang,jenisBiaya,ta, page));
        }else if (jenjang != null && jenisBiaya == null && pendaftar != null){
            m.addAttribute("pendaftar",pendaftar);
            m.addAttribute("jenjang",jenjang);
            m.addAttribute("daftarPembayaran", pembayaranDao.findByTagihanPendaftarLeadsJenjangAndTagihanPendaftarLeadsNamaContainingIgnoreCaseAndTagihanPendaftarTahunAjaran(jenjang,pendaftar,ta, page));
        }else if (jenjang != null && jenisBiaya == null && pendaftar == null){
            m.addAttribute("jenjang",jenjang);
            m.addAttribute("daftarPembayaran", pembayaranDao.findByTagihanPendaftarLeadsJenjangAndTagihanPendaftarTahunAjaran(jenjang,ta, page));
        }else if (jenjang == null && jenisBiaya != null && pendaftar == null){
            m.addAttribute("jenisBiaya",jenisBiaya);
            m.addAttribute("daftarPembayaran", pembayaranDao.findByTagihanJenisBiayaAndTagihanPendaftarTahunAjaran(jenisBiaya,ta, page));
        }else if (jenjang == null && pendaftar != null && jenisBiaya == null) {
            m.addAttribute("pendaftar", pendaftar);
            m.addAttribute("daftarPembayaran", pembayaranDao.findByTagihanPendaftarLeadsNamaContainingIgnoreCaseAndTagihanPendaftarTahunAjaran(pendaftar,ta, page));
        }else{
            m.addAttribute("daftarPembayaran", pembayaranDao.findByTagihanPendaftarTahunAjaran(ta,page));
        }
    }

    @GetMapping("/pembayaran/listS2")
    public void daftarPembayaranS2(@RequestParam(required = false) String pendaftar, Model m, Pageable page) {
        if (pendaftar != null) {
            m.addAttribute("pendaftar", pendaftar);
            m.addAttribute("daftarPembayaran", pembayaranDao.findByTagihanPendaftarLeadsJenjangAndTagihanPendaftarLeadsNamaContainingIgnoreCase(Jenjang.S2,pendaftar, page));
        }else{
            m.addAttribute("daftarPembayaran", pembayaranDao.findByTagihanPendaftarLeadsJenjang(Jenjang.S2,page));
        }
    }

    @ModelAttribute("awalBulan")
    public String awalBulan(){
        return LocalDate.now().withDayOfMonth(1).format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

    @ModelAttribute("akhirBulan")
    public String akhirBulan(){
        return LocalDate.now().withDayOfMonth(LocalDate.now().lengthOfMonth()).format(DateTimeFormatter.ISO_LOCAL_DATE);
    }
    @GetMapping("/pembayaran/xlsx")
    public void rekapPembayaranCsv(@RequestParam JenisBiaya jenis,
                                   @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate mulai,
                                   @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate sampai,
                                   HttpServletResponse response) throws Exception {

        Iterable<Pembayaran> dataPembayaran = pembayaranDao
                .findByTagihanJenisBiayaAndTagihanPendaftarStatusNotAndWaktuPembayaranBetweenOrderByWaktuPembayaran(jenis,"Close Lose - Mengundurkan Diri",mulai.atStartOfDay(), sampai.plusDays(1).atStartOfDay());
        Iterable<HasilTest> hasilTest = hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAktif(Status.AKTIF);

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Pembayaran 2020");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);


        if (jenis.getId().equals("002")) {
            String[] columns = {"No", "Nomor Registrasi", "Nama", "Email", "Periode", "Grade", "Jenis Test", "Program Studi", "Konsentrasi", "Nominal", "Bank", "Tanggal"};
            for (int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
                cell.setCellStyle(headerCellStyle);
            }
            for (int i = 0; i < columns.length; i++) {
                sheet.autoSizeColumn(i);
            }
        }else {
            String[] columns = {"No","Nomor Registrasi","Nama","Email","Program Studi","Konsentrasi","Nominal","Bank","Tanggal"};
            for (int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
                cell.setCellStyle(headerCellStyle);
            }
            for (int i = 0; i < columns.length; i++) {
                sheet.autoSizeColumn(i);
            }

        }

        int rowNum = 1 ;
        int baris = 1 ;

        for (Pembayaran p : dataPembayaran) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(baris++);
                row.createCell(1).setCellValue(p.getTagihan().getPendaftar().getNomorRegistrasi());
                row.createCell(2).setCellValue(p.getTagihan().getPendaftar().getLeads().getNama());
                row.createCell(3).setCellValue(p.getTagihan().getPendaftar().getLeads().getEmail());
                if (jenis.getId().equals("002")) {
                    for (HasilTest ht : hasilTest) {
                        if (p.getTagihan().getPendaftar().getId() == ht.getJadwalTest().getPendaftar().getId()) {
                            row.createCell(4).setCellValue(ht.getPeriode().getNama());
                            row.createCell(5).setCellValue(ht.getGrade().getNama());
                            row.createCell(6).setCellValue(ht.getJadwalTest().getJenisTest().toString());

                        }
                    }
                    row.createCell(7).setCellValue(p.getTagihan().getPendaftar().getProgramStudi().getNama());
                    row.createCell(8).setCellValue(p.getTagihan().getPendaftar().getKonsentrasi());
                    Locale localeID = new Locale("in", "ID");
                    NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
                    double nilai = p.getNilai().doubleValue();
                    row.createCell(9).setCellValue(formatRupiah.format((double) nilai));
                    row.createCell(10).setCellValue(p.getBank().getNamaBank());
                    row.createCell(11).setCellValue(p.getWaktuPembayaran().format(DateTimeFormatter.ISO_LOCAL_DATE));

                }else{
                    row.createCell(4).setCellValue(p.getTagihan().getPendaftar().getProgramStudi().getNama());
                    row.createCell(5).setCellValue(p.getTagihan().getPendaftar().getKonsentrasi());
                    Locale localeID = new Locale("in", "ID");
                    NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
                    double nilai = p.getNilai().doubleValue();
                    row.createCell(6).setCellValue(formatRupiah.format((double)nilai));
                    row.createCell(7).setCellValue(p.getBank().getNamaBank());
                    row.createCell(8).setCellValue(p.getWaktuPembayaran().format(DateTimeFormatter.ISO_LOCAL_DATE));
                }
        }



        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=Pembayaran_"+ jenis.getNama() +"_"+ mulai.format(DateTimeFormatter.ISO_LOCAL_DATE) +"_"+ sampai.format(DateTimeFormatter.ISO_LOCAL_DATE) +".xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();

    }

    @GetMapping("/kwitansi")
    public void kwitansi(@RequestParam(name = "id") Pembayaran pembayaran,
                           HttpServletResponse response){
        try {
            // 0. Setup converter
            Options options = Options.getFrom(DocumentKind.ODT).to(ConverterTypeTo.PDF);

            // 1. Load template dari file
            InputStream in = templateKwitansi.getInputStream();

            // 2. Inisialisasi template engine, menentukan sintaks penulisan variabel
            IXDocReport report = XDocReportRegistry.getRegistry().
                    loadReport(in, TemplateEngineKind.Freemarker);

            // 3. Context object, untuk mengisi variabel

            IContext ctx = report.createContext();
            ctx.put("noRegistrasi", pembayaran.getTagihan().getPendaftar().getNomorRegistrasi());
            ctx.put("tglBayar",pembayaran.getWaktuPembayaran());
            ctx.put("nama", pembayaran.getTagihan().getPendaftar().getLeads().getNama());
            ctx.put("program", pembayaran.getTagihan().getPendaftar().getLeads().getJenjang()+" - "+pembayaran.getTagihan().getPendaftar().getProgramStudi().getNama());
            ctx.put("email", pembayaran.getTagihan().getPendaftar().getLeads().getEmail());
            ctx.put("noHp", pembayaran.getTagihan().getPendaftar().getLeads().getNoHp());
            ctx.put("noTagihan", pembayaran.getTagihan().getNomorTagihan());
            ctx.put("bank", pembayaran.getBank().getNamaBank());
            ctx.put("jenisBiaya", pembayaran.getTagihan().getJenisBiaya().getNama());
            ctx.put("nilai", pembayaran.getNilai());
            ctx.put("tanggal", LocalDate.now().format(DateTimeFormatter.ofPattern("MMM dd, yyyy")));

            response.setHeader("Content-Disposition", "attachment;filename=kwitansi_pembayaran_"+ pembayaran.getTagihan().getJenisBiaya().getNama() +".pdf");
            OutputStream out = response.getOutputStream();
            report.convert(ctx, options, out);
            out.flush();
        } catch (Exception err){
            logger.error(err.getMessage(), err);
        }
    }

    public void hitungTotalDiskon(Tagihan tagihan){
        Date date = new Date();
        SimpleDateFormat formatter= new SimpleDateFormat("MM");
        String bulan = formatter.format(date);
        TotalDiskon cekBulan = totalDiskonDao.findByBulan(bulan);
        if (cekBulan == null) {
            TotalDiskon td = new TotalDiskon();
            td.setBulan(bulan);
            BigDecimal total =new BigDecimal(16000000- tagihan.getTotalTagihan().intValue());
            System.out.println("Total Diskon : " + total);
            td.setTotal(total);
            td.setTglUpdate(LocalDateTime.now());
            totalDiskonDao.save(td);
            System.out.println("Bulan Baru Telah di tambahkan = " + td);
        } else {
            BigDecimal hitung = BigDecimal.valueOf(16000000-tagihan.getTotalTagihan().intValue());
            BigDecimal totalAwal = cekBulan.getTotal();
            BigDecimal total = new BigDecimal(totalAwal.intValue() + hitung.intValue());
            System.out.println("Total Diskonss : " + total);

            cekBulan.setTotal(total);
            cekBulan.setTglUpdate(LocalDateTime.now());
            totalDiskonDao.save(cekBulan);
            System.out.println("Bulan lama di perbarui = " + cekBulan);
        }
    }


    @GetMapping("/pembayaran/fullDU")
    public void rekapHasilTestXlsx(HttpServletResponse response) throws Exception {

        String[] columns = {"No","No Registrasi","Nama","Email","No Telephone","Prodi","Cicilan","Uang Masuk","Total Tagihan","Keterangan","Grade","Periode","Jenis Test","Status", "Jenjang", "Konsentrasi"};

        Iterable<Object[]> pembayarans = pembayaranDao.findByAllPembayaranDU(tahunAjaranDao.findByAktif(Status.AKTIF).getId(), "Close Lose - Mengundurkan Diri");
        Iterable<HasilTest> hasilTest = hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAktif(Status.AKTIF);

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("DataPembayaranDu");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1 ;
        int baris = 1 ;

        for (Object[] p : pembayarans) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(baris++);
            row.createCell(1).setCellValue(p[0].toString());
            row.createCell(2).setCellValue(p[1].toString());
            row.createCell(3).setCellValue(p[2].toString());
            row.createCell(4).setCellValue(p[3].toString());
            row.createCell(5).setCellValue(p[4].toString());
            row.createCell(6).setCellValue(p[5].toString());
            row.createCell(7).setCellValue(p[6].toString());
            row.createCell(8).setCellValue(p[7].toString());
            if (p[8] == null) {
                row.createCell(9).setCellValue("-");
            } else {
                row.createCell(9).setCellValue(p[8].toString());
            }
            for (HasilTest ht : hasilTest) {
                if (ht.getJadwalTest().getPendaftar().getId().equals(p[9].toString())) {
                    row.createCell(10).setCellValue(ht.getGrade().getNama());
                    row.createCell(11).setCellValue(ht.getPeriode().getNama());
                    row.createCell(12).setCellValue(ht.getJadwalTest().getJenisTest().toString());
                    row.createCell(13).setCellValue(ht.getJadwalTest().getPendaftar().getStatus());
                    row.createCell(14).setCellValue(ht.getJadwalTest().getPendaftar().getLeads().getJenjang().toString());
                    row.createCell(15).setCellValue(ht.getJadwalTest().getPendaftar().getKonsentrasi());

                }
            }

        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=DaftarUlangFull.xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();

    }
}

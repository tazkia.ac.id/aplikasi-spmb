package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.User;
import id.ac.tazkia.spmb.aplikasispmb.entity.UserPassword;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserPasswordDao extends PagingAndSortingRepository<UserPassword, String>, CrudRepository<UserPassword, String> {
    UserPassword findByUser(User user);
}

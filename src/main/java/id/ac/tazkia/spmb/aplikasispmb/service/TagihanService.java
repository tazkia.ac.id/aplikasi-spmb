package id.ac.tazkia.spmb.aplikasispmb.service;

import id.ac.tazkia.spmb.aplikasispmb.constants.AppConstants;
import id.ac.tazkia.spmb.aplikasispmb.controller.PembayaranController;
import id.ac.tazkia.spmb.aplikasispmb.dao.NilaiBiayaDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.PembayaranDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.PendaftarDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.TagihanDao;
import id.ac.tazkia.spmb.aplikasispmb.dto.*;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.awt.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class TagihanService{

    private static final Logger LOGGER = LoggerFactory.getLogger(TagihanService.class);
    private static final DateTimeFormatter FORMATTER_ISO_DATE_TIME = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Value("${tagihan.id.registrasi}") private String idTagihanRegistrasi;
    @Value("${tagihan.id.daftarUlang}") private String idTagihanDaftarUlang;


    @Autowired private NilaiBiayaDao nilaiBiayaDao;
    @Autowired private TagihanDao tagihanDao;
    @Autowired private KafkaSender kafkaSender;
    @Autowired private PembayaranDao pembayaranDao;
    @Autowired private PembayaranController pembayaranController;
    @Autowired private ApiService apiService;

    @Autowired private PendaftarDao pendaftarDao;


    private JenisBiaya pendaftaran;
    private ProgramStudi programStudi;
    private JenisBiaya daftarUlangDiskon;


    public TagihanService(){
        pendaftaran = new JenisBiaya();
        programStudi = new ProgramStudi();
        pendaftaran.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);

    }

    public void prosesTagihanPendaftaran(Pendaftar p){
        DebiturRequest request = DebiturRequest.builder()
                .email(p.getLeads().getEmail())
                .nama(p.getLeads().getNama())
                .noHp(p.getLeads().getNoHp())
                .nomorDebitur(p.getNomorRegistrasi())
                .build();

        kafkaSender.requestCreateDebitur(request);

    }

    public List<JournalGetTemplateDetail> journalGetTemplateDetail(BigDecimal amount){
        return journalGetTemplateDetail(amount);
    }

    public void createTagihanRegistrasi(Pendaftar p) {
        if (p.getReferal() != null) {
            Integer nilaiA = hitungBiayaPendaftaran(p).intValue() - p.getReferal().getNominal().intValue();
            LOGGER.info("Nominal dengan referal {}",nilaiA);
            TagihanRequest tagihanRequest = TagihanRequest.builder()
                    .jenisTagihan(idTagihanRegistrasi)
                    .kodeBiaya(p.getProgramStudi().getKodeBiaya())
                    .nilaiTagihan(new BigDecimal(nilaiA))
                    .debitur(p.getNomorRegistrasi())
                    .keterangan("Registrasi Mahasiswa Baru a.n " + p.getLeads().getNama())
                    .jenisRequest(TagihanRequest.Type.CREATE)
                    .tanggalJatuhTempo(Date.from(LocalDate.now().with(TemporalAdjusters.lastDayOfMonth()).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                    .build();
            LOGGER.info("Create Tagihan : {}", tagihanRequest);
            kafkaSender.requestCreateTagihan(tagihanRequest);
        }else {
            LOGGER.info("Nominal tanpa referal {}", hitungBiayaPendaftaran(p));
            TagihanRequest tagihanRequest = TagihanRequest.builder()
                    .jenisTagihan(idTagihanRegistrasi)
                    .kodeBiaya(p.getProgramStudi().getKodeBiaya())
                    .nilaiTagihan(hitungBiayaPendaftaran(p))
                    .debitur(p.getNomorRegistrasi())
                    .keterangan("Registrasi Mahasiswa Baru a.n " + p.getLeads().getNama())
                    .jenisRequest(TagihanRequest.Type.CREATE)
                    .tanggalJatuhTempo(Date.from(LocalDate.now().plusYears(1).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                    .build();
            LOGGER.info("Create Tagihan : {}", tagihanRequest);
            kafkaSender.requestCreateTagihan(tagihanRequest);
        }
    }

//    public void prosesTagihanSitin(PesertaCourse pc){
//        DebiturRequest request = DebiturRequest.builder()
//                .email(pc.getEmail())
//                .nama(pc.getNama())
//                .noHp(pc.getNoHp())
//                .nomorDebitur(pc.getNomor())
//                .build();
//
//        kafkaSender.requestCreateDebitur(request);
//    }
//
//    public void createTagihanSitin(PesertaCourse p) {
//            TagihanRequest tagihanRequest = TagihanRequest.builder()
//                    .jenisTagihan(idTagihanRegistrasi)
//                    .kodeBiaya("SITIN-001")
//                    .nilaiTagihan(p.getCourse().getHarga())
//                    .debitur(p.getNomor())
//                    .keterangan("Pendaftaran peserta course" + p.getCourse().getNamaMatakuliah() + " a.n " + p.getNama())
//                    .tanggalJatuhTempo(Date.from(LocalDate.now().plusYears(1).atStartOfDay(ZoneId.systemDefault()).toInstant()))
//                    .build();
//
//            kafkaSender.requestCreateTagihan(tagihanRequest);
//    }

    @Value("${group.pendaftar}") private String  deleteGroupPendaftar;
    @Value("${group.lunas.pendaftar}") private String insertGroupLunasPendaftar;
    @Value("${group.lunas.du}") private String insertGroupLunasDu;
    @Value("${group.lulus.test}") private String deleteGroupHasilTest;
    public void prosesPembayaran(Tagihan tagihan, PembayaranTagihan pt) {
        tagihan.setLunas(true);

        System.out.println("Pembayaran Tagihan = " + pt);

        Pembayaran pembayaran = new Pembayaran();
        pembayaran.setTagihan(tagihan);
        pembayaran.setNilai(pt.getNilaiPembayaran());
        pembayaran.setWaktuPembayaran(LocalDateTime.parse(pt.getWaktuPembayaran(), FORMATTER_ISO_DATE_TIME));
        pembayaran.setReferensi(pt.getReferensiPembayaran());

        Bank bank = new Bank();
        bank.setId(pt.getBank());
        pembayaran.setBank(bank);
        pembayaran.setBuktiPembayaran(pt.getReferensiPembayaran());

        tagihanDao.save(tagihan);
        pembayaranDao.save(pembayaran);


        if (tagihan.getJenisBiaya().equals(idTagihanDaftarUlang)) {
            Pendaftar p= pendaftarDao.findById(tagihan.getPendaftar().getId()).get();
            p.setStatus("Close Win - Sudah Lunas");
            pendaftarDao.save(p);
            LOGGER.info("Status endaftar {} sudah di perbarui", p.getNomorRegistrasi());
        }
//        if (tagihan.getJenisBiaya().equals(AppConstants.JENIS_BIAYA_PENDAFTARAN)) {
//            apiService.insertGroup(tagihan.getPendaftar().getLeads(), insertGroupLunasPendaftar);
//            apiService.deleteGroup(tagihan.getPendaftar().getLeads(), deleteGroupPendaftar);
//        }
//        if (tagihan.getJenisBiaya().getId().equals(AppConstants.JENIS_BIAYA_DAFTAR_ULANG)) {
//            pembayaranController.hitungTotalDiskon(tagihan);
//            apiService.insertGroup(tagihan.getPendaftar().getLeads(), insertGroupLunasDu);
//            apiService.deleteGroup(tagihan.getPendaftar().getLeads(), deleteGroupHasilTest);
//        }
        LOGGER.debug("Pembayaran untuk tagihan {} berhasil disimpan", pt.getNomorTagihan());

    }

    public BigDecimal hitungBiayaPendaftaran(Pendaftar p){
        p.getProgramStudi();
        Page<NilaiBiaya> biaya = nilaiBiayaDao.findByJenisBiayaAndProgramStudi(pendaftaran, p.getProgramStudi(), PageRequest.of(0,1));
        if(!biaya.hasContent()){
            return BigDecimal.ZERO;
        }
        return biaya.getContent().get(0).getNilai();
    }

    public void createTagihanDaftarUlang(Pendaftar p,BigDecimal nilai, String cicilan){
        LocalDate localDate = LocalDate.now();
        LocalDate day = localDate.with(TemporalAdjusters.lastDayOfMonth());

        TagihanRequest tagihanRequest = TagihanRequest.builder()
                .kodeBiaya(p.getProgramStudi().getKodeBiaya())
                .jenisTagihan(idTagihanDaftarUlang)
                .nilaiTagihan(nilai)
                .debitur(p.getNomorRegistrasi())
                .keterangan("Daftar Ulang Mahasiswa Baru Institut Tazkia a.n "+p.getLeads().getNama())
                .jenisRequest(TagihanRequest.Type.CREATE)
                .tanggalJatuhTempo(Date.from(day.atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .cicilan(cicilan)
                .build();

        kafkaSender.requestCreateTagihan(tagihanRequest);
        System.out.println("Nilai Daftar Ulang (SUKSES) : "+ nilai);
    }


    public void hapusTagihan(Tagihan tagihan){
        HapusTagihanRequest hapusTagihan = HapusTagihanRequest.builder()
                .nomorTagihan(tagihan.getNomorTagihan())
                .jenisTagihan(tagihan.getJenisBiaya().getId())
                .debitur(tagihan.getPendaftar().getNomorRegistrasi())
                .kodeBiaya(tagihan.getPendaftar().getProgramStudi().getKodeBiaya())
                .build();
        kafkaSender.requsetHapusTagihan(hapusTagihan);
    }

    public void editTagihan(Tagihan tagihan) {
        LocalDate localDate = LocalDate.now();
        LocalDate day = localDate.with(TemporalAdjusters.lastDayOfMonth());

        TagihanRequest tagihanRequest = TagihanRequest.builder()
                .kodeBiaya(tagihan.getPendaftar().getProgramStudi().getKodeBiaya())
                .jenisTagihan(idTagihanDaftarUlang)
                .nilaiTagihan(tagihan.getNilai())
                .debitur(tagihan.getPendaftar().getNomorRegistrasi())
                .cicilan(tagihan.getCicilan())
                .keterangan("Daftar Ulang Mahasiswa Baru Institut Tazkia a.n "+tagihan.getPendaftar().getLeads().getNama())
                .tanggalJatuhTempo(Date.from(day.atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .jenisRequest(TagihanRequest.Type.REPLACE)
                .nomorTagihanLama(tagihan.getNomorTagihan())
                .build();
        kafkaSender.requestCreateTagihan(tagihanRequest);
        LOGGER.info("Edit Nominal Tagihan Pendaftar {}", tagihan.getPendaftar().getLeads().getNama());
    }

    public void editTagihanRegisrtasi(Tagihan tagihan) {
        LocalDate localDate = LocalDate.now();
        LocalDate day = localDate.with(TemporalAdjusters.lastDayOfMonth());

        TagihanRequest tagihanRequest = TagihanRequest.builder()
                .kodeBiaya(tagihan.getPendaftar().getProgramStudi().getKodeBiaya())
                .jenisTagihan(idTagihanRegistrasi)
                .nilaiTagihan(tagihan.getNilai())
                .debitur(tagihan.getPendaftar().getNomorRegistrasi())
                .cicilan(tagihan.getCicilan())
                .keterangan("Registrasi Mahasiswa Institut Tazkia a.n "+tagihan.getPendaftar().getLeads().getNama())
                .tanggalJatuhTempo(Date.from(day.atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .jenisRequest(TagihanRequest.Type.REPLACE)
                .nomorTagihanLama(tagihan.getNomorTagihan())
                .build();
        kafkaSender.requestCreateTagihan(tagihanRequest);
        LOGGER.info("Edit Nominal Tagihan Pendaftar {}", tagihan.getPendaftar().getLeads().getNama());
    }


}

package id.ac.tazkia.spmb.aplikasispmb.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.tazkia.spmb.aplikasispmb.constants.AppConstants;
import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.dto.DebiturResponse;
import id.ac.tazkia.spmb.aplikasispmb.dto.PembayaranTagihan;
import id.ac.tazkia.spmb.aplikasispmb.dto.TagihanResponse;
import id.ac.tazkia.spmb.aplikasispmb.dto.VaResponse;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Transactional
public class KafkaListenerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaListenerService.class);
    private static final List<String> JENIS_TAGIHAN_SPMB = new ArrayList<>();

    @Value("${tagihan.id.registrasi}") private String idTagihanRegistrasi;
    @Value("${tagihan.id.daftarUlang}") private String idTagihanDaftarUlang;

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private TagihanService tagihanService;
    @Autowired
    private PendaftarDao pendaftarDao;
    @Autowired
    private TagihanDao tagihanDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private LeadsDao leadsDao;
    @Autowired
    private VirtualAccountDao virtualAccountDao;
    @Autowired
    private BankDao bankDao;
    @Autowired
    private ProgramStudiDao programStudiDao;
    @Autowired
    private PendaftarDetailDao pendaftarDetailDao;
    @Autowired
    private  CicilanDao cicilanDao;

    @PostConstruct
    public void inisialisasi(){
        JENIS_TAGIHAN_SPMB.add(idTagihanRegistrasi);
        JENIS_TAGIHAN_SPMB.add(idTagihanDaftarUlang);
    }

    @KafkaListener(topics = "${kafka.topic.debitur.response}", groupId = "${spring.kafka.consumer.group-id}")
    public void handleDebiturResponse(String message) {
        try {
            LOGGER.debug("Terima message : {}", message);
            DebiturResponse response = objectMapper.readValue(message, DebiturResponse.class);
            if (!response.getSukses()) {
                LOGGER.warn("Create debitur gagal : {}", response.getData());
                return;
            }

            Pendaftar p = pendaftarDao.findByNomorRegistrasi(response.getNomorDebitur());
            if (p == null) {
                LOGGER.warn("Pendaftar dengan nomor registrasi {} tidak ditemukan", response.getNomorDebitur());
                return;
            }
            tagihanService.createTagihanRegistrasi(p);

        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
        }
    }

    @KafkaListener(topics = "${kafka.topic.tagihan.response}", groupId = "${spring.kafka.consumer.group-id}")
    public void handleTagihanResponse(String message) {
        try {
            TagihanResponse response = objectMapper.readValue(message, TagihanResponse.class);
            if (!JENIS_TAGIHAN_SPMB.contains(response.getJenisTagihan())) {
                LOGGER.debug("Bukan Tagihan SPMB");
                return;
            }
            LOGGER.debug("Terima message : {}", message);

            if (!response.getSukses()) {
                LOGGER.warn("Create tagihan gagal : {}", response.getDebitur());
                return;
            }

            LOGGER.debug("Create tagihan untuk pendaftar {} sukses dengan nomor {}",
                    response.getDebitur(), response.getNomorTagihan());

            System.out.println("Tagihan Response : " + response);
            if (Objects.equals(response.getJenisTagihan(), idTagihanRegistrasi)) {
                Pendaftar p = pendaftarDao.findByNomorRegistrasi(response.getDebitur());
                JenisBiaya jb = new JenisBiaya();
                jb.setId("001");
                Tagihan tg = tagihanDao.findByPendaftarAndJenisBiayaAndLunas(p,  jb, false);
                if (tg != null) {
                    tg.setNomorTagihan(response.getNomorTagihan());
                    tg.setTanggalJatuhTempo(response.getTanggalJatuhTempo().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
                    tagihanDao.save(tg);
                    System.out.println("Nomor Tagihan Berhasil Diperbarui");
                } else {
                    System.out.println("Tagihan Baru Berhasil");
                    insertTagihanRegistrasi(response);
                }
            } else if (Objects.equals(response.getJenisTagihan(), idTagihanDaftarUlang)) {
                Pendaftar p = pendaftarDao.findByNomorRegistrasi(response.getDebitur());
                JenisBiaya jb = new JenisBiaya();
                jb.setId("002");
                Tagihan tg = tagihanDao.findByPendaftarAndJenisBiayaAndLunas(p, jb, false);
                if (tg != null) {
                    tg.setNomorTagihan(response.getNomorTagihan());
                    tagihanDao.save(tg);
                    System.out.println("Nomor Tagihan Berhasil Diperbarui");
                } else {
                    System.out.println("Tagihan Baru Berhasil");
                    insertTagihanRegistrasi(response);
                }

            }

        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
        }
    }


//handleVaResponsePendaftar
    @KafkaListener(topics = "${kafka.topic.va.response}", groupId = "${spring.kafka.consumer.group-id}")
    public void handleVaResponse(String message){
        try {
            LOGGER.debug("Terima message : {}", message);
            VaResponse vaResponse = objectMapper.readValue(message, VaResponse.class);

            LOGGER.info("Memproses VA no {} di bank {} untuk tagihan {} ",
                    vaResponse.getAccountNumber(),
                    vaResponse.getBankId(),
                    vaResponse.getInvoiceNumber());
            insertNoVirtualAccount(vaResponse);

        } catch (IOException err) {
            LOGGER.warn(err.getMessage(), err);
        }
    }

    private void insertNoVirtualAccount(VaResponse vaResponse){
        Tagihan tagihan = tagihanDao.findByNomorTagihan(vaResponse.getInvoiceNumber());
        if (tagihan == null) {
            LOGGER.info("Tagihan dengan nomor {} tidak ada dalam database", vaResponse.getInvoiceNumber());
            return;
        }
        VirtualAccount virtualAccount = new VirtualAccount();
        virtualAccount.setTagihan(tagihan);

        Optional<Bank> b = bankDao.findById(vaResponse.getBankId());
        if (!b.isPresent()) {
            throw new IllegalStateException("Bank dengan id " + vaResponse.getBankId() + " tidak ada di database");
        }

        virtualAccount.setBank(b.get());
        virtualAccount.setNomorVa(vaResponse.getAccountNumber());

        virtualAccountDao.save(virtualAccount);
        LOGGER.info("Nomor VA {} di bank {} dengan nomor tagihan {} berhasil disimpan",
                vaResponse.getAccountNumber(), vaResponse.getBankId(),
                tagihan.getNomorTagihan());

    }

    private void insertTagihanRegistrasi(TagihanResponse tagihanResponse) {
        LOGGER.debug("Insert tagihan nomor {} untuk pendaftar {} ", tagihanResponse.getNomorTagihan(), tagihanResponse.getDebitur());
        Pendaftar pendaftar = pendaftarDao.findByNomorRegistrasi(tagihanResponse.getDebitur());
        if (pendaftar == null) {
            LOGGER.warn("Pendaftar dengan nomor registrasi {} tidak ditemukan", tagihanResponse.getDebitur());
        }

        Tagihan tagihan = new Tagihan();
        tagihan.setPendaftar(pendaftar);
        tagihan.setNomorTagihan(tagihanResponse.getNomorTagihan());
        tagihan.setLunas(false);
        tagihan.setTanggalTagihan(tagihanResponse.getTanggalTagihan().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
        tagihan.setNilai(tagihanResponse.getNilaiTagihan());
        tagihan.setKeterangan(tagihanResponse.getKeterangan());


        JenisBiaya jenisBiaya = new JenisBiaya();
        if(idTagihanRegistrasi.equals(tagihanResponse.getJenisTagihan())) {
            jenisBiaya.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
        } else if(idTagihanDaftarUlang.equals(tagihanResponse.getJenisTagihan())){
            jenisBiaya.setId(AppConstants.JENIS_BIAYA_DAFTAR_ULANG);
        } else {
            LOGGER.error("Jenis Biaya {} belum terdaftar", tagihanResponse.getJenisTagihan());
            return;
        }

        Long cekCicilan= cicilanDao.itungCicilan(pendaftar);
        LOGGER.info("Cicilan : {}", cekCicilan);
        tagihan.setCicilan(cekCicilan.toString());

        BigDecimal cekNominal = cicilanDao.itungNominalCicilan(pendaftar);
        if (cekNominal != null) {
            BigDecimal total = cekNominal.add(tagihanResponse.getNilaiTagihan());
            LOGGER.info("Total Tagihan : {}", total);
        }else{
            tagihan.setTotalTagihan(tagihanResponse.getNilaiTagihan());
        }

        BigDecimal cekSisa = cicilanDao.itungNominalCicilandanStatus(pendaftar, Boolean.FALSE);
        if (cekSisa != null) {
            LOGGER.info("Sisa Tagihan Awal : {}", cekSisa);
            tagihan.setSisaTagihan(cekSisa);
        }

        tagihan.setTanggalJatuhTempo(tagihanResponse.getTanggalJatuhTempo().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
        tagihan.setJenisBiaya(jenisBiaya);
        tagihanDao.save(tagihan);
        LOGGER.info("Tagihannya  : {} ", tagihan);

    }

    @KafkaListener(topics = "${kafka.topic.tagihan.payment}", groupId = "${spring.kafka.consumer.group-id}")
    public void handlePayment(String message) {
        try {
            PembayaranTagihan pt = objectMapper.readValue(message, PembayaranTagihan.class);
            if (!JENIS_TAGIHAN_SPMB.contains(pt.getJenisTagihan())) {
                LOGGER.debug("Bukan Tagihan SPMB");
                return;
            }
            LOGGER.debug("Terima message : {}", message);

            Tagihan tagihan = tagihanDao.findByNomorTagihan(pt.getNomorTagihan());

            if (tagihan == null) {
                LOGGER.warn("Tagihan Pendaftar {} tidak ditemukan", tagihan.getPendaftar().getNomorRegistrasi());
                return;
            }

            if (tagihan != null) {
                tagihanService.prosesPembayaran(tagihan, pt);

                System.out.println("jenis tagihan : " + tagihan.getJenisBiaya().getId());

                if (tagihan.getJenisBiaya().getId().equals(AppConstants.JENIS_BIAYA_DAFTAR_ULANG)) {
                    PendaftarDetail dp = pendaftarDetailDao.findByPendaftar(tagihan.getPendaftar());
                    if (dp == null) {
                        LOGGER.warn("Tagihan dengan nomor {} tidak memiliki data detail pendaftar", pt.getNomorTagihan());
                        return;
                    }
//suketPasca
                    ProgramStudi pr07 = programStudiDao.findById("007").get();
                    ProgramStudi pr09 = programStudiDao.findById("009").get();
                    ProgramStudi pr08 = programStudiDao.findById("008").get();
                    Boolean pen1 = tagihan.getPendaftar().getProgramStudi().equals(pr07);
                    Boolean pen2 = tagihan.getPendaftar().getProgramStudi().equals(pr08);
                    Boolean pen3 = tagihan.getPendaftar().getProgramStudi().equals(pr09);
                    Boolean cekPodi = pen1 == true || pen2 == true || pen3 == true;
                    if (cekPodi == true) {
                        LOGGER.debug("Program Studi Pasca :" + tagihan.getPendaftar().getProgramStudi().getNama());
//                        notifikasiService.kirimNotifikasiKeteranganLulusPasca(dp);
                    }else{
//suketS1
                        LOGGER.debug("Program Studi S1  :" + tagihan.getPendaftar().getProgramStudi().getNama());
//                        notifikasiService.kirimNotifikasiKeteranganLulus(dp);
                    }
                }
            }

        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
        }
    }

}

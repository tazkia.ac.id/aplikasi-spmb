package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.constants.AppConstants;
import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.dto.*;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import id.ac.tazkia.spmb.aplikasispmb.service.NotifikasiService;
import id.ac.tazkia.spmb.aplikasispmb.service.PendaftarService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class SmartTestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(TagihanController.class);

    @Autowired
    private UserDao userDao;
    @Autowired
    private SmartTestDao smartTestDao;
    @Autowired
    private ProgramStudiDao programStudiDao;
    @Autowired
    private KabupatenKotaDao kabupatenKotaDao;

    @Value("classpath:sample/sample_smartTest.csv")
    private Resource contohFileSmartTest;

    @Autowired
    private GradeDao gradeDao;
    @Autowired
    private TahunAjaranDao tahunAjaranDao;
    @Autowired
    private HasilTestController hasilTestController;


    @GetMapping("/contoh/smartTest")
    public void downloadContohFileTagihan(HttpServletResponse response) throws Exception {
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", "attachment; filename=sample_smartTest.csv");
        FileCopyUtils.copy(contohFileSmartTest.getInputStream(), response.getOutputStream());
        response.getOutputStream().flush();
    }

    @GetMapping("/smartTest/form")
    public void formSmartTest (@RequestParam(required = false)String cari, Model m,
                               @PageableDefault(sort = "nama", direction = Sort.Direction.DESC) Pageable page){

        Iterable<SmartTest> smartTests = smartTestDao.findAll();
        List<SmartTestDto> smartTestDtos = new ArrayList<>();

        for (SmartTest smartTest : smartTests) {
           Grade hitungGrade = hasilTestController.hitungGrade(smartTest.getNilai());
           SmartTestDto smtDto = new SmartTestDto();
           smtDto.setNama(smartTest.getNama());
           smtDto.setAsalSekolah(smartTest.getAsalSekolah());
           smtDto.setTanggalTest(smartTest.getTanggalTest());
           smtDto.setGrade(hitungGrade);
           smartTestDtos.add(smtDto);

        }
        m.addAttribute("gS",smartTestDtos);
        TahunAjaran t = tahunAjaranDao.findByAktif(Status.AKTIF);
        if(StringUtils.hasText(cari)) {
            m.addAttribute("cari", cari);
            m.addAttribute("smartTest", smartTestDao.findByNamaContainingIgnoreCaseOrAsalSekolahContainingIgnoreCaseAndTahunAjaran(cari,cari,t, page));
        } else {
            m.addAttribute("smartTest", smartTestDao.findAllByTahunAjaran(t,page));
        }
    }

    @PostMapping("/smartTest/form")
    public String processFormUpload(@RequestParam String  asalSekolah, @DateTimeFormat(pattern = "yyyy-MM-dd") Date tanggalTest,
                                    @RequestParam(required = false) Boolean pakaiHeader,
                                    MultipartFile fileSmartTest, RedirectAttributes redirectAttrs, Authentication currentUser){

        LOGGER.debug("Authentication class : {}",currentUser.getClass().getName());

        if(currentUser == null){
            LOGGER.warn("Current user is null");
        }

        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        LOGGER.debug("User ID : {}", u.getId());
        if(u == null){
            LOGGER.warn("Username {} not found in database ", username);
        }

        LOGGER.debug("Asal Sekolah : {}",asalSekolah);
        LOGGER.debug("Tanggal Test : {}",tanggalTest);
        LOGGER.debug("Pakai Header : {}",pakaiHeader);
        LOGGER.debug("Nama File : {}",fileSmartTest.getName());
        LOGGER.debug("Ukuran File : {}",fileSmartTest.getSize());

        List<UploadError> errors = new ArrayList<>();
        Integer baris = 0;

        if(asalSekolah == null){
            errors.add(new UploadError(baris, "Asal sekolah harus diisi", ""));
            redirectAttrs
                    .addFlashAttribute("jumlahBaris", 0)
                    .addFlashAttribute("jumlahSukses", 0)
                    .addFlashAttribute("jumlahError", errors.size())
                    .addFlashAttribute("errors", errors);
            return "redirect:hasil";
        }
        if(tanggalTest == null){
            errors.add(new UploadError(baris, "Tanggal test harus diisi", ""));
            redirectAttrs
                    .addFlashAttribute("jumlahBaris", 0)
                    .addFlashAttribute("jumlahSukses", 0)
                    .addFlashAttribute("jumlahError", errors.size())
                    .addFlashAttribute("errors", errors);
            return "redirect:hasil";
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(fileSmartTest.getInputStream()));
            String content;

            if((pakaiHeader != null && pakaiHeader)){
                content = reader.readLine();
            }

            while ((content = reader.readLine()) != null) {
                baris++;
                String[] data = content.split(",");
                if (data.length != 2) {
                    errors.add(new UploadError(baris, "Format data salah", content));
                    continue;
                }

                // Save table smartTest
                SmartTest p = new SmartTest();
                Grade hitungGrade = hasilTestController.hitungGrade(new BigDecimal(data[1]));
                TahunAjaran t = tahunAjaranDao.findByAktif(Status.AKTIF);
                p.setNama(data[0]);
                p.setAsalSekolah(asalSekolah);
                p.setNilai(new BigDecimal(data[1]));
                p.setGrade(hitungGrade);
                p.setTanggalTest(tanggalTest);
                p.setTahunAjaran(t);
                smartTestDao.save(p);

            }
        } catch (Exception err){
            LOGGER.warn(err.getMessage(), err);
            errors.add(new UploadError(0, "Format file salah", ""));
        }

        redirectAttrs
                .addFlashAttribute("jumlahBaris", baris)
                .addFlashAttribute("jumlahSukses", baris - errors.size())
                .addFlashAttribute("jumlahError", errors.size())
                .addFlashAttribute("errors", errors);

        return "redirect:hasil";
    }

    @GetMapping("/smartTest/hasil")
    public void hasilFormUpload() {
    }

    @GetMapping("/smartTest/listIndex")
    public void listSmartTest(@RequestParam(required = false) String nama
                              ,Model m, @PageableDefault(size = 15, sort = "nama", direction = Sort.Direction.ASC)Pageable page){

        TahunAjaran t = tahunAjaranDao.findByAktif(Status.AKTIF);
       if (nama != null ) {
            m.addAttribute("nama", nama);
            m.addAttribute("listSmart",smartTestDao.findAllByNamaContainingIgnoreCaseAndTahunAjaran(nama,t,page));
        } else {
            m.addAttribute("listSmart",smartTestDao.findAllByTahunAjaran(t,page));
        }

    }

    @ModelAttribute("daftarProdiS1")
    public Iterable<ProgramStudi> daftarProdiS1(){
        String id ="007";
        String tidak = "005";
        String tidak2 = "000";

        return programStudiDao.cariProdiS1(id,tidak, tidak2);
    }
    @ModelAttribute("daftarKokab")
    public Iterable<KabupatenKota> daftarKokab(){
        return kabupatenKotaDao.findAllByOrderByNamaAsc();
    }

    @GetMapping("/formRegistrasi")
    public String  formRegistrasi(@RequestParam(required = false) String id, Model m){
        if (id != null) {
            SmartTest smartTest = smartTestDao.findById(id).get();
            m.addAttribute("cek", smartTest);
        }

        return "smartTest/formRegistrasi";
    }


    @Autowired
    private RoleDao roleDao;
    @Autowired private PasswordEncoder passwordEncoder;
    @Autowired private UserPasswordDao userPasswordDao;
    @Autowired private ReferalDao referalDao;
    @Autowired private LeadsDao leadsDao;
    @Autowired private NotifikasiService notifikasiService;
    @Autowired private PendaftarService pendaftarService;
    @Autowired private JadwalTestController jadwalTestController;
    @Autowired private  PendaftarDao pendaftarDao;
    @Autowired private  JadwalTestDao jadwalTestDao;
    @Autowired private  HasilTestDao hasilTestDao;

    @PostMapping("/formRegistrasi")
    public String  prosesRegistrasi(@Valid DaftarSmartTestDto dst){

        Leads leads = new Leads();

//        Leads cekLeads = leadsDao.cariUsername("%"+leadsDto.getUsername().toLowerCase()+"%");
        User cekLeads = userDao.findByUsername(dst.getEmail());

        if (cekLeads == null) {
            //CreateUser&Password
            User user = new User();
            Role role = roleDao.findById(AppConstants.ROLE_PENDAFTAR).get();

            user.setUsername(dst.getEmail());
            user.setActive(true);
            user.setRole(role);
            userDao.save(user);

            UserPassword password = new UserPassword();
            password.setUser(user);
            password.setPassword(passwordEncoder.encode(dst.getPassword()));
            userPasswordDao.save(password);
            //

            TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
            leads.setTahunAjaran(ta);
            leads.setUser(user);

            Referal referal = referalDao.findAllByKodeReferal(dst.getKodeReferal());
            if (referal == null) {
                LOGGER.info("Tidak menggunakan kode referal");
            } else {
                LOGGER.info("Kode Referal ditemukan {}", referal);
                leads.setKode(referal);
            }

            BeanUtils.copyProperties(dst, leads);
            leads.setJenjang(Jenjang.S1);
            leads.setKelas(dst.getKonsentrasi());
            leadsDao.save(leads);

//            apiService.insertSubcriber(leads);
//            apiService.insertGroup(leads, insertGroupLeads);

            if (referal != null) {
                NotifRegistrasiReferalDto nrf = new NotifRegistrasiReferalDto();
                nrf.setEmail(dst.getEmail());
                nrf.setNama(dst.getNama());
                nrf.setNoHp(dst.getNoHp());
                nrf.setEmailRef(referal.getEmail());
                nrf.setKodeRef(referal.getKodeReferal());

                notifikasiService.NotifikasiReferal(nrf);
            } else {
                LOGGER.info("Kode referal tidak memiliki email");
            }

            LOGGER.info("Leads a.n {} Berhasiil di simpan", leads.getNama());
        } else {
            LOGGER.info("Username a.n {} sudah ada", cekLeads.getUsername());

        }
//porses pendafrtaran
        KabupatenKota kk = kabupatenKotaDao.findById(dst.getIdKabupatenKota()).get();
        ProgramStudi prodi = programStudiDao.findById(dst.getProgramStudi()).get();


        PendaftarDto pdt = new PendaftarDto();
        pdt.setLeads(leads);
        pdt.setIdKabupatenKota(dst.getIdKabupatenKota());
        pdt.setNamaAsalSekolah(dst.getNamaAsalSekolah());
        pdt.setProgramStudi(dst.getProgramStudi());
        pdt.setKonsentrasi(dst.getKonsentrasi());
        pdt.setPerekomendasi(dst.getPerekomendasi());
        pdt.setNamaPerekomendasi(dst.getNamaPerekomendasi());
        pdt.setKodeReferal(dst.getKodeReferal());
        pdt.setAlasanMemilihProdi(null);

        pendaftarService.prosesPendaftar(pdt, prodi, kk);
        LOGGER.warn("Pendaftar dengan nama {} sudah ada.", pdt);

//Simpan Jadwal & Hasil Test

        SmartTest smartTest = smartTestDao.findById(dst.getIdHasilTest()).get();
        JadwalTest jadwalTest = new JadwalTest();
        jadwalTest.setJenisTest(JenisTest.SMART_TEST);

        Pendaftar pe = pendaftarDao.findByLeads(leads);
        jadwalTest.setPendaftar(pe);
        LocalDate tglTest = LocalDate.from(smartTest.getTanggalTest().toInstant().atZone(ZoneId.systemDefault()));
        jadwalTest.setTanggalTest(tglTest);
        jadwalTestDao.save(jadwalTest);
        LOGGER.info("Jadwal Test Berhasil Disimpan");

        HasilTest hasilTest = new HasilTest();
        hasilTest.setJadwalTest(jadwalTest);
        hasilTest.setGrade(smartTest.getGrade());
        hasilTest.setUser(leads.getUser());
        hasilTest.setTanggalInsert(LocalDateTime.now());
        hasilTest.setNilai(smartTest.getNilai());
        List<Periode> periode = hasilTestController.cariPeriode(tglTest);
        for (Periode periode1 : periode) {
            hasilTest.setPeriode(periode1);
        }
        hasilTestDao.save(hasilTest);
        LOGGER.info("Hasil Test Berhasil Disimpan");


        return "leads/selesai";

    }

}

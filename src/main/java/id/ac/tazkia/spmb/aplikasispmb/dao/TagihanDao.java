package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.constants.ZahirExport;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface TagihanDao extends PagingAndSortingRepository<Tagihan, String>, CrudRepository<Tagihan, String> {
    Long countByPendaftar(Pendaftar pendaftar);

    Page<Tagihan> findByPendaftarAndJenisBiaya(Pendaftar  pendaftar, JenisBiaya jenisBiaya, Pageable pageable);

    List<Tagihan> findByPendaftarOrderByTanggalTagihan(Pendaftar pendaftar, Pageable page);

    Page<Tagihan> findById(String tagihan, Pageable page);

    Tagihan findByPendaftar(Pendaftar p);

    Tagihan findByNomorTagihan(String nomor);

    Tagihan findByPendaftarAndJenisBiaya(Pendaftar pendaftar, JenisBiaya jenisBiaya);
    Tagihan findByPendaftarAndJenisBiayaAndLunas(Pendaftar pendaftar, JenisBiaya jenisBiaya, Boolean lunas);

    @Query(value = "select count(*) as jumlah from  (select count(p.id)  from pembayaran pb " +
            "inner join tagihan tg on tg.id = pb.id_tagihan " +
            "inner join pendaftar p on p.id = tg.id_pendaftar " +
            "inner join leads l on l.id = p.id_leads " +
            "where tg.id_jenisbiaya = 002 and  p.id_tahun = :tahun " +
            "and l.jenjang = 'S1' and p.konsentrasi = :konsentrasi  and p.status = :status and tg.lunas = true  group by tg.id_pendaftar) aa", nativeQuery = true)
    Long itungBerdasarkanStatus (String  tahun,String konsentrasi, String status);

    @Query(value = "select count(*) as jumlah from  (select count(p.id)  from pembayaran pb " +
            "inner join tagihan tg on tg.id = pb.id_tagihan " +
            "inner join pendaftar p on p.id = tg.id_pendaftar " +
            "inner join leads l on l.id = p.id_leads " +
            "where tg.id_jenisbiaya = 002 and  p.id_tahun = :tahun " +
            "and l.jenjang = 'S2'   and p.status = :status and tg.lunas = true  group by tg.id_pendaftar) aa", nativeQuery = true)
    Long itungBerdasarkanStatusS2 (String  tahun, String status);

    Long countByPendaftarKonsentrasiAndPendaftarLeadsJenjangAndJenisBiayaAndLunasAndPendaftarTahunAjaran(String konsen,Jenjang jenjang, JenisBiaya jenisBiaya, Boolean lunas, TahunAjaran ta);
    Long countByPendaftarLeadsJenjangAndJenisBiayaAndLunasAndPendaftarTahunAjaran(Jenjang jenjang, JenisBiaya jenisBiaya, Boolean lunas, TahunAjaran ta);

    Long countByPendaftarKonsentrasiAndPendaftarLeadsJenjangAndJenisBiayaAndPendaftarStatusContainingIgnoreCaseAndPendaftarTahunAjaran(String konsen,Jenjang jenjang, JenisBiaya jenisBiaya, String status, TahunAjaran ta);
    Long countByPendaftarLeadsJenjangAndJenisBiayaAndPendaftarStatusContainingIgnoreCaseAndPendaftarTahunAjaran(Jenjang jenjang, JenisBiaya jenisBiaya, String status, TahunAjaran ta);
    Long countByPendaftarKonsentrasiAndPendaftarLeadsJenjangAndJenisBiayaAndPendaftarStatusContainingIgnoreCaseAndPendaftarStatusNotAndPendaftarTahunAjaran(String konsen,Jenjang jenjang, JenisBiaya jenisBiaya, String status,String statusnot, TahunAjaran ta);
    Long countByAndPendaftarLeadsJenjangAndJenisBiayaAndPendaftarStatusContainingIgnoreCaseAndPendaftarStatusNotAndPendaftarTahunAjaran(Jenjang jenjang, JenisBiaya jenisBiaya, String status,String statusnot, TahunAjaran ta);

    Page<Tagihan> findByPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarTahunAjaranAndJenisBiaya(String nama, TahunAjaran tahunAjaran, JenisBiaya  jenisBiaya, Pageable pageable);
    Page<Tagihan> findByPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarTahunAjaranAndPendaftarKonsentrasiAndJenisBiayaAndPendaftarLeadsJenjang(String nama, TahunAjaran tahunAjaran, String konsentrasi, JenisBiaya  jenisBiaya, Jenjang jenjang, Pageable pageable);
    Page<Tagihan> findByPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarTahunAjaranAndJenisBiayaAndPendaftarLeadsJenjang(String nama, TahunAjaran tahunAjaran, JenisBiaya  jenisBiaya, Jenjang jenjang, Pageable pageable);
    Page<Tagihan> findByPendaftarFollowupAndPendaftarTahunAjaranAndPendaftarKonsentrasiAndJenisBiayaAndPendaftarLeadsJenjang( Followup fl, TahunAjaran tahunAjaran, String konsentrasi, JenisBiaya  jenisBiaya, Jenjang jenjang, Pageable pageable);
    Page<Tagihan> findByPendaftarFollowupAndPendaftarTahunAjaranAndJenisBiayaAndPendaftarLeadsJenjang( Followup fl, TahunAjaran tahunAjaran, JenisBiaya  jenisBiaya, Jenjang jenjang, Pageable pageable);
    Page<Tagihan> findByPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarFollowupAndPendaftarTahunAjaranAndJenisBiaya(String nama, Followup fl, TahunAjaran tahunAjaran, JenisBiaya  jenisBiaya, Pageable pageable);
    Page<Tagihan> findByPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarFollowupAndPendaftarTahunAjaranAndPendaftarKonsentrasiAndJenisBiayaAndPendaftarLeadsJenjang(String nama, Followup fl, TahunAjaran tahunAjaran,String konsentrasi, JenisBiaya  jenisBiaya, Jenjang jenjang, Pageable pageable);
    Page<Tagihan> findByPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarFollowupAndPendaftarTahunAjaranAndJenisBiayaAndPendaftarLeadsJenjang(String nama, Followup fl, TahunAjaran tahunAjaran,JenisBiaya  jenisBiaya, Jenjang jenjang, Pageable pageable);
    Page<Tagihan> findByPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarTahunAjaran(String nama,TahunAjaran tahunAjaran, Pageable pageable);
    Page<Tagihan> findByPendaftarLeadsNamaContainingIgnoreCaseOrPendaftarNomorRegistrasiContainingIgnoreCaseAndPendaftarTahunAjaranAndJenisBiaya(String nama,String noReg,TahunAjaran tahunAjaran, JenisBiaya jenisBiaya, Pageable pageable);
    Page<Tagihan> findByJenisBiayaAndPendaftarTahunAjaran(JenisBiaya  jenisBiaya, TahunAjaran tahunAjaran, Pageable pageable);
    Page<Tagihan> findByPendaftarTahunAjaran(TahunAjaran ta, Pageable page);
    Page<Tagihan> findByPendaftarTahunAjaranAndJenisBiaya(TahunAjaran ta, JenisBiaya jenisBiaya, Pageable page);
    Page<Tagihan> findByPendaftarTahunAjaranAndPendaftarKonsentrasiAndJenisBiayaAndPendaftarLeadsJenjang(TahunAjaran ta, String konsentrasi, JenisBiaya jenisBiaya, Jenjang jenjang, Pageable page);
    Page<Tagihan> findByPendaftarTahunAjaranAndJenisBiayaAndPendaftarLeadsJenjang(TahunAjaran ta,  JenisBiaya jenisBiaya, Jenjang jenjang, Pageable page);

    @Query(value = "select * from tagihan where id_pendaftar = ?1 and id_jenisbiaya = ?2 and lunas= true limit 1", nativeQuery = true)
    Tagihan cekPendaftarDanJenisBiaya(String pendaftar, String jenisBiaya);


    @Query(value = "select * from tagihan where tanggal_jatuh_tempo < date(now()) and lunas = 0 and id_pendaftar = ?1 and id_jenisbiaya = ?2", nativeQuery = true)
    Iterable<Tagihan> cekTagihan(String pendaftar, String jenisBiaya);

    @Query(value = "select * from tagihan where lunas = 1 and cicilan != 0 and id_jenisbiaya = ?1", nativeQuery = true)
    Iterable<Tagihan> cekTagianCicilan(String jenisBiaya);

    List<Tagihan> findByPendaftarAndJenisBiayaAndTanggalJatuhTempo(Pendaftar pendaftar, JenisBiaya jenisBiaya, LocalDate tglJatuhTempo);

    Page<Tagihan> findByPendaftarIdAndJenisBiayaId(String p,String idJb, Pageable pageable);
    Tagihan findByPendaftarIdAndJenisBiayaId(String p,String idJb);

    @Query(value = "select sum(nilai) from tagihan where lunas = :lunas and id_jenisbiaya = 002 and id_pendaftar = :idPendaftar", nativeQuery = true)
    BigDecimal totalBayar(@Param("lunas")Boolean lunas,@Param("idPendaftar")String pendaftar);

    Long countTagihanByPendaftarIdAndJenisBiaya(String pendaftar, JenisBiaya jenisBiaya);

    @Query("SELECT t FROM Tagihan t " +
            "INNER JOIN t.pendaftar p " +
            "INNER JOIN p.leads l " +
            "WHERE t.jenisBiaya.id = '001' AND p.tahunAjaran = ?1 " +
            "GROUP BY p")
    Iterable<Tagihan> cekTagihanLunas(TahunAjaran tahunAjaran);

    Iterable<Tagihan> findByJenisBiayaAndPendaftarTahunAjaran(JenisBiaya jb, TahunAjaran ta);

    @Query("select t from Tagihan t " +
            "inner join t.pendaftar p " +
            "inner join p.leads l " +
            "where t.jenisBiaya = ?1 and p.tahunAjaran = ?2 and p.id = ?3 " +
            "group by p.id")
    Iterable<Tagihan> cekTagihanByPendaftar(JenisBiaya jenisBiaya, TahunAjaran tahunAjaran, String p);

    @Query("SELECT t FROM Tagihan t " +
            "INNER JOIN t.pendaftar p " +
            "INNER JOIN p.leads l " +
            "WHERE t.jenisBiaya.id = '002' AND t.pendaftar = ?1 " +
            "GROUP BY p")
    Tagihan cekTagihanDu(Pendaftar p);

    List<Tagihan> findByLunasAndZahirExportAndTanggalTagihanGreaterThanEqual(Boolean lunas, ZahirExport zahir, LocalDate tglPembuatan);
}

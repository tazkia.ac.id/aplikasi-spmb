package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.constants.StatusRecord;
import id.ac.tazkia.spmb.aplikasispmb.entity.TableUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TableUserDao extends PagingAndSortingRepository<TableUser,String>, CrudRepository<TableUser, String> {


    Iterable<TableUser> findByStatus(StatusRecord finish);
}

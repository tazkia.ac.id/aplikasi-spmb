package id.ac.tazkia.spmb.aplikasispmb.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;

import jakarta.servlet.http.HttpServletResponse;

@Controller
public class IndexController {

    @Value("classpath:sample/Bro_PES.pdf")
    private Resource brosurPei;

    @Value("classpath:sample/Bro_MUA.pdf")
    private Resource brosurMua;

    @Value("classpath:sample/Bro_AS.pdf")
    private Resource brosurAi;

    @Value("classpath:sample/Bro_MBS.pdf")
    private Resource brosurBmi;

    @Value("classpath:sample/Bro_ES.pdf")
    private Resource brosurEi;

    @Value("classpath:sample/D3.pdf")
    private Resource d3;

    @Value("classpath:sample/mesbrosur.pdf")
    private Resource mes;

    @Value("classpath:sample/masbrosur.pdf")
    private Resource mas;

    @Value("classpath:sample/iaibrosur.pdf")
    private Resource steiTazkia;


    @GetMapping("/")
    public String formIndex(){
        return "redirect:https://tazkia.ac.id/index.php/spmbtazkia";
    }

    @GetMapping("/mbs")
    public String formMbs(){
        return "mbs";
    }

    @GetMapping("/as")
    public String formAS(){
        return "as";
    }
    @GetMapping("/hes")
    public String formHES(){ return "hes"; }
    @GetMapping("/kpi")
    public String formKPI(){ return "kpi"; }

    @GetMapping("/panduanPembayaran")
    public String panduanPembayaran(){
        return "panduanPembayaran";
    }

    @GetMapping("/brosur/pei")
    public void downloadBrosurPei(HttpServletResponse response) throws Exception {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=Pendidikan_Ekonomi_Syariah.pdf");
        FileCopyUtils.copy(brosurPei.getInputStream(), response.getOutputStream());
        response.getOutputStream().flush();
    }

    @GetMapping("/brosur/mua")
    public void downloadBrosurMua(HttpServletResponse response) throws Exception {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=Hukum_Ekonomi_Syariah.pdf");
        FileCopyUtils.copy(brosurMua.getInputStream(), response.getOutputStream());
        response.getOutputStream().flush();
    }

    @GetMapping("/brosur/ai")
    public void downloadBrosurAi(HttpServletResponse response) throws Exception {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=Akuntansi_Syariah.pdf");
        FileCopyUtils.copy(brosurAi.getInputStream(), response.getOutputStream());
        response.getOutputStream().flush();
    }

    @GetMapping("/brosur/bmi")
    public void downloadBrosurBmi(HttpServletResponse response) throws Exception {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=Manajemen_Bisnis_Syariah.pdf");
        FileCopyUtils.copy(brosurBmi.getInputStream(), response.getOutputStream());
        response.getOutputStream().flush();
    }

    @GetMapping("/brosur/ei")
    public void downloadBrosurEi(HttpServletResponse response) throws Exception {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=Ilmu_Ekonomi_Syariah.pdf");
        FileCopyUtils.copy(brosurEi.getInputStream(), response.getOutputStream());
        response.getOutputStream().flush();
    }

    @GetMapping("/brosur/d3")
    public void downloadBrosurD3(HttpServletResponse response) throws Exception {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=Manajemen_Keuangan_Mikro_Syariah.pdf");
        FileCopyUtils.copy(d3.getInputStream(), response.getOutputStream());
        response.getOutputStream().flush();
    }

    @GetMapping("/brosur/institut")
    public void downloadBrosurStei(HttpServletResponse response) throws Exception {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=Institut_Tazkia.pdf");
        FileCopyUtils.copy(steiTazkia.getInputStream(), response.getOutputStream());
        response.getOutputStream().flush();
    }

    @GetMapping("/brosur/mes")
    public void downloadBrosurMes(HttpServletResponse response) throws Exception {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=Pascasarja_MES.pdf");
        FileCopyUtils.copy(mes.getInputStream(), response.getOutputStream());
        response.getOutputStream().flush();
    }

    @GetMapping("/brosur/mas")
    public void downloadBrosurMas(HttpServletResponse response) throws Exception {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=Pascasarja_MAS.pdf");
        FileCopyUtils.copy(mas.getInputStream(), response.getOutputStream());
        response.getOutputStream().flush();
    }

}

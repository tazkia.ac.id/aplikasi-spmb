package id.ac.tazkia.spmb.aplikasispmb.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class JournalGetTemplateDetail {
    private BigDecimal amount;
}

package id.ac.tazkia.spmb.aplikasispmb.dto;

import lombok.Data;

@Data
public class JournalResponse {
    private String responseCode;
    private String responseMessage;
}

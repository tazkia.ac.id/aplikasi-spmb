package id.ac.tazkia.spmb.aplikasispmb.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Column;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class MigrasiCicilanDto {

    private String urutan;
    private String keterangan;
    private BigDecimal nominal;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalKirim;
    private Boolean status;
    private String nomorTagihan;
}

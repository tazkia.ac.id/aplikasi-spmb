package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.constants.AppConstants;
import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.dao.beasiswa.PendaftarBeasiswaDao;
import id.ac.tazkia.spmb.aplikasispmb.dto.NotifRegistrasiReferalDto;
import id.ac.tazkia.spmb.aplikasispmb.dto.PendaftarDto;
import id.ac.tazkia.spmb.aplikasispmb.dto.SarjanaDto;
import id.ac.tazkia.spmb.aplikasispmb.dto.beasiswa.BeasiswaDto;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import id.ac.tazkia.spmb.aplikasispmb.entity.beasiswa.PendaftarBeasiswa;
import id.ac.tazkia.spmb.aplikasispmb.service.LeadsService;
import id.ac.tazkia.spmb.aplikasispmb.service.NotifikasiService;
import id.ac.tazkia.spmb.aplikasispmb.service.PendaftarService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

@Controller
public class RegistrasiController {
    private static final Logger logger = LoggerFactory.getLogger(RegistrasiController.class);

    //Pendaftaran Sarjana Reguler
    @Autowired  private KabupatenKotaDao kabupatenKotaDao;
    @Autowired  private ProgramStudiDao programStudiDao;
    @Autowired  private DashboardPendaftarController dpc;
    @Autowired  private SekolahDao sekolahDao;
    @Autowired  private LeadsService leadsService;
    @Autowired  private UserDao userDao;
    @Autowired  private LeadsDao leadsDao;
    @Autowired  private RoleDao roleDao;
    @Autowired  private UserPasswordDao userPasswordDao;
    @Autowired  private PasswordEncoder passwordEncoder;
    @Autowired  private TahunAjaranDao tahunAjaranDao;
    @Autowired  private  ReferalDao referalDao;
    @Autowired  private NotifikasiService notifikasiService;
    @Autowired  private PendaftarService pendaftarService;
    @Autowired  private PendaftarDao pendaftarDao;
    @Autowired  private JadwalTestDao jadwalTestDao;
    @Autowired private  SubscribeDao subscribeDao;
    @Autowired private  FollowupDao followupDao;
    @Autowired private InstitutDao institutDao;
    @Autowired private PendaftarBeasiswaDao beasiswaDao;

    @ModelAttribute("daftarSekolah")
    public Iterable<Sekolah> daftarSekolah(){
        return sekolahDao.findAll();
    }

    @GetMapping("/sarjanaReguler")
    public void formSarjanaReguler(Model m, String kode){
        m.addAttribute("daftarProdiS1",dpc.daftarProdiS1Tazkia());
        m.addAttribute("daftarKokab", dpc.daftarKokab());

        if (kode != null){
            Referal cekReferal = referalDao.findAllByKodeReferal(kode);
            if (cekReferal != null) {
                logger.info("Referal {} ditemukan", kode);
                m.addAttribute("kode", kode);
            }
        }
    }

    @GetMapping("/registrasi/selesai")
    public void registrasiSelesai(){}

    @PostMapping("/sarjanaReguler")
    public String fungsiSarjanaReguler(@ModelAttribute @Valid SarjanaDto sarjanaDto, RedirectAttributes redirectAttributes){
        TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);
        User cekLeads = userDao.findByUsername(sarjanaDto.getEmail());
        ProgramStudi prodi = programStudiDao.findById(sarjanaDto.getProgramStudi()).get();
        Institut institut = prodi.getInstitut();
        System.out.println("leads nya : "+cekLeads);
        if (cekLeads == null) {
            Leads leads = new Leads();
            //CreateUser&Password
            User user = new User();
            Role role = roleDao.findById(AppConstants.ROLE_PENDAFTAR).get();

            user.setUsername(sarjanaDto.getEmail());
            user.setActive(true);
            user.setRole(role);
            userDao.save(user);

            UserPassword password = new UserPassword();
            password.setUser(user);
            password.setPassword(passwordEncoder.encode(sarjanaDto.getPassword()));
            userPasswordDao.save(password);
            //

            TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
            leads.setTahunAjaran(ta);
            leads.setUser(user);

            Referal referal = referalDao.findAllByKodeReferal(sarjanaDto.getKode());
            if (referal == null) {
                logger.info("Tidak menggunakan kode referal");
            } else {
                logger.info("Kode Referal ditemukan {}", referal);
                leads.setKode(referal);
            }
            logger.info("Form Sarjana Reguler : {}", sarjanaDto);

            BeanUtils.copyProperties(sarjanaDto, leads);
            leads.setKelas("Reguler");
            leads.setJenjang(Jenjang.S1);
            leads.setNama(sarjanaDto.getNama());

            Subscribe subscribe = subscribeDao.cekNama(sarjanaDto.getNama(), sarjanaDto.getEmail(),"Reguler");
            if (subscribe == null) {
                Subscribe sc = new Subscribe();
                sc.setStatus("None");
                sc.setTglInsert(LocalDateTime.now());

                Followup followup = followupDao.cariUserFuS1(institut);
                Integer jumlah = Integer.valueOf(followup.getJumlah());
                Integer jmlh = jumlah + 1;
                followup.setJumlah(jmlh.toString());
                followupDao.save(followup);

                sc.setNama(sarjanaDto.getNama());
                sc.setEmail(sarjanaDto.getEmail());
                sc.setNoWa(sarjanaDto.getNoHp());
                sc.setAsalKota(sarjanaDto.getIdKabupatenKota().getNama());
                sc.setAsalSekolah(sarjanaDto.getNamaAsalSekolah());
                sc.setFollowup(followup);
                sc.setStatus("Relevant - Sudah Registrasi");
                sc.setHubungi(0);
                sc.setSumber("spmb.tazkia.ac.id");
                sc.setKelas("Reguler");
                sc.setInstitut(institut);
                sc.setTahunAjaran(tahunAjaran);
                sc.setJenjang(Jenjang.S1);
                subscribeDao.save(sc);
                logger.info("Subscribe baru : {}", sc.getNama());
                leads.setSubscribe(sc);
            }else {
                leads.setSubscribe(subscribe);
                subscribe.setStatus("Relevant - Sudah Registrasi");
                subscribe.setTglUpdate(LocalDate.now());
                subscribe.setUserUpdate(subscribe.getFollowup().getUser());
                subscribeDao.save(subscribe);
            }
            leads.setInstitut(institut);
            leadsDao.save(leads);

            if (referal != null) {
                NotifRegistrasiReferalDto nrf = new NotifRegistrasiReferalDto();
                nrf.setEmail(sarjanaDto.getEmail());
                nrf.setNama(sarjanaDto.getNama());
                nrf.setNoHp(sarjanaDto.getNoHp());
                nrf.setEmailRef(referal.getEmail());
                nrf.setKodeRef(referal.getKodeReferal());

                notifikasiService.NotifikasiReferal(nrf);
            } else {
                logger.info("Kode referal tidak memiliki email");
            }

            PendaftarDto pd = new PendaftarDto();
            pd.setLeads(leads);
            pd.setKonsentrasi("Reguler");
            if (referal != null) {
                pd.setKodeReferal(referal.getKodeReferal());
            }
            if (leads.getSubscribe() != null) {
                pd.setFollowup(leads.getSubscribe().getFollowup().getId());
            }else {
                pd.setFollowup(null);
            }
            BeanUtils.copyProperties(sarjanaDto, pd);

            // load kabupaten kota
            KabupatenKota kk = kabupatenKotaDao.findById(sarjanaDto.getIdKabupatenKota().getId()).get();

            // load program studi
            pendaftarService.prosesPendaftar(pd, prodi, kk);
            logger.info("Pendaftar {} berhasil disimpan", sarjanaDto.getNama());


            Pendaftar p = pendaftarDao.findByLeads(leads);
            logger.info("Leads a.n {} Berhasiil di simpan", leads.getNama());

            redirectAttributes.addFlashAttribute("nomorRegistrasi", p.getNomorRegistrasi());
            return "redirect:/registrasi/selesai";

        } else {
            redirectAttributes.addFlashAttribute("email", sarjanaDto.getEmail());
            return "redirect:/leads/gagal";

        }

    }

    @GetMapping("/kelasKaryawan")
    public void formkelasKaryawan(@RequestParam (required = false) String id, String kode, Model m){
        if (id != null) {
            Subscribe subscribe = subscribeDao.findById(id).get();
            System.out.println("Subcriber : "+subscribe);
            m.addAttribute("sc", subscribe);
        }
        m.addAttribute("daftarProdiS1",dpc.daftarProdiS1Tazkia());
        m.addAttribute("daftarKokab", dpc.daftarKokab());

        if (kode != null){
            Referal cekReferal = referalDao.findAllByKodeReferal(kode);
            if (cekReferal != null) {
                logger.info("Referal {} ditemukan", kode);
                m.addAttribute("kode", kode);
            }
        }
    }

    @PostMapping("/kelasKaryawan")
    public String fungsikelasKaryawan(@ModelAttribute @Valid SarjanaDto karyawanDto, RedirectAttributes redirectAttributes){
        TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);
        User cekLeads = userDao.findByUsername(karyawanDto.getEmail());
        Institut institut = institutDao.findById("1").get(); // Institut Tazkia
        if (cekLeads == null) {
            Leads leads = new Leads();
            //CreateUser&Password
            User user = new User();
            Role role = roleDao.findById(AppConstants.ROLE_PENDAFTAR).get();

            user.setUsername(karyawanDto.getEmail());
            user.setActive(true);
            user.setRole(role);
            userDao.save(user);

            UserPassword password = new UserPassword();
            password.setUser(user);
            password.setPassword(passwordEncoder.encode(karyawanDto.getPassword()));
            userPasswordDao.save(password);
            //

            TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
            leads.setTahunAjaran(ta);
            leads.setUser(user);

            Referal referal = referalDao.findAllByKodeReferal(karyawanDto.getKode());
            if (referal == null) {
                logger.info("Tidak menggunakan kode referal");
            } else {
                logger.info("Kode Referal ditemukan {}", referal);
                leads.setKode(referal);
            }
            String prd = karyawanDto.getProgramStudi().substring(0, 3);
            logger.info("Form Karyawan Reguler : {}", prd);

            BeanUtils.copyProperties(karyawanDto, leads);
            leads.setKelas("Karyawan");
            leads.setJenjang(Jenjang.S1);
            leads.setNama(karyawanDto.getNama());

            Subscribe subscribe = subscribeDao.cekNama(karyawanDto.getNama(), karyawanDto.getEmail(),"Karyawan");
            if (subscribe == null) {
                Subscribe sc = new Subscribe();
                sc.setStatus("None");
                sc.setTglInsert(LocalDateTime.now());

                Followup followup = followupDao.cariUserFuKk();
                Integer jumlah = Integer.valueOf(followup.getJumlah());
                Integer jmlh = jumlah + 1;
                followup.setJumlah(jmlh.toString());
                followupDao.save(followup);

                sc.setNama(karyawanDto.getNama());
                sc.setEmail(karyawanDto.getEmail());
                sc.setNoWa(karyawanDto.getNoHp());
                sc.setAsalKota(karyawanDto.getIdKabupatenKota().getNama());
                sc.setAsalSekolah(karyawanDto.getNamaAsalSekolah());
                sc.setFollowup(followup);
                sc.setStatus("Relevant - Sudah Registrasi");
                sc.setHubungi(0);
                sc.setSumber("spmb.tazkia.ac.id");
                sc.setKelas("Karyawan");
                sc.setInstitut(institut);
                sc.setTahunAjaran(tahunAjaran);
                sc.setJenjang(Jenjang.S1);
                sc.setLulusan(karyawanDto.getLulusan());
                subscribeDao.save(sc);
                logger.info("Subscribe baru : {}", sc.getNama());
                leads.setSubscribe(sc);
            }else {
                leads.setSubscribe(subscribe);
                subscribe.setStatus("Relevant - Sudah Registrasi");
                subscribe.setTglUpdate(LocalDate.now());
                subscribe.setUserUpdate(subscribe.getFollowup().getUser());
                subscribeDao.save(subscribe);
            }
            leads.setInstitut(institut);
            leadsDao.save(leads);

            if (referal != null) {
                NotifRegistrasiReferalDto nrf = new NotifRegistrasiReferalDto();
                nrf.setEmail(karyawanDto.getEmail());
                nrf.setNama(karyawanDto.getNama());
                nrf.setNoHp(karyawanDto.getNoHp());
                nrf.setEmailRef(referal.getEmail());
                nrf.setKodeRef(referal.getKodeReferal());

                notifikasiService.NotifikasiReferal(nrf);
            } else {
                logger.info("Kode referal tidak memiliki email");
            }

            PendaftarDto pd = new PendaftarDto();
            pd.setLeads(leads);
            pd.setKonsentrasi("Karyawan");
            if (referal != null) {
                pd.setKodeReferal(referal.getKodeReferal());
            }            if (leads.getSubscribe() != null) {
                pd.setFollowup(leads.getSubscribe().getFollowup().getId());
            }else {
                pd.setFollowup(null);
            }
            BeanUtils.copyProperties(karyawanDto, pd);

            // load kabupaten kota
            KabupatenKota kk = kabupatenKotaDao.findById(karyawanDto.getIdKabupatenKota().getId()).get();

            // load program studi
            ProgramStudi prodi = programStudiDao.findById(prd).get();
            pendaftarService.prosesPendaftar(pd, prodi, kk);
            logger.info("Pendaftar {} berhasil disimpan", karyawanDto.getNama());


            Pendaftar p = pendaftarDao.findByLeads(leads);
            logger.info("Leads a.n {} Berhasiil di simpan", leads.getNama());

            redirectAttributes.addFlashAttribute("nomorRegistrasi", p.getNomorRegistrasi());
            return "redirect:/registrasi/selesai";

        } else {
            redirectAttributes.addFlashAttribute("email", karyawanDto.getEmail());
            return "redirect:/leads/gagal";

        }

    }

    @GetMapping("/pascasarjana")
    public void formPascasarjana(Model m, String kode){
        m.addAttribute("daftarProdiS2",dpc.daftarProdiS2());
        m.addAttribute("daftarKokab", dpc.daftarKokab());

        if (kode != null){
            Referal cekReferal = referalDao.findAllByKodeReferal(kode);
            if (cekReferal != null) {
                logger.info("Referal {} ditemukan", kode);
                m.addAttribute("kode", kode);
            }
        }
    }

    @PostMapping("/pascasarjana")
    public String fungsiPascasarjana(@ModelAttribute @Valid SarjanaDto pascasarjanaDto, RedirectAttributes redirectAttributes){
        Institut institut = institutDao.findById("1").get(); // Institut Tazkia
        User cekLeads = userDao.findByUsername(pascasarjanaDto.getEmail());
        if (cekLeads == null) {
            Leads leads = new Leads();
            //CreateUser&Password
            User user = new User();
            Role role = roleDao.findById(AppConstants.ROLE_PENDAFTAR).get();

            user.setUsername(pascasarjanaDto.getEmail());
            user.setActive(true);
            user.setRole(role);
            userDao.save(user);

            UserPassword password = new UserPassword();
            password.setUser(user);
            password.setPassword(passwordEncoder.encode(pascasarjanaDto.getPassword()));
            userPasswordDao.save(password);
            //

            TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
            leads.setTahunAjaran(ta);
            leads.setUser(user);

            Subscribe subscribe = subscribeDao.cekNama(pascasarjanaDto.getNama(), pascasarjanaDto.getEmail(),"S2");
            if (subscribe == null) {
                Subscribe sc = new Subscribe();
                sc.setStatus("None");
                sc.setTglInsert(LocalDateTime.now());

                Followup followup = followupDao.cariUserFuKk();
                Integer jumlah = Integer.valueOf(followup.getJumlah());
                Integer jmlh = jumlah + 1;
                followup.setJumlah(jmlh.toString());
                followupDao.save(followup);

                sc.setNama(pascasarjanaDto.getNama());
                sc.setEmail(pascasarjanaDto.getEmail());
                sc.setNoWa(pascasarjanaDto.getNoHp());
                sc.setAsalKota(pascasarjanaDto.getIdKabupatenKota().getNama());
                sc.setAsalSekolah(pascasarjanaDto.getNamaAsalSekolah());
                sc.setFollowup(followup);
                sc.setStatus("Relevant - Sudah Registrasi");
                sc.setHubungi(0);
                sc.setSumber("spmb.tazkia.ac.id");
                sc.setKelas(pascasarjanaDto.getKonsentrasi());
                sc.setInstitut(institut);
                sc.setTahunAjaran(ta);
                sc.setJenjang(Jenjang.S2);
                sc.setLulusan(pascasarjanaDto.getLulusan());
                subscribeDao.save(sc);
                logger.info("Subscribe baru : {}", sc.getNama());
                leads.setSubscribe(sc);
            }else {
                leads.setSubscribe(subscribe);
                subscribe.setStatus("Relevant - Sudah Registrasi");
                subscribe.setTglUpdate(LocalDate.now());
                subscribe.setUserUpdate(subscribe.getFollowup().getUser());
                subscribeDao.save(subscribe);
            }

            Referal referal = referalDao.findAllByKodeReferal(pascasarjanaDto.getKode());
            if (referal == null) {
                logger.info("Tidak menggunakan kode referal");
            } else {
                logger.info("Kode Referal ditemukan {}", referal);
                leads.setKode(referal);
            }

            BeanUtils.copyProperties(pascasarjanaDto, leads);
            leads.setKelas(pascasarjanaDto.getKonsentrasi());
            leads.setJenjang(Jenjang.S2);
            leads.setNama(pascasarjanaDto.getNama());
            leads.setInstitut(institut);
            leadsDao.save(leads);

//            apiService.insertSubcriber(leads);
//            apiService.insertGroup(leads, insertGroupLeads);

            if (referal != null) {
                NotifRegistrasiReferalDto nrf = new NotifRegistrasiReferalDto();
                nrf.setEmail(pascasarjanaDto.getEmail());
                nrf.setNama(pascasarjanaDto.getNama());
                nrf.setNoHp(pascasarjanaDto.getNoHp());
                nrf.setEmailRef(referal.getEmail());
                nrf.setKodeRef(referal.getKodeReferal());

                notifikasiService.NotifikasiReferal(nrf);
            } else {
                logger.info("Kode referal tidak memiliki email");
            }

            logger.info("Leads a.n {} Berhasiil di simpan", leads.getNama());
            PendaftarDto pd = new PendaftarDto();
            pd.setLeads(leads);
            pd.setKonsentrasi(pascasarjanaDto.getKonsentrasi());
            if (referal != null) {
                pd.setKodeReferal(referal.getKodeReferal());
            }
            BeanUtils.copyProperties(pascasarjanaDto, pd);

            // load kabupaten kota
            KabupatenKota kk = kabupatenKotaDao.findById(pascasarjanaDto.getIdKabupatenKota().getId()).get();

            // load program studi
            ProgramStudi prodi = programStudiDao.findById(pascasarjanaDto.getProgramStudi()).get();
            pendaftarService.prosesPendaftar(pd, prodi, kk);
            logger.info("Pendaftar {} berhasil disimpan", pascasarjanaDto.getNama());


            Pendaftar p = pendaftarDao.findByLeads(leads);

            redirectAttributes.addFlashAttribute("nomorRegistrasi", p.getNomorRegistrasi());
            return "redirect:/registrasi/selesai";
        } else {
            redirectAttributes.addFlashAttribute("email", pascasarjanaDto.getEmail());
            return "redirect:/leads/gagal";

        }
    }



    @GetMapping("/stmiktazkia")
    public void formStmikTazkia(Model m, String kode){
        m.addAttribute("daftarProdiS1",dpc.daftarProdiS1Stmik());
        m.addAttribute("daftarKokab", dpc.daftarKokab());

        if (kode != null){
            Referal cekReferal = referalDao.findAllByKodeReferal(kode);
            if (cekReferal != null) {
                logger.info("Referal {} ditemukan", kode);
                m.addAttribute("kode", kode);
            }
        }
    }

    @PostMapping("/stmiktazkia")
    public String saveDaftarStmik(@ModelAttribute @Valid SarjanaDto sarjanaDto, RedirectAttributes redirectAttributes){
        TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);
        User cekLeads = userDao.findByUsername(sarjanaDto.getEmail());
        Institut institut = institutDao.findById("2").get(); // STMIK Tazkia
        System.out.println("leads nya : "+cekLeads);
        if (cekLeads == null) {
            Leads leads = new Leads();
            //CreateUser&Password
            User user = new User();
            Role role = roleDao.findById(AppConstants.ROLE_PENDAFTAR).get();

            user.setUsername(sarjanaDto.getEmail());
            user.setActive(true);
            user.setRole(role);
            userDao.save(user);

            UserPassword password = new UserPassword();
            password.setUser(user);
            password.setPassword(passwordEncoder.encode(sarjanaDto.getPassword()));
            userPasswordDao.save(password);
            //

            TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
            leads.setTahunAjaran(ta);
            leads.setUser(user);

            Referal referal = referalDao.findAllByKodeReferal(sarjanaDto.getKode());
            if (referal == null) {
                logger.info("Tidak menggunakan kode referal");
            } else {
                logger.info("Kode Referal ditemukan {}", referal);
                leads.setKode(referal);
            }
            logger.info("Form Sarjana Reguler : {}", sarjanaDto);

            BeanUtils.copyProperties(sarjanaDto, leads);
            leads.setKelas("Reguler");
            leads.setJenjang(Jenjang.S1);
            leads.setNama(sarjanaDto.getNama());

            Subscribe subscribe = subscribeDao.cekNama(sarjanaDto.getNama(), sarjanaDto.getEmail(),"Reguler");
            if (subscribe == null) {
                Subscribe sc = new Subscribe();
                sc.setStatus("None");
                sc.setTglInsert(LocalDateTime.now());

                Followup followup;
                Optional<Followup> fu = followupDao.findById("U0002");
                followup = fu.orElseGet(() -> followupDao.findById("005").get());

                sc.setNama(sarjanaDto.getNama());
                sc.setEmail(sarjanaDto.getEmail());
                sc.setNoWa(sarjanaDto.getNoHp());
                sc.setAsalKota(sarjanaDto.getIdKabupatenKota().getNama());
                sc.setAsalSekolah(sarjanaDto.getNamaAsalSekolah());
                sc.setFollowup(followup);
                sc.setStatus("Relevant - Sudah Registrasi");
                sc.setHubungi(0);
                sc.setSumber("stmik.tazkia.ac.id");
                sc.setKelas("Reguler");
                sc.setInstitut(institut);
                sc.setTahunAjaran(tahunAjaran);
                sc.setJenjang(Jenjang.S1);
                subscribeDao.save(sc);
                logger.info("Subscribe baru : {}", sc.getNama());
                leads.setSubscribe(sc);
            }else {
                leads.setSubscribe(subscribe);
                subscribe.setStatus("Relevant - Sudah Registrasi");
                subscribe.setSumber("stmik.tazkia.ac.id");
                subscribe.setTglUpdate(LocalDate.now());
                subscribe.setUserUpdate(subscribe.getFollowup().getUser());
                subscribeDao.save(subscribe);
            }
            leads.setInstitut(institut);
            leadsDao.save(leads);

            if (referal != null) {
                NotifRegistrasiReferalDto nrf = new NotifRegistrasiReferalDto();
                nrf.setEmail(sarjanaDto.getEmail());
                nrf.setNama(sarjanaDto.getNama());
                nrf.setNoHp(sarjanaDto.getNoHp());
                nrf.setEmailRef(referal.getEmail());
                nrf.setKodeRef(referal.getKodeReferal());

                notifikasiService.NotifikasiReferal(nrf);
            } else {
                logger.info("Kode referal tidak memiliki email");
            }

            PendaftarDto pd = new PendaftarDto();
            pd.setLeads(leads);
            pd.setKonsentrasi("Reguler");
            if (referal != null) {
                pd.setKodeReferal(referal.getKodeReferal());
            }
            if (leads.getSubscribe() != null) {
                pd.setFollowup(leads.getSubscribe().getFollowup().getId());
            }else {
                pd.setFollowup(null);
            }
            BeanUtils.copyProperties(sarjanaDto, pd);

            // load kabupaten kota
            KabupatenKota kk = kabupatenKotaDao.findById(sarjanaDto.getIdKabupatenKota().getId()).get();

            // load program studi
            ProgramStudi prodi = programStudiDao.findById(sarjanaDto.getProgramStudi()).get();
            pendaftarService.prosesPendaftar(pd, prodi, kk);
            logger.info("Pendaftar {} berhasil disimpan", sarjanaDto.getNama());


            Pendaftar p = pendaftarDao.findByLeads(leads);
            logger.info("Leads a.n {} Berhasiil di simpan", leads.getNama());

            redirectAttributes.addFlashAttribute("nomorRegistrasi", p.getNomorRegistrasi());
            return "redirect:/registrasi/selesai";

        } else {
            redirectAttributes.addFlashAttribute("email", sarjanaDto.getEmail());
            return "redirect:/leads/gagal";

        }

    }

    @GetMapping("/daftarBeasiswa")
    public void fomrBeasiswa(Model m, String kode){
//        m.addAttribute("daftarProdiS1",dpc.daftarProdiS1Stmik());
        m.addAttribute("daftarKokab", dpc.daftarKokab());
        m.addAttribute("beasiswa", new BeasiswaDto());
    }

    @GetMapping("/beasiswa/selesai")
    public void berhasilDaftarBeasiswa(){}

    @GetMapping("/beasiswa/gagal")
    public void gagalDaftarBeasiswa(){}

    @PostMapping("/daftarBeasiswa")
    public String savePendaftarBeasiswa(@ModelAttribute @Valid BeasiswaDto beasiswaDto, RedirectAttributes attributes){
        TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);

        PendaftarBeasiswa cekUser = beasiswaDao.findByEmail(beasiswaDto.getEmail());

        if (cekUser == null) {

            User cekusername = userDao.findByUsername(beasiswaDto.getEmail());
            if (cekusername != null) {
                attributes.addFlashAttribute("email", beasiswaDto.getEmail());
                return "redirect:/beasiswa/gagal";
            }

            PendaftarBeasiswa beasiswa = new PendaftarBeasiswa();
//            CreateUser&Password
            User user = new User();
            Role role = roleDao.findById(AppConstants.ROLE_BEASISWA).get();

            user.setUsername(beasiswaDto.getEmail());
            user.setActive(true);
            user.setRole(role);
            userDao.save(user);

            UserPassword password = new UserPassword();
            password.setUser(user);
            password.setPassword(passwordEncoder.encode(beasiswaDto.getPassword()));
            userPasswordDao.save(password);

            beasiswa.setUser(user);
            BeanUtils.copyProperties(beasiswaDto, beasiswa);
            beasiswaDao.save(beasiswa);

            return "redirect:/beasiswa/selesai";
//
        } else {
            attributes.addFlashAttribute("email", beasiswaDto.getEmail());
            return "redirect:/beasiswa/gagal";
        }
    }

}

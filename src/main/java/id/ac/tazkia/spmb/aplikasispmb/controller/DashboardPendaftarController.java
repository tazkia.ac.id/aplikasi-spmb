package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.dto.PendaftarDto;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import id.ac.tazkia.spmb.aplikasispmb.service.PendaftarService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import jakarta.validation.Valid;
import java.util.List;

@Controller
public class DashboardPendaftarController {
    private static final Logger logger = LoggerFactory.getLogger(DashboardPendaftarController.class);


    @Autowired private ProgramStudiDao programStudiDao;
    @Autowired private KabupatenKotaDao kabupatenKotaDao;
    @Autowired private PendaftarService pendaftarService;
    @Autowired private UserDao userDao;
    @Autowired private LeadsDao leadsDao;
    @Autowired private PendaftarDao pendaftarDao;
    @Autowired private PembayaranController pembayaranController;
    @Autowired private ReferalDao referalDao;
    @Autowired private InstitutDao institutDao;


    @ModelAttribute("daftarProdiS1Tazkia")
    public Iterable<ProgramStudi> daftarProdiS1Tazkia(){

        Institut instTazkia = institutDao.findById("1").get();

        return programStudiDao.findByJenjangAndInstitutAndStatus(Jenjang.S1, instTazkia, Status.AKTIF);
    }
    @ModelAttribute("daftarProdiD3")
    public Iterable<ProgramStudi> daftarProdiD3(){
        String id ="012";
        return programStudiDao.cariProdiD3(id);
    }

    @ModelAttribute("daftarProdiS2")
    public Iterable<ProgramStudi> daftarProdiS2(){
        Institut instTazkia = institutDao.findById("1").get();
        return programStudiDao.findByJenjangAndInstitutAndStatus(Jenjang.S2, instTazkia, Status.AKTIF);
    }

    @ModelAttribute("daftarProdiS1Stmik")
    public Iterable<ProgramStudi> daftarProdiS1Stmik(){
        Institut stmikTazkia = institutDao.findById("2").get();
        return programStudiDao.findByJenjangAndInstitutAndStatus(Jenjang.S1, stmikTazkia, Status.AKTIF);
    }

    @ModelAttribute("daftarKokab")
    public Iterable<KabupatenKota> daftarKokab(){
        return kabupatenKotaDao.findAll();
    }

//S1
    @GetMapping("/dashboardPendaftar")
    public  String dashboardPendaftar(@ModelAttribute @Valid PendaftarDto pendaftarDto,Model model, BindingResult errors,
                                      Authentication currentUser, Pageable page){
        logger.debug("Authentication class : {}",currentUser.getClass().getName());

        if(currentUser == null){
            logger.warn("Current user is null");
        }

        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }

        Leads leads = leadsDao.findByUser(u);
        pendaftarDto.setLeads(leads);
        model.addAttribute("pendaftarDto", pendaftarDto);



        Pendaftar pendaftar = pendaftarDao.findByLeads(leads);
        if (pendaftar != null) {
            model.addAttribute("pendaftar", pendaftar);
        }

////Cek Pembayaran
//        Pembayaran pembayaran = pembayaranController.cekPembayaran(pendaftar);
//        model.addAttribute("pembayaran", pembayaran);

        return  "dashboardPendaftar";
    }

    //S1
    @PostMapping("/dashboardPendaftar")
    public  String prosesDashboardPendaftar(@ModelAttribute @Valid PendaftarDto pendaftarDto,Model model, BindingResult errors,
                                      Authentication currentUser, Pageable page){
        logger.debug("Authentication class : {}",currentUser.getClass().getName());

        if(currentUser == null){
            logger.warn("Current user is null");
        }

        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }

        Leads leads = leadsDao.findByUser(u);

        // load kabupaten kota
        KabupatenKota kk = kabupatenKotaDao.findById(pendaftarDto.getIdKabupatenKota()).get();
        if(kk == null){
            errors.reject("idKabupatenKota", "Data kabupaten tidak ada dalam database");
        }

        // load program studi
        ProgramStudi prodi = programStudiDao.findById(pendaftarDto.getProgramStudi()).get();
        if(prodi == null){
            errors.reject("programStudiPilihan", "Program studi tidak ada dalam database");
        }

        if(errors.hasErrors()){
            return "dashboardPendaftar";
        }

        Pendaftar pe = pendaftarDao.findByLeads(leads);
        System.out.println("Kode referal "+ pendaftarDto.getKodeReferal());
        if (pe == null) {
            pendaftarDto.setLeads(leads);
            pendaftarService.prosesPendaftar(pendaftarDto, prodi, kk);
        } else {
            logger.warn("Pendaftar dengan nama {} sudah ada.", pe.getLeads().getNama());
        }



        return "redirect:tagihan/listPendaftar";
    }

}

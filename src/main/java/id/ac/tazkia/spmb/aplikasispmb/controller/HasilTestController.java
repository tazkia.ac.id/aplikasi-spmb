package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.constants.AppConstants;
import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.dto.HasilTestDto;
import id.ac.tazkia.spmb.aplikasispmb.dto.ValidasiTagihan;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import id.ac.tazkia.spmb.aplikasispmb.service.ApiService;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class HasilTestController {
    private static final Logger logger = LoggerFactory.getLogger(HasilTestController.class);


    @Autowired private GradeDao gradeDao;
    @Autowired private PeriodeDao periodeDao;
    @Autowired private UserDao userDao;
    @Autowired private HasilTestDao hasilTestDao;
    @Autowired private JadwalTestDao jadwalTestDao;
    @Autowired private LeadsDao leadsDao;
    @Autowired private PendaftarDao pendaftarDao;
    @Autowired private PembayaranController pembayaranController;
    @Autowired private SmartTestDao smartTestDao;
    @Autowired private TahunAjaranDao tahunAjaranDao;
    @Autowired private ApiService apiService;
    @Autowired private FollowupDao followupDao;
    @Autowired private TagihanDao tagihanDao;

    @GetMapping("/hasilTest/form")
    public  void formHasilTest(@RequestParam(value = "id", required = true) String id,
                               @RequestParam(required = false) String error,
                               @RequestParam(required = false) String jdwl,
                               @RequestParam(required = false) String smartT,
                               @RequestParam(required = false) String smart,
                               Model m, Pageable stPage){

        if (jdwl != null) {
            m.addAttribute("jdwl",jdwl);
        }
        if (smartT != null) {
            m.addAttribute("smartT",smartT);
        }

        //defaultnya, isi dengan object baru
        HasilTestDto h = new HasilTestDto();
        m.addAttribute("hasil", h);
        m.addAttribute("error", error);

        JadwalTest jadwalTest = jadwalTestDao.findById(id).get();
        m.addAttribute("jadwal", jadwalTest);
        if (jadwalTest != null){
            h.setJadwalTest(jadwalTest);
        }

        HasilTest d = hasilTestDao.findByJadwalTest(jadwalTest);
        logger.debug("Nomor Registrasi :"+ jadwalTest.getPendaftar().getNomorRegistrasi());
        if (d != null){
            HasilTestDto ht = new HasilTestDto();
            Pendaftar p = pendaftarDao.findById(d.getJadwalTest().getPendaftar().getId()).get();

            System.out.println(d.getNilai());
            ht.setNilai(d.getNilai());
            ht.setUangPangkal(p.getUangPangkal());
            ht.setKeteranganTagihan(p.getKeteranganTagihan());
            ht.setExpiredDate(p.getExpiredDate());
            m.addAttribute("hasil", ht);
        }


//        String nama = jadwalTest.getPendaftar().getLeads().getNama().substring(0,4);
//        TahunAjaran t = tahunAjaranDao.findByAktif(Status.AKTIF);
//        if(nama != null) {
//            m.addAttribute("daftarJadwalSt", smartTestDao.findByNamaSt(nama,t,stPage));
//        } else {
//            m.addAttribute("daftarJadwalSt", smartTestDao.findAll(stPage));
//        }

    }

    public Grade hitungGrade(BigDecimal nilai){
        return gradeDao.findTopByNilaiMinimumLessThanOrderByNilaiMinimumDesc(nilai);
    }

    public List<Periode> cariPeriode (LocalDate tanggalTest){
        return periodeDao.cariPeriodeUntukTanggal(tanggalTest);
    }

    @Value("${group.lunas.pendaftar}") private String  deleteGroupPendaftar;
    @Value("${group.lulus.test}") private String insertHasilTest;
    @RequestMapping(value = "/hasilTest/form", method = RequestMethod.POST)
    public String prosesForm(@Valid HasilTestDto hasilTestDto, String nilai, BindingResult errors, Authentication currentUser){
        logger.debug("Authentication class : {}",currentUser.getClass().getName());

        if(currentUser == null){
            logger.warn("Current user is null");
        }

        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }

        if(errors.hasErrors()){
            return "/hasilTest/form";
        }

        Grade hasil = hitungGrade(new BigDecimal(nilai));
        logger.debug("ID GRADE :"+ hasil.getId());

        HasilTest hasilTest = new HasilTest();
        BeanUtils.copyProperties(hasilTestDto, hasilTest);

        List<Periode> periode = cariPeriode(hasilTest.getJadwalTest().getTanggalTest());
        for (Periode periode1 : periode) {
            hasilTest.setPeriode(periode1);
        }
        hasilTest.setGrade(hasil);
        hasilTest.setUser(u);
        hasilTestDao.save(hasilTest);
//        apiService.insertGroup(hasilTestDto.getJadwalTest().getPendaftar().getLeads(), insertHasilTest);
//        apiService.deleteGroup(hasilTestDto.getJadwalTest().getPendaftar().getLeads(), deleteGroupPendaftar);

        JadwalTest jadwalTest = hasilTestDto.getJadwalTest();
        jadwalTest.setJenisTest(hasilTestDto.getJenisTest());
        jadwalTest.setTanggalTest(hasilTestDto.getTanggalTest().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
        jadwalTest.setPendaftar(jadwalTest.getPendaftar());
        jadwalTestDao.save(jadwalTest);

        System.out.println(hasilTestDto);
        Pendaftar pendaftar = pendaftarDao.findById(jadwalTest.getPendaftar().getId()).get();
        pendaftar.setUangPangkal(hasilTestDto.getUangPangkal());
        pendaftar.setKeteranganTagihan(hasilTestDto.getKeteranganTagihan());
        pendaftar.setTglUpdate(LocalDate.now());
        pendaftar.setUserUpdate(u);
        pendaftar.setExpiredDate(hasilTestDto.getExpiredDate());
        pendaftarDao.save(pendaftar);

        return "redirect:/pendaftar/list";
    }

    @GetMapping("/hasilTest/formPendaftar")
    public String formHasilPendaftar(Model model,  Authentication currentUser) {

        logger.debug("Authentication class : {}", currentUser.getClass().getName());

        if (currentUser == null) {
            logger.warn("Current user is null");
        }

        String username = ((UserDetails) currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if (u == null) {
            logger.warn("Username {} not found in database ", username);
        }

        Leads leads = leadsDao.findByUser(u);
        Pendaftar pendaftar = pendaftarDao.findByLeads(leads);
        if (pendaftar != null) {
            model.addAttribute("pendaftar", pendaftar);
        }

        JadwalTest jt = jadwalTestDao.findByPendaftar(pendaftar);
        if (jt != null) {
            model.addAttribute("jadwalTest", jt);
        } else {
            logger.debug("Jadwal Test tidak ada dalam database");
        }
        HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
        if (hasilTest != null){
            model.addAttribute("hasilTest", hasilTest);
            model.addAttribute("tahunAjaran", tahunAjaranDao.findByAktif(Status.AKTIF));
        }
//Cek Pembayaran
        Pembayaran pembayaran = pembayaranController.cekPembayaran(pendaftar);
        model.addAttribute("pembayaran", pembayaran);

        return "hasilTest/formPendaftar";
    }

    @GetMapping("/hasilTest/list")
    public void hasilTestList(@RequestParam(required = false)String nama, Model m, Pageable page){
        Iterable<HasilTest> hasilTest = hasilTestDao.findAll();
        List<HasilTestDto> hasilTestDtos = new ArrayList<>();

        for (HasilTest hasilTest1 : hasilTest) {
            List<Periode> periode = periodeDao.cariPeriodeUntukTanggal(hasilTest1.getJadwalTest().getTanggalTest());
            for (Periode periode1 : periode) {
                HasilTestDto hasilTestDto = new HasilTestDto();
                hasilTestDto.setPeriode(periode1);
                hasilTestDto.setId(hasilTest1.getId());
                hasilTestDto.setPendaftar(hasilTest1.getJadwalTest().getPendaftar().getId());
                hasilTestDto.setJenisTest(hasilTest1.getJadwalTest().getJenisTest());
                hasilTestDtos.add(hasilTestDto);
                logger.debug(hasilTestDto.getId() + " " + hasilTestDto.getPeriode());
            }
        }
        m.addAttribute("dto",hasilTestDtos);

        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
        if(StringUtils.hasText(nama)) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarHasilTest", hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarLeadsNamaContainingIgnoreCase(ta,nama, page));
        } else {
            m.addAttribute("daftarHasilTest", hasilTestDao.findAllByJadwalTestPendaftarTahunAjaran(ta, page));
        }
    }


    @GetMapping("/hasilTest/xlsxHms")
    public void rekapHasilTestXlsx(HttpServletResponse response) throws Exception {

        String[] columns = {"no","no registrasi","nama","email","no telephone","Tanggal Test","grade","periode","jenis test", "Program Studi", "Jenjang"};

        Iterable<HasilTest> hasilTest = hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAktif(Status.AKTIF);

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("DetailNimPendaftar");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1 ;
        int baris = 1 ;

        for (HasilTest p : hasilTest) {
                Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(baris++);
            row.createCell(1).setCellValue(p.getJadwalTest().getPendaftar().getNomorRegistrasi());
            row.createCell(2).setCellValue(WordUtils.capitalize(p.getJadwalTest().getPendaftar().getLeads().getNama()));
            row.createCell(3).setCellValue(p.getJadwalTest().getPendaftar().getLeads().getEmail());
            row.createCell(4).setCellValue(p.getJadwalTest().getPendaftar().getLeads().getNoHp());
            row.createCell(5).setCellValue(p.getJadwalTest().getTanggalTest().format(DateTimeFormatter.ofPattern(("dd-MM-yyyy"))));
            row.createCell(6).setCellValue(p.getGrade().getNama());
            row.createCell(7).setCellValue(p.getPeriode().getNama());
            row.createCell(8).setCellValue(p.getJadwalTest().getJenisTest().toString());
            row.createCell(9).setCellValue(p.getJadwalTest().getPendaftar().getProgramStudi().getNama());
            row.createCell(10).setCellValue(p.getJadwalTest().getPendaftar().getLeads().getJenjang().toString());

        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=HasilTest.xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();

    }

    @GetMapping("/hasilTest/listNew")
    public void hasilTestListNew(@RequestParam(required = false)String nama, String user, Model m, Pageable page){
        Iterable<HasilTest> hasilTest = hasilTestDao.findAll();
        List<HasilTestDto> hasilTestDtos = new ArrayList<>();

        for (HasilTest hasilTest1 : hasilTest) {
            List<Periode> periode = periodeDao.cariPeriodeUntukTanggal(hasilTest1.getJadwalTest().getTanggalTest());
            for (Periode periode1 : periode) {
                HasilTestDto hasilTestDto = new HasilTestDto();
                hasilTestDto.setPeriode(periode1);
                hasilTestDto.setId(hasilTest1.getId());
                hasilTestDto.setPendaftar(hasilTest1.getJadwalTest().getPendaftar().getId());
                hasilTestDto.setJenisTest(hasilTest1.getJadwalTest().getJenisTest());
                hasilTestDtos.add(hasilTestDto);
                logger.debug(hasilTestDto.getId() + " " + hasilTestDto.getPeriode());
            }
        }
        m.addAttribute("dto",hasilTestDtos);

        Iterable<Followup> fu = followupDao.findByStatus(Status.AKTIF);
        m.addAttribute("listFu", fu);

        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
        if(StringUtils.hasText(nama) && !StringUtils.hasText(user)) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarHasilTest", hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarLeadsNamaContainingIgnoreCaseAndJadwalTestPendaftarLeadsJenjangAndJadwalTestPendaftarKonsentrasiOrderByTanggalInsertDesc(ta,nama, Jenjang.S1, "Reguler", page));
        } else if (!StringUtils.hasText(nama) && StringUtils.hasText(user)) {
            m.addAttribute("user", user);
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarHasilTest", hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarFollowupAndJadwalTestPendaftarLeadsJenjangAndJadwalTestPendaftarKonsentrasiOrderByTanggalInsertDesc(ta,fl,Jenjang.S1,"Reguler", page));
        }else if (StringUtils.hasText(nama) && StringUtils.hasText(user)) {
            m.addAttribute("nama", nama);
            m.addAttribute("user", user);
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarHasilTest", hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarLeadsNamaContainingIgnoreCaseAndJadwalTestPendaftarFollowupAndJadwalTestPendaftarLeadsJenjangAndJadwalTestPendaftarKonsentrasiOrderByTanggalInsertDesc(ta,nama, fl, Jenjang.S1, "Reguler",page));
        } else {
            m.addAttribute("daftarHasilTest", hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarLeadsJenjangAndJadwalTestPendaftarKonsentrasiOrderByTanggalInsertDesc(ta, Jenjang.S1,"Reguler", page));
        }

        JenisBiaya jenisBiaya = new JenisBiaya();
        jenisBiaya.setId(AppConstants.JENIS_BIAYA_DAFTAR_ULANG);
        if (jenisBiaya == null) {
            logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());

        }
        List<ValidasiTagihan> tagihans = new ArrayList<>();
        for (HasilTest pe : hasilTest) {
            Tagihan tagihan = tagihanDao.cekTagihanDu(pe.getJadwalTest().getPendaftar());
            ValidasiTagihan validasiTagihan = new ValidasiTagihan();
            validasiTagihan.setPendaftar(pe.getId());
            if (tagihan != null){
                validasiTagihan.setStatus("Ada");
            }
            if (tagihan == null){
                validasiTagihan.setStatus("Kosong");
            }
            tagihans.add(validasiTagihan);
        }

        m.addAttribute("tagihanDu", tagihans);

    }

    @GetMapping("/hasilTest/listNewKk")
    public void hasilTestListNewKk(@RequestParam(required = false)String nama, String user, Model m, Pageable page){
        Iterable<HasilTest> hasilTest = hasilTestDao.findAll();
        List<HasilTestDto> hasilTestDtos = new ArrayList<>();

        for (HasilTest hasilTest1 : hasilTest) {
            List<Periode> periode = periodeDao.cariPeriodeUntukTanggal(hasilTest1.getJadwalTest().getTanggalTest());
            for (Periode periode1 : periode) {
                HasilTestDto hasilTestDto = new HasilTestDto();
                hasilTestDto.setPeriode(periode1);
                hasilTestDto.setId(hasilTest1.getId());
                hasilTestDto.setPendaftar(hasilTest1.getJadwalTest().getPendaftar().getId());
                hasilTestDto.setJenisTest(hasilTest1.getJadwalTest().getJenisTest());
                hasilTestDtos.add(hasilTestDto);
                logger.debug(hasilTestDto.getId() + " " + hasilTestDto.getPeriode());
            }
        }
        m.addAttribute("dto",hasilTestDtos);

        Iterable<Followup> fu = followupDao.findByStatus(Status.AKTIF);
        m.addAttribute("listFu", fu);

        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
        if(StringUtils.hasText(nama) && !StringUtils.hasText(user)) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarHasilTest", hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarLeadsNamaContainingIgnoreCaseAndJadwalTestPendaftarLeadsJenjangAndJadwalTestPendaftarKonsentrasiOrderByTanggalInsertDesc(ta,nama, Jenjang.S1, "Karyawan", page));
        } else if (!StringUtils.hasText(nama) && StringUtils.hasText(user)) {
            m.addAttribute("user", user);
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarHasilTest", hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarFollowupAndJadwalTestPendaftarLeadsJenjangAndJadwalTestPendaftarKonsentrasiOrderByTanggalInsertDesc(ta,fl,Jenjang.S1,"Karyawan", page));
        }else if (StringUtils.hasText(nama) && StringUtils.hasText(user)) {
            m.addAttribute("nama", nama);
            m.addAttribute("user", user);
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarHasilTest", hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarLeadsNamaContainingIgnoreCaseAndJadwalTestPendaftarFollowupAndJadwalTestPendaftarLeadsJenjangAndJadwalTestPendaftarKonsentrasiOrderByTanggalInsertDesc(ta,nama, fl, Jenjang.S1, "Karyawan",page));
        } else {
            m.addAttribute("daftarHasilTest", hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarLeadsJenjangAndJadwalTestPendaftarKonsentrasiOrderByTanggalInsertDesc(ta, Jenjang.S1,"Karyawan", page));
        }



        JenisBiaya jenisBiaya = new JenisBiaya();
        jenisBiaya.setId(AppConstants.JENIS_BIAYA_DAFTAR_ULANG);
        if (jenisBiaya == null) {
            logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());

        }
        List<ValidasiTagihan> tagihans = new ArrayList<>();
        for (HasilTest pe : hasilTest) {
            Tagihan tagihan = tagihanDao.cekTagihanDu(pe.getJadwalTest().getPendaftar());
            ValidasiTagihan validasiTagihan = new ValidasiTagihan();
            validasiTagihan.setPendaftar(pe.getId());
            if (tagihan != null){
                validasiTagihan.setStatus("Ada");
            }
            if (tagihan == null){
                validasiTagihan.setStatus("Kosong");
            }
            tagihans.add(validasiTagihan);
        }

        m.addAttribute("tagihanDu", tagihans);
    }


    @GetMapping("/hasilTest/listNewS2")
    public void hasilTestListNewS2(@RequestParam(required = false)String nama, String user, Model m, Pageable page){
        Iterable<HasilTest> hasilTest = hasilTestDao.findAll();
        List<HasilTestDto> hasilTestDtos = new ArrayList<>();

        for (HasilTest hasilTest1 : hasilTest) {
            List<Periode> periode = periodeDao.cariPeriodeUntukTanggal(hasilTest1.getJadwalTest().getTanggalTest());
            for (Periode periode1 : periode) {
                HasilTestDto hasilTestDto = new HasilTestDto();
                hasilTestDto.setPeriode(periode1);
                hasilTestDto.setId(hasilTest1.getId());
                hasilTestDto.setPendaftar(hasilTest1.getJadwalTest().getPendaftar().getId());
                hasilTestDto.setJenisTest(hasilTest1.getJadwalTest().getJenisTest());
                hasilTestDtos.add(hasilTestDto);
                logger.debug(hasilTestDto.getId() + " " + hasilTestDto.getPeriode());
            }
        }
        m.addAttribute("dto",hasilTestDtos);

        Iterable<Followup> fu = followupDao.findByStatus(Status.AKTIF);
        m.addAttribute("listFu", fu);

        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
        if(StringUtils.hasText(nama) && !StringUtils.hasText(user)) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarHasilTest", hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarLeadsNamaContainingIgnoreCaseAndJadwalTestPendaftarLeadsJenjangOrderByTanggalInsertDesc(ta,nama, Jenjang.S2,  page));
        } else if (!StringUtils.hasText(nama) && StringUtils.hasText(user)) {
            m.addAttribute("user", user);
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarHasilTest", hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarFollowupAndJadwalTestPendaftarLeadsJenjangOrderByTanggalInsertDesc(ta,fl,Jenjang.S2, page));
        }else if (StringUtils.hasText(nama) && StringUtils.hasText(user)) {
            m.addAttribute("nama", nama);
            m.addAttribute("user", user);
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarHasilTest", hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarLeadsNamaContainingIgnoreCaseAndJadwalTestPendaftarFollowupAndJadwalTestPendaftarLeadsJenjangOrderByTanggalInsertDesc(ta,nama, fl, Jenjang.S2,page));
        } else {
            m.addAttribute("daftarHasilTest", hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarLeadsJenjangOrderByTanggalInsertDesc(ta, Jenjang.S2, page));
        }


        JenisBiaya jenisBiaya = new JenisBiaya();
        jenisBiaya.setId(AppConstants.JENIS_BIAYA_DAFTAR_ULANG);
        if (jenisBiaya == null) {
            logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());

        }
        List<ValidasiTagihan> tagihans = new ArrayList<>();
        for (HasilTest pe : hasilTest) {
            Tagihan tagihan = tagihanDao.cekTagihanDu(pe.getJadwalTest().getPendaftar());
            ValidasiTagihan validasiTagihan = new ValidasiTagihan();
            validasiTagihan.setPendaftar(pe.getId());
            if (tagihan != null){
                validasiTagihan.setStatus("Ada");
            }
            if (tagihan == null){
                validasiTagihan.setStatus("Kosong");
            }
            tagihans.add(validasiTagihan);
        }

        m.addAttribute("tagihanDu", tagihans);
    }
}

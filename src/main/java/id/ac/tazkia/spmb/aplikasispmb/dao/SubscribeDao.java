package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.Followup;
import id.ac.tazkia.spmb.aplikasispmb.entity.Jenjang;
import id.ac.tazkia.spmb.aplikasispmb.entity.Subscribe;
import id.ac.tazkia.spmb.aplikasispmb.entity.TahunAjaran;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.xml.crypto.Data;
import java.time.LocalDate;
import java.time.LocalDateTime;

public interface SubscribeDao extends PagingAndSortingRepository<Subscribe,String>, CrudRepository<Subscribe,String> {
    Page<Subscribe> findByNamaContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(String nama, String kelas, String sumber, String Status,String st, Jenjang jenjang, Pageable page);
    Page<Subscribe> findByNamaContainingIgnoreCaseAndKelasAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(String nama, String kelas, String sumber,String sumber2, String Status,String st, Jenjang jenjang, Pageable page);
    Page<Subscribe> findByTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(String tahun,String kelas,String sumber, String Status, String st,Jenjang jenjang, Pageable page);
    Page<Subscribe> findByTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(String tahun,String kelas,String sumber,String sumber2, String Status, String st,Jenjang jenjang, Pageable page);
    Page<Subscribe> findBySumberContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(String ss,String kelas,String sumber, String Status, String st,Jenjang jenjang, Pageable page);
    Page<Subscribe> findBySumberContainingIgnoreCaseAndKelasAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(String ss,String kelas,String sumber,String sumber2, String Status, String st,Jenjang jenjang, Pageable page);
    Page<Subscribe> findByNamaAndFollowupIdAndTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(String nama, String user, String tahun, String kelas, String sumber, String Status, String st, Jenjang jenjang, Pageable page);
    Page<Subscribe> findByNamaAndFollowupIdAndTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(String nama, String user, String tahun, String kelas, String sumber, String sumber2, String Status, String st, Jenjang jenjang, Pageable page);
    Page<Subscribe> findByFollowupIdAndTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(String user, String tahun, String kelas, String sumber, String Status, String st, Jenjang jenjang , Pageable page);
    Page<Subscribe> findByFollowupIdAndTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(String user, String tahun, String kelas, String sumber, String sumber2, String Status, String st, Jenjang jenjang , Pageable page);
    Page<Subscribe> findByNamaContainingIgnoreCaseAndFollowupIdContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(String nama,String user,String kelas,String sumber, String Status, String st,Jenjang jenjang, Pageable page);
    Page<Subscribe> findByNamaAndFollowupIdContainingIgnoreCaseAndKelasAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(String nama,String user,String kelas,String sumber,String sumber2, String Status, String st,Jenjang jenjang, Pageable page);
    Page<Subscribe> findBySumberContainingIgnoreCaseAndFollowupIdAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(String ss,String  user,String kelas,String sumber, String Status, String st,Jenjang jenjang, Pageable page);
    Page<Subscribe> findBySumberContainingIgnoreCaseAndFollowupIdAndKelasAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(String ss,String  user,String kelas,String sumber,String sumber2, String Status, String st,Jenjang jenjang, Pageable page);
    Page<Subscribe> findBySumberContainingIgnoreCaseAndNamaContainingIgnoreCaseAndFollowupIdAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(String ss,String nama,String  user,String kelas,String sumber, String Status, String st,Jenjang jenjang, Pageable page);
    Page<Subscribe> findBySumberContainingIgnoreCaseAndNamaContainingIgnoreCaseAndFollowupIdAndKelasAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(String ss,String nama,String  user,String kelas,String sumber,String sumber2, String Status, String st,Jenjang jenjang, Pageable page);
    Page<Subscribe> findByNamaAndTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(String nama, String  tahun, String kelas, String sumber, String Status,String st, Jenjang jenjang, Pageable page);
    Page<Subscribe> findByNamaAndTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(String nama, String  tahun, String kelas, String sumber,String sumber2, String Status,String st, Jenjang jenjang, Pageable page);
    Page<Subscribe> findByNamaAndTahunAjaranIdAndKelasAndSumberNotAndStatusNotOrderByTglInsertDesc(String nama,TahunAjaran tahun,String kelas,String sumber, String Status, Pageable page);
    Page<Subscribe> findByNamaContainingIgnoreCaseAndKelasAndStatusAndSumberNotAndJenjangOrderByTglInsertDesc(String nama,String kelas, String status,String sumber,Jenjang jenjang, Pageable page);
    Page<Subscribe> findByNamaContainingIgnoreCaseAndKelasAndTahunAjaranAndStatusContainingIgnoreCaseAndSumberNotAndJenjangOrderByTglInsertDesc(String nama,String kelas, TahunAjaran tahun, String status,String sumber,Jenjang jenjang, Pageable page);

    Page<Subscribe> findByFollowupAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(Followup user,String Kelas,String sumber, String Status, String st,Jenjang jenjang, Pageable page);
    Page<Subscribe> findByFollowupAndKelasAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(Followup user,String Kelas,String sumber,String sumber2, String Status, String st,Jenjang jenjang, Pageable page);
    Page<Subscribe> findByFollowupAndKelasAndStatusAndSumberNotAndJenjangOrderByTglInsertDesc(Followup user,String Kelas, String status,String sumber,Jenjang jenjang, Pageable page);
    Page<Subscribe> findByFollowupAndKelasAndTahunAjaranAndStatusContainingIgnoreCaseAndSumberNotAndJenjangOrderByTglInsertDesc(Followup user,String Kelas, TahunAjaran tahun, String status,String sumber,Jenjang jenjang ,Pageable page);
    Page<Subscribe> findByKelasAndTahunAjaranAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(String kelas, TahunAjaran tahun, String sumber, String Status, String st, Jenjang jenjang, Pageable pageable);
    Page<Subscribe> findByKelasAndTahunAjaranAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(String kelas, TahunAjaran tahun, String sumber,String sumber2, String Status, String st, Jenjang jenjang, Pageable pageable);
    Page<Subscribe>findByKelasAndStatusAndSumberNotAndJenjangOrderByTglInsertDesc(String kelas, String status,String sumber,Jenjang jenjang,Pageable pageable);
    Page<Subscribe>findByKelasAndTahunAjaranAndStatusContainingIgnoreCaseAndSumberNotAndJenjangOrderByTglInsertDesc(String kelas, TahunAjaran tahun, String status,String sumber,Jenjang jenjang,Pageable pageable);
    Iterable<Subscribe>findByKelasAndSumberNotAndJenjangAndTahunAjaranIdOrderByTglInsertDesc(String kelas, String sumber, Jenjang jenjang,String id);

    Iterable<Subscribe>findBySumberNotAndJenjangOrderByTglInsertDesc(String sumber, Jenjang jenjang);

    @Query("select s from Subscribe s where (lower(s.nama) = :nama) or (lower(s.email) = :email ) and s.kelas = :kelas group by s.nama")
    Subscribe cekNama (String nama, String email, String kelas);

    Long countLeadsByTahunAjaranAndKelasAndSumberNotAndJenjang(TahunAjaran th, String kelas, String sum, Jenjang jenjang);

    Long countLeadsByTahunAjaranAndSumberNotAndJenjang(TahunAjaran th, String sum, Jenjang jenjang);

    Iterable<Subscribe> findByKelasAndSumberNotAndTglInsertBetweenOrderByTglInsert(String kelas,String sumber, LocalDateTime ld1, LocalDateTime ld2);
    Iterable<Subscribe> findBySumberNotAndJenjangAndTglInsertBetweenOrderByTglInsert(String sumber, Jenjang jenjang, LocalDateTime ld1, LocalDateTime ld2);
    Iterable<Subscribe> findByKelasAndStatusAndSumberNotOrderByTglInsert(String kelas, String st, String sumber);

    Subscribe findByEmail(String username);

    Subscribe findByNamaAndEmailAndNoWaAndJenjangAndTahunAjaran(String nama, String email, String noWa, Jenjang jenjang, TahunAjaran tahunAjaran);

    Iterable<Subscribe> findByTahunAjaran(TahunAjaran ta);
}

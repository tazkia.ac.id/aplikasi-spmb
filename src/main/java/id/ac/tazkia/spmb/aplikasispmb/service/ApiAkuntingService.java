package id.ac.tazkia.spmb.aplikasispmb.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.tazkia.spmb.aplikasispmb.constants.ZahirExport;
import id.ac.tazkia.spmb.aplikasispmb.dao.PembayaranDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.TagihanDao;
import id.ac.tazkia.spmb.aplikasispmb.dto.akunting.SendPembayaranDto;
import id.ac.tazkia.spmb.aplikasispmb.dto.akunting.SendTagihanDto;
import id.ac.tazkia.spmb.aplikasispmb.entity.Jenjang;
import id.ac.tazkia.spmb.aplikasispmb.entity.Pembayaran;
import id.ac.tazkia.spmb.aplikasispmb.entity.Tagihan;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.websocket.servlet.TomcatWebSocketServletWebServerCustomizer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ApiAkuntingService {

    private static final String TAGIHAN_PENDAFTARAN = "001";
    private static final String TAGIHAN_UANGPANGKAL = "002";

    @Value("${akuntansi.baseurl}") private String baseUrl;

    @Value("${template.journal.pendaftaran}") private String templatePendaftaran;
    @Value("${template.journal.uangpangkal.s1.tagihan}") private String templateTagihanS1;
    @Value("${template.journal.uangpangkal.s1.pembayaran}") private String templatePembayaranS1;
    @Value("${template.journal.uangpangkal.s2.tagihan}") private String templateTagihanS2;
    @Value("${template.journal.uangpangkal.s2.pembayaran}") private String templatePembayaranS2;
    @Value("${template.journal.pembayaran.yayasan}") private String templatePembayaranYayasan;

    @Autowired private ObjectMapper objectMapper;

    @Autowired private TagihanDao tagihanDao;

    @Autowired private PembayaranDao pembayaranDao;

    WebClient webClient = WebClient.builder().baseUrl(baseUrl).defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE).build();

    private boolean sendTagihan(SendTagihanDto tagihanDto) throws JsonProcessingException {
        String request = objectMapper.writeValueAsString(tagihanDto);
        log.debug("request: {}", request);

        try {
            SendTagihanDto response = webClient.post()
                    .uri(baseUrl + "journal")
                    .contentType(MediaType.APPLICATION_JSON)
                    .bodyValue(request)
                    .retrieve()
                    .bodyToMono(SendTagihanDto.class)
                    .block();

            return response != null;
        }catch (Exception e){
            log.error(e.getMessage());
            return false;
        }
    }

    private boolean sendPembayaran(SendPembayaranDto pembayaranDto) throws JsonProcessingException {

        String request = objectMapper.writeValueAsString(pembayaranDto);
        log.debug("request: {}", request);
        try {
            SendPembayaranDto response = webClient.post()
                    .uri(baseUrl + "journal-array")
                    .contentType(MediaType.APPLICATION_JSON)
                    .bodyValue(request)
                    .retrieve()
                    .bodyToMono(SendPembayaranDto.class)
                    .block();

            return response != null;
        }catch (Exception e){
            log.error(e.getMessage());
            return false;
        }
    }

    @Scheduled(cron = "${scheduled.tagihan.akunting}", zone = "Asia/Jakarta")
    public void kirimTagihan() throws JsonProcessingException {
        List<Tagihan> cekTagihan = tagihanDao.findByLunasAndZahirExportAndTanggalTagihanGreaterThanEqual(Boolean.TRUE, ZahirExport.NEW, LocalDate.parse("2024-02-01"));
        if (!cekTagihan.isEmpty()) {
            for (Tagihan tagihan : cekTagihan) {
                Pembayaran cekPembayaran = pembayaranDao.findByTagihanAndZahirExport(tagihan, ZahirExport.NEW);
                if (cekPembayaran != null) {
                    if (TAGIHAN_PENDAFTARAN.equalsIgnoreCase(tagihan.getJenisBiaya().getId())) {
                        tagihan.setZahirExport(ZahirExport.SENT);
                        tagihanDao.save(tagihan);
                    }
                    if (TAGIHAN_UANGPANGKAL.equalsIgnoreCase(tagihan.getJenisBiaya().getId())) {
                        if (Jenjang.S1.equals(tagihan.getPendaftar().getLeads().getJenjang())) {
                            SendTagihanDto tagihanDto = new SendTagihanDto();
                            tagihanDto.setCodeTemplate(templateTagihanS1);
                            tagihanDto.setDateTransaction(String.valueOf(tagihan.getTanggalTagihan()));
                            tagihanDto.setDescription("Tagihan " + tagihan.getJenisBiaya().getNama() + " a.n " + tagihan.getPendaftar().getLeads().getNama() + " " + tagihan.getPendaftar().getNomorRegistrasi());
                            tagihanDto.setTags("SPMB");
                            tagihanDto.setAmounts(tagihan.getNilai());
                            if (sendTagihan(tagihanDto)) {
                                tagihan.setZahirExport(ZahirExport.SENT);
                                tagihanDao.save(tagihan);
                                log.info("Mengirim {}", tagihanDto);
                            }else{
                                log.error("Gagal mengirim tagihan ke akuting.");
                            }
                        } else if (Jenjang.S2.equals(tagihan.getPendaftar().getLeads().getJenjang())) {
                            SendTagihanDto tagihanDto = new SendTagihanDto();
                            tagihanDto.setCodeTemplate(templateTagihanS2);
                            tagihanDto.setDateTransaction(String.valueOf(tagihan.getTanggalTagihan()));
                            tagihanDto.setDescription("Tagihan " + tagihan.getJenisBiaya().getNama() + " a.n " + tagihan.getPendaftar().getLeads().getNama() + " " + tagihan.getPendaftar().getNomorRegistrasi());
                            tagihanDto.setTags("SPMB");
                            tagihanDto.setAmounts(tagihan.getNilai());
                            if (sendTagihan(tagihanDto)) {
                                tagihan.setZahirExport(ZahirExport.SENT);
                                tagihanDao.save(tagihan);
                                log.info("Mengirim {}", tagihanDto);
                            }else{
                                log.error("Gagal mengirim tagihan ke akuting.");
                            }
                        }
                    }
                }
            }
        }
    }

    @Scheduled(cron = "${scheduled.pembayaran.akunting}", zone = "Asia/Jakarta")
    public void pembayaranAkunting() throws JsonProcessingException {
        List<Pembayaran> cekPembayaran = pembayaranDao.findByTagihanZahirExportAndZahirExport(ZahirExport.SENT, ZahirExport.NEW);
        if (!cekPembayaran.isEmpty()) {
            for (Pembayaran p : cekPembayaran){
                log.info("tgl pembayaran sebelum update : {}", p.getWaktuPembayaran());
                if (TAGIHAN_PENDAFTARAN.equalsIgnoreCase(p.getTagihan().getJenisBiaya().getId())) {
                    SendPembayaranDto pembayaranDto = new SendPembayaranDto();
                    pembayaranDto.setCodeTemplate(templatePendaftaran);
                    pembayaranDto.setDateTransaction(String.valueOf(p.getWaktuPembayaran().toLocalDate()));
                    pembayaranDto.setDescription("Pembayaran " + p.getTagihan().getJenisBiaya().getNama() + " a.n " + p.getTagihan().getPendaftar().getLeads().getNama() + " - " + p.getTagihan().getPendaftar().getNomorRegistrasi());
                    pembayaranDto.setTags("SPMB");
                    pembayaranDto.setAmounts(setPembayaranAkunting(p, "INST"));

                    SendPembayaranDto pembayaranYysDto = new SendPembayaranDto();
                    pembayaranYysDto.setCodeTemplate(templatePembayaranYayasan);
                    pembayaranYysDto.setDateTransaction(String.valueOf(p.getWaktuPembayaran().toLocalDate()));
                    pembayaranYysDto.setDescription("Pembayaran " + p.getTagihan().getJenisBiaya().getNama() + " a.n " + p.getTagihan().getPendaftar().getLeads().getNama() + " - " + p.getTagihan().getPendaftar().getNomorRegistrasi());
                    pembayaranYysDto.setTags("SPMB");
                    pembayaranYysDto.setAmounts(setPembayaranAkunting(p, "YYSN"));

                    if (sendPembayaran(pembayaranDto) && sendPembayaran(pembayaranYysDto)) {
                        p.setZahirExport(ZahirExport.SENT);
                        pembayaranDao.save(p);
                        log.info("Mengirim {}", pembayaranDto);
                    }else{
                        log.error("Gagal mengirim tagihan ke akuting.");
                    }
                }
                if (TAGIHAN_UANGPANGKAL.equalsIgnoreCase(p.getTagihan().getJenisBiaya().getId())) {
                    if (Jenjang.S1.equals(p.getTagihan().getPendaftar().getLeads().getJenjang())) {
                        SendPembayaranDto pembayaranDto = new SendPembayaranDto();
                        pembayaranDto.setCodeTemplate(templatePembayaranS1);
                        pembayaranDto.setDateTransaction(String.valueOf(p.getWaktuPembayaran().toLocalDate()));
                        pembayaranDto.setDescription("Pembayaran " + p.getTagihan().getJenisBiaya().getNama() + " a.n " + p.getTagihan().getPendaftar().getLeads().getNama() + " - " + p.getTagihan().getPendaftar().getNomorRegistrasi());
                        pembayaranDto.setTags("SPMB");
                        pembayaranDto.setAmounts(setPembayaranAkunting(p, "INST"));

                        SendPembayaranDto pembayaranYysDto = new SendPembayaranDto();
                        pembayaranYysDto.setCodeTemplate(templatePembayaranYayasan);
                        pembayaranYysDto.setDateTransaction(String.valueOf(p.getWaktuPembayaran().toLocalDate()));
                        pembayaranYysDto.setDescription("Pembayaran " + p.getTagihan().getJenisBiaya().getNama() + " a.n " + p.getTagihan().getPendaftar().getLeads().getNama() + " - " + p.getTagihan().getPendaftar().getNomorRegistrasi());
                        pembayaranYysDto.setTags("SPMB");
                        pembayaranYysDto.setAmounts(setPembayaranAkunting(p, "YYSN"));
                        if (sendPembayaran(pembayaranDto) && sendPembayaran(pembayaranYysDto)) {
                            p.setZahirExport(ZahirExport.SENT);
                            pembayaranDao.save(p);
                            log.info("Mengirim {}", pembayaranDto);
                        }else{
                            log.error("Gagal mengirim tagihan ke akunting");
                        }
                    } else if (Jenjang.S2.equals(p.getTagihan().getPendaftar().getLeads().getJenjang())) {
                        SendPembayaranDto pembayaranDto = new SendPembayaranDto();
                        pembayaranDto.setCodeTemplate(templatePembayaranS2);
                        pembayaranDto.setDateTransaction(String.valueOf(p.getWaktuPembayaran().toLocalDate()));
                        pembayaranDto.setDescription("Pembayaran " + p.getTagihan().getJenisBiaya().getNama() + " a.n " + p.getTagihan().getPendaftar().getLeads().getNama() + " - " + p.getTagihan().getPendaftar().getNomorRegistrasi());
                        pembayaranDto.setTags("SPMB");
                        pembayaranDto.setAmounts(setPembayaranAkunting(p, "INST"));

                        SendPembayaranDto pembayaranYysDto = new SendPembayaranDto();
                        pembayaranYysDto.setCodeTemplate(templatePembayaranYayasan);
                        pembayaranYysDto.setDateTransaction(String.valueOf(p.getWaktuPembayaran().toLocalDate()));
                        pembayaranYysDto.setDescription("Pembayaran " + p.getTagihan().getJenisBiaya().getNama() + " a.n " + p.getTagihan().getPendaftar().getLeads().getNama() + " - " + p.getTagihan().getPendaftar().getNomorRegistrasi());
                        pembayaranYysDto.setTags("SPMB");
                        pembayaranYysDto.setAmounts(setPembayaranAkunting(p, "YYSN"));
                        if (sendPembayaran(pembayaranDto) && sendPembayaran(pembayaranYysDto)) {
                            p.setZahirExport(ZahirExport.SENT);
                            pembayaranDao.save(p);
                            log.info("Mengirim {}", pembayaranDto);
                        }else{
                            log.error("Gagal mengirim tagihan ke akunting");
                        }
                    }
                }
            }
        }
    }

    private ArrayList<BigDecimal> setPembayaranAkunting(Pembayaran p, String untuk){

        ArrayList<BigDecimal> pembayaran = new ArrayList<>();

        if ("YYSN".equals(untuk)) {
            switch (p.getCaraPembayaran()){
                case TUNAI, TRANSFER:
                    pembayaran.add(p.getNilai());
                    pembayaran.add(BigDecimal.ZERO);
                    pembayaran.add(p.getNilai());
                    break;
                case VIRTUAL_ACCOUNT:
                    pembayaran.add(p.getNilai().subtract(new BigDecimal(2000)));
                    pembayaran.add(new BigDecimal(2000));
                    pembayaran.add(p.getNilai());
                    break;
                case BEASISWA:
                    pembayaran.add(BigDecimal.ZERO);
                    pembayaran.add(BigDecimal.ZERO);
                    pembayaran.add(BigDecimal.ZERO);
                    break;
                default:
                    pembayaran.add(p.getNilai());
                    pembayaran.add(BigDecimal.ZERO);
                    pembayaran.add(p.getNilai());
                    break;
            }
        }else{
            pembayaran.add(p.getNilai());
            pembayaran.add(p.getNilai());
        }

        return pembayaran;
    }

}

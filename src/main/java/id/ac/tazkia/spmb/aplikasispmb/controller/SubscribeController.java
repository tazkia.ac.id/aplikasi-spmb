package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.constants.AppConstants;
import id.ac.tazkia.spmb.aplikasispmb.constants.StatusRecord;
import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.dto.UploadError;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class SubscribeController {
    private static final Logger logger = LoggerFactory.getLogger(SubscribeController.class);


    @Autowired private SubscribeDao subscribeDao;
    @Autowired private FollowupDao followupDao;
    @Autowired private UserDao userDao;
    @Autowired private TahunAjaranDao tahunAjaranDao;
    @Autowired private TableUserDao tableUserDao;
    @Autowired private InstitutDao institutDao;

    @Value("${upload.folder.sertifikat}")
    private String uploadFolder;
    @GetMapping("/subscribe/selesai")
    public void selesaiSubscribe(){}

    @GetMapping("/subscribe/list")
    public void listSubscribe(@RequestParam(required = false)String nama, @RequestParam(required = false) String tahun, String flw,String sumber, Model m,
                             Pageable page, Authentication authentication) {

        String username = ((UserDetails) authentication.getPrincipal()).getUsername();
        User userEdu = userDao.findByUsername(username);
        if (AppConstants.ROLE_EDU.equals(userEdu.getRole().getId())) {
            Followup fu = followupDao.findByUser(userEdu);
            flw = fu.getId();
            m.addAttribute("UserEdu", flw);
        }

        if(StringUtils.hasText(nama) && StringUtils.hasText(flw) && StringUtils.hasText(tahun)) {
            m.addAttribute("nama", nama);
            m.addAttribute("flw", flw);
            m.addAttribute("tahun", tahun);
            m.addAttribute("daftarSc", subscribeDao.findByNamaAndFollowupIdAndTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(nama, flw, tahun,"Reguler","spmb.tazkia.ac.id","Relevant - Sudah Registrasi","Not Relevant",Jenjang.S1, page));
        } else if (StringUtils.hasText(flw) && StringUtils.hasText(tahun) && !StringUtils.hasText(nama) ){
            m.addAttribute("flw", flw);
            m.addAttribute("tahun", tahun);
            m.addAttribute("daftarSc", subscribeDao.findByFollowupIdAndTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(flw, tahun,"Reguler","spmb.tazkia.ac.id","Relevant - Sudah Registrasi","Not Relevant",Jenjang.S1 ,page));
        }else if (StringUtils.hasText(nama) && StringUtils.hasText(flw) && !StringUtils.hasText(tahun) && !StringUtils.hasText(sumber)) {
            m.addAttribute("nama", nama);
            m.addAttribute("flw", flw);
            m.addAttribute("daftarSc", subscribeDao.findByNamaContainingIgnoreCaseAndFollowupIdContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(nama, flw,"Reguler","spmb.tazkia.ac.id","Relevant - Sudah Registrasi", "Not Relevant",Jenjang.S1,page));
        }else if (StringUtils.hasText(nama)  && StringUtils.hasText(tahun) && !StringUtils.hasText(flw) && !StringUtils.hasText(sumber)) {
            m.addAttribute("nama", nama);
            m.addAttribute("tahun", tahun);
            m.addAttribute("daftarSc", subscribeDao.findByNamaAndTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(nama, tahun,"Reguler","spmb.tazkia.ac.id","Relevant - Sudah Registrasi","Not Relevant",Jenjang.S1,page));
        }else if (StringUtils.hasText(nama) && !StringUtils.hasText(flw) && !StringUtils.hasText(tahun) && !StringUtils.hasText(sumber)) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarSc", subscribeDao.findByNamaContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(nama,"Reguler","spmb.tazkia.ac.id","Relevant - Sudah Registrasi","Not Relevant",Jenjang.S1,page));
        }else if (StringUtils.hasText(flw) && !StringUtils.hasText(nama) &&  !StringUtils.hasText(tahun) && !StringUtils.hasText(sumber)) {
            m.addAttribute("flw", flw);
            Followup fl = followupDao.findById(flw).get();
            m.addAttribute("daftarSc", subscribeDao.findByFollowupAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(fl,"Reguler","spmb.tazkia.ac.id","Relevant - Sudah Registrasi","Not Relevant",Jenjang.S1,page));
        }else if (StringUtils.hasText(tahun) && !StringUtils.hasText(nama) && !StringUtils.hasText(flw) && !StringUtils.hasText(sumber)) {
            m.addAttribute("tahun", tahun);
            m.addAttribute("daftarSc", subscribeDao.findByTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(tahun, "Reguler", "spmb.tazkia.ac.id", "Relevant - Sudah Registrasi","Not Relevant",Jenjang.S1, page));
        }else if (!StringUtils.hasText(tahun) && !StringUtils.hasText(nama) && StringUtils.hasText(flw) && StringUtils.hasText(sumber)) {
            m.addAttribute("sumber", sumber);
            m.addAttribute("flw", flw);
            m.addAttribute("daftarSc", subscribeDao.findBySumberContainingIgnoreCaseAndFollowupIdAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(sumber, flw,"Reguler","spmb.tazkia.ac.id","Relevant - Sudah Registrasi", "Not Relevant",Jenjang.S1,page));
        }else if (!StringUtils.hasText(tahun) && StringUtils.hasText(nama) && StringUtils.hasText(flw) && StringUtils.hasText(sumber)) {
            m.addAttribute("sumber", sumber);
            m.addAttribute("nama", nama);
            m.addAttribute("flw", flw);
            m.addAttribute("daftarSc", subscribeDao.findBySumberContainingIgnoreCaseAndNamaContainingIgnoreCaseAndFollowupIdAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(sumber,nama, flw,"Reguler","spmb.tazkia.ac.id","Relevant - Sudah Registrasi", "Not Relevant",Jenjang.S1,page));
        }else if (!StringUtils.hasText(tahun) && !StringUtils.hasText(nama) && !StringUtils.hasText(flw) && StringUtils.hasText(sumber)) {
            m.addAttribute("sumber", sumber);
            m.addAttribute("daftarSc", subscribeDao.findBySumberContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(sumber, "Reguler", "spmb.tazkia.ac.id", "Relevant - Sudah Registrasi","Not Relevant",Jenjang.S1, page));
        } else {
            TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);
            m.addAttribute("daftarSc", subscribeDao.findByKelasAndTahunAjaranAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc
                    ("Reguler",tahunAjaran,"spmb.tazkia.ac.id","Relevant - Sudah Registrasi", "Not Relevant",Jenjang.S1,page));
        }

        Iterable<Followup> fu = followupDao.findByStatus(Status.AKTIF);
        m.addAttribute("listFu", fu);

        Iterable<Subscribe> sc = subscribeDao.findAll();
        m.addAttribute("subscribe", sc);

        User user = userDao.findById("System").get();
        m.addAttribute("userUpdate", user);

        Iterable<TahunAjaran> taj = tahunAjaranDao.findAll();
        m.addAttribute("tahunAjaran", taj);

        Iterable<TableUser> tb = tableUserDao.findByStatus(StatusRecord.FINISH);
        m.addAttribute("user", tb);
    }

//list ADS
    @GetMapping("/subscribe/listAds")
    public void listSubscribeAds(@RequestParam(required = false)String nama, @RequestParam(required = false) String tahun, String flw,String sumber, Model m,
                              Pageable page) {

        if(StringUtils.hasText(nama) && StringUtils.hasText(flw) && StringUtils.hasText(tahun)) {
            m.addAttribute("nama", nama);
            m.addAttribute("flw", flw);
            m.addAttribute("tahun", tahun);
            m.addAttribute("daftarSc", subscribeDao.findByNamaAndFollowupIdAndTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(nama, flw, tahun,"Reguler","spmb.tazkia.ac.id","ADS","Relevant - Sudah Registrasi","Not Relevant",Jenjang.S1, page));
        } else if (StringUtils.hasText(flw) && StringUtils.hasText(tahun) && !StringUtils.hasText(nama) ){
            m.addAttribute("flw", flw);
            m.addAttribute("tahun", tahun);
            m.addAttribute("daftarSc", subscribeDao.findByFollowupIdAndTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(flw, tahun,"Reguler","spmb.tazkia.ac.id","ADS","Relevant - Sudah Registrasi","Not Relevant",Jenjang.S1 ,page));
        }else if (StringUtils.hasText(nama) && StringUtils.hasText(flw) && !StringUtils.hasText(tahun) && !StringUtils.hasText(sumber)) {
            m.addAttribute("nama", nama);
            m.addAttribute("flw", flw);
            m.addAttribute("daftarSc", subscribeDao.findByNamaAndFollowupIdContainingIgnoreCaseAndKelasAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(nama, flw,"Reguler","spmb.tazkia.ac.id","ADS","Relevant - Sudah Registrasi", "Not Relevant",Jenjang.S1,page));
        }else if (StringUtils.hasText(nama)  && StringUtils.hasText(tahun) && !StringUtils.hasText(flw) && !StringUtils.hasText(sumber)) {
            m.addAttribute("nama", nama);
            m.addAttribute("tahun", tahun);
            m.addAttribute("daftarSc", subscribeDao.findByNamaAndTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(nama, tahun,"Reguler","spmb.tazkia.ac.id","ADS","Relevant - Sudah Registrasi","Not Relevant",Jenjang.S1,page));
        }else if (StringUtils.hasText(nama) && !StringUtils.hasText(flw) && !StringUtils.hasText(tahun) && !StringUtils.hasText(sumber)) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarSc", subscribeDao.findByNamaContainingIgnoreCaseAndKelasAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(nama,"Reguler","spmb.tazkia.ac.id","ADS","Relevant - Sudah Registrasi","Not Relevant",Jenjang.S1,page));
        }else if (StringUtils.hasText(flw) && !StringUtils.hasText(nama) &&  !StringUtils.hasText(tahun) && !StringUtils.hasText(sumber)) {
            m.addAttribute("flw", flw);
            Followup fl = followupDao.findById(flw).get();
            m.addAttribute("daftarSc", subscribeDao.findByFollowupAndKelasAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(fl,"Reguler","spmb.tazkia.ac.id","ADS","Relevant - Sudah Registrasi","Not Relevant",Jenjang.S1,page));
        }else if (StringUtils.hasText(tahun) && !StringUtils.hasText(nama) && !StringUtils.hasText(flw) && !StringUtils.hasText(sumber)) {
            m.addAttribute("tahun", tahun);
            m.addAttribute("daftarSc", subscribeDao.findByTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(tahun, "Reguler", "spmb.tazkia.ac.id","ADS", "Relevant - Sudah Registrasi","Not Relevant",Jenjang.S1, page));
        }else if (!StringUtils.hasText(tahun) && !StringUtils.hasText(nama) && StringUtils.hasText(flw) && StringUtils.hasText(sumber)) {
            m.addAttribute("sumber", sumber);
            m.addAttribute("flw", flw);
            m.addAttribute("daftarSc", subscribeDao.findBySumberContainingIgnoreCaseAndFollowupIdAndKelasAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(sumber, flw,"Reguler","spmb.tazkia.ac.id","ADS","Relevant - Sudah Registrasi", "Not Relevant",Jenjang.S1,page));
        }else if (!StringUtils.hasText(tahun) && StringUtils.hasText(nama) && StringUtils.hasText(flw) && StringUtils.hasText(sumber)) {
            m.addAttribute("sumber", sumber);
            m.addAttribute("nama", nama);
            m.addAttribute("flw", flw);
            m.addAttribute("daftarSc", subscribeDao.findBySumberContainingIgnoreCaseAndNamaContainingIgnoreCaseAndFollowupIdAndKelasAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(sumber,nama, flw,"Reguler","spmb.tazkia.ac.id","ADS","Relevant - Sudah Registrasi", "Not Relevant",Jenjang.S1,page));
        }else if (!StringUtils.hasText(tahun) && !StringUtils.hasText(nama) && !StringUtils.hasText(flw) && StringUtils.hasText(sumber)) {
            m.addAttribute("sumber", sumber);
            m.addAttribute("daftarSc", subscribeDao.findBySumberContainingIgnoreCaseAndKelasAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(sumber, "Reguler", "spmb.tazkia.ac.id", "ADS","Relevant - Sudah Registrasi","Not Relevant",Jenjang.S1, page));
        } else {
            TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);
            m.addAttribute("daftarSc", subscribeDao.findByKelasAndTahunAjaranAndSumberNotAndSumberContainingIgnoreCaseAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc
                    ("Reguler",tahunAjaran,"spmb.tazkia.ac.id","ADS","Relevant - Sudah Registrasi", "Not Relevant",Jenjang.S1,page));
        }

        Iterable<Followup> fu = followupDao.findByStatus(Status.AKTIF);
        m.addAttribute("listFu", fu);

        Iterable<Subscribe> sc = subscribeDao.findAll();
        m.addAttribute("subscribe", sc);

        Iterable<TahunAjaran> taj = tahunAjaranDao.findAll();
        m.addAttribute("tahunAjaran", taj);

        Iterable<TableUser> tb = tableUserDao.findByStatus(StatusRecord.FINISH);
        m.addAttribute("user", tb);
    }

    @GetMapping("/subscribe/listKaryawan")
    public void listSubscribeKaryawan(@RequestParam(required = false)String nama, @RequestParam(required = false) String tahun, String user, Model m,
                                      Pageable page, Authentication authentication) {

        String username = ((UserDetails) authentication.getPrincipal()).getUsername();
        User userEdu = userDao.findByUsername(username);
        if (AppConstants.ROLE_EDU.equals(userEdu.getRole().getId())) {
            Followup fu = followupDao.findByUser(userEdu);
            user = fu.getId();
            m.addAttribute("UserEdu", user);
        }

        if(StringUtils.hasText(nama) && StringUtils.hasText(user) && StringUtils.hasText(tahun)) {
            m.addAttribute("nama", nama);
            m.addAttribute("user", user);
            m.addAttribute("tahun", tahun);
            m.addAttribute("daftarSc", subscribeDao.findByNamaAndFollowupIdAndTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(nama, user, tahun,"Karyawan","spmb.tazkia.ac.id","Relevant - Sudah Registrasi","Not Relevant", Jenjang.S1 ,page));
        } else if (StringUtils.hasText(user) && StringUtils.hasText(tahun) && !StringUtils.hasText(nama) ){
            m.addAttribute("user", user);
            m.addAttribute("tahun", tahun);
            m.addAttribute("daftarSc", subscribeDao.findByFollowupIdAndTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(user, tahun,"Karyawan","spmb.tazkia.ac.id","Relevant - Sudah Registrasi","Not Relevant", Jenjang.S1,page));
        }else if (StringUtils.hasText(nama) && StringUtils.hasText(user) && !StringUtils.hasText(tahun)) {
            m.addAttribute("nama", nama);
            m.addAttribute("user", user);
            m.addAttribute("daftarSc", subscribeDao.findByNamaContainingIgnoreCaseAndFollowupIdContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(nama, user,"Karyawan","spmb.tazkia.ac.id","Relevant - Sudah Registrasi","Not Relevant", Jenjang.S1,page));
        }else if (StringUtils.hasText(nama)  && StringUtils.hasText(tahun) && !StringUtils.hasText(user)) {
            m.addAttribute("nama", nama);
            m.addAttribute("tahun", tahun);
            m.addAttribute("daftarSc", subscribeDao.findByNamaAndTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(nama, tahun,"Karyawan","spmb.tazkia.ac.id","Relevant - Sudah Registrasi","Not Relevant",Jenjang.S1,page));
        }else if (StringUtils.hasText(nama) && !StringUtils.hasText(user) && !StringUtils.hasText(tahun)) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarSc", subscribeDao.findByNamaContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(nama,"Karyawan","spmb.tazkia.ac.id","Relevant - Sudah Registrasi","Not Relevant",Jenjang.S1,page));
        }else if (StringUtils.hasText(user) && !StringUtils.hasText(nama) &&  !StringUtils.hasText(tahun)) {
            m.addAttribute("user", user);
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarSc", subscribeDao.findByFollowupAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(fl,"Karyawan","spmb.tazkia.ac.id","Relevant - Sudah Registrasi","Not Relevant",Jenjang.S1,page));
        }else if (StringUtils.hasText(tahun) && !StringUtils.hasText(nama) && !StringUtils.hasText(user)) {
            m.addAttribute("tahun", tahun);
            m.addAttribute("daftarSc", subscribeDao.findByTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(tahun, "Karyawan", "spmb.tazkia.ac.id", "Relevant - Sudah Registrasi", "Not Relevant",Jenjang.S1, page));
        } else {
            TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);
            m.addAttribute("daftarSc", subscribeDao.findByKelasAndTahunAjaranAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc
                    ("Karyawan",tahunAjaran,"spmb.tazkia.ac.id","Relevant - Sudah Registrasi","Not Relevant",Jenjang.S1,page));
        }

        Iterable<Followup> fu = followupDao.findByStatus(Status.AKTIF);
        m.addAttribute("listFu", fu);

        Iterable<Subscribe> sc = subscribeDao.findAll();
        m.addAttribute("subscribe", sc);

        Iterable<TahunAjaran> taj = tahunAjaranDao.findAll();
        m.addAttribute("tahunAjaran", taj);

    }

    @RequestMapping(value = "/subscribe/update", method = RequestMethod.POST)
    public String  updateSubscribe(@RequestParam(required = false) Subscribe id, String status
                                    ,Authentication currentUser){
        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }

        Subscribe subscribe = subscribeDao.findById(id.getId()).get();
        subscribe.setStatus(status);
        subscribe.setTglUpdate(LocalDate.now());
        subscribe.setUserUpdate(u);
        subscribeDao.save(subscribe);
        if (subscribe.getKelas().equals("Reguler")) {
            return "redirect:list";
        }else {
            return "redirect:listKaryawan";
        }

    }

    @PostMapping("/subscribe/followup")
    public RedirectView updateFollowup(@RequestParam(required = false) Subscribe id, Authentication currentUser, Model model) {
        String username = ((UserDetails) currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if (u == null) {
            logger.warn("Username {} not found in database ", username);
        }

        Subscribe subscribe = subscribeDao.findById(id.getId()).get();
        if (subscribe.getHubungi() == null) {
            subscribe.setHubungi(1);
        } else {
            subscribe.setHubungi(subscribe.getHubungi() + 1);
        }
        subscribe.setUserUpdate(u);
        subscribe.setTglUpdate(LocalDate.now());
        subscribeDao.save(subscribe);

        String nomor = subscribe.getNoWa().replaceFirst("0", "62");

        String sumber = subscribe.getSumber();
        System.out.println("cek : " + subscribe.getSumber());
        if (sumber == null || sumber.isEmpty()) {
            sumber = "None";
        }

        String cekSumber = sumber.substring(0, 2);
        logger.info("Cek {} ", cekSumber);
        RedirectView redirectUrl = new RedirectView();
        //FD
        if (subscribe.getJenjang().equals(Jenjang.S1) &&subscribe.getKelas().equals("Reguler") && cekSumber.equals("FD")) {
            if (subscribe.getHubungi() <= 1) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20"+ subscribe.getNama() +"%20perkenalkan%20saya%20" + subscribe.getFollowup().getNama() + "%20Edu%20Consultant%20kampus%20Tazkia.%0AApakah%20kakak%20sedang%20berencana%20untuk%20melanjutkan%20ke%20perguruan%20tinggi.%0AKhusus%20hari%20ini%20aku%20bisa%20bantu%20kaka%20untuk%20mendapatkan%20potongan%20Biaya%20Formulir%20Pendaftaran%20apakah%20kakak%20tertarik?");
            } else if (subscribe.getHubungi() >= 1 && subscribe.getHubungi() < 3) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20" + subscribe.getNama() + "%0AKampus%20Tazkia%20Bogor%20merupakan%20kampus%20Pelopor%20Bisnis%20Digital%20Syariah%20berbasis%20syariah%20yang%20saat%20ini%20memiliki%208%20Program%20Studi.%0AUntuk%20lebih%20jelasnya%20Bolehkan%20"+ subscribe.getFollowup().getNama() +"%20Mengirimkan%20Video%20Profile%20Tentang%20Kampus%20Tazkia?");
            } else if (subscribe.getHubungi() >= 1) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20" + subscribe.getNama() + ",%20sudah%20dapat%20informasi%20terbaru%20tentang%20beasiswa%20kampus%20Tazkia%20?%20bisa%20klik%20di%20sini%20yah%20https://www.instagram.com/tazkiaofficial%20-%20https://www.tiktok.com/@kampustazkia");
            }
        //Presentasi
        } else if (subscribe.getJenjang().equals(Jenjang.S1) && subscribe.getKelas().equals("Reguler") && cekSumber.equals("Pr")) {
            if (subscribe.getHubungi() <= 1) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20"+ subscribe.getNama() +"%20perkenalkan%20saya%20" + subscribe.getFollowup().getNama() + "%20Edu%20Consultant%20kampus%20Tazkia.%0AApakah%20kakak%20sedang%20berencana%20untuk%20melanjutkan%20ke%20perguruan%20tinggi.%0AKhusus%20hari%20ini%20aku%20bisa%20bantu%20kaka%20untuk%20mendapatkan%20potongan%20Biaya%20Formulir%20Pendaftaran%20apakah%20kakak%20tertarik?");
            } else if (subscribe.getHubungi() >= 1 && subscribe.getHubungi() < 3) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20" + subscribe.getNama() + "%0AKampus%20Tazkia%20Bogor%20merupakan%20kampus%20Pelopor%20Bisnis%20Digital%20Syariah%20berbasis%20syariah%20yang%20saat%20ini%20memiliki%208%20Program%20Studi.%0AUntuk%20lebih%20jelasnya%20Bolehkan%20"+ subscribe.getFollowup().getNama() +"%20Mengirimkan%20Video%20Profile%20Tentang%20Kampus%20Tazkia?");
            } else if (subscribe.getHubungi() >= 1) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20" + subscribe.getNama() + ",%20sudah%20dapat%20informasi%20terbaru%20tentang%20beasiswa%20kampus%20Tazkia%20?%20bisa%20klik%20di%20sini%20yah%20https://www.instagram.com/tazkiaofficial%20-%20https://www.tiktok.com/@kampustazkia");
            }
        //Booth
        } else if (subscribe.getJenjang().equals(Jenjang.S1) && subscribe.getKelas().equals("Reguler") && cekSumber.equals("Bo")) {
            if (subscribe.getHubungi() <= 1) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20"+ subscribe.getNama() +"%20perkenalkan%20saya%20" + subscribe.getFollowup().getNama() + "%20Edu%20Consultant%20kampus%20Tazkia.%0AApakah%20kakak%20sedang%20berencana%20untuk%20melanjutkan%20ke%20perguruan%20tinggi.%0AKhusus%20hari%20ini%20aku%20bisa%20bantu%20kaka%20untuk%20mendapatkan%20potongan%20Biaya%20Formulir%20Pendaftaran%20apakah%20kakak%20tertarik?");
            } else if (subscribe.getHubungi() >= 1 && subscribe.getHubungi() < 3) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20" + subscribe.getNama() + "%0AKampus%20Tazkia%20Bogor%20merupakan%20kampus%20Pelopor%20Bisnis%20Digital%20Syariah%20berbasis%20syariah%20yang%20saat%20ini%20memiliki%208%20Program%20Studi.%0AUntuk%20lebih%20jelasnya%20Bolehkan%20"+ subscribe.getFollowup().getNama() +"%20Mengirimkan%20Video%20Profile%20Tentang%20Kampus%20Tazkia?");
            } else if (subscribe.getHubungi() >= 1) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20" + subscribe.getNama() + ",%20sudah%20dapat%20informasi%20terbaru%20tentang%20beasiswa%20kampus%20Tazkia%20?%20bisa%20klik%20di%20sini%20yah%20https://www.instagram.com/tazkiaofficial%20-%20https://www.tiktok.com/@kampustazkia");
            }
        //Data Sekolah
        } else if (subscribe.getJenjang().equals(Jenjang.S1) && subscribe.getKelas().equals("Reguler") && cekSumber.equals("Da")) {
            if (subscribe.getHubungi() <= 1) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20"+ subscribe.getNama() +"%20perkenalkan%20saya%20" + subscribe.getFollowup().getNama() + "%20Edu%20Consultant%20kampus%20Tazkia.%0AApakah%20kakak%20sedang%20berencana%20untuk%20melanjutkan%20ke%20perguruan%20tinggi.%0AKhusus%20hari%20ini%20aku%20bisa%20bantu%20kaka%20untuk%20mendapatkan%20potongan%20Biaya%20Formulir%20Pendaftaran%20apakah%20kakak%20tertarik?");
            } else if (subscribe.getHubungi() >= 1 && subscribe.getHubungi() < 3) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20" + subscribe.getNama() + "%0AKampus%20Tazkia%20Bogor%20merupakan%20kampus%20Pelopor%20Bisnis%20Digital%20Syariah%20berbasis%20syariah%20yang%20saat%20ini%20memiliki%208%20Program%20Studi.%0AUntuk%20lebih%20jelasnya%20Bolehkan%20"+ subscribe.getFollowup().getNama() +"%20Mengirimkan%20Video%20Profile%20Tentang%20Kampus%20Tazkia?");
            } else if (subscribe.getHubungi() >= 1) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20" + subscribe.getNama() + ",%20sudah%20dapat%20informasi%20terbaru%20tentang%20beasiswa%20kampus%20Tazkia%20?%20bisa%20klik%20di%20sini%20yah%20https://www.instagram.com/tazkiaofficial%20-%20https://www.tiktok.com/@kampustazkia");
            }
        //TEST TOEFL
        }else if (subscribe.getJenjang().equals(Jenjang.S1) && subscribe.getKelas().equals("Reguler") && cekSumber.equals("Te")) {
            if (subscribe.getHubungi() <= 1) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20"+ subscribe.getNama() +"%20perkenalkan%20saya%20" + subscribe.getFollowup().getNama() + "%20Edu%20Consultant%20kampus%20Tazkia.%0AApakah%20kakak%20sedang%20berencana%20untuk%20melanjutkan%20ke%20perguruan%20tinggi.%0AKhusus%20hari%20ini%20aku%20bisa%20bantu%20kaka%20untuk%20mendapatkan%20potongan%20Biaya%20Formulir%20Pendaftaran%20apakah%20kakak%20tertarik?");
            } else if (subscribe.getHubungi() >= 1 && subscribe.getHubungi() < 3) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20" + subscribe.getNama() + "%0AKampus%20Tazkia%20Bogor%20merupakan%20kampus%20Pelopor%20Bisnis%20Digital%20Syariah%20berbasis%20syariah%20yang%20saat%20ini%20memiliki%208%20Program%20Studi.%0AUntuk%20lebih%20jelasnya%20Bolehkan%20"+ subscribe.getFollowup().getNama() +"%20Mengirimkan%20Video%20Profile%20Tentang%20Kampus%20Tazkia?");
            } else if (subscribe.getHubungi() >= 1) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20" + subscribe.getNama() + ",%20sudah%20dapat%20informasi%20terbaru%20tentang%20beasiswa%20kampus%20Tazkia%20?%20bisa%20klik%20di%20sini%20yah%20https://www.instagram.com/tazkiaofficial%20-%20https://www.tiktok.com/@kampustazkia");
            }
        //Campus Tour
        }else if (subscribe.getJenjang().equals(Jenjang.S1) && subscribe.getKelas().equals("Reguler") && cekSumber.equals("Ca")) {
            if (subscribe.getHubungi() <= 1) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20"+ subscribe.getNama() +"%20perkenalkan%20saya%20" + subscribe.getFollowup().getNama() + "%20Edu%20Consultant%20kampus%20Tazkia.%0AApakah%20kakak%20sedang%20berencana%20untuk%20melanjutkan%20ke%20perguruan%20tinggi.%0AKhusus%20hari%20ini%20aku%20bisa%20bantu%20kaka%20untuk%20mendapatkan%20potongan%20Biaya%20Formulir%20Pendaftaran%20apakah%20kakak%20tertarik?");
            } else if (subscribe.getHubungi() >= 1 && subscribe.getHubungi() < 3) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20" + subscribe.getNama() + "%0AKampus%20Tazkia%20Bogor%20merupakan%20kampus%20Pelopor%20Bisnis%20Digital%20Syariah%20berbasis%20syariah%20yang%20saat%20ini%20memiliki%208%20Program%20Studi.%0AUntuk%20lebih%20jelasnya%20Bolehkan%20"+ subscribe.getFollowup().getNama() +"%20Mengirimkan%20Video%20Profile%20Tentang%20Kampus%20Tazkia?");
            } else if (subscribe.getHubungi() >= 1) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20" + subscribe.getNama() + ",%20sudah%20dapat%20informasi%20terbaru%20tentang%20beasiswa%20kampus%20Tazkia%20?%20bisa%20klik%20di%20sini%20yah%20https://www.instagram.com/tazkiaofficial%20-%20https://www.tiktok.com/@kampustazkia");
            }
        }else if (subscribe.getJenjang().equals(Jenjang.S1) && subscribe.getKelas().equals("Reguler") &&
                !cekSumber.equals("FD")  && !cekSumber.equals("Pr") && !cekSumber.equals("Bo")&& !cekSumber.equals("Da")&& !cekSumber.equals("Te") && !cekSumber.equals("Ca")) {
            if (subscribe.getHubungi() <= 1) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20"+ subscribe.getNama() +"%20perkenalkan%20saya%20" + subscribe.getFollowup().getNama() + "%20Edu%20Consultant%20kampus%20Tazkia.%0AApakah%20kakak%20sedang%20berencana%20untuk%20melanjutkan%20ke%20perguruan%20tinggi.%0AKhusus%20hari%20ini%20aku%20bisa%20bantu%20kaka%20untuk%20mendapatkan%20potongan%20Biaya%20Formulir%20Pendaftaran%20apakah%20kakak%20tertarik?");
            } else if (subscribe.getHubungi() >= 1 && subscribe.getHubungi() < 3) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20" + subscribe.getNama() + "%0AKampus%20Tazkia%20Bogor%20merupakan%20kampus%20Pelopor%20Bisnis%20Digital%20Syariah%20berbasis%20syariah%20yang%20saat%20ini%20memiliki%208%20Program%20Studi.%0AUntuk%20lebih%20jelasnya%20Bolehkan%20"+ subscribe.getFollowup().getNama() +"%20Mengirimkan%20Video%20Profile%20Tentang%20Kampus%20Tazkia?");
            } else if (subscribe.getHubungi() >= 1) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20" + subscribe.getNama() + ",%20sudah%20dapat%20informasi%20terbaru%20tentang%20beasiswa%20kampus%20Tazkia%20?%20bisa%20klik%20di%20sini%20yah%20https://www.instagram.com/tazkiaofficial%20-%20https://www.tiktok.com/@kampustazkia");
            }
        }else if (subscribe.getJenjang().equals(Jenjang.S1) && subscribe.getKelas().equals("Karyawan")) {
            if (subscribe.getHubungi().equals(1)){
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20"+ subscribe.getNama() +"%20perkenalkan%20saya%20" + subscribe.getFollowup().getNama() + "%20Edu%20Consultant%20kampus%20Tazkia.%0AApakah%20kakak%20sedang%20berencana%20untuk%20melanjutkan%20ke%20perguruan%20tinggi.%0AKhusus%20hari%20ini%20aku%20bisa%20bantu%20kaka%20untuk%20mendapatkan%20potongan%20Biaya%20Formulir%20Pendaftaran%20apakah%20kakak%20tertarik?");
            }else if (subscribe.getHubungi().equals(2)) {
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20" + subscribe.getNama() + "%0AKampus%20Tazkia%20Bogor%20merupakan%20kampus%20Pelopor%20Bisnis%20Digital%20Syariah%20berbasis%20syariah%20yang%20saat%20ini%20memiliki%208%20Program%20Studi.%0AUntuk%20lebih%20jelasnya%20Bolehkan%20"+ subscribe.getFollowup().getNama() +"%20Mengirimkan%20Video%20Profile%20Tentang%20Kampus%20Tazkia?");
            }else if (subscribe.getHubungi().equals(3)){
                redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20" + subscribe.getNama() + ",%20sudah%20dapat%20informasi%20terbaru%20tentang%20beasiswa%20kampus%20Tazkia%20?%20bisa%20klik%20di%20sini%20yah%20https://www.instagram.com/tazkiaofficial%20-%20https://www.tiktok.com/@kampustazkia");
            }else if (subscribe.getHubungi().equals(4)){
                if (subscribe.getLulusan().equals("Lulusan SMA / SMK")) {
                    redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20" + subscribe.getNama() + ",%20sudah%20dapat%20informasi%20terbaru%20tentang%20beasiswa%20kampus%20Tazkia%20?%20bisa%20klik%20di%20sini%20yah%20https://www.instagram.com/tazkiaofficial%20-%20https://www.tiktok.com/@kampustazkia");
                } else if (subscribe.getLulusan().equals("Lulusan D3 / D4")) {
                    redirectUrl.setUrl("https://wa.me/" + nomor + "?text=Assalamualaikum%20Kak%20" + subscribe.getNama() + ",%20sudah%20dapat%20informasi%20terbaru%20tentang%20beasiswa%20kampus%20Tazkia%20?%20bisa%20klik%20di%20sini%20yah%20https://www.instagram.com/tazkiaofficial%20-%20https://www.tiktok.com/@kampustazkia");
                }
            }

        } else if (subscribe.getJenjang().equals(Jenjang.S2)) {
            if (subscribe.getHubungi().equals(1)){
                redirectUrl.setUrl("https://wa.me/"+nomor+"?text=Assalamualaikum%20Bapak/Ibu%20" + subscribe.getNama() + "%20Perkenalkan%20nama%20saya%20"+ subscribe.getFollowup().getNama() +",%20Edu%20Consultant%20Pascasarjana%20Kampus%20Tazkia.%20Anda%20baru%20saja%20mengisi%20form%20informasi%20kami?%20Bulan%20ini%20kami%20sedang%20ada%20promo%20Potongan%20Biaya%20Pendaftaran%20dan%20Cicilan%20Biaya%20Kuliah.%20Hubungi%20saya%20untuk%20dapatkan%20informasi%20lengkapnya%20ya.");
            }else if (subscribe.getHubungi().equals(2)) {
                redirectUrl.setUrl("https://wa.me/"+nomor+"?text=Assalamualaikum%20Bapak/Ibu%20"+ subscribe.getNama() +",%20Anda%20pernah%20mengisi%20di%20form%20Pascasarjana%20Kampus%20Tazkia?%20Dengan%20mendaftarkan%20diri%20di%20bulan%20ini,%20Anda%20bisa%20mendapatkan%20Potongan%20Biaya%20Pendaftaran.%0AKeuntungan%20lainnya%20yang%20bisa%20didapat%20adalah%20biaya%20kuliah%20yang%20bisa%20dicicil%20di%20setiap%20bulannya.%20Jangan%20ragu%20untuk%20hubungi%20saya%20supaya%20benefit%20di%20atas%20bisa%20Anda%20dapatkan!");
            }else if (subscribe.getHubungi().equals(3)){
                redirectUrl.setUrl("https://wa.me/"+nomor+"?text=Assalamualaikum%20Bapak/Ibu%20"+ subscribe.getNama() +",%20sebentar%20lagi%20batas%20pendaftaran%20Program%20Pascasarjana%20Kampus%20Tazkia%20akan%20ditutup,%20lho!%20Jangan%20lewatkan%20potongan%20Biaya%20Pendaftaran%20Up%20To%2050%%20dan%20Biaya%20Kuliah%201,7jutaan%20per%20bulan%20hubungi%20saya%20"+ subscribe.getFollowup().getNama() +"%20untuk%20segera%20dibantu%20prosesnya.%0AJangan%20lupa%20save%20no%20aku%20ya.%0APendaftaran%20Klik%20link%20dibawah%20ini%0Ahttps://spmb.tazkia.ac.id/pascasarjana");
            }
        }
        return redirectUrl;
    }

    @PostMapping("/sarjana")
    public String prosesSubscribe(Subscribe subscribe, RedirectAttributes redirectAttributes)  {
        subscribe.setStatus("None");
        subscribe.setTglInsert(LocalDateTime.now());

        Institut institut = institutDao.findById("1").get();

        Followup followup = followupDao.cariUserFuS1(institut);
        Integer jumlah = Integer.valueOf(followup.getJumlah());
        Integer jmlh = jumlah + 1;
        followup.setJumlah(jmlh.toString());
        followupDao.save(followup);

        subscribe.setFollowup(followup);
        subscribe.setHubungi(0);
        subscribeDao.save(subscribe);
        redirectAttributes.addFlashAttribute("email", subscribe.getEmail());
        return "redirect:/subscribe/selesai";



//            return "redirect:/sarjana";
    }

    @GetMapping("/landingReguler/xlsx")
    public void rekappascaXlsx(@RequestParam(required = false) String  kelas, HttpServletResponse response) throws Exception {

        String[] columns = {"No","Nama","Email","No Telephone","Kota Asal Sekolah","Asal Sekolah","Sumber","User FU","Status","Tanggal Insert","Jam Insert", "Tanggal Update", "User Update", "Hubungi","Lulusan","Note"};

        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
        Iterable<Subscribe> dataSubscribe = subscribeDao.findByKelasAndSumberNotAndJenjangAndTahunAjaranIdOrderByTglInsertDesc(kelas, "spmb.tazkia.ac.id", Jenjang.S1,ta.getId());
        System.out.println("Kelasnya : "+ kelas);


        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Landing Page Sarjana "+ kelas);

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1 ;
        int baris = 1 ;

        for (Subscribe p : dataSubscribe) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(baris++);
            row.createCell(1).setCellValue(p.getNama());
            row.createCell(2).setCellValue(p.getEmail());
            row.createCell(3).setCellValue(p.getNoWa());
            row.createCell(4).setCellValue(p.getAsalKota());
            row.createCell(5).setCellValue(p.getAsalSekolah());
            row.createCell(6).setCellValue(p.getSumber());
            row.createCell(7).setCellValue(p.getFollowup().getNama());
            row.createCell(8).setCellValue(p.getStatus());

            String europeanDatePattern = "yyyy-MM-dd";
            DateTimeFormatter europeanDateFormatter = DateTimeFormatter.ofPattern(europeanDatePattern);
            row.createCell(9).setCellValue(p.getTglInsert().format(europeanDateFormatter));

            String timeColonPattern = "HH:mm:ss";
            DateTimeFormatter timeColonFormatter = DateTimeFormatter.ofPattern(timeColonPattern);
            row.createCell(10).setCellValue(p.getTglInsert().format(timeColonFormatter));

            if (p.getTglUpdate() == null) {
                row.createCell(11).setCellValue("Belum di update");
            } else {
                row.createCell(11).setCellValue(p.getTglUpdate().toString());
            }
            if (p.getUserUpdate() == null) {
                row.createCell(12).setCellValue("Belum di update");
            } else {
                row.createCell(12).setCellValue(p.getUserUpdate().getUsername());
            }
            row.createCell(13).setCellValue(p.getHubungi());
            if (p.getLulusan() == null) {
                row.createCell(14).setCellValue("-");
            } else {
                row.createCell(14).setCellValue(p.getLulusan());
            }
            if (p.getNoteEc() == null) {
                row.createCell(15).setCellValue("-");
            } else {
                row.createCell(15).setCellValue(p.getNoteEc());
            }
        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }



        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=Landing-Page-"+kelas+" - "+ LocalDate.now() +".xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    @GetMapping("/landingS2/xlsx")
    public void rekapS2Xlsx(@RequestParam(required = false) String  kelas, HttpServletResponse response) throws Exception {

        String[] columns = {"No","Nama","Email","No Telephone","Kota Asal Sekolah","Asal Sekolah","Sumber","User FU","Status","Tanggal Insert","Jam Insert", "Tanggal Update", "User Update", "Hubungi"};

        Iterable<Subscribe> dataSubscribe = subscribeDao.findBySumberNotAndJenjangOrderByTglInsertDesc( "spmb.tazkia.ac.id", Jenjang.S2);
        System.out.println("Kelasnya : "+ kelas);


        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Landing Page Pascasarjana "+ kelas);

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1 ;
        int baris = 1 ;

        for (Subscribe p : dataSubscribe) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(baris++);
            row.createCell(1).setCellValue(p.getNama());
            row.createCell(2).setCellValue(p.getEmail());
            row.createCell(3).setCellValue(p.getNoWa());
            row.createCell(4).setCellValue(p.getAsalKota());
            row.createCell(5).setCellValue(p.getAsalSekolah());
            row.createCell(6).setCellValue(p.getSumber());
            row.createCell(7).setCellValue(p.getFollowup().getNama());
            row.createCell(8).setCellValue(p.getStatus());

            String europeanDatePattern = "yyyy-MM-dd";
            DateTimeFormatter europeanDateFormatter = DateTimeFormatter.ofPattern(europeanDatePattern);
            row.createCell(9).setCellValue(p.getTglInsert().format(europeanDateFormatter));

            String timeColonPattern = "HH:mm:ss";
            DateTimeFormatter timeColonFormatter = DateTimeFormatter.ofPattern(timeColonPattern);
            row.createCell(10).setCellValue(p.getTglInsert().format(timeColonFormatter));

            if (p.getTglUpdate() == null) {
                row.createCell(11).setCellValue("Belum di update");
            } else {
                row.createCell(11).setCellValue(p.getTglUpdate().toString());
            }
            if (p.getUserUpdate() == null) {
                row.createCell(12).setCellValue("Belum di update");
            } else {
                row.createCell(12).setCellValue(p.getUserUpdate().getUsername());
            }
            row.createCell(13).setCellValue(p.getHubungi());
        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }



        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=Landing-Page-"+"Pascasarjana"+" - "+ LocalDate.now() +".xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    @GetMapping("/exportS2/xlsxLp")
    public void rekapLandingPage(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate mulai,
                                 @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate sampai,
                                 HttpServletResponse response) throws Exception {
        Iterable<Subscribe> dataSubcribe = subscribeDao.findBySumberNotAndJenjangAndTglInsertBetweenOrderByTglInsert("spmb.tazkia.ac.id",Jenjang.S2, mulai.atStartOfDay(), sampai.plusDays(1).atStartOfDay());

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Landing Page Pascasarjana"+ "-" + mulai + " - " + sampai);
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);
        String[] columns = {"No", "Nama", "Email", "No Hp", "Asal Sekolah", "Kota Asal Sekolah", "User Fu","Sumber","Tgl Insert", "Status", "Tgl Update","Jam Insert", "User Update", "Hubungi"};
        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        int rowNum = 1 ;
        int baris = 1 ;

        for (Subscribe sc : dataSubcribe){
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(baris++);
            row.createCell(1).setCellValue(sc.getNama());
            row.createCell(2).setCellValue(sc.getEmail());
            row.createCell(3).setCellValue(sc.getNoWa());
            row.createCell(4).setCellValue(sc.getAsalSekolah());
            row.createCell(5).setCellValue(sc.getAsalKota());
            row.createCell(6).setCellValue(sc.getFollowup().getNama());
            row.createCell(7).setCellValue(sc.getSumber());

            String europeanDatePattern = "yyyy-MM-dd";
            DateTimeFormatter europeanDateFormatter = DateTimeFormatter.ofPattern(europeanDatePattern);
            row.createCell(8).setCellValue(sc.getTglInsert().format(europeanDateFormatter));

            String timeColonPattern = "HH:mm:ss";
            DateTimeFormatter timeColonFormatter = DateTimeFormatter.ofPattern(timeColonPattern);
            row.createCell(9).setCellValue(sc.getTglInsert().format(timeColonFormatter));


            row.createCell(10).setCellValue(sc.getStatus());
            if (sc.getTglUpdate() == null) {
                row.createCell(11).setCellValue("Belum di update");
            } else {
                row.createCell(11).setCellValue(sc.getTglUpdate().toString());
            }
            if (sc.getUserUpdate() == null) {
                row.createCell(12).setCellValue("Belum di update");
            } else {
                row.createCell(12).setCellValue(sc.getUserUpdate().getUsername());
            }
            row.createCell(13).setCellValue(sc.getHubungi());
        }
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=Landing Page Pascasarjana" +"_"+ mulai.format(DateTimeFormatter.ISO_LOCAL_DATE) +"_"+ sampai.format(DateTimeFormatter.ISO_LOCAL_DATE) +".xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();
    }


    @PostMapping("/update/status/{id}")
    @ResponseBody
    public void updateStatus (@PathVariable Subscribe id,@RequestParam String status,Authentication currentUser){
        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }

        Subscribe subscribe = subscribeDao.findById(id.getId()).get();
        subscribe.setStatus(status);
        subscribe.setTglUpdate(LocalDate.now());
        subscribe.setUserUpdate(u);
        subscribeDao.save(subscribe);

    }

    @PostMapping("/update/fu/{id}")
    @ResponseBody
    public void updateFollowup (@PathVariable Subscribe id,@RequestParam String followup,Authentication currentUser){
        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }

        Subscribe subscribe = subscribeDao.findById(id.getId()).get();
        Followup fu = followupDao.findById(followup).get();
        subscribe.setFollowup(fu);
        subscribe.setTglUpdate(LocalDate.now());
        subscribe.setUserUpdate(u);
        subscribeDao.save(subscribe);
        logger.info("User EC diperbaharui {}", fu.getNama());

    }

    @PostMapping("/update/lulusan/{id}")
    @ResponseBody
    public void updateLulusan (@PathVariable Subscribe id,@RequestParam String lulusan,Authentication currentUser){
        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }

        Subscribe subscribe = subscribeDao.findById(id.getId()).get();
        subscribe.setLulusan(lulusan);
        subscribe.setTglUpdate(LocalDate.now());
        subscribe.setUserUpdate(u);
        subscribeDao.save(subscribe);
        logger.info("Lulusan diperbaharui {}", lulusan);

    }

    @GetMapping("/subscribe/listWin")
    public void listSubscribeWinReguler(@RequestParam(required = false)String nama, Model m, String user,
                              @PageableDefault(sort = "nama", direction = Sort.Direction.DESC)  Pageable page){

        if(StringUtils.hasText(nama)) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarSc", subscribeDao.findByNamaContainingIgnoreCaseAndKelasAndStatusAndSumberNotAndJenjangOrderByTglInsertDesc(nama,"Reguler","Relevant - Sudah Registrasi","spmb.tazkia.ac.id",Jenjang.S1, page));
        } else if (StringUtils.hasText(user)){
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarSc", subscribeDao.findByFollowupAndKelasAndStatusAndSumberNotAndJenjangOrderByTglInsertDesc(fl,"Reguler","Relevant - Sudah Registrasi","spmb.tazkia.ac.id", Jenjang.S1,page));
        }else {
            m.addAttribute("daftarSc", subscribeDao.findByKelasAndStatusAndSumberNotAndJenjangOrderByTglInsertDesc("Reguler","Relevant - Sudah Registrasi","spmb.tazkia.ac.id",Jenjang.S1,page));
        }

        Iterable<Followup> fu = followupDao.findAll();
        m.addAttribute("listFu", fu);

        Iterable<Subscribe> sc = subscribeDao.findAll();
        m.addAttribute("subscribe", sc);

    }

    @GetMapping("/subscribe/listWinKaryawan")
    public void listSubscribeWinKaryawan(@RequestParam(required = false)String nama, Model m, String user,
                                        @PageableDefault(sort = "nama", direction = Sort.Direction.DESC)  Pageable page){

        if(StringUtils.hasText(nama)) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarSc", subscribeDao.findByNamaContainingIgnoreCaseAndKelasAndStatusAndSumberNotAndJenjangOrderByTglInsertDesc(nama,"Karyawan","Relevant - Sudah Registrasi","spmb.tazkia.ac.id",Jenjang.S1, page));
        } else if (StringUtils.hasText(user)){
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarSc", subscribeDao.findByFollowupAndKelasAndStatusAndSumberNotAndJenjangOrderByTglInsertDesc(fl,"Karyawan","Relevant - Sudah Registrasi","spmb.tazkia.ac.id",Jenjang.S1, page));
        }else {
            m.addAttribute("daftarSc", subscribeDao.findByKelasAndStatusAndSumberNotAndJenjangOrderByTglInsertDesc("Karyawan","Relevant - Sudah Registrasi","spmb.tazkia.ac.id",Jenjang.S1,page));
        }

        Iterable<Followup> fu = followupDao.findAll();
        m.addAttribute("listFu", fu);

        Iterable<Subscribe> sc = subscribeDao.findAll();
        m.addAttribute("subscribe", sc);

    }

    @GetMapping("/subscribe/listLose")
    public void listSubscribeLoseReguler(@RequestParam(required = false)String nama, Model m, String user,
                                        @PageableDefault(sort = "nama", direction = Sort.Direction.DESC)  Pageable page){

        TahunAjaran tahunAktif = tahunAjaranDao.findByAktif(Status.AKTIF);

        if(StringUtils.hasText(nama)) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarSc", subscribeDao.findByNamaContainingIgnoreCaseAndKelasAndTahunAjaranAndStatusContainingIgnoreCaseAndSumberNotAndJenjangOrderByTglInsertDesc(nama,"Reguler", tahunAktif,"Not Relevant","spmb.tazkia.ac.id",Jenjang.S1, page));
        } else if (StringUtils.hasText(user)){
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarSc", subscribeDao.findByFollowupAndKelasAndTahunAjaranAndStatusContainingIgnoreCaseAndSumberNotAndJenjangOrderByTglInsertDesc(fl,"Reguler", tahunAktif,"Not Relevant","spmb.tazkia.ac.id",Jenjang.S1, page));
        }else {
            m.addAttribute("daftarSc", subscribeDao.findByKelasAndTahunAjaranAndStatusContainingIgnoreCaseAndSumberNotAndJenjangOrderByTglInsertDesc("Reguler", tahunAktif,"Not Relevant","spmb.tazkia.ac.id", Jenjang.S1,page));
        }

        Iterable<Followup> fu = followupDao.findAll();
        m.addAttribute("listFu", fu);

        Iterable<Subscribe> sc = subscribeDao.findAll();
        m.addAttribute("subscribe", sc);

    }

    @GetMapping("/subscribe/listLoseKaryawan")
    public void listSubscribeLoseKaryawan(@RequestParam(required = false)String nama, Model m, String user,
                                         @PageableDefault(sort = "nama", direction = Sort.Direction.DESC)  Pageable page){

        TahunAjaran tahunAktif = tahunAjaranDao.findByAktif(Status.AKTIF);
        if(StringUtils.hasText(nama)) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarSc", subscribeDao.findByNamaContainingIgnoreCaseAndKelasAndTahunAjaranAndStatusContainingIgnoreCaseAndSumberNotAndJenjangOrderByTglInsertDesc(nama,"Karyawan", tahunAktif,"Not Relevant","spmb.tazkia.ac.id",Jenjang.S1, page));
        } else if (StringUtils.hasText(user)){
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarSc", subscribeDao.findByFollowupAndKelasAndTahunAjaranAndStatusContainingIgnoreCaseAndSumberNotAndJenjangOrderByTglInsertDesc(fl,"Karyawan", tahunAktif,"Not Relevant","spmb.tazkia.ac.id",Jenjang.S1, page));
        }else {
            m.addAttribute("daftarSc", subscribeDao.findByKelasAndTahunAjaranAndStatusContainingIgnoreCaseAndSumberNotAndJenjangOrderByTglInsertDesc("Karyawan", tahunAktif,"Not Relevant","spmb.tazkia.ac.id",Jenjang.S1,page));
        }

        Iterable<Followup> fu = followupDao.findAll();
        m.addAttribute("listFu", fu);

        Iterable<Subscribe> sc = subscribeDao.findAll();
        m.addAttribute("subscribe", sc);

    }

    @GetMapping("/export/xlsxLp")
    public void rekapLandingPage(@RequestParam(required = false) String  kelas,
                                 @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate mulai,
                                 @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate sampai,
                                 HttpServletResponse response) throws Exception {
        Iterable<Subscribe> dataSubcribe = subscribeDao.findByKelasAndSumberNotAndTglInsertBetweenOrderByTglInsert(kelas,"spmb.tazkia.ac.id", mulai.atStartOfDay(), sampai.plusDays(1).atStartOfDay());

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Landing Page "+ kelas + "-" + mulai + " - " + sampai);
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);
        String[] columns = {"No", "Nama", "Email", "No Hp", "Asal Sekolah", "Kota Asal Sekolah", "User Fu","Sumber","Tgl Insert", "Status", "Tgl Update","Jam Insert", "User Update", "Hubungi","Lulusan","Note"};
        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        int rowNum = 1 ;
        int baris = 1 ;

        for (Subscribe sc : dataSubcribe){
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(baris++);
            row.createCell(1).setCellValue(sc.getNama());
            row.createCell(2).setCellValue(sc.getEmail());
            row.createCell(3).setCellValue(sc.getNoWa());
            row.createCell(4).setCellValue(sc.getAsalSekolah());
            row.createCell(5).setCellValue(sc.getAsalKota());
            row.createCell(6).setCellValue(sc.getFollowup().getNama());
            row.createCell(7).setCellValue(sc.getSumber());

            String europeanDatePattern = "yyyy-MM-dd";
            DateTimeFormatter europeanDateFormatter = DateTimeFormatter.ofPattern(europeanDatePattern);
            row.createCell(8).setCellValue(sc.getTglInsert().format(europeanDateFormatter));

            String timeColonPattern = "HH:mm:ss";
            DateTimeFormatter timeColonFormatter = DateTimeFormatter.ofPattern(timeColonPattern);
            row.createCell(9).setCellValue(sc.getTglInsert().format(timeColonFormatter));


            row.createCell(10).setCellValue(sc.getStatus());
            if (sc.getTglUpdate() == null) {
                row.createCell(11).setCellValue("Belum di update");
            } else {
                row.createCell(11).setCellValue(sc.getTglUpdate().toString());
            }
            if (sc.getUserUpdate() == null) {
                row.createCell(12).setCellValue("Belum di update");
            } else {
                row.createCell(12).setCellValue(sc.getUserUpdate().getUsername());
            }
            row.createCell(13).setCellValue(sc.getHubungi());
            if (sc.getKelas() == null) {
                row.createCell(14).setCellValue("-");
            } else {
                row.createCell(14).setCellValue(sc.getLulusan());
            }
            if (sc.getNoteEc() == null) {
                row.createCell(15).setCellValue("-");
            } else {
                row.createCell(15).setCellValue(sc.getNoteEc());
            }
        }
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=Landing Page"+ kelas +"_"+ mulai.format(DateTimeFormatter.ISO_LOCAL_DATE) +"_"+ sampai.format(DateTimeFormatter.ISO_LOCAL_DATE) +".xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    @GetMapping("/subscribe/listPasca")
    public void listSubscribePasca(@RequestParam(required = false)String nama, @RequestParam(required = false) String tahun, String user, Model m,
                              Pageable page, Authentication authentication) {

        String username = ((UserDetails) authentication.getPrincipal()).getUsername();
        User userEdu = userDao.findByUsername(username);
        if (AppConstants.ROLE_EDU.equals(userEdu.getRole().getId())) {
            Followup fu = followupDao.findByUser(userEdu);
            user = fu.getId();
            m.addAttribute("UserEdu", user);
        }

        if(StringUtils.hasText(nama) && StringUtils.hasText(user) && StringUtils.hasText(tahun)) {
            m.addAttribute("nama", nama);
            m.addAttribute("user", user);
            m.addAttribute("tahun", tahun);
            m.addAttribute("daftarSc", subscribeDao.findByNamaAndFollowupIdAndTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(nama, user, tahun,"Reguler","spmb.tazkia.ac.id","Relevant - Sudah Registrasi", "Not Relevant",Jenjang.S2, page));
        } else if (StringUtils.hasText(user) && StringUtils.hasText(tahun) && !StringUtils.hasText(nama) ){
            m.addAttribute("user", user);
            m.addAttribute("tahun", tahun);
            m.addAttribute("daftarSc", subscribeDao.findByFollowupIdAndTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(user, tahun,"Reguler","spmb.tazkia.ac.id","Relevant - Sudah Registrasi","Not Relevant",Jenjang.S2 ,page));
        }else if (StringUtils.hasText(nama) && StringUtils.hasText(user) && !StringUtils.hasText(tahun)) {
            m.addAttribute("nama", nama);
            m.addAttribute("user", user);
            m.addAttribute("daftarSc", subscribeDao.findByNamaContainingIgnoreCaseAndFollowupIdContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(nama, user,"Reguler","spmb.tazkia.ac.id","Relevant - Sudah Registrasi","Not Relevant",Jenjang.S2,page));
        }else if (StringUtils.hasText(nama)  && StringUtils.hasText(tahun) && !StringUtils.hasText(user)) {
            m.addAttribute("nama", nama);
            m.addAttribute("tahun", tahun);
            m.addAttribute("daftarSc", subscribeDao.findByNamaAndTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(nama, tahun,"Reguler","spmb.tazkia.ac.id","Relevant - Sudah Registrasi","Not Relevant",Jenjang.S2,page));
        }else if (StringUtils.hasText(nama) && !StringUtils.hasText(user) && !StringUtils.hasText(tahun)) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarSc", subscribeDao.findByNamaContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(nama,"Reguler","spmb.tazkia.ac.id","Relevant - Sudah Registrasi","Not Relevant",Jenjang.S2,page));
        }else if (StringUtils.hasText(user) && !StringUtils.hasText(nama) &&  !StringUtils.hasText(tahun)) {
            m.addAttribute("user", user);
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarSc", subscribeDao.findByFollowupAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(fl,"Reguler","spmb.tazkia.ac.id","Relevant - Sudah Registrasi","Not Relevant",Jenjang.S2,page));
        }else if (StringUtils.hasText(tahun) && !StringUtils.hasText(nama) && !StringUtils.hasText(user)) {
            m.addAttribute("tahun", tahun);
            m.addAttribute("daftarSc", subscribeDao.findByTahunAjaranIdContainingIgnoreCaseAndKelasAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc(tahun, "Reguler", "spmb.tazkia.ac.id", "Relevant - Sudah Registrasi", "Not Relevant",Jenjang.S2, page));
        } else {
            TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);
            m.addAttribute("daftarSc", subscribeDao.findByKelasAndTahunAjaranAndSumberNotAndStatusNotAndStatusNotContainingIgnoreCaseAndJenjangOrderByTglInsertDesc("Reguler",tahunAjaran,"spmb.tazkia.ac.id","Relevant - Sudah Registrasi","Not Relevant",Jenjang.S2,page));
        }

        Iterable<Followup> fu = followupDao.findByStatus(Status.AKTIF);
        m.addAttribute("listFu", fu);

        Iterable<Subscribe> sc = subscribeDao.findAll();
        m.addAttribute("subscribe", sc);

        Iterable<TahunAjaran> taj = tahunAjaranDao.findAll();
        m.addAttribute("tahunAjaran", taj);

    }


    @GetMapping("/export/xlsxLpWin")
    public void rekapLandingPage(@RequestParam(required = false) String  kelas,
                                 HttpServletResponse response) throws Exception {
        Iterable<Subscribe> dataSubcribe = subscribeDao.findByKelasAndStatusAndSumberNotOrderByTglInsert(kelas, "Relevant - Sudah Registrasi","spmb.tazkia.ac.id");

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Landing Page WIN "+ kelas + "-" + LocalDate.now());
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);
        String[] columns = {"No", "Nama", "Email", "No Hp", "Asal Sekolah", "Kota Asal Sekolah", "User Fu","Sumber","Tgl Insert", "Status", "Tgl Update", "User Update"};
        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        int rowNum = 1 ;
        int baris = 1 ;

        for (Subscribe sc : dataSubcribe){
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(baris++);
            row.createCell(1).setCellValue(sc.getNama());
            row.createCell(2).setCellValue(sc.getEmail());
            row.createCell(3).setCellValue(sc.getNoWa());
            row.createCell(4).setCellValue(sc.getAsalSekolah());
            row.createCell(5).setCellValue(sc.getAsalKota());
            row.createCell(6).setCellValue(sc.getFollowup().getNama());
            row.createCell(7).setCellValue(sc.getSumber());
            row.createCell(8).setCellValue(sc.getTglInsert().toString());
            row.createCell(9).setCellValue(sc.getStatus());
            if (sc.getTglUpdate() == null) {
                row.createCell(10).setCellValue("Belum di update");
            } else {
                row.createCell(10).setCellValue(sc.getTglUpdate().toString());
            }
            if (sc.getUserUpdate() == null) {
                row.createCell(11).setCellValue("Belum di update");
            } else {
                row.createCell(11).setCellValue(sc.getUserUpdate().getUsername());
            }
        }
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=Landing Page WIN "+ kelas +"_"+ LocalDate.now() +".xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    @PostMapping("/subscribe/updateNote")
    public String updateStatusPendaftar(@RequestParam Subscribe id, String noteEc, String kelas, @RequestParam(required = false, defaultValue = "0") int page, RedirectAttributes attributes, Authentication currentUser){
        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.info("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }

        Subscribe subscribe = subscribeDao.findById(id.getId()).get();
        subscribe.setNoteEc(noteEc);
        subscribe.setTglUpdate(LocalDate.now());
        subscribe.setUserUpdate(u);
        subscribeDao.save(subscribe);
        attributes.addAttribute("page", page);
        if (Jenjang.S1.equals(id.getJenjang())) {
            if (kelas.equals("Reguler")) {
                return "redirect:/subscribe/list";
            } else {
                return "redirect:/subscribe/listKaryawan";
            }
        }else{
            return "redirect:/subscribe/listPasca";
        }
    }

    @GetMapping("/tamplate/lp")
    public void rekapPembayaranCsv(HttpServletResponse response) throws Exception {
        String filename = "Template_LP.csv";
        response.setHeader("Content-Disposition", "attachment;filename="+filename);
        response.setContentType("text/csv");
        response.getWriter().println("Nama,Email, No Whatsapp, Kota Asal Sekolah, Nama Sekolah, Sumber,User EC (ID)");
        response.getWriter().flush();
    }

    @PostMapping("/subscribe/import")
    public String processFormUpload(@RequestParam String kelas,@RequestParam(required = false) Boolean pakaiHeader,
                                    MultipartFile fileLp,
                                    RedirectAttributes redirectAttrs, Authentication currentUser){

        logger.info("Pakai Header : {}",pakaiHeader);
        logger.info("Nama File : {}",fileLp.getName());
        logger.info("Ukuran File : {}",fileLp.getSize());

        List<UploadError> errors = new ArrayList<>();
        Integer baris = 0;

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(fileLp.getInputStream()));
            String content;

            if((pakaiHeader != null && pakaiHeader)){
                content = reader.readLine();
            }

            while ((content = reader.readLine()) != null) {
                baris++;
                String[] data = content.split(",");
                if (data.length != 7) {
                    errors.add(new UploadError(baris, "Format data salah", content));
                    continue;
                }

                // save
                Subscribe subscribe = new Subscribe();
                subscribe.setJenjang(Jenjang.S1);
                if (kelas.equals("Reguler")) {
                    subscribe.setKelas("Reguler");
                } else if (kelas.equals("Karyawan")) {
                    subscribe.setKelas("Karyawan");
                }
                subscribe.setTglInsert(LocalDateTime.now());
                TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
                subscribe.setTahunAjaran(ta);
                subscribe.setHubungi(0);


                subscribe.setNama(data[0]);
                subscribe.setEmail(data[1]);
                subscribe.setNoWa(data[2]);
                subscribe.setAsalKota(data[3]);
                subscribe.setAsalSekolah(data[4]);
                subscribe.setSumber(data[5]);

                Followup fu = followupDao.findById(data[6]).get();
                subscribe.setFollowup(fu);
                subscribe.setStatus("None");

                subscribeDao.save(subscribe);

            }
        } catch (Exception err){
            logger.warn(err.getMessage(), err);
            errors.add(new UploadError(0, "Format file salah", ""));
        }

        redirectAttrs
                .addFlashAttribute("jumlahBaris", baris)
                .addFlashAttribute("jumlahSukses", baris - errors.size())
                .addFlashAttribute("jumlahError", errors.size())
                .addFlashAttribute("errors", errors);

        return "redirect:hasil";
    }
    @GetMapping("/tamplate/lpS2")
    public void templateCsvS2(HttpServletResponse response) throws Exception {
        String filename = "Template_LP.csv";
        response.setHeader("Content-Disposition", "attachment;filename="+filename);
        response.setContentType("text/csv");
        response.getWriter().println("Nama,Email, No Whatsapp, Kota Asal Sekolah, Nama Sekolah, Sumber,User EC (ID), Kelas");
        response.getWriter().flush();
    }
  @PostMapping("/subscribe/importS2")
    public String processFormUploadS2(@RequestParam(required = false) Boolean pakaiHeader,
                                    MultipartFile fileLp,
                                    RedirectAttributes redirectAttrs, Authentication currentUser){

        logger.info("Pakai Header : {}",pakaiHeader);
        logger.info("Nama File : {}",fileLp.getOriginalFilename());
        logger.info("Ukuran File : {}",fileLp.getSize());

        List<UploadError> errors = new ArrayList<>();
        Integer baris = 0;

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(fileLp.getInputStream()));
            String content;

            if((pakaiHeader != null && pakaiHeader)){
                content = reader.readLine();
            }

            while ((content = reader.readLine()) != null) {
                baris++;
                String[] data = content.split(",");
                if (data.length != 8) {
                    errors.add(new UploadError(baris, "Format data salah", content));
                    continue;
                }

                TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
                Subscribe cekData = subscribeDao.findByNamaAndEmailAndNoWaAndJenjangAndTahunAjaran(data[0], data[1], data[2], Jenjang.S2, ta);
                if (cekData == null) {
                    // save
                    Subscribe subscribe = new Subscribe();
                    subscribe.setJenjang(Jenjang.S2);

                    subscribe.setTglInsert(LocalDateTime.now());
                    subscribe.setTahunAjaran(ta);
                    subscribe.setHubungi(0);


                    subscribe.setNama(data[0]);
                    subscribe.setEmail(data[1]);
                    subscribe.setNoWa(data[2]);
                    subscribe.setAsalKota(data[3]);
                    subscribe.setAsalSekolah(data[4]);
                    subscribe.setSumber(data[5]);

                    Followup fu = followupDao.findById(data[6]).get();
                    subscribe.setFollowup(fu);
                    subscribe.setStatus("None");

                    subscribe.setKelas(data[7]);

                    subscribeDao.save(subscribe);
                }else{
                    logger.info("Data atas nama {} dan {} dan {} sudah ada", data[0], data[1], data[2]);
                }

            }
        } catch (Exception err){
            logger.warn(err.getMessage(), err);
            errors.add(new UploadError(0, "Format file salah", ""));
        }

        redirectAttrs
                .addFlashAttribute("jumlahBaris", baris)
                .addFlashAttribute("jumlahSukses", baris - errors.size())
                .addFlashAttribute("jumlahError", errors.size())
                .addFlashAttribute("errors", errors);

        return "redirect:hasil";
    }
    @GetMapping("/subscribe/hasil")
    public void hasilFormUpload(){}


    @GetMapping("/sertifikat/{sertifikat}/sertifikat/")
    public ResponseEntity<byte[]> tampilkanBerkas(@PathVariable TableUser sertifikat, HttpServletResponse response) throws Exception {
        String lokasiFile = uploadFolder + File.separator + sertifikat.getId() + ".pdf";
        logger.debug("Lokasi file bukti : {}", lokasiFile);

        Subscribe subscribe = subscribeDao.findByEmail(sertifikat.getUsername());
        try {
            HttpHeaders headers = new HttpHeaders();
            if (sertifikat.getId().toLowerCase().endsWith("jpeg") || sertifikat.getId().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (sertifikat.getId().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if (sertifikat.getId().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }

            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            response.setHeader("Content-disposition", "attachment; filename=Sertifikat "+ subscribe.getNama()+".pdf");
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            logger.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


//    @Scheduled(cron = "${update.status.landingPage}", zone = "Asia/Jakarta")
//    public String tagihanExpired(){
//        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
//        Iterable<Subscribe> subscribe = subscribeDao.findByTahunAjaran(ta);
//
//        for (Subscribe sb : subscribe) {
//            if (sb.getTglUpdate() == null) {
//                sb.setTglUpdate(LocalDate.now().minusDays(3));
//            }
//            LocalDate tglUpdate = sb.getTglUpdate();
//            LocalDate tglHariIni = LocalDate.now();
//            long selisihHari = ChronoUnit.DAYS.between(tglUpdate, tglHariIni);
//
//
//            if (sb.getHubungi() == 1 || sb.getHubungi() == 2 && selisihHari >= 2 && sb.getStatus() != "Relevant - No Interes") {
//                sb.setStatus("Relevant - No Respon");
//                sb.setTglUpdate(LocalDate.now());
//
//                User u = userDao.findById("System").get();
//                sb.setUserUpdate(u);
//                subscribeDao.save(sb);
//            }
//
//            if (sb.getHubungi() == 1 || sb.getHubungi() == 2 && selisihHari >= 4) {
//                sb.setStatus("Relevant - No Interes");
//                sb.setTglUpdate(LocalDate.now());
//
//                User u = userDao.findById("System").get();
//                sb.setUserUpdate(u);
//                subscribeDao.save(sb);
//            }
//
//            if (sb.getHubungi() == 3 && selisihHari > 5 && sb.getStatus() != "Relevant - Sudah Registrasi") {
//                sb.setStatus("Not Relevant - Tidak Membalas");
//                sb.setTglUpdate(LocalDate.now());
//
//                User u = userDao.findById("System").get();
//                sb.setUserUpdate(u);
//                subscribeDao.save(sb);
//            }
//        }
//        logger.info("update otomatis status landing page");
//        return "/";
//    }
}

package id.ac.tazkia.spmb.aplikasispmb.service;

import id.ac.tazkia.spmb.aplikasispmb.dao.KabupatenKotaDao;
import id.ac.tazkia.spmb.aplikasispmb.dto.BaseResponseDto;
import id.ac.tazkia.spmb.aplikasispmb.dto.SarjanaDto;
import id.ac.tazkia.spmb.aplikasispmb.entity.KabupatenKota;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Service
@RestController
@Slf4j
public class ApiStmik {

    @Autowired private KabupatenKotaDao kabupatenKotaDao;

    @GetMapping("/api/list-kabupatenkota")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> getKabupateKota(){
        try {
            Iterable<KabupatenKota> kabupatenKota = kabupatenKotaDao.findAllByOrderByNamaAsc();
            if (kabupatenKota == null) {
                return ResponseEntity.ok().body(
                        BaseResponseDto.builder()
                                .responseCode("200")
                                .responseMessage("Data kosong")
                                .build()
                );
            }else{
                return ResponseEntity.ok().body(
                        BaseResponseDto.builder()
                                .responseCode("200")
                                .responseMessage("Data ditemukan")
                                .data(kabupatenKota)
                                .build()
                );
            }
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(
                    BaseResponseDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build()
            );
        }
    }

//    @PostMapping("/api/pendaftarstmik")
//    public ResponseEntity<BaseResponseDto> savePendaftar(@Valid @RequestBody SarjanaDto sarjanaDto, BindingResult bindingResult){
//        log.info("save pendaftar stmik {}", sarjanaDto);
//        try{
//            if (bindingResult.hasErrors()) {
//                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new BaseResponseDto("ERR01", "Invalid Request Body, " + bindingResult.getFieldError().getDefaultMessage()));
//            }
//            log.info("========== DTO ========== : {}", sarjanaDto);
//
//        }catch (Exception e){
//            log.error("gagal menyimpan data , {}", e.getMessage(), e);
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
//                    BaseResponseDto.builder()
//                            .responseCode("ER500")
//                            .responseMessage(e.getLocalizedMessage()).build()
//            );
//        }
//    }

}

package id.ac.tazkia.spmb.aplikasispmb.dto;

import lombok.Data;

@Data
public class GetDosen {
    private String id;
    private String nik;
    private String namaKaryawan;
    private String gelar;
    private String jenisKelamin;
    private String idUser;
    private String nidn;
    private String email;
    private String tanggalLahir;
    private String rfid;
    private String idAbsen;
    private String foto;
    private String status;
}

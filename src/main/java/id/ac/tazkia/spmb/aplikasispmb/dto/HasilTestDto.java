package id.ac.tazkia.spmb.aplikasispmb.dto;

import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Column;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

@Data
public class HasilTestDto {
    private  String id;

    private JadwalTest jadwalTest;

    private BigDecimal nilai;

    private JenisTest jenisTest;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(columnDefinition = "DATE")
    private Date tanggalTest;

    private String pendaftar;

    private Periode periode;

    private Grade grade;

    private String keterangan;

    private NilaiBiaya nilaiBiaya;

    private BigDecimal nilaiAsrama;
    private BigDecimal nilaiUkt;
    private BigDecimal nilaiWisuda;
    private BigDecimal nilaiPembelajaran;

    private BigDecimal uangPangkal;
    private String keteranganTagihan;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(columnDefinition = "DATE")
    private LocalDate expiredDate;


}

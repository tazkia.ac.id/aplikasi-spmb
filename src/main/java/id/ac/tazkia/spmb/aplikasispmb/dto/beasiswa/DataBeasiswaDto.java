package id.ac.tazkia.spmb.aplikasispmb.dto.beasiswa;

import groovyjarjarantlr4.v4.runtime.misc.NotNull;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import id.ac.tazkia.spmb.aplikasispmb.entity.beasiswa.PendaftarBeasiswa;
import jakarta.persistence.Column;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
public class DataBeasiswaDto {

    // Pendaftar Beasiswa
    private String id;
    private String namaLengkap;
    private String noTelpon;
    private String email;
    private String lulusan;
    private String kotaAsalSekolah;
    private String namaSekolah;
    private String tahunLulus;
    private String statusOrtu;
    private String nik;
    private String tempatLahir;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalLahir;

    private String nisn;
    private String npsn;
    private String alamat;
    private Provinsi provinsi;
    private KabupatenKota kabupatenKota;
    private String kecamatan;
    private String desaKelurahan;
    private String kodePos;
    private String noPendaftaranKip;
    private String statusDisabilitas;
    private String deskripsiDisabilitas;

    // Pendaftar Beasiswa Orangtua
    private String namaIbu;
    private String agamaIbu;
    private Pendidikan pendidikanIbu;
    private Pekerjaan pekerjaanIbu;
    private String statusIbu;
    private String namaAyah;
    private String agamaAyah;
    private Pendidikan pendidikanAyah;
    private Pekerjaan pekerjaanAyah;
    private String statusAyah;
    private String alamatOrangTua;
    private String noTelponOrangTua;
    private Penghasilan penghasilanOrangtua;
    private Integer jumlahTanggungan;

    // Pendaftar Beasiswa File
    private String fileId;
    private String sktm;
    private String tagihanListrik;
    private String fotoRumah;
    private String resume;
    private String prestasi1;
    private String prestasi2;
    private String prestasi3;
    private String prestasi4;

}

package id.ac.tazkia.spmb.aplikasispmb.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

@Entity @Data
public class PendaftarOrangtua {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_pendaftar")
    private Pendaftar pendaftar;

    @NotNull
    @NotEmpty
    private String namaAyah;

    @NotNull
    @NotEmpty
    private String agamaAyah;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "pendidikan_ayah")
    private Pendidikan pendidikanAyah;

    @NotNull
    @ManyToOne @JoinColumn(name = "pekerjaan_ayah")
    private Pekerjaan pekerjaanAyah;

    @NotNull
    @NotEmpty
    private String statusAyah;

    @NotNull
    @NotEmpty
    private String namaIbu;

    @NotNull
    @NotEmpty
    private String agamaIbu;

    @NotNull
    @ManyToOne @JoinColumn(name = "pendidikan_ibu")
    private Pendidikan pendidikanIbu;

    @NotNull
    @ManyToOne @JoinColumn(name = "pekerjaan_ibu")
    private Pekerjaan pekerjaanIbu;

    @NotNull
    @NotEmpty
    private String statusIbu;

    @NotNull
    @NotEmpty
    private String alamat;

    @NotNull
    @NotEmpty
    private String noOrangtua;

    @NotNull
    @ManyToOne @JoinColumn(name = "penghasilan")
    private Penghasilan penghasilanOrangtua;

    @NotNull
    @NotEmpty
    private String jumlahTanggungan;
}

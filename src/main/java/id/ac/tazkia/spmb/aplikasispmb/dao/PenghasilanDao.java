package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.Penghasilan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PenghasilanDao extends PagingAndSortingRepository<Penghasilan, String>, CrudRepository<Penghasilan, String> {

}

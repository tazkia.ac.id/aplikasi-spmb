package id.ac.tazkia.spmb.aplikasispmb.dto;

import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Column;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class DetailPendaftarPascaDto {
    private String id;
    private Pendaftar pendaftar;
    private String tempatLahir;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull
    @Column(columnDefinition = "DATE")
    private LocalDate tanggalLahir;
    private String agama;
    private String jenisKelamin;
    private String golonganDarah;
    private String noKtp;
    private String alamatRumah;
    private String negara;
    private String statusSipil;
    private Provinsi provinsi;
    private KabupatenKota kokab;
    private String kodePos;
    private String pekerjaan;
    private String alamatPekerjaan;
    private String asalPendidikan;
    private  String sumberBiaya;
    private  String namaAyah;
    private  String namaIbu;

    private ProgramStudi programStudi;
    private String  konsentrasi;

    private Leads leads;
    private String nama;
    private String email;
    private String noHp;

    private String nim;

    private ProgramStudi prodiLama;
}

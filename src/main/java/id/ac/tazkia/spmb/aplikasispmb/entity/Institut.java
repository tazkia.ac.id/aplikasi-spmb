package id.ac.tazkia.spmb.aplikasispmb.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Data
@Entity
public class Institut {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String kode;

    private String nama;

    @Enumerated(EnumType.STRING)
    private Status status;

}

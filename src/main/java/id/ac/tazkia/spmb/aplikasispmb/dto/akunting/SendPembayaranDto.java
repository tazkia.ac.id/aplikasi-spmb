package id.ac.tazkia.spmb.aplikasispmb.dto.akunting;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;

@Data
public class SendPembayaranDto {

    private String codeTemplate;
    private String dateTransaction;
    private String description;
    private String institut = "bed6a06c-afe9-4345-8b3f-ebe2cbb49a51";
    private String attachment;
    @JsonProperty(defaultValue = "SPMB")
    private String tags;
    private ArrayList<BigDecimal> amounts;

}

package id.ac.tazkia.spmb.aplikasispmb.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

@Entity @Data
public class Berkas {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_pendaftar")
    private Pendaftar pendaftar;

    @NotNull @Enumerated(EnumType.STRING)
    private JenisBerkas jenisBerkas;

    private String fileBerkas;

    private String alasan;

    @Enumerated(EnumType.STRING)
    private Status status = Status.WAITING;

}

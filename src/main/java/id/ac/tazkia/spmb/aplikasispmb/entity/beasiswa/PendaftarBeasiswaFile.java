package id.ac.tazkia.spmb.aplikasispmb.entity.beasiswa;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;


@Data
@Entity
public class PendaftarBeasiswaFile {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_pendaftar_beasiswa")
    private PendaftarBeasiswa pendaftarBeasiswa;

    private String sktm;

    private String tagihanListrik;

    private String fotoRumah;

    private String resume;

    private String prestasi1;

    private String prestasi2;

    private String prestasi3;

    private String prestasi4;

}

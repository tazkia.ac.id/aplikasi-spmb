package id.ac.tazkia.spmb.aplikasispmb.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
//@EnableWebSecurity
public class KonfigurasiSecurity {
    private static final String SQL_LOGIN
            = "select u.username as username,p.password as password, active\n"
            + "FROM s_user u\n"
            + "inner join s_user_password p on p.id_user = u.id\n"
            + "WHERE u.username = ?";

    private static final String SQL_PERMISSION
            = "select u.username, p.permission_value as authority "
            + "from s_user u "
            + "inner join s_role r on u.id_role = r.id "
            + "inner join s_role_permission rp on rp.id_role = r.id "
            + "inner join s_permission p on rp.id_permission = p.id "
            + "where u.username = ?";

    @Value("${bcrypt.strength}")
    private Integer bcryptStrength;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public UserDetailsManager userDetailsManager(DataSource dataSource){
        JdbcUserDetailsManager manager = new JdbcUserDetailsManager(dataSource);
        manager.setUsersByUsernameQuery(SQL_LOGIN);
        manager.setAuthoritiesByUsernameQuery(SQL_PERMISSION);
        return manager;
    }

    @Bean
    @Order(2)
    public SecurityFilterChain configureHttpSecurity(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(auth -> 
            auth.requestMatchers("/suratKeterangan"
                    ,"/brosur/pei"
                    ,"/brosur/mua"
                    ,"/brosur/ai"
                    ,"/brosur/bmi"
                    ,"/brosur/ei"
                    ,"/brosur/institut"
                    ,"/brosur/d3"
                    ,"/brosur/mes"
                    ,"/brosur/mas"
                    ,"/leads/form"
                    ,"/leads/selesai"
                    ,"/leads/gagal"
                    ,"/api/username"
                    ,"/favicon.ico"
                    ,"/info"
                    ,"/js/**"
                    ,"../img/*"
                    ,"/images/**"
                    ,"/index/**"
                    ,"/frontend"
                    ,"/reset"
                    ,"/reset_sukses"
                    ,"/reset_gagal"
                    ,"/confirm"
                    ,"/institut"
                    ,"/succes"
                    ,"/fail"
                    ,"/"
                    ,"https://tazkia.ac.id/index.php/spmbtazkia"
                    ,"/mbs"
                    ,"/as"
                    ,"/hes"
                    ,"/kpi"
                    ,"/smartTest/listIndex"
                    ,"/smartTest/formRegistrasi"
                    ,"/formRegistrasi"
                    ,"/panduanPembayaran"
                    ,"/api/mahasiswanim"
                    ,"/api/program-studi"
                    ,"/api/input-leads"
                    ,"/api/kabupaten"
                    ,"/api/getPesertaCourse"
                    ,"/api/pendaftarPerprodi"
                    ,"/api/pendaftarPertahun"
                    ,"/sitinprogram"
                    ,"/sitin_sukses"
                    ,"/sitinprogram-detail"
                    ,"/fotoCover/{course}/cover/"
                    ,"/fotoDosen/{dosen}/foto/"
                    ,"/css/**"
                    ,"/lib/**"
                    ,"/sarjanaReguler"
                    ,"/stmiktazkia"
                    ,"/kelasKaryawan"
                    ,"/pascasarjana"
                    ,"/daftarBeasiswa"
                    ,"/daftarProdiS1"
                    ,"/api/list-kabupatenkota"
                    ,"/sarjana"
                    ,"/login"
                    ,"/subscribe/selesai"
                    ,"/registrasi/selesai"
                    ,"/beasiswa/selesai"
                    ,"/beasiswa/gagal"
                    ,"/dist/**"
                    ,"/img/**"
                    ,"/prosesSubscribe").permitAll()
                    .requestMatchers("/pendaftar/list")
                        .hasAnyAuthority("VIEW_MASTER","VIEW_FINANCE","VIEW_AKADEMIK")
                        .anyRequest().authenticated())
                    .headers(headers -> headers
                        .frameOptions(frameOptions -> frameOptions
                            .sameOrigin()
                    )
        )
        .logout(logout -> logout.permitAll())
        .formLogin(login -> login
            .loginPage("/login")
            .defaultSuccessUrl("/dashboard", true))
        ;

        return http.build();
    }
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//                .authorizeRequests()
//                .antMatchers("/pendaftar/list").hasAnyAuthority("VIEW_MASTER","VIEW_FINANCE","VIEW_AKADEMIK")
//                .antMatchers("/pendaftar/list").hasAnyAuthority("VIEW_MASTER","VIEW_FINANCE","VIEW_AKADEMIK")
//                .anyRequest().authenticated()
//                .and().logout().permitAll()
//                .and().formLogin().defaultSuccessUrl("/dashboard", true)
//                .loginPage("/login")
//                .permitAll();
//
//    }

//    @Bean
//    public void configure(WebSecurity web) throws Exception {
//        web.ignoring()
//                .requestMatchers("/suratKeterangan")
//                .requestMatchers("/brosur/pei")
//                .requestMatchers("/brosur/mua")
//                .requestMatchers("/brosur/ai")
//                .requestMatchers("/brosur/bmi")
//                .requestMatchers("/brosur/ei")
//                .requestMatchers("/brosur/institut")
//                .requestMatchers("/brosur/d3")
//                .requestMatchers("/brosur/mes")
//                .requestMatchers("/brosur/mas")
//                .requestMatchers("/leads/form")
//                .requestMatchers("/leads/selesai")
//                .requestMatchers("/leads/gagal")
//                .requestMatchers("/api/username")
//                .requestMatchers("/favicon.ico")
//                .requestMatchers("/info")
//                .requestMatchers("/js/**")
//                .requestMatchers("../img/*")
//                .requestMatchers("/images/**")
//                .requestMatchers("/index/**")
//                .requestMatchers("/frontend")
//                .requestMatchers("/reset")
//                .requestMatchers("/reset_sukses")
//                .requestMatchers("/reset_gagal")
//                .requestMatchers("/confirm")
//                .requestMatchers("/institut")
//                .requestMatchers("/succes")
//                .requestMatchers("/fail")
//                .requestMatchers("/")
//                .requestMatchers("https://tazkia.ac.id/index.php/spmbtazkia")
//                .requestMatchers("/mbs")
//                .requestMatchers("/as")
//                .requestMatchers("/hes")
//                .requestMatchers("/kpi")
//                .requestMatchers("/smartTest/listIndex")
//                .requestMatchers("/smartTest/formRegistrasi")
//                .requestMatchers("/formRegistrasi")
//                .requestMatchers("/panduanPembayaran")
//                .requestMatchers("/api/mahasiswanim")
//                .requestMatchers("/api/program-studi")
//                .requestMatchers("/api/input-leads")
//                .requestMatchers("/api/kabupaten")
//                .requestMatchers("/api/getPesertaCourse")
//                .requestMatchers("/api/pendaftarPerprodi")
//                .requestMatchers("/api/pendaftarPertahun")
//                .requestMatchers("/sitinprogram")
//                .requestMatchers("/sitin_sukses")
//                .requestMatchers("/sitinprogram-detail")
//                .requestMatchers("/fotoCover/{course}/cover/")
//                .requestMatchers("/fotoDosen/{dosen}/foto/")
//                .requestMatchers("/css/**")
//                .requestMatchers("/lib/**")
//                .requestMatchers("/sarjanaReguler")
//                .requestMatchers("/kelasKaryawan")
//                .requestMatchers("/pascasarjana")
//                .requestMatchers("/daftarProdiS1")
//
//                .requestMatchers("/sarjana")
//                .requestMatchers("/subscribe/selesai")
//                .requestMatchers("/registrasi/selesai")
//                .requestMatchers("/prosesSubscribe")
//        ;
//
//    }
}


package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.Tagihan;
import id.ac.tazkia.spmb.aplikasispmb.entity.VirtualAccount;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.domain.Pageable;

public interface VirtualAccountDao extends PagingAndSortingRepository<VirtualAccount, String>, CrudRepository<VirtualAccount, String> {

    Page<VirtualAccount> findByTagihan(Tagihan tagihan, Pageable pageable);
    Iterable<VirtualAccount> findByTagihan(Tagihan tagihan);
}

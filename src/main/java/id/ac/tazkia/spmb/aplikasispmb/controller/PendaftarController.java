package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.constants.AppConstants;
import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.dto.ValidasiTagihan;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Controller
public class PendaftarController {
    private static final Logger logger = LoggerFactory.getLogger(PendaftarController.class);

    @Autowired private TahunAjaranDao tahunAjaranDao;
    @Autowired private PendaftarDao pendaftarDao;
    @Autowired private ProgramStudiDao programStudiDao;
    @Autowired private JadwalTestDao jadwalTestDao;
    @Autowired private HasilTestDao hasilTestDao;
    @Autowired private PendaftarDetailDao pendaftarDetailDao;
    @Autowired private TagihanDao tagihanDao;
    @Autowired private PembayaranDao pembayaranDao;
    @Autowired private UploadBerkasDao uploadBerkasDao;
    @Autowired private PendaftarDetailPascaDao pendaftarDetailPascaDao;
    @Autowired private KabupatenKotaDao kabupatenKotaDao;
    @Autowired private UserDao userDao;

    @ModelAttribute("daftarKokab")
    public Iterable<KabupatenKota> daftarKokab(){
        return kabupatenKotaDao.findAll();
    }

    @ModelAttribute("daftarProdi")
    public Iterable<ProgramStudi> daftarProdi(){
        return programStudiDao.findAll();
    }
    @ModelAttribute("daftarProdiS1")
    public Iterable<ProgramStudi> daftarProdiS1(){
        String id ="007";
        String tidak = "005";
        String tidak2 = "000";
        return programStudiDao.cariProdiS1(id,tidak, tidak2);
    }
    @ModelAttribute("daftarProdiD3")
    public Iterable<ProgramStudi> daftarProdiD3(){
        String id ="010";
        return programStudiDao.cariProdiD3(id);
    }


    @ModelAttribute("daftarProdiS2")
    public Iterable<ProgramStudi> daftarProdiS2(){
        String id = "008";
        String dua = "009";
        return programStudiDao.cariProdi(id,dua);
    }

    @RequestMapping("/pendaftar/list")
    public void listLeads(@RequestParam(required = false)String search, Model m,
                          @PageableDefault(size = 10, sort = "nomorRegistrasi", direction = Sort.Direction.DESC)Pageable page){
        Page<Pendaftar> pendaftar = pendaftarDao.findByTahunAjaranAktif(Status.AKTIF,page);

        if(StringUtils.hasText(search)) {
            TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
            Page<Pendaftar> ps= pendaftarDao.findByNomorRegistrasiContainingIgnoreCaseOrLeadsNamaContainingIgnoreCaseAndTahunAjaranOrderByNomorRegistrasi(search,search,ta,page);
            m.addAttribute("search", search);
            m.addAttribute("daftarPendaftar", ps );

            List<JadwalTest> jadwalTests = new ArrayList<>();
            for (Pendaftar pe : ps) {
                JadwalTest jadwalTest = jadwalTestDao.findByPendaftar(pe);
                if (jadwalTest != null) {
                    jadwalTests.add(jadwalTest);
                }
            }
            if (jadwalTests != null) {
                m.addAttribute("jadwalTest", jadwalTests);
            }

            List<HasilTest> hasilTests = new ArrayList<>();
            for (JadwalTest jt : jadwalTests) {
                HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
                if (hasilTest != null) {
                    hasilTests.add(hasilTest);
                }
            }
            if (hasilTests != null) {
                m.addAttribute("hasilTest", hasilTests);
            }

            List<ValidasiTagihan> pendaftarDetails = new ArrayList<>();
            for (Pendaftar pe : ps) {
                PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetail != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetail == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetails.add(validasiTagihan);

            }
                m.addAttribute("detail", pendaftarDetails);

            List<ValidasiTagihan> pendaftarDetailPasca = new ArrayList<>();
            for (Pendaftar pe : ps) {
                PendaftarDetailPasca pendaftarDetailPascas = pendaftarDetailPascaDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetailPascas != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetailPascas == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetailPasca.add(validasiTagihan);

            }
            m.addAttribute("detailS2", pendaftarDetailPasca);



            JenisBiaya jenisBiaya = new JenisBiaya();
            jenisBiaya.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
            if (jenisBiaya == null) {
                logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());

            }
            List<ValidasiTagihan> tagihans = new ArrayList<>();
            for (Pendaftar pe : ps) {
                Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiaya(pe, jenisBiaya);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (tagihan != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (tagihan == null){
                    validasiTagihan.setStatus("Kosong");
                }
                tagihans.add(validasiTagihan);
            }

            m.addAttribute("tagihan", tagihans);

            List<Berkas> berkas = new  ArrayList<>();

            List<Berkas> berkas1 = uploadBerkasDao.findPendaftar();
//            berkas.add(berkas1);


            m.addAttribute("berkas", berkas1);


        } else {
            m.addAttribute("daftarPendaftar", pendaftar);
            List<JadwalTest> jadwalTests = new ArrayList<>();
            for (Pendaftar pe : pendaftar) {
                JadwalTest jadwalTest = jadwalTestDao.findByPendaftar(pe);
                if (jadwalTest != null) {
                    jadwalTests.add(jadwalTest);
                }
            }
            if (jadwalTests != null) {
                m.addAttribute("jadwalTest", jadwalTests);
            }

            List<HasilTest> hasilTests = new ArrayList<>();
            for (JadwalTest jt : jadwalTests) {
                HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
                if (hasilTest != null) {
                    hasilTests.add(hasilTest);
                }
            }
            if (hasilTests != null) {
                m.addAttribute("hasilTest", hasilTests);
            }

            List<ValidasiTagihan> pendaftarDetails = new ArrayList<>();
            for (Pendaftar pe : pendaftar) {
                PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetail != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetail == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetails.add(validasiTagihan);

            }
            m.addAttribute("detail", pendaftarDetails);

            List<ValidasiTagihan> pendaftarDetailPasca = new ArrayList<>();
            for (Pendaftar pe : pendaftar) {
                PendaftarDetailPasca pendaftarDetailPascas = pendaftarDetailPascaDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetailPascas != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetailPascas == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetailPasca.add(validasiTagihan);

            }
            m.addAttribute("detailS2", pendaftarDetailPasca);

            JenisBiaya jenisBiaya = new JenisBiaya();
            jenisBiaya.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
            if (jenisBiaya == null) {
                logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());

            }
            List<ValidasiTagihan> tagihans = new ArrayList<>();
            for (Pendaftar pe : pendaftar) {
                Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiaya(pe, jenisBiaya);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (tagihan != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (tagihan == null){
                    validasiTagihan.setStatus("Kosong");
                }
                tagihans.add(validasiTagihan);
            }

            m.addAttribute("tagihan", tagihans);

            List<Berkas> berkas = new  ArrayList<>();

            List<Berkas> berkas1 = uploadBerkasDao.findPendaftar();
//            berkas.add(berkas1);

//            System.out.println("Berkas :"+ berkas1);
            m.addAttribute("berkas", berkas1);

        }



    }


    @RequestMapping(value = "/pendaftar/update", method = RequestMethod.GET)
    public String leadsUpdateForm(@RequestParam(value = "id", required = false) String id,
                                  Model m){
        //defaultnya, isi dengan object baru
        m.addAttribute("pendaftar", new Pendaftar());

        if (id != null && !id.isEmpty()){
            Pendaftar pendaftar= pendaftarDao.findById(id).get();
            if (pendaftar != null){
                m.addAttribute("pendaftar", pendaftar);
            }

        }
        PendaftarDetail pd = pendaftarDetailDao.findByPendaftarIdAndNimNotNull(id);
        if (pd != null) {
            m.addAttribute("detail", pd);
        }

        return "pendaftar/update";
    }

    @RequestMapping(value = "/pendaftar/update", method = RequestMethod.POST)
    public String prosesForm(@Valid Pendaftar pendaftar, BindingResult errors){
        if(errors.hasErrors()){
            return "/pendaftar/update";
        }
        pendaftarDao.save(pendaftar);
        return "redirect:list";
    }

    @GetMapping("/pendaftar/xlsx")
    public void rekapHumasXlsx(HttpServletResponse response) throws Exception {

        String[] columns = {"No","Nomor Registrasi","Nama","Email","No Telephone","Kota Asal Sekolah","Asal Sekolah","Rekomendasi",
                "Nama Perekomendasi","Program Studi","Kelas","Alasan Memilih Jurusan","Tanggal Daftar","Registrasi","Nominal Regist","Test","Daftar Ulang", "Nominal DU", "Tanggal DU"
                ,"Status", "Catatan", "Catatan PIC", "User Followup", "User Update", "Tanggal Update", "Sumber", "Tanggal Insert","Kode Voucher", "Beasiswa", "Note", "Lulusan", "Data Detail", "Berkas"};

        Iterable<Pendaftar> dataPendaftar = pendaftarDao.findByTahunAjaranAktif(Status.AKTIF);

        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("Pendaftar");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1 ;
        int baris = 1 ;
        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
        JenisBiaya jb = new JenisBiaya();
        jb.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
        Iterable<Tagihan> cekTagihanLunas = tagihanDao.cekTagihanLunas(ta);

        for (Pendaftar p : dataPendaftar) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(baris++);
                row.createCell(1).setCellValue(p.getNomorRegistrasi());
                row.createCell(2).setCellValue(p.getLeads().getNama());
                row.createCell(3).setCellValue(p.getLeads().getEmail());
                row.createCell(4).setCellValue(p.getLeads().getNoHp());
                row.createCell(5).setCellValue(p.getIdKabupatenKota().getNama());
                row.createCell(6).setCellValue(p.getNamaAsalSekolah());
                row.createCell(7).setCellValue(p.getPerekomendasi());
                row.createCell(8).setCellValue(p.getNamaPerekomendasi());
                row.createCell(9).setCellValue(p.getProgramStudi().getNama());
                row.createCell(10).setCellValue(p.getLeads().getJenjang() +" - "+p.getKonsentrasi());
                row.createCell(11).setCellValue(p.getAlasanMemilihProdi());
                row.createCell(12).setCellValue(p.getLeads().getCreateDate().toString());
            for (Tagihan cekRegis : cekTagihanLunas) {
                if (cekRegis.getPendaftar().getId() == p.getId() && cekRegis.getLunas() == true) {
                    row.createCell(13).setCellValue("Lunas Registrasi");
                    row.createCell(14).setCellValue(cekRegis.getNilai().toString());
                } else  if (cekRegis.getPendaftar().getId() == p.getId() && cekRegis.getLunas() == false){
                    row.createCell(13).setCellValue("Belum Lunas Registrasi");
                    row.createCell(14).setCellValue("~");
                }
            }
            Iterable<HasilTest> hasilTest = hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAktifAndJadwalTestPendaftar(Status.AKTIF,p);

            for (HasilTest ht : hasilTest) {
                if (ht != null) {
                    row.createCell(15).setCellValue("Sudah Test + "+ ht.getGrade().getNama());
                } else if (ht == null) {
                    row.createCell(15).setCellValue("Belum Test");
                }
            }

            JenisBiaya jd = new JenisBiaya();
            jd.setId(AppConstants.JENIS_BIAYA_DAFTAR_ULANG);
            Iterable<Tagihan> cekDaftarUlang = tagihanDao.cekTagihanByPendaftar(jd,ta, p.getId());
            for (Tagihan cekDu : cekDaftarUlang) {
                if (cekDu != null && cekDu.getLunas() == true) {
                    row.createCell(16).setCellValue("Lunas Daftar Ulang");
                    row.createCell(17).setCellValue(cekDu.getNilai().toString());
                    System.out.println("cek pembayaran export excel : " + cekDu);
                    Pembayaran pembayaran = pembayaranDao.findTopByTagihanOrderByTagihanDesc(cekDu);
                    row.createCell(18).setCellValue(pembayaran.getWaktuPembayaran().toString());
                } else  if (cekDu != null && cekDu.getLunas() == false){
                    row.createCell(16).setCellValue("-");
                    row.createCell(17).setCellValue("-");
                    row.createCell(18).setCellValue("-");
                } else if (cekDu == null) {
                    row.createCell(16).setCellValue("-");
                    row.createCell(17).setCellValue("-");
                    row.createCell(18).setCellValue("-");
                }
            }
            row.createCell(19).setCellValue(p.getStatus());
            row.createCell(20).setCellValue(p.getNote());
            row.createCell(21).setCellValue(p.getNotePic());
            if (p.getFollowup() != null) {
                row.createCell(22).setCellValue(p.getFollowup().getNama());
            } else {
                row.createCell(22).setCellValue("-");
            }

            if (p.getUserUpdate() != null) {
                row.createCell(23).setCellValue(p.getUserUpdate().getUsername());
            } else {
                row.createCell(23).setCellValue("-");
            }

            if (p.getTglUpdate() != null) {
                row.createCell(24).setCellValue(p.getTglUpdate().toString());
            } else {
                row.createCell(24).setCellValue("-");
            }
            if (p.getLeads().getSubscribe() != null) {
                row.createCell(25).setCellValue(p.getLeads().getSubscribe().getSumber());

                LocalDateTime now = p.getLeads().getSubscribe().getTglInsert();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                String formatDateTime = now.format(formatter);
                row.createCell(26).setCellValue(formatDateTime);
            } else if (p.getLeads().getSubscribe() == null) {
                row.createCell(25).setCellValue("-");
                row.createCell(26).setCellValue("-");
            }
            if (p.getReferal() != null) {
                row.createCell(27).setCellValue(p.getReferal().getKodeReferal() +" - "+ p.getReferal().getNama());
            } else {
                row.createCell(27).setCellValue("-");
            }
            if (p.getKeteranganTagihan() != null) {
                row.createCell(28).setCellValue(p.getKeteranganTagihan());
            } else {
                row.createCell(28).setCellValue("-");
            }
            row.createCell(29).setCellValue(p.getNote());
            if (p.getLeads().getSubscribe() != null) {
                row.createCell(30).setCellValue(p.getLeads().getSubscribe().getLulusan());
            } else {
                row.createCell(30).setCellValue("-");
            }
            Iterable<PendaftarDetail> pd = pendaftarDetailDao.findByPendaftarAndPendaftarTahunAjaranAktif(p,Status.AKTIF);
            for (PendaftarDetail cekDetail : pd ) {
                if (cekDetail != null) {
                    row.createCell(31).setCellValue("Sudah Isi Detail");
                } else {
                    row.createCell(31).setCellValue("Belum Isi Detail");
                }
            }
            Iterable<Berkas> berkas = uploadBerkasDao.findByPendaftar(p);
            for (Berkas bkr : berkas) {
                if (bkr != null) {
                    row.createCell(32).setCellValue("Sudah Ada Berkas");
                } else {
                    row.createCell(32).setCellValue("Belum Ada Berkas");
                }
            }


        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }



        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=Pendaftar-"+ LocalDate.now() +".xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    @GetMapping("/rekapPerkelas/xlsx")
    public void rekapHumasXlsxPerkelas(@RequestParam(required = false) String  kelas,
                               HttpServletResponse response) throws Exception {

        String[] columns = {"No","Nomor Registrasi","Nama","Email","No Telephone","Kota Asal Sekolah","Asal Sekolah","Rekomendasi",
                "Nama Perekomendasi","Program Studi","Kelas","Alasan Memilih Jurusan","Tanggal Daftar","Registrasi","Nominal Regist","Test","Daftar Ulang", "Nominal DU", "Tanggal DU"
                ,"Status", "Catatan", "Catatan PIC", "User Followup", "User Update", "Tanggal Update","Sumber", "Tanggal Insert","Kode Voucher", "Beasiswa", "Note","Lulusan"};

        Iterable<Pendaftar> dataPendaftar = pendaftarDao.findByKonsentrasiAndTahunAjaranAktif(kelas,Status.AKTIF);

        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("Pendaftar");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1 ;
        int baris = 1 ;
        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
        Iterable<Tagihan> cekTagihanLunas = tagihanDao.cekTagihanLunas(ta);

        for (Pendaftar p : dataPendaftar) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(baris++);
            row.createCell(1).setCellValue(p.getNomorRegistrasi());
            row.createCell(2).setCellValue(p.getLeads().getNama());
            row.createCell(3).setCellValue(p.getLeads().getEmail());
            row.createCell(4).setCellValue(p.getLeads().getNoHp());
            row.createCell(5).setCellValue(p.getIdKabupatenKota().getNama());
            row.createCell(6).setCellValue(p.getNamaAsalSekolah());
            row.createCell(7).setCellValue(p.getPerekomendasi());
            row.createCell(8).setCellValue(p.getNamaPerekomendasi());
            row.createCell(9).setCellValue(p.getProgramStudi().getNama());
            row.createCell(10).setCellValue(p.getLeads().getJenjang() +" - "+p.getKonsentrasi());
            row.createCell(11).setCellValue(p.getAlasanMemilihProdi());
            row.createCell(12).setCellValue(p.getLeads().getCreateDate().toString());
            for (Tagihan cekRegis : cekTagihanLunas) {
                if (cekRegis.getPendaftar().getId() == p.getId() && cekRegis.getLunas() == true) {
                    row.createCell(13).setCellValue("Lunas Registrasi");
                    row.createCell(14).setCellValue(cekRegis.getNilai().toString());
                } else  if (cekRegis.getPendaftar().getId() == p.getId() && cekRegis.getLunas() == false){
                    row.createCell(13).setCellValue("Belum Lunas Registrasi");
                    row.createCell(14).setCellValue("~");
                }
            }
            Iterable<HasilTest> hasilTest = hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAktifAndJadwalTestPendaftar(Status.AKTIF,p);

            for (HasilTest ht : hasilTest) {
                if (ht != null) {
                    row.createCell(15).setCellValue("Sudah Test + "+ ht.getGrade().getNama());
                } else if (ht == null) {
                    row.createCell(15).setCellValue("Belum Test");
                }
            }

            JenisBiaya jd = new JenisBiaya();
            jd.setId(AppConstants.JENIS_BIAYA_DAFTAR_ULANG);
            Iterable<Tagihan> cekDaftarUlang = tagihanDao.cekTagihanByPendaftar(jd,ta, p.getId());
            for (Tagihan cekDu : cekDaftarUlang) {
                if (cekDu != null && cekDu.getLunas() == true) {
                    row.createCell(16).setCellValue("Lunas Daftar Ulang");
                    row.createCell(17).setCellValue(cekDu.getNilai().toString());
                    System.out.println("cek pembayaran rekap per kelas : " + cekDu);
                    Pembayaran pembayaran = pembayaranDao.findTopByTagihanOrderByTagihanDesc(cekDu);
                    row.createCell(18).setCellValue(pembayaran.getWaktuPembayaran().toString());
                } else  if (cekDu != null && cekDu.getLunas() == false){
                    row.createCell(16).setCellValue("-");
                    row.createCell(17).setCellValue("-");
                    row.createCell(18).setCellValue("-");
                } else if (cekDu == null) {
                    row.createCell(16).setCellValue("-");
                    row.createCell(17).setCellValue("-");
                    row.createCell(18).setCellValue("-");
                }
            }
            row.createCell(19).setCellValue(p.getStatus());
            row.createCell(20).setCellValue(p.getNote());
            row.createCell(21).setCellValue(p.getNotePic());
            if (p.getFollowup() != null) {
                row.createCell(22).setCellValue(p.getFollowup().getNama());
            } else {
                row.createCell(22).setCellValue("-");
            }

            if (p.getUserUpdate() != null) {
                row.createCell(23).setCellValue(p.getUserUpdate().getUsername());
            } else {
                row.createCell(23).setCellValue("-");
            }

            if (p.getTglUpdate() != null) {
                row.createCell(24).setCellValue(p.getTglUpdate().toString());
            } else {
                row.createCell(24).setCellValue("-");
            }
            if (p.getLeads().getSubscribe() != null) {
                row.createCell(25).setCellValue(p.getLeads().getSubscribe().getSumber());

                LocalDateTime now = p.getLeads().getSubscribe().getTglInsert();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                String formatDateTime = now.format(formatter);
                row.createCell(26).setCellValue(formatDateTime);

                if (p.getLeads().getSubscribe().getLulusan() == null) {
                    row.createCell(30).setCellValue("-");
                } else {
                    row.createCell(30).setCellValue(p.getLeads().getSubscribe().getLulusan());
                }

            } else if (p.getLeads().getSubscribe() == null) {
                row.createCell(25).setCellValue("-");
                row.createCell(26).setCellValue("-");
            }
            if (p.getReferal() != null) {
                row.createCell(27).setCellValue(p.getReferal().getKodeReferal() +" - "+ p.getReferal().getNama());
            } else {
                row.createCell(27).setCellValue("-");
            }
            if (p.getKeteranganTagihan() != null) {
                row.createCell(28).setCellValue(p.getKeteranganTagihan());
            } else {
                row.createCell(28).setCellValue("-");
            }
            row.createCell(29).setCellValue(p.getNote());

        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }



        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=Pendaftar-"+ LocalDate.now() +".xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();
    }

//Pascasarjana

    @RequestMapping("/pendaftar/listS2")
    public void listPendaftarS2(@RequestParam(required = false)String search, Model m,
                          @PageableDefault(size = 10, sort = "nomorRegistrasi", direction = Sort.Direction.DESC)Pageable page){
        Page<Pendaftar> pendaftar = pendaftarDao.findByTahunAjaranAktifAndLeadsJenjang(Status.AKTIF,Jenjang.S2,page);

        if(StringUtils.hasText(search)) {
            TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
            Page<Pendaftar> ps= pendaftarDao.findByNomorRegistrasiContainingIgnoreCaseOrLeadsNamaContainingIgnoreCaseAndTahunAjaranAndLeadsJenjangOrderByNomorRegistrasi(search,search,ta,Jenjang.S2,page);
            m.addAttribute("search", search);
            m.addAttribute("daftarPendaftar", ps );

            List<JadwalTest> jadwalTests = new ArrayList<>();
            for (Pendaftar pe : ps) {
                JadwalTest jadwalTest = jadwalTestDao.findByPendaftar(pe);
                if (jadwalTest != null) {
                    jadwalTests.add(jadwalTest);
                }
            }
            if (jadwalTests != null) {
                m.addAttribute("jadwalTest", jadwalTests);
            }

            List<HasilTest> hasilTests = new ArrayList<>();
            for (JadwalTest jt : jadwalTests) {
                HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
                if (hasilTest != null) {
                    hasilTests.add(hasilTest);
                }
            }
            if (hasilTests != null) {
                m.addAttribute("hasilTest", hasilTests);
            }

            List<ValidasiTagihan> pendaftarDetails = new ArrayList<>();
            for (Pendaftar pe : ps) {
                PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetail != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetail == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetails.add(validasiTagihan);

            }
            m.addAttribute("detail", pendaftarDetails);

            List<ValidasiTagihan> pendaftarDetailPasca = new ArrayList<>();
            for (Pendaftar pe : ps) {
                PendaftarDetailPasca pendaftarDetailPascas = pendaftarDetailPascaDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetailPascas != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetailPascas == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetailPasca.add(validasiTagihan);

            }
            m.addAttribute("detailS2", pendaftarDetailPasca);



            JenisBiaya jenisBiaya = new JenisBiaya();
            jenisBiaya.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
            if (jenisBiaya == null) {
                logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());

            }
            List<ValidasiTagihan> tagihans = new ArrayList<>();
            for (Pendaftar pe : ps) {
                Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiaya(pe, jenisBiaya);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (tagihan != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (tagihan == null){
                    validasiTagihan.setStatus("Kosong");
                }
                tagihans.add(validasiTagihan);
            }

            m.addAttribute("tagihan", tagihans);

            List<Berkas> berkas = new  ArrayList<>();

            List<Berkas> berkas1 = uploadBerkasDao.findPendaftar();
//            berkas.add(berkas1);


            System.out.println("Berkas :"+ berkas1);
            m.addAttribute("berkas", berkas1);


        } else {
            m.addAttribute("daftarPendaftar", pendaftar);
            List<JadwalTest> jadwalTests = new ArrayList<>();
            for (Pendaftar pe : pendaftar) {
                JadwalTest jadwalTest = jadwalTestDao.findByPendaftar(pe);
                if (jadwalTest != null) {
                    jadwalTests.add(jadwalTest);
                }
            }
            if (jadwalTests != null) {
                m.addAttribute("jadwalTest", jadwalTests);
            }

            List<HasilTest> hasilTests = new ArrayList<>();
            for (JadwalTest jt : jadwalTests) {
                HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
                if (hasilTest != null) {
                    hasilTests.add(hasilTest);
                }
            }
            if (hasilTests != null) {
                m.addAttribute("hasilTest", hasilTests);
            }

            List<ValidasiTagihan> pendaftarDetails = new ArrayList<>();
            for (Pendaftar pe : pendaftar) {
                PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetail != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetail == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetails.add(validasiTagihan);

            }
            m.addAttribute("detail", pendaftarDetails);

            List<ValidasiTagihan> pendaftarDetailPasca = new ArrayList<>();
            for (Pendaftar pe : pendaftar) {
                PendaftarDetailPasca pendaftarDetailPascas = pendaftarDetailPascaDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetailPascas != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetailPascas == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetailPasca.add(validasiTagihan);

            }
            m.addAttribute("detailS2", pendaftarDetailPasca);

            JenisBiaya jenisBiaya = new JenisBiaya();
            jenisBiaya.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
            if (jenisBiaya == null) {
                logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());

            }
            List<ValidasiTagihan> tagihans = new ArrayList<>();
            for (Pendaftar pe : pendaftar) {
                Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiaya(pe, jenisBiaya);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (tagihan != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (tagihan == null){
                    validasiTagihan.setStatus("Kosong");
                }
                tagihans.add(validasiTagihan);
            }

            m.addAttribute("tagihan", tagihans);

            List<Berkas> berkas = new  ArrayList<>();

            List<Berkas> berkas1 = uploadBerkasDao.findPendaftar();
//            berkas.add(berkas1);

            System.out.println("Berkas :"+ berkas1);
            m.addAttribute("berkas", berkas1);

        }
    }

    @GetMapping("/pendaftar/xlsxS2")
    public void rekappascaXlsx(HttpServletResponse response) throws Exception {

        String[] columns = {"No","Nomor Registrasi","Nama","Email","No Telephone","Kota Asal Sekolah","Asal Sekolah","Rekomendasi",
                "Nama Perekomendasi","Program Studi","Konsentrasi", "Alasan Memilih Jurusan","Tanggal Daftar","Sumber","Tanggal Insert", "Kode Voucher", "Beasiswa", "Note" };

        Iterable<Pendaftar> dataPendaftar = pendaftarDao.findByTahunAjaranAktifAndLeadsJenjang(Status.AKTIF,Jenjang.S2);

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Pendaftar 2021");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1 ;
        int baris = 1 ;

        for (Pendaftar p : dataPendaftar) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(baris++);
            row.createCell(1).setCellValue(p.getNomorRegistrasi());
            row.createCell(2).setCellValue(p.getLeads().getNama());
            row.createCell(3).setCellValue(p.getLeads().getEmail());
            row.createCell(4).setCellValue(p.getLeads().getNoHp());
            row.createCell(5).setCellValue(p.getIdKabupatenKota().getNama());
            row.createCell(6).setCellValue(p.getNamaAsalSekolah());
            row.createCell(7).setCellValue(p.getPerekomendasi());
            row.createCell(8).setCellValue(p.getNamaPerekomendasi());
            row.createCell(9).setCellValue(p.getProgramStudi().getNama());
            row.createCell(10).setCellValue(p.getKonsentrasi());
            row.createCell(11).setCellValue(p.getAlasanMemilihProdi());
            row.createCell(12).setCellValue(p.getLeads().getCreateDate().toString());
            if (p.getLeads().getSubscribe() != null) {
                row.createCell(13).setCellValue(p.getLeads().getSubscribe().getSumber());

                LocalDateTime now = p.getLeads().getSubscribe().getTglInsert();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                String formatDateTime = now.format(formatter);
                row.createCell(14).setCellValue(formatDateTime);
            } else if (p.getLeads().getSubscribe() == null) {
                row.createCell(13).setCellValue("-");
                row.createCell(14).setCellValue("-");
            }
            if (p.getReferal() != null) {
                row.createCell(15).setCellValue(p.getReferal().getKodeReferal() +" - "+ p.getReferal().getNama());
            } else {
                row.createCell(15).setCellValue("-");
            }
            if (p.getKeteranganTagihan() != null) {
                row.createCell(16).setCellValue(p.getKeteranganTagihan());
            } else {
                row.createCell(16).setCellValue("-");
            }
            row.createCell(17).setCellValue(p.getNote());

        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }



        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=Pendaftar-S2-"+ LocalDate.now() +".xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    @PostMapping("/pendaftar/updateTagihan")
    public String updateTagihanPendftar (@RequestParam(required = false) String id,String uangPangkal, String keteranganTagihan, String expiredDate
            , Authentication currentUser){
        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }

        Pendaftar pendaftar = pendaftarDao.findById(id).get();
        pendaftar.setUangPangkal(new BigDecimal(uangPangkal));
        pendaftar.setKeteranganTagihan(keteranganTagihan);
        LocalDate ex = LocalDate.parse(expiredDate);
        pendaftar.setExpiredDate(ex);
        pendaftar.setUserUpdate(u);
        pendaftarDao.save(pendaftar);

        return "redirect:/pendaftar/list";
    }
}

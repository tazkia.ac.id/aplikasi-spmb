package id.ac.tazkia.spmb.aplikasispmb.service;

import id.ac.tazkia.spmb.aplikasispmb.constants.AppConstants;
import id.ac.tazkia.spmb.aplikasispmb.controller.TagihanController;
import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.dto.LeadsPendaftarDto;
import id.ac.tazkia.spmb.aplikasispmb.dto.NotifRegistrasiReferalDto;
import id.ac.tazkia.spmb.aplikasispmb.dto.PendaftarDto;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import jakarta.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service @Transactional
public class PendaftarService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PendaftarService.class);

    @Autowired private PendaftarDao pendaftarDao;
    @Autowired private TahunAjaranDao tahunAjaranDao;
    @Autowired private RunningNumberService runningNumberService;
    @Autowired private TagihanService tagihanService;
    @Autowired private ReferalDao referalDao;
    @Autowired private UserPasswordDao userPasswordDao;
    @Autowired private UserDao userDao;
    @Autowired private RoleDao roleDao;
    @Autowired private LeadsDao leadsDao;
    @Autowired private PasswordEncoder passwordEncoder;
    @Autowired private KabupatenKotaDao kabupatenKotaDao;
    @Autowired private ProgramStudiDao programStudiDao;
    @Autowired private  ApiService apiService;
    @Autowired private TagihanController tagihanController;
    @Autowired
    private NotifikasiService notifikasiService;


//ApiSobatSyariah
    @Value("${group.pendaftar}") private String  insertGroupPendaftar;
    @Value("${group.leads}") private String deleteGroupLeads;
    public void prosesLeadsPendaftar(LeadsPendaftarDto lpDto){
//Save Leads
        Leads leads = new Leads();
        User cekLeads = userDao.findByUsername(lpDto.getEmail());
        if (cekLeads == null) {
            //CreateUser&Password
            User user = new User();
            Role role = roleDao.findById(AppConstants.ROLE_PENDAFTAR).get();

            user.setUsername(lpDto.getEmail());
            user.setActive(true);
            user.setRole(role);
            userDao.save(user);

            UserPassword password = new UserPassword();
            password.setUser(user);
            password.setPassword(passwordEncoder.encode(lpDto.getPassword()));
            userPasswordDao.save(password);
            //

            TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
            leads.setTahunAjaran(ta);
            leads.setUser(user);

            BeanUtils.copyProperties(lpDto, leads);
            leadsDao.save(leads);

            LOGGER.info("Leads a.n {} Berhasiil di simpan", leads.getNama());
        } else {
            LOGGER.info("Username a.n {} sudah ada", cekLeads.getUsername());

        }
//Save Pendaftar & Kirim Tagihan

        KabupatenKota kk = kabupatenKotaDao.findById(lpDto.getIdKabupatenKota()).get();
        ProgramStudi prodi = programStudiDao.findById(lpDto.getProgramStudi()).get();

        Pendaftar pe = new Pendaftar();
        pe.setProgramStudi(prodi);
        pe.setKonsentrasi(lpDto.getKonsentrasi());
        System.out.println("Konsentarsi : " + lpDto.getKonsentrasi());

        pe.setIdKabupatenKota(kk);

        pe.setLeads(leads);
        pe.setNamaAsalSekolah(lpDto.getNamaAsalSekolah());
        pe.setPerekomendasi("Sobat Syariah");
        pe.setNamaPerekomendasi("Sobat Syariah");
        pe.setAlasanMemilihProdi(lpDto.getAlasanMemilihProdi());

        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
        pe.setTahunAjaran(ta);
        pe.setNomorRegistrasi(generateNomorRegistrasi());
//        tagihanService.prosesTagihanPendaftaran(pe);
        pendaftarDao.save(pe);
//        apiService.insertSubcriber(leads);
//        apiService.insertGroup(leads, insertGroupPendaftar);



    }

    public void  prosesPendaftar(PendaftarDto pd, ProgramStudi ps, KabupatenKota kk){
        Pendaftar pe = new Pendaftar();
        pe.setProgramStudi(ps);
        pe.setIdKabupatenKota(kk);

        BeanUtils.copyProperties(pd, pe);
        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
        pe.setTahunAjaran(ta);
        if (pd.getLeads().getSubscribe() != null) {
            pe.setFollowup(pd.getLeads().getSubscribe().getFollowup());
        }
        pe.setKonsentrasi(pd.getLeads().getKelas());
        if (!StringUtils.hasText(pd.getId())) {
            LOGGER.debug("Pendaftar Belum ada");

            Referal cekReferal = referalDao.findAllByKodeReferal(pd.getKodeReferal());

            if (pd.getKodeReferal() != null && cekReferal != null) {
                Referal referal = referalDao.findAllByKodeReferal(pd.getKodeReferal());
                if (referal.getProdi().getId().equals(pd.getProgramStudi()) && referal != null && referal.getStatus() == true) {
                    pe.setReferal(referal);
                    LOGGER.info("Kode referal ditemukan dengan id & prod {}", referal, referal.getProdi());
                } else if (!referal.getProdi().getId().equals(pd.getProgramStudi()) && referal.getProdi().getId().equals("000") && referal != null && referal.getStatus() == true) {
                    pe.setReferal(referal);
                    LOGGER.info("Kode referal Semua ditemukan dengan id {} & prods {}", referal, referal.getProdi());
                } else if (!referal.getProdi().getId().equals(pd.getProgramStudi()) && referal.getProdi().getId().equals("991")
                        && Integer.parseInt(pd.getProgramStudi()) > 007
                        && referal != null && referal.getStatus() == true) {
                    pe.setReferal(referal);
                    LOGGER.info("Kode referal S2 ditemukan dengan id {} & prods {}", referal, referal.getProdi());
                } else if ( referal.getProdi().getId().equals("999") && !referal.getProdi().getId().equals(pd.getProgramStudi())
                        && Integer.parseInt(pd.getProgramStudi()) <= 007
                        && referal != null && referal.getStatus() == true) {
                    pe.setReferal(referal);
                    LOGGER.info("Kode referal S1 ditemukan dengan id {} & prods {}", referal, referal.getProdi());}
                else {
                    pe.setReferal(null);
                    LOGGER.info("Kode referal {} tidak sesuai", pd.getKodeReferal());
                }
            } else {
                pe.setReferal(null);
                LOGGER.info("Kode referal {} tidak ditemukan", pd.getKodeReferal());
            }


            pe.setNomorRegistrasi(generateNomorRegistrasi());

            pendaftarDao.save(pe);
//            apiService.insertGroup(pe.getLeads(), insertGroupPendaftar);
//            apiService.deleteGroup(pe.getLeads(), deleteGroupLeads);
//            if (pe.getReferal() != null) {
//            }
//

            LOGGER.info("Pendaftar Dengan No Registrasi {} berhasil disimpan", pe.getNomorRegistrasi());
            tagihanService.prosesTagihanPendaftaran(pe);
            tagihanService.createTagihanRegistrasi(pe);

        }
        else {
            LOGGER.debug("Pendaftar sudah ada");
            pe.setNomorRegistrasi(pd.getNomorRegistrasi());
            pendaftarDao.save(pe);
        }
    }

    public String generateNomorRegistrasi(){
        String tanggalSekarang = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMM"));
        RunningNumber terbaru = runningNumberService.generate(tanggalSekarang);

        LOGGER.debug("Tanggal Sekarang : {}", tanggalSekarang);
        LOGGER.debug("Nomer Terakhir : {}", terbaru.getNomerTerakhir());

        String nomorRegistrasi = tanggalSekarang + String.format("%04d", terbaru.getNomerTerakhir());
        LOGGER.debug("Nomor Registrasi : {}", nomorRegistrasi);

        return nomorRegistrasi;
    }
}

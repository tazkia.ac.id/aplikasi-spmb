package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.Sekolah;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SekolahDao extends PagingAndSortingRepository<Sekolah, String>, CrudRepository<Sekolah, String> {
    Page<Sekolah> findByNamaContainingIgnoreCaseOrderByNama(String nama, Pageable page);
}

package id.ac.tazkia.spmb.aplikasispmb.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DataResetPassword {
    private String code;
    private String email;
    private String nama;
}

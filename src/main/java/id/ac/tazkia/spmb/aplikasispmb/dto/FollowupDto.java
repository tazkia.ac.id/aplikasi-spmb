package id.ac.tazkia.spmb.aplikasispmb.dto;

import id.ac.tazkia.spmb.aplikasispmb.entity.Status;
import lombok.Data;

@Data
public class FollowupDto {
    private String id;
    private String nama;
    private String username;
    private String pasword;
    private String noWhatsapp;
    private String jumlah;
    private Status status;
    private Status s1;
    private Status s2;
    private Status kk;
}

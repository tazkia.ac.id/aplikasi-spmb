package id.ac.tazkia.spmb.aplikasispmb.dto;

import id.ac.tazkia.spmb.aplikasispmb.entity.Leads;
import lombok.Data;

@Data
public class DaftarSmartTestDto {
    private String idHasilTest;
    private String nama;
    private String email;
    private String noHp;
    private String password;

    private String nomorRegistrasi;
    private String idKabupatenKota;
    private String namaAsalSekolah;
    private String programStudi;
    private String konsentrasi;
    private String perekomendasi;
    private String namaPerekomendasi;
    private String KodeReferal;
    private String alasanMemilihProdi;

}

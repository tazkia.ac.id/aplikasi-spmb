package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.constants.AppConstants;
import id.ac.tazkia.spmb.aplikasispmb.dao.ResetPasswordDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.UserDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.UserPasswordDao;
import id.ac.tazkia.spmb.aplikasispmb.entity.ResetPassword;
import id.ac.tazkia.spmb.aplikasispmb.entity.User;
import id.ac.tazkia.spmb.aplikasispmb.entity.UserPassword;
import id.ac.tazkia.spmb.aplikasispmb.service.NotifikasiService;
import jakarta.validation.Valid;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.util.Optional;

@Controller
public class ResetPasswordController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResetPasswordController.class);

    @Autowired
    private UserDao userDao;
    @Autowired
    private ResetPasswordDao resetPasswordDao;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private NotifikasiService notifikasiService;
    @Autowired
    private UserPasswordDao userPasswordDao;


    @GetMapping("/reset")
    public void  resetPassword(){}

    @PostMapping(value = "/reset")
    public String reset(@Valid @RequestParam String user, RedirectAttributes redirectAttributes){
        User u = userDao.findByUsername(user);

        if(u == null){
            LOGGER.info("Reset password untuk username belum terdaftar : {}", user);
            redirectAttributes.addFlashAttribute("user", user);
            return "redirect:reset_gagal";
        }


        ResetPassword rp = resetPasswordDao.findByUser(u);

        if(rp == null){
            rp = new ResetPassword();
            rp.setUser(u);
        }

        rp.setCode(RandomStringUtils.randomAlphabetic(6));
        rp.setExpired(LocalDate.now().plusDays(3));

        resetPasswordDao.save(rp);

        if (rp.getUser().getRole().getId().equals(AppConstants.ROLE_PENDAFTAR)) {
            notifikasiService.resetPassword(rp);
        }
        redirectAttributes.addFlashAttribute("user", user);
        return "redirect:reset_sukses";
    }

    @GetMapping(value = "/reset_sukses")
    public void resetSukses(){}
    @GetMapping(value = "/reset_gagal")
    public void resetGagal(){}

    @GetMapping(value = "/confirm")
    public String confirm(@RequestParam String code, Model m) {
        ResetPassword resetPassword = resetPasswordDao.findByCode(code);

        if (resetPassword == null){
            return "redirect:/404";
        }

        if (code != null && !code.isEmpty()){
            Optional<UserPassword> userPassword= userPasswordDao.findById(resetPassword.getUser().getId());
            if (userPassword != null){
                m.addAttribute("confirm", userPassword);
            }

        }

        if(resetPassword.getExpired().isBefore(LocalDate.now())){
            return "redirect:404";
        }

        m.addAttribute("code", code);
        return "confirm";
    }


    @PostMapping("/confirm")
    public String updatePassword(@RequestParam String code,
                                 @RequestParam String password){


        ResetPassword resetPassword = resetPasswordDao.findByCode(code);


        UserPassword up = userPasswordDao.findByUser(resetPassword.getUser());
        if(up == null){
            LOGGER.info("User tidak ditemukan :" + up);
            up = new UserPassword();
            up.setUser(resetPassword.getUser());
        }
        up.setPassword(passwordEncoder.encode(password));


        userPasswordDao.save(up);
        return "redirect:login";


    }
}

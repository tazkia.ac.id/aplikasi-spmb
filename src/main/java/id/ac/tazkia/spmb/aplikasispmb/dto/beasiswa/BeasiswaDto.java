package id.ac.tazkia.spmb.aplikasispmb.dto.beasiswa;

import groovyjarjarantlr4.v4.runtime.misc.NotNull;
import lombok.Data;

@Data
public class BeasiswaDto {

    private String namaLengkap;

    private String noTelpon;

    private String email;

    private String password;

    private String lulusan;

    private String kotaAsalSekolah;

    private String namaSekolah;

    private String tahunLulus;

    private String statusOrtu;

}

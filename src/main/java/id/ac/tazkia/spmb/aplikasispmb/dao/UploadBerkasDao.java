package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.Berkas;
import id.ac.tazkia.spmb.aplikasispmb.entity.Pendaftar;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface UploadBerkasDao extends PagingAndSortingRepository<Berkas, String>, CrudRepository<Berkas, String> {

    Page<Berkas> findByPendaftarOrderByJenisBerkas(Pendaftar p, Pageable page);
    List<Berkas> findByPendaftar(Pendaftar p);

    @Query(value = "SELECT * from berkas b  group by b.id_pendaftar;", nativeQuery = true)
    List<Berkas> findPendaftar();
}

package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import id.ac.tazkia.spmb.aplikasispmb.constants.AppConstants;
import id.ac.tazkia.spmb.aplikasispmb.entity.JenisBiaya;
import id.ac.tazkia.spmb.aplikasispmb.entity.JenisTest;
import id.ac.tazkia.spmb.aplikasispmb.entity.Jenjang;
import id.ac.tazkia.spmb.aplikasispmb.entity.ProgramStudi;
import id.ac.tazkia.spmb.aplikasispmb.entity.Role;
import id.ac.tazkia.spmb.aplikasispmb.entity.Status;
import id.ac.tazkia.spmb.aplikasispmb.entity.TahunAjaran;
import id.ac.tazkia.spmb.aplikasispmb.entity.User;

@Controller
public class DashboardController {
    private static final Logger logger = LoggerFactory.getLogger(DashboardController.class);

    @Autowired private UserDao userDao;
    @Autowired private RoleDao roleDao;
    @Autowired private TahunAjaranDao tahunAjaranDao;
    @Autowired private PendaftarDao pendaftarDao;
    @Autowired private LeadsDao leadsDao;
    @Autowired private PendaftarDetailDao pendaftarDetailDao;
    @Autowired private PendaftarDetailPascaDao pendaftarDetailPascaDao;
    @Autowired private PembayaranDao pembayaranDao;
    @Autowired private HasilTestDao hasilTestDao;
    @Autowired private ProgramStudiDao programStudiDao;
    @Autowired private SubscribeDao subscribeDao;
    @Autowired private TagihanDao tagihanDao;


    @GetMapping("/dashboard")
    public String dashboard(Authentication currentUser, Model model, Pageable page){
        logger.debug("Authentication class : {}",currentUser.getClass().getName());

        if(currentUser == null){
            logger.warn("Current user is null");
        }

        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }

        Role role = roleDao.findById(AppConstants.ROLE_PENDAFTAR).get();
        Role beasiswa = roleDao.findById(AppConstants.ROLE_BEASISWA).get();
        Role adminbeasiswa = roleDao.findById(AppConstants.ROLE_ADMIN_BEASISWA).get();

        if (u.getRole() == role ){
            return "redirect:/tagihan/listPendaftar";
        } else if (u.getRole() == beasiswa) {

            return "redirect:/dashboardBeasiswa";
        } else if (u.getRole() == adminbeasiswa) {
            return "redirect:/dashboardAdminbeasiswa";
        } else{
            TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
            model.addAttribute("tahunAjaran", ta);
            model.addAttribute("cS1Leads", subscribeDao.countLeadsByTahunAjaranAndKelasAndSumberNotAndJenjang(ta, "Reguler","spmb.tazkia.ac.id", Jenjang.S1));
            model.addAttribute("cS1Pendaftar", pendaftarDao.countPendaftarByLeadsJenjangAndTahunAjaranAndKonsentrasi(Jenjang.S1,ta, "Reguler"));
            model.addAttribute("cS1BelumDaftar", leadsDao.countLeadsByTahunAjaranAndJenjangAndKelas(ta, Jenjang.S1, "Reguler") -   pendaftarDao.countPendaftarByLeadsJenjangAndTahunAjaranAndKonsentrasi(Jenjang.S1,ta, "Reguler"));
            model.addAttribute("cS1Detail", pendaftarDetailDao.countByPendaftarTahunAjaran(ta.getId(), "Reguler"));
            model.addAttribute("cS1BelumDetail", pembayaranDao.hitungDUJenByKelas(ta.getId(),  "Reguler") - pendaftarDetailDao.countByPendaftarTahunAjaran(ta.getId(), "Reguler"));

            JenisBiaya jb = new JenisBiaya();
            jb.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
            model.addAttribute("cS1Pembayaran", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarKonsentrasi(jb, ta, Jenjang.S1, "Reguler"));
            model.addAttribute("cS1BelumBayar", pendaftarDao.countPendaftarByLeadsJenjangAndTahunAjaranAndKonsentrasi(Jenjang.S1,ta, "Reguler") - pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarKonsentrasi(jb, ta, Jenjang.S1, "Reguler"));

            model.addAttribute("cS1SudahTest", pembayaranDao.hitungYangSudahTestByKelas(ta.getId(), "Reguler"));
            model.addAttribute("cS1BelumTest", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarKonsentrasi(jb, ta, Jenjang.S1, "Reguler") - pembayaranDao.hitungYangSudahTestByKelas(ta.getId(), "Reguler"));

            JenisBiaya jbD = new JenisBiaya();
            jbD.setId(AppConstants.JENIS_BIAYA_DAFTAR_ULANG);
            model.addAttribute("cDu1Reg", tagihanDao.itungBerdasarkanStatus(ta.getId(),"Reguler","Close Win - Sudah Lunas"));
            model.addAttribute("cDu1Bea",tagihanDao.itungBerdasarkanStatus(ta.getId(),"Reguler","Beasiswa Daerah Close Win"));
            model.addAttribute("cDu1Mengundurkan",tagihanDao.itungBerdasarkanStatus(ta.getId(),"Reguler","Close Lose - Mengundurkan Diri"));
            model.addAttribute("cDu1Dp",tagihanDao.itungBerdasarkanStatus(ta.getId(),"Reguler","Close Win - Sudah Dp"));
            model.addAttribute("cBelumDu1",pembayaranDao.hitungYangSudahTestByKelas(ta.getId(), "Reguler")-pembayaranDao.hitungDUJenByKelas(ta.getId(), "Reguler"));
//Kelas Karyawan
            model.addAttribute("cS1LeadsK", subscribeDao.countLeadsByTahunAjaranAndKelasAndSumberNotAndJenjang(ta, "Karyawan", "spmb.tazkia.ac.id", Jenjang.S1));
            model.addAttribute("cS1PendaftarK", pendaftarDao.countPendaftarByLeadsJenjangAndTahunAjaranAndKonsentrasi(Jenjang.S1,ta, "Karyawan"));
            model.addAttribute("cS1BelumDaftarK", leadsDao.countLeadsByTahunAjaranAndJenjangAndKelas(ta, Jenjang.S1, "Karyawan") -   pendaftarDao.countPendaftarByLeadsJenjangAndTahunAjaranAndKonsentrasi(Jenjang.S1,ta, "Karyawan"));
            model.addAttribute("cS1DetailK", pendaftarDetailDao.countByPendaftarTahunAjaran(ta.getId(), "Karyawan"));
            model.addAttribute("cS1BelumDetailK", pembayaranDao.hitungDUJenByKelas(ta.getId(),  "Karyawan") - pendaftarDetailDao.countByPendaftarTahunAjaran(ta.getId(), "Karyawan"));


            model.addAttribute("cS1PembayaranK", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarKonsentrasi(jb, ta, Jenjang.S1, "Karyawan"));
            model.addAttribute("cS1BelumBayarK", pendaftarDao.countPendaftarByLeadsJenjangAndTahunAjaranAndKonsentrasi(Jenjang.S1,ta, "Karyawan") - pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarKonsentrasi(jb, ta, Jenjang.S1, "Karyawan"));

            model.addAttribute("cS1SudahTestK", pembayaranDao.hitungYangSudahTestByKelas(ta.getId(), "Karyawan"));
            model.addAttribute("cS1BelumTestK", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarKonsentrasi(jb, ta, Jenjang.S1, "Karyawan") - pembayaranDao.hitungYangSudahTestByKelas(ta.getId(), "Karyawan"));

            model.addAttribute("cDu1K", pembayaranDao.hitungDUJenByKelas(ta.getId(), "Karyawan"));
            model.addAttribute("cBelumDu1K",pembayaranDao.hitungYangSudahTestByKelas(ta.getId(), "Karyawan")-pembayaranDao.hitungDUJenByKelas(ta.getId(), "Karyawan"));

            model.addAttribute("cDu1KKMengundurkan",tagihanDao.itungBerdasarkanStatus(ta.getId(),"Karyawan","Close Lose - Mengundurkan Diri"));

//PascaSarjana
            model.addAttribute("cS2Leads", subscribeDao.countLeadsByTahunAjaranAndSumberNotAndJenjang(ta,"spmb.tazkia.ac.id",Jenjang.S2));
            model.addAttribute("cS2Pendaftar", pendaftarDao.countPendaftarByLeadsJenjangAndTahunAjaran(Jenjang.S2,ta));
            model.addAttribute("cS2BelumDaftar", leadsDao.countLeadsByTahunAjaranAndJenjang(ta, Jenjang.S2) -   pendaftarDao.countPendaftarByLeadsJenjangAndTahunAjaran(Jenjang.S2,ta));
            model.addAttribute("cS2Detail", pendaftarDetailPascaDao.countByPendaftarTahunAjaran(ta.getId()));
            model.addAttribute("cS2BelumDetail", pembayaranDao.hitungYangSudahTest(ta.getId(), "S2") - pendaftarDetailPascaDao.countByPendaftarTahunAjaran(ta.getId()));

            model.addAttribute("cS2Pembayaran", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjang(jb, ta, Jenjang.S2));
            model.addAttribute("cS2BelumBayar", pendaftarDao.countPendaftarByLeadsJenjangAndTahunAjaran(Jenjang.S2,ta) - pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjang(jb, ta, Jenjang.S2));

            model.addAttribute("cS2SudahTest", pembayaranDao.hitungYangSudahTest(ta.getId(), "S2"));
            model.addAttribute("cS2BelumTest", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjang(jb, ta, Jenjang.S2) - pembayaranDao.hitungYangSudahTest(ta.getId(), "S2"));

            model.addAttribute("cDu2", pembayaranDao.hitungDUJen(ta.getId(), "S2"));
            model.addAttribute("cBelumDu2",pendaftarDetailPascaDao.countByPendaftarTahunAjaran(ta.getId()) -pembayaranDao.hitungDUJen(ta.getId(), "S2"));

            model.addAttribute("cDu1S2Mengundurkan",tagihanDao.itungBerdasarkanStatusS2(ta.getId(),"Close Lose - Mengundurkan Diri"));


            ProgramStudi pr1 = new ProgramStudi();
            pr1.setId("001");
            model.addAttribute("ikhwanDuEs", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"PRIA", pr1, "Reguler"));
            model.addAttribute("akhwatDuEs", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"WANITA", pr1, "Reguler"));

            model.addAttribute("jpaEs", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr1,JenisTest.JPA, "Reguler"));
            model.addAttribute("tpaEs", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr1,JenisTest.TPA, "Reguler"));
            model.addAttribute("smrtEs", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr1,JenisTest.SMART_TEST, "Reguler"));

            model.addAttribute("jmlhPeEs", pendaftarDao.countPendaftarByProgramStudiAndTahunAjaranAndKonsentrasi(pr1,ta, "Reguler"));

            model.addAttribute("regisEs", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jb, ta, Jenjang.S1, pr1, "Reguler"));

            model.addAttribute("duEs",pembayaranDao.hitungDUJenisTestKonsentrasi(ta.getId(),pr1.getId(), "Reguler"));

            ProgramStudi pr2 = new ProgramStudi();
            pr2.setId("002");
            model.addAttribute("ikhwanDuAs", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"PRIA", pr2, "Reguler"));
            model.addAttribute("akhwatDuAs", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"WANITA", pr2, "Reguler"));

            model.addAttribute("jpaAs", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr2,JenisTest.JPA, "Reguler"));
            model.addAttribute("tpaAs", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr2,JenisTest.TPA, "Reguler"));
            model.addAttribute("smrtAs", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr2,JenisTest.SMART_TEST, "Reguler"));

            model.addAttribute("jmlhPeAs", pendaftarDao.countPendaftarByProgramStudiAndTahunAjaranAndKonsentrasi(pr2,ta, "Reguler"));

            model.addAttribute("regisAs", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jb, ta, Jenjang.S1, pr2, "Reguler"));

            model.addAttribute("duAs",pembayaranDao.hitungDUJenisTestKonsentrasi(ta.getId(),pr2.getId(), "Reguler"));

            ProgramStudi pr3 = new ProgramStudi();
            pr3.setId("003");
            model.addAttribute("ikhwanDuMb", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"PRIA", pr3, "Reguler"));
            model.addAttribute("akhwatDuMb", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"WANITA", pr3, "Reguler"));

            model.addAttribute("jpaMb", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr3,JenisTest.JPA, "Reguler"));
            model.addAttribute("tpaMb", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr3,JenisTest.TPA, "Reguler"));
            model.addAttribute("smrtMb", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr3,JenisTest.SMART_TEST, "Reguler"));

            model.addAttribute("jmlhPeMb", pendaftarDao.countPendaftarByProgramStudiAndTahunAjaranAndKonsentrasi(pr3,ta, "Reguler"));

            model.addAttribute("regisMb", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jb, ta, Jenjang.S1, pr3, "Reguler"));

            model.addAttribute("duMb",pembayaranDao.hitungDUJenisTestKonsentrasi(ta.getId(),pr3.getId(), "Reguler"));

            ProgramStudi pr4 = new ProgramStudi();
            pr4.setId("004");
            model.addAttribute("ikhwanDuHu", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"PRIA", pr4, "Reguler"));
            model.addAttribute("akhwatDuHu", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"WANITA", pr4, "Reguler"));

            model.addAttribute("jpaHu", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr4,JenisTest.JPA, "Reguler"));
            model.addAttribute("tpaHu", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr4,JenisTest.TPA, "Reguler"));
            model.addAttribute("smrtHu", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr4,JenisTest.SMART_TEST, "Reguler"));

            model.addAttribute("jmlhPeHu", pendaftarDao.countPendaftarByProgramStudiAndTahunAjaranAndKonsentrasi(pr4,ta, "Reguler"));

            model.addAttribute("regisHu", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jb, ta, Jenjang.S1, pr4, "Reguler"));

            model.addAttribute("duHu",pembayaranDao.hitungDUJenisTestKonsentrasi(ta.getId(),pr4.getId(), "Reguler"));

            ProgramStudi pr7 = new ProgramStudi();
            pr7.setId("007");
            model.addAttribute("ikhwanDuKpi", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"PRIA", pr7, "Reguler"));
            model.addAttribute("akhwatDuKpi", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"WANITA", pr7, "Reguler"));

            model.addAttribute("jpaKpi", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr7,JenisTest.JPA, "Reguler"));
            model.addAttribute("tpaKpi", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr7,JenisTest.TPA, "Reguler"));
            model.addAttribute("smrtKpi", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr7,JenisTest.SMART_TEST, "Reguler"));

            model.addAttribute("jmlhPeKpi", pendaftarDao.countPendaftarByProgramStudiAndTahunAjaranAndKonsentrasi(pr7,ta, "Reguler"));

            model.addAttribute("regisKpi", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jb, ta, Jenjang.S1, pr7, "Reguler"));

            model.addAttribute("duKpi",pembayaranDao.hitungDUJenisTestKonsentrasi(ta.getId(),pr7.getId(), "Reguler"));

            ProgramStudi pr6 = new ProgramStudi();
            pr6.setId("006");
            model.addAttribute("ikhwanDuPe", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"PRIA", pr6, "Reguler"));
            model.addAttribute("akhwatDuPe", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"WANITA", pr6, "Reguler"));

            model.addAttribute("jpaPe", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr6,JenisTest.JPA, "Reguler"));
            model.addAttribute("tpaPe", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr6,JenisTest.TPA, "Reguler"));
            model.addAttribute("smrtPe", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr6,JenisTest.SMART_TEST, "Reguler"));

            model.addAttribute("jmlhPePe", pendaftarDao.countPendaftarByProgramStudiAndTahunAjaranAndKonsentrasi(pr6,ta, "Reguler"));

            model.addAttribute("regisPe", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jb, ta, Jenjang.S1, pr6, "Reguler"));

            model.addAttribute("duPe",pembayaranDao.hitungDUJenisTestKonsentrasi(ta.getId(),pr6.getId(), "Reguler"));

            ProgramStudi MES = new ProgramStudi();
            MES.setId("008");

            model.addAttribute("ikhwanDuS2", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi(jbD,ta,"PRIA", MES));
            model.addAttribute("akhwatDuS2", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi(jbD,ta,"WANITA", MES));
            model.addAttribute("jpaS2", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,MES,JenisTest.JPA));
            model.addAttribute("tpaS2", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,MES,JenisTest.TPA));
            model.addAttribute("smrtS2", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,MES,JenisTest.SMART_TEST));

            model.addAttribute("jmlhPeS2", pendaftarDao.countPendaftarByProgramStudiAndTahunAjaran(MES,ta));

            model.addAttribute("regisMes", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudi(jb, ta, Jenjang.S2, MES));

            model.addAttribute("duMes",pembayaranDao.hitungDUJenisTest(ta.getId(),MES.getId()));

            ProgramStudi mas = new ProgramStudi();
            mas.setId("009");

            model.addAttribute("ikhwanDuS2AS", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi(jbD,ta,"PRIA", mas));
            model.addAttribute("akhwatDuS2AS", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi(jbD,ta,"WANITA", mas));
            model.addAttribute("jpaS2AS", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,mas,JenisTest.JPA));
            model.addAttribute("tpaS2AS", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,mas,JenisTest.TPA));
            model.addAttribute("smrtS2AS", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(jbD,ta,mas,JenisTest.SMART_TEST));

            model.addAttribute("jmlhPeS2AS", pendaftarDao.countPendaftarByProgramStudiAndTahunAjaran(mas,ta));

            model.addAttribute("regisS2As", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudi(jb, ta, Jenjang.S2, mas));

            model.addAttribute("duS2As",pembayaranDao.hitungDUJenisTest(ta.getId(),mas.getId()));

            model.addAttribute("gIkhwanDu", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarKonsentrasi(jbD,ta,"PRIA", Jenjang.S1, "Reguler"));

            model.addAttribute("gAkhwatDu", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarKonsentrasi(jbD,ta,"WANITA", Jenjang.S1, "Reguler"));
            model.addAttribute("gJpa", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarKonsentrasi(jbD,ta,JenisTest.JPA, Jenjang.S1, "Reguler"));
            model.addAttribute("gTpa", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarKonsentrasi(jbD,ta,JenisTest.TPA, Jenjang.S1, "Reguler"));
            model.addAttribute("gSmrt", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarKonsentrasi(jbD,ta,JenisTest.SMART_TEST, Jenjang.S1, "Reguler"));
            model.addAttribute("cPendaftar", pendaftarDao.countPendaftarByTahunAjaranAndLeadsJenjangAndKonsentrasi(ta, Jenjang.S1, "Reguler"));
            model.addAttribute("cRegistrasi", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarKonsentrasi(jb, ta, Jenjang.S1, "Reguler"));

            model.addAttribute("du", pembayaranDao.hitungDUTotalJenjangKonsentrasi(ta.getId(), "S1", "Reguler"));


            model.addAttribute("gIkhwanDuK", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarKonsentrasi(jbD,ta,"PRIA", Jenjang.S1, "Karyawan"));

            model.addAttribute("gAkhwatDuK", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarKonsentrasi(jbD,ta,"WANITA", Jenjang.S1, "Karyawan"));
            model.addAttribute("gJpaK", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarKonsentrasi(jbD,ta,JenisTest.JPA, Jenjang.S1, "Karyawan"));
            model.addAttribute("gTpaK", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarKonsentrasi(jbD,ta,JenisTest.TPA, Jenjang.S1, "Karyawan"));
            model.addAttribute("gSmrtK", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarKonsentrasi(jbD,ta,JenisTest.SMART_TEST, Jenjang.S1, "Karyawan"));
            model.addAttribute("cPendaftarK", pendaftarDao.countPendaftarByTahunAjaranAndLeadsJenjangAndKonsentrasi(ta, Jenjang.S1, "Karyawan"));
            model.addAttribute("cRegistrasiK", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarKonsentrasi(jb, ta, Jenjang.S1, "Karyawan"));

            model.addAttribute("duK", pembayaranDao.hitungDUTotalJenjangKonsentrasi(ta.getId(), "S1", "Karyawan"));



            model.addAttribute("gIkhwanDuS2", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarLeadsJenjang(jbD,ta,"PRIA", Jenjang.S2));

            model.addAttribute("gAkhwatDuS2", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarLeadsJenjang(jbD,ta,"WANITA", Jenjang.S2));
            model.addAttribute("gJpaS2", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarLeadsJenjang(jbD,ta,JenisTest.JPA, Jenjang.S2));
            model.addAttribute("gTpaS2", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarLeadsJenjang(jbD,ta,JenisTest.TPA, Jenjang.S2));
            model.addAttribute("gSmrtS2", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarLeadsJenjang(jbD,ta,JenisTest.SMART_TEST, Jenjang.S2));
            model.addAttribute("cPendaftarS2", pendaftarDao.countPendaftarByTahunAjaranAndLeadsJenjang(ta, Jenjang.S2));
            model.addAttribute("cRegistrasiS2", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjang(jb, ta, Jenjang.S2));

            model.addAttribute("duS2", pembayaranDao.hitungDUTotalJenjang(ta.getId(), "S2"));

//Kelas Karyawan

            model.addAttribute("ikhwanDuAsK", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"PRIA", pr2, "Karyawan"));
            model.addAttribute("akhwatDuAsK", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"WANITA", pr2, "Karyawan"));

            model.addAttribute("jpaAsK", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr2,JenisTest.JPA, "Karyawan"));
            model.addAttribute("tpaAsK", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr2,JenisTest.TPA, "Karyawan"));
            model.addAttribute("smrtAsK", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr2,JenisTest.SMART_TEST, "Karyawan"));

            model.addAttribute("jmlhPeAsK", pendaftarDao.countPendaftarByProgramStudiAndTahunAjaranAndKonsentrasi(pr2,ta, "Karyawan"));

            model.addAttribute("regisAsK", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jb, ta, Jenjang.S1, pr2, "Karyawan"));

            model.addAttribute("duAsK",pembayaranDao.hitungDUJenisTestKonsentrasi(ta.getId(),pr2.getId(), "Karyawan"));


            model.addAttribute("ikhwanDuMbK",pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"PRIA", pr3, "Karyawan"));
            model.addAttribute("akhwatDuMbK", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"WANITA", pr3, "Karyawan"));

            model.addAttribute("jpaMbK", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr3,JenisTest.JPA, "Karyawan"));
            model.addAttribute("tpaMbK", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr3,JenisTest.TPA, "Karyawan"));
            model.addAttribute("smrtMbK", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr3,JenisTest.SMART_TEST, "Karyawan"));

            model.addAttribute("jmlhPeMbK", pendaftarDao.countPendaftarByProgramStudiAndTahunAjaranAndKonsentrasi(pr3,ta, "Karyawan"));

            model.addAttribute("regisMbK", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jb, ta, Jenjang.S1, pr3, "Karyawan"));

            model.addAttribute("duMbK",pembayaranDao.hitungDUJenisTestKonsentrasi(ta.getId(),pr3.getId(), "Karyawan"));


            model.addAttribute("ikhwanDuKpiK", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"PRIA", pr7, "Karyawan"));
            model.addAttribute("akhwatDuKpiK", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"WANITA", pr7, "Karyawan"));

            model.addAttribute("jpaKpiK", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr7,JenisTest.JPA, "Karyawan"));
            model.addAttribute("tpaKpiK", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr7,JenisTest.TPA, "Karyawan"));
            model.addAttribute("smrtKpiK", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr7,JenisTest.SMART_TEST, "Karyawan"));

            model.addAttribute("jmlhPeKpiK", pendaftarDao.countPendaftarByProgramStudiAndTahunAjaranAndKonsentrasi(pr7,ta, "Karyawan"));

            model.addAttribute("regisKpiK", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jb, ta, Jenjang.S1, pr7, "Karyawan"));

            model.addAttribute("duKpiK",pembayaranDao.hitungDUJenisTestKonsentrasi(ta.getId(),pr7.getId(), "Karyawan"));



            ProgramStudi pr14 = new ProgramStudi();
            pr14.setId("014");
            model.addAttribute("ikhwanDuSi", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"PRIA", pr14, "Reguler"));
            model.addAttribute("akhwatDuSi", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"WANITA", pr14, "Reguler"));

            model.addAttribute("jpaSi", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr14,JenisTest.JPA, "Reguler"));
            model.addAttribute("tpaSi", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr14,JenisTest.TPA, "Reguler"));
            model.addAttribute("smrtSi", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr14,JenisTest.SMART_TEST, "Reguler"));

            model.addAttribute("jmlhPeSi", pendaftarDao.countPendaftarByProgramStudiAndTahunAjaranAndKonsentrasi(pr14,ta, "Reguler"));

            model.addAttribute("regisSi", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jb, ta, Jenjang.S1, pr14, "Reguler"));

            model.addAttribute("duSi",pembayaranDao.hitungDUJenisTestKonsentrasi(ta.getId(),pr14.getId(), "Reguler"));

            ProgramStudi pr15 = new ProgramStudi();
            pr15.setId("013");
            model.addAttribute("ikhwanDuTi", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"PRIA", pr15, "Reguler"));
            model.addAttribute("akhwatDuTi", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jbD,ta,"WANITA", pr15, "Reguler"));

            model.addAttribute("jpaTi", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr15,JenisTest.JPA, "Reguler"));
            model.addAttribute("tpaTi", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr15,JenisTest.TPA, "Reguler"));
            model.addAttribute("smrtTi", pembayaranDao.
                    countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(jbD,ta,pr15,JenisTest.SMART_TEST, "Reguler"));

            model.addAttribute("jmlhPeTi", pendaftarDao.countPendaftarByProgramStudiAndTahunAjaranAndKonsentrasi(pr15,ta, "Reguler"));

            model.addAttribute("regisTi", pembayaranDao.countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(jb, ta, Jenjang.S1, pr15, "Reguler"));

            model.addAttribute("duTi",pembayaranDao.hitungDUJenisTestKonsentrasi(ta.getId(),pr15.getId(), "Reguler"));
            return "dashboard";
        }
    }
}

package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.Referal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ReferalDao extends PagingAndSortingRepository<Referal, String>, CrudRepository<Referal, String> {
    Page<Referal> findByNamaContainingIgnoreCaseOrKodeReferalOrderByNama(String search,String search1, Pageable page);

    Referal findAllByKodeReferal(String kodeReferal);
    Referal findAllByKodeReferalAndStatus(String kodeReferal, Boolean status);
}

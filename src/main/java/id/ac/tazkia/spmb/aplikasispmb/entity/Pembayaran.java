package id.ac.tazkia.spmb.aplikasispmb.entity;

import id.ac.tazkia.spmb.aplikasispmb.constants.ZahirExport;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity @Data
public class Pembayaran {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_tagihan")
    private Tagihan tagihan;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_bank")
    private Bank bank;

    @NotNull
    private LocalDateTime waktuPembayaran;

    private String buktiPembayaran;

    @NotNull @Enumerated(EnumType.STRING)
    private CaraPembayaran caraPembayaran = CaraPembayaran.VIRTUAL_ACCOUNT;

    @NotNull @Min(0)
    private BigDecimal nilai;

    private String referensi;

    @Enumerated(EnumType.STRING)
    private ZahirExport zahirExport = ZahirExport.NEW;
}

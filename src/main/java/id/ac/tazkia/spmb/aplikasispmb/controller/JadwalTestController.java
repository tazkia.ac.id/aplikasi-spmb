package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.dto.ValidasiTagihan;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
public class JadwalTestController {
    private static final Logger logger = LoggerFactory.getLogger(JadwalTestController.class);

    @Autowired private UserDao userDao;
    @Autowired private LeadsDao leadsDao;
    @Autowired private PendaftarDao pendaftarDao;
    @Autowired private JadwalTestDao jadwalTestDao;
    @Autowired private PembayaranController pembayaranController;
    @Autowired private TahunAjaranDao tahunAjaranDao;
    @Autowired private HasilTestDao hasilTestDao;
    @Autowired private BeasiswaDao beasiswaDao;

    @Autowired private  FollowupDao followupDao;

    @Value("${upload.folder.raport}")
    private String uploadFolder;

    @GetMapping("/jadwalTest/form")
    public String formJadwalTest(Model model,  Authentication currentUser) {

        logger.debug("Authentication class : {}", currentUser.getClass().getName());

        if (currentUser == null) {
            logger.warn("Current user is null");
        }

        String username = ((UserDetails) currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if (u == null) {
            logger.warn("Username {} not found in database ", username);
        }

        Leads leads = leadsDao.findByUser(u);
        Pendaftar pendaftar = pendaftarDao.findByLeads(leads);
        if (pendaftar != null) {
            model.addAttribute("pendaftar", pendaftar);
        }
        JadwalTest jadwalTest = new JadwalTest();

        JadwalTest jt = jadwalTestDao.findByPendaftar(pendaftar);
        if (jt == null){
            model.addAttribute("jadwalTest", jadwalTest);

        }else {
            model.addAttribute("jadwalTest", jt);
        }

//Cek Pembayaran
        Pembayaran pembayaran = pembayaranController.cekPembayaran(pendaftar);
        model.addAttribute("pembayaran", pembayaran);

        HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
        model.addAttribute("hasilTest", hasilTest);

        Iterable<Beasiswa> beasiswa = beasiswaDao.findByStatus(Status.AKTIF);
        model.addAttribute("beasiswa", beasiswa);

        return "jadwalTest/form";
    }

    @PostMapping("/jadwalTest/form")
    public String prosesJadwalTest(@ModelAttribute JadwalTest jadwalTest, Authentication currentUser,String noReg,String idBeasiswa,
                                   @RequestParam(value = "fileUpload", required = false) MultipartFile  file, BindingResult errors) throws IOException {
        logger.debug("Authentication class : {}", currentUser.getClass().getName());
        if (currentUser == null) {
            logger.warn("Current user is null");
        }
        Pendaftar pp = pendaftarDao.findByNomorRegistrasi(noReg);
        if (jadwalTest.getJenisTest().equals(JenisTest.JPA) && pp.getKonsentrasi().equals("Reguler") && pp.getLeads().getJenjang().equals(Jenjang.S1) ) {
//           persiapan lokasi upload
            String idPeserta = noReg;
            String lokasiUpload = uploadFolder + File.separator + idPeserta;
            logger.debug("Lokasi upload : {}", lokasiUpload);
            new File(lokasiUpload).mkdirs();

            if (file == null || file.isEmpty()) {
                logger.info("File berkas kosong, tidak diproses");
                return idPeserta;
            }
            String namaAsli = file.getOriginalFilename();
            //memisahkan extensi
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }

            String idFile = UUID.randomUUID().toString();
            jadwalTest.setFileRaport(idFile + "." + extension);
            File tujuan = new File(lokasiUpload + File.separator + jadwalTest.getFileRaport());
            file.transferTo(tujuan);

            jadwalTest.setTanggalTest(LocalDate.now());
        } else if (jadwalTest.getJenisTest().equals(JenisTest.SMART_TEST) && pp.getKonsentrasi().equals("Reguler") && pp.getLeads().getJenjang().equals(Jenjang.S1)) {
            Beasiswa beasiswa = beasiswaDao.findById(idBeasiswa).get();
                if (beasiswa == null) {
                    logger.info("Beasiswa dengan id {} tidak ditemukan.", idBeasiswa);
                }
            jadwalTest.setBeasiswa(beasiswa);
            jadwalTest.setTanggalTest(beasiswa.getTanggalTest());
        } else if (jadwalTest.getJenisTest().equals(JenisTest.SMART_TEST) && pp.getKonsentrasi().equals("Karyawan") && pp.getLeads().getJenjang().equals(Jenjang.S1)) {
            jadwalTest.setTanggalTest(LocalDate.now());
            logger.info("Konsentrasi Karyawan, Jawabannya {}",jadwalTest.getSoal1());

        } else if (pp.getLeads().getJenjang().equals(Jenjang.S2)){
            jadwalTest.setTanggalTest(LocalDate.now());
            logger.info("Pascasarjana, Jawabannya {}",jadwalTest.getSoal1());
        }

        String username = ((UserDetails) currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if (u == null) {
            logger.warn("Username {} not fount in database", username);
        }
        Leads leads = leadsDao.findByUser(u);
        Pendaftar pendaftar1 = pendaftarDao.findByLeads(leads);

        jadwalTest.setPendaftar(pendaftar1);
        jadwalTestDao.save(jadwalTest);
        logger.info("Jadwal test dengan no pendaftar {} berhasil di simpan", pendaftar1.getNomorRegistrasi());

        return "redirect:/kartuUjian/form";

    }

    @GetMapping("/jadwalTest/list")
    public void listJadwalTest(@RequestParam(required = false)String nama,@RequestParam(required = false)String smart, Model m,@Qualifier("tjp") Pageable tjpPage, @Qualifier("smartTest") Pageable stPage){
        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer bulan = localDate.getMonthValue();

        TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);
        if(StringUtils.hasText(nama)) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarJadwal", jadwalTestDao.findJadwalTest(nama,tahunAjaran,JenisTest.SMART_TEST,tjpPage));
        } else {
            m.addAttribute("daftarJadwal", jadwalTestDao.findJadwalTestAll(tahunAjaran,JenisTest.SMART_TEST,tjpPage));
        }

        if(StringUtils.hasText(smart)) {
            m.addAttribute("smart", smart);
            m.addAttribute("daftarJadwalSt", jadwalTestDao.findJadwalTestSt(smart,bulan,tahunAjaran,JenisTest.SMART_TEST,stPage));
        } else {
            m.addAttribute("daftarJadwalSt", jadwalTestDao.findJadwalTestSmart(bulan,tahunAjaran,JenisTest.SMART_TEST,stPage));
        }

        Iterable<JadwalTest> jadwalTest = jadwalTestDao.findAll();
        for (JadwalTest jt : jadwalTest) {
            HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
            m.addAttribute("hasilTest", hasilTest);
        }
    }

    @GetMapping("/jadwalTest/listNew")
    public void listJadwalTestNew(@RequestParam(required = false)String nama, String user,String jenis, Model m, Pageable pageable){

        TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);

        if(StringUtils.hasText(nama) && !StringUtils.hasText(user) && !StringUtils.hasText(jenis)) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarJadwal", jadwalTestDao.findByPendaftarNomorRegistrasiContainingIgnoreCaseOrPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(nama,nama, Jenjang.S1,"Reguler",tahunAjaran,pageable));
        }else if (!StringUtils.hasText(nama) && StringUtils.hasText(user) && !StringUtils.hasText(jenis)) {
            m.addAttribute("user", user);
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarJadwal", jadwalTestDao.findByPendaftarFollowupAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(fl, Jenjang.S1,"Reguler",tahunAjaran,pageable));
        }else if (StringUtils.hasText(nama) && StringUtils.hasText(user) && !StringUtils.hasText(jenis)) {
            m.addAttribute("nama", nama);
            m.addAttribute("user", user);
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarJadwal", jadwalTestDao.findByPendaftarNomorRegistrasiContainingIgnoreCaseOrPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarFollowupAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(nama,nama,fl, Jenjang.S1,"Reguler", tahunAjaran, pageable));
        }else if (StringUtils.hasText(nama) && StringUtils.hasText(user) && StringUtils.hasText(jenis)) {
            m.addAttribute("nama", nama);
            m.addAttribute("user", user);
            m.addAttribute("jenis", jenis);
            JenisTest jt = JenisTest.valueOf(jenis);
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarJadwal", jadwalTestDao.findByPendaftarNomorRegistrasiContainingIgnoreCaseOrPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarFollowupAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndJenisTestAndPendaftarTahunAjaranOrderByTanggalTestDesc(nama,nama,fl, Jenjang.S1,"Reguler",jt, tahunAjaran, pageable));
        } else if (!StringUtils.hasText(nama) && !StringUtils.hasText(user) && StringUtils.hasText(jenis)) {
            m.addAttribute("jenis", jenis);
            JenisTest jt = JenisTest.valueOf(jenis);
            m.addAttribute("daftarJadwal", jadwalTestDao.findByJenisTestAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(jt, Jenjang.S1,"Reguler",tahunAjaran,pageable));
        }else if (StringUtils.hasText(nama) && !StringUtils.hasText(user) && StringUtils.hasText(jenis)) {
            m.addAttribute("jenis", jenis);
            m.addAttribute("nama", nama);
            JenisTest jt = JenisTest.valueOf(jenis);
            m.addAttribute("daftarJadwal", jadwalTestDao.findByPendaftarNomorRegistrasiContainingIgnoreCaseOrPendaftarLeadsNamaContainingIgnoreCaseAndJenisTestAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(nama,nama,jt, Jenjang.S1,"Reguler", tahunAjaran, pageable));
        }else if (!StringUtils.hasText(nama) && StringUtils.hasText(user) && StringUtils.hasText(jenis)) {
            m.addAttribute("jenis", jenis);
            m.addAttribute("user", user);
            JenisTest jt = JenisTest.valueOf(jenis);
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarJadwal", jadwalTestDao.findByJenisTestAndPendaftarFollowupAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(jt,fl, Jenjang.S1,"Reguler", tahunAjaran, pageable));
        }else {
            m.addAttribute("daftarJadwal", jadwalTestDao.findByPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(Jenjang.S1, "Reguler",tahunAjaran, pageable));
        }

        Iterable<Followup> fu = followupDao.findByStatus(Status.AKTIF);
        m.addAttribute("listFu", fu);


//        Iterable<JadwalTest> jadwalTest = jadwalTestDao.findByPendaftarLeadsJenjangAndPendaftarTahunAjaranOrderByPendaftarNomorRegistrasi( Jenjang.S1,tahunAjaran);
//        for (JadwalTest jt : jadwalTest) {
//            HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
//            m.addAttribute("hasilTest", hasilTest);
//            System.out.println(hasilTest);
//        }

        Iterable<JadwalTest> jd = jadwalTestDao.findByPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(Jenjang.S1,"Reguler",tahunAjaran);
        List<ValidasiTagihan> cekHasilTest = new ArrayList<>();
        for (JadwalTest pe : jd) {
            HasilTest hasilTest =hasilTestDao.findByJadwalTest(pe);
            ValidasiTagihan validasiTagihan = new ValidasiTagihan();
            validasiTagihan.setPendaftar(pe.getId());
            if (hasilTest != null){
                validasiTagihan.setStatus("Ada");
            }
            if (hasilTest == null){
                validasiTagihan.setStatus("Kosong");
            }
            cekHasilTest.add(validasiTagihan);

        }
        m.addAttribute("cekHasilTest", cekHasilTest);

    }

    @GetMapping("/jadwalTest/listNewKk")
    public void listJadwalTestNewKk(@RequestParam(required = false)String nama, String user, String  jenis ,Model m, Pageable pageable){

        TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);

        if(StringUtils.hasText(nama) && !StringUtils.hasText(user) && !StringUtils.hasText(jenis)) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarJadwal", jadwalTestDao.findByPendaftarNomorRegistrasiContainingIgnoreCaseOrPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(nama,nama, Jenjang.S1,"Karyawan",tahunAjaran,pageable));
        }else if (!StringUtils.hasText(nama) && StringUtils.hasText(user) && !StringUtils.hasText(jenis)) {
            m.addAttribute("user", user);
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarJadwal", jadwalTestDao.findByPendaftarFollowupAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(fl, Jenjang.S1,"Karyawan",tahunAjaran,pageable));
        }else if (StringUtils.hasText(nama) && StringUtils.hasText(user) && !StringUtils.hasText(jenis)) {
            m.addAttribute("nama", nama);
            m.addAttribute("user", user);
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarJadwal", jadwalTestDao.findByPendaftarNomorRegistrasiContainingIgnoreCaseOrPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarFollowupAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(nama,nama,fl, Jenjang.S1,"Karyawan", tahunAjaran, pageable));
        }else if (StringUtils.hasText(nama) && StringUtils.hasText(user) && StringUtils.hasText(jenis)) {
            m.addAttribute("nama", nama);
            m.addAttribute("user", user);
            m.addAttribute("jenis", jenis);
            JenisTest jt = JenisTest.valueOf(jenis);
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarJadwal", jadwalTestDao.findByPendaftarNomorRegistrasiContainingIgnoreCaseOrPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarFollowupAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndJenisTestAndPendaftarTahunAjaranOrderByTanggalTestDesc(nama,nama,fl, Jenjang.S1,"Karyawan",jt, tahunAjaran, pageable));
        } else if (!StringUtils.hasText(nama) && !StringUtils.hasText(user) && StringUtils.hasText(jenis)) {
            m.addAttribute("jenis", jenis);
            JenisTest jt = JenisTest.valueOf(jenis);
            m.addAttribute("daftarJadwal", jadwalTestDao.findByJenisTestAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(jt, Jenjang.S1,"Karyawan",tahunAjaran,pageable));
        }else if (StringUtils.hasText(nama) && !StringUtils.hasText(user) && StringUtils.hasText(jenis)) {
            m.addAttribute("jenis", jenis);
            m.addAttribute("nama", nama);
            JenisTest jt = JenisTest.valueOf(jenis);
            m.addAttribute("daftarJadwal", jadwalTestDao.findByPendaftarNomorRegistrasiContainingIgnoreCaseOrPendaftarLeadsNamaContainingIgnoreCaseAndJenisTestAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(nama,nama,jt, Jenjang.S1,"Karyawan", tahunAjaran, pageable));
        }else if (!StringUtils.hasText(nama) && StringUtils.hasText(user) && StringUtils.hasText(jenis)) {
            m.addAttribute("jenis", jenis);
            m.addAttribute("user", user);
            JenisTest jt = JenisTest.valueOf(jenis);
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarJadwal", jadwalTestDao.findByJenisTestAndPendaftarFollowupAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(jt,fl, Jenjang.S1,"Karyawan", tahunAjaran, pageable));
        }else {
            m.addAttribute("daftarJadwal", jadwalTestDao.findByPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(Jenjang.S1, "Karyawan",tahunAjaran, pageable));
        }

        Iterable<Followup> fu = followupDao.findByStatus(Status.AKTIF);
        m.addAttribute("listFu", fu);


//        Iterable<JadwalTest> jadwalTest = jadwalTestDao.findByPendaftarLeadsJenjangAndPendaftarTahunAjaranOrderByPendaftarNomorRegistrasi( Jenjang.S1,tahunAjaran);
//        for (JadwalTest jt : jadwalTest) {
//            HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
//            m.addAttribute("hasilTest", hasilTest);
//            System.out.println(hasilTest);
//        }

        Iterable<JadwalTest> jd = jadwalTestDao.findByPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(Jenjang.S1,"Karyawan",tahunAjaran);
        List<ValidasiTagihan> cekHasilTest = new ArrayList<>();
        for (JadwalTest pe : jd) {
            HasilTest hasilTest =hasilTestDao.findByJadwalTest(pe);
            ValidasiTagihan validasiTagihan = new ValidasiTagihan();
            validasiTagihan.setPendaftar(pe.getId());
            if (hasilTest != null){
                validasiTagihan.setStatus("Ada");
            }
            if (hasilTest == null){
                validasiTagihan.setStatus("Kosong");
            }
            cekHasilTest.add(validasiTagihan);

        }
        m.addAttribute("cekHasilTest", cekHasilTest);

    }


    @GetMapping("/jadwalTest/listNewS2")
    public void listJadwalTestNewS2(@RequestParam(required = false)String nama, String user, Model m, Pageable pageable){

        TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);

        if(StringUtils.hasText(nama) && !StringUtils.hasText(user)) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarJadwal", jadwalTestDao.findByPendaftarNomorRegistrasiContainingIgnoreCaseOrPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarLeadsJenjangAndPendaftarTahunAjaranOrderByTanggalTestDesc(nama,nama, Jenjang.S2,tahunAjaran,pageable));
        }else if (!StringUtils.hasText(nama) && StringUtils.hasText(user)) {
            m.addAttribute("user", user);
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarJadwal", jadwalTestDao.findByPendaftarFollowupAndPendaftarLeadsJenjangAndPendaftarTahunAjaranOrderByTanggalTestDesc(fl, Jenjang.S2,tahunAjaran,pageable));
        }else if (StringUtils.hasText(nama) && StringUtils.hasText(user)) {
            m.addAttribute("nama", nama);
            m.addAttribute("user", user);
            Followup fl = followupDao.findById(user).get();
            m.addAttribute("daftarJadwal", jadwalTestDao.findByPendaftarNomorRegistrasiContainingIgnoreCaseOrPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarFollowupAndPendaftarLeadsJenjangAndPendaftarTahunAjaranOrderByTanggalTestDesc(nama,nama,fl, Jenjang.S2, tahunAjaran, pageable));
        }else {
            m.addAttribute("daftarJadwal", jadwalTestDao.findByPendaftarLeadsJenjangAndPendaftarTahunAjaranOrderByTanggalTestDesc(Jenjang.S2, tahunAjaran, pageable));
        }

        Iterable<Followup> fu = followupDao.findByStatus(Status.AKTIF);
        m.addAttribute("listFu", fu);


//        Iterable<JadwalTest> jadwalTest = jadwalTestDao.findByPendaftarLeadsJenjangAndPendaftarTahunAjaranOrderByPendaftarNomorRegistrasi( Jenjang.S1,tahunAjaran);
//        for (JadwalTest jt : jadwalTest) {
//            HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
//            m.addAttribute("hasilTest", hasilTest);
//            System.out.println(hasilTest);
//        }

        Iterable<JadwalTest> jd = jadwalTestDao.findByPendaftarLeadsJenjangAndPendaftarTahunAjaranOrderByTanggalTestDesc(Jenjang.S2,tahunAjaran);
        List<ValidasiTagihan> cekHasilTest = new ArrayList<>();
        for (JadwalTest pe : jd) {
            HasilTest hasilTest =hasilTestDao.findByJadwalTest(pe);
            ValidasiTagihan validasiTagihan = new ValidasiTagihan();
            validasiTagihan.setPendaftar(pe.getId());
            if (hasilTest != null){
                validasiTagihan.setStatus("Ada");
            }
            if (hasilTest == null){
                validasiTagihan.setStatus("Kosong");
            }
            cekHasilTest.add(validasiTagihan);

        }
        m.addAttribute("cekHasilTest", cekHasilTest);

    }
}

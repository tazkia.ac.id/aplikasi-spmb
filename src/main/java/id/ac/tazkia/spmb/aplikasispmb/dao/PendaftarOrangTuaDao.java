package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PendaftarOrangTuaDao extends PagingAndSortingRepository<PendaftarOrangtua, String>, CrudRepository<PendaftarOrangtua, String> {
    PendaftarOrangtua findByPendaftar(Pendaftar pendaftar);

    Iterable<PendaftarOrangtua> findByPendaftarTahunAjaranAktif(Status aktif);
    Iterable<PendaftarOrangtua> findByPendaftarTahunAjaranAktifAndPendaftarLeadsJenjang(Status aktif, Jenjang jenjang);
    Iterable<PendaftarOrangtua> findByPendaftarTahunAjaranAndPendaftar(TahunAjaran ta, Pendaftar pendaftar);

    List<PendaftarOrangtua> findByPendaftarTahunAjaran(TahunAjaran ta);
}

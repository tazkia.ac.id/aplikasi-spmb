package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PendaftarDao extends PagingAndSortingRepository <Pendaftar, String>, CrudRepository<Pendaftar, String> {
    Pendaftar findByLeads(Leads leads);
    Page<Pendaftar> findByNomorRegistrasiContainingIgnoreCaseOrLeadsNamaContainingIgnoreCaseAndTahunAjaranAndKonsentrasiAndLeadsJenjangOrderByNomorRegistrasiDesc(String nomorRegistrasi, String nama, TahunAjaran tahunAjaran, String konsentrasi, Jenjang jenjang, Pageable pageable);
    Page<Pendaftar> findByNomorRegistrasiContainingIgnoreCaseOrLeadsNamaContainingIgnoreCaseAndFollowupAndTahunAjaranAndKonsentrasiAndLeadsJenjangOrderByNomorRegistrasiDesc(String nomorRegistrasi, String nama, Followup fl, TahunAjaran tahunAjaran, String konsentrasi, Jenjang jenjang, Pageable pageable);
    Page<Pendaftar> findByStatusContainingIgnoreCaseAndFollowupAndTahunAjaranAndKonsentrasiAndLeadsJenjangOrderByNomorRegistrasiDesc(String status, Followup fl, TahunAjaran tahunAjaran, String konsentrasi, Jenjang jenjang, Pageable pageable);
    Page<Pendaftar> findByStatusContainingIgnoreCaseAndTahunAjaranAndKonsentrasiAndLeadsJenjangOrderByNomorRegistrasiDesc(String status, TahunAjaran tahunAjaran, String konsentrasi, Jenjang jenjang, Pageable pageable);
    Page<Pendaftar> findByNomorRegistrasiContainingIgnoreCaseOrLeadsNamaContainingIgnoreCaseAndFollowupAndTahunAjaranAndLeadsJenjangOrderByNomorRegistrasi(String nomorRegistrasi,String nama,Followup fl, TahunAjaran tahunAjaran,Jenjang jenjang, Pageable pageable);
    Page<Pendaftar> findByFollowupAndTahunAjaranAndKonsentrasiAndLeadsJenjangOrderByNomorRegistrasiDesc(Followup fu, TahunAjaran tahunAjaran, String konsentrasi, Jenjang jenjang, Pageable pageable);
    Page<Pendaftar> findByFollowupAndTahunAjaranAndLeadsJenjangOrderByNomorRegistrasi(Followup fu, TahunAjaran tahunAjaran,Jenjang jenjang, Pageable pageable);
    Page<Pendaftar> findByNomorRegistrasiContainingIgnoreCaseOrLeadsNamaContainingIgnoreCaseOrFollowupNamaAndTahunAjaranAndKonsentrasiAndLeadsJenjangOrderByNomorRegistrasi(String nomorRegistrasi,String nama, String fu,TahunAjaran tahunAjaran,String konsentrasi,Jenjang jenjang, Pageable pageable);
    Page<Pendaftar> findByNomorRegistrasiContainingIgnoreCaseOrLeadsNamaContainingIgnoreCaseAndTahunAjaranOrderByNomorRegistrasi(String nomorRegistrasi,String nama, TahunAjaran tahunAjaran, Pageable pageable);
    Page<Pendaftar> findByNomorRegistrasiContainingIgnoreCaseOrLeadsNamaContainingIgnoreCaseAndTahunAjaranAndLeadsJenjangOrderByNomorRegistrasi(String nomorRegistrasi,String nama, TahunAjaran tahunAjaran,Jenjang jenjang, Pageable pageable);

    Page<Pendaftar> findByTahunAjaranAktifAndKonsentrasiAndLeadsJenjang(Status status,String konsentrasi,Jenjang jenjang, Pageable page);
    Page<Pendaftar> findByTahunAjaranAktif(Status status, Pageable page);
    Page<Pendaftar> findByTahunAjaranAktifAndLeadsJenjang(Status status,Jenjang jenjang, Pageable page);
    Iterable<Pendaftar> findByTahunAjaranAktifAndLeadsJenjang(Status status,Jenjang jenjang);

    Pendaftar findByNomorRegistrasi(String nomor);

    Iterable<Pendaftar> findByTahunAjaranAktif(Status status);
    Iterable<Pendaftar> findByKonsentrasiAndTahunAjaranAktif(String konsentrasi,Status status);

    Long countPendaftarByLeadsJenjangNotAndTahunAjaran(Jenjang jenjang,TahunAjaran tahun);


    Long countPendaftarByProgramStudiAndTahunAjaran(ProgramStudi programStudi, TahunAjaran tahun);
    Long countPendaftarByProgramStudiAndTahunAjaranAndKonsentrasi(ProgramStudi programStudi, TahunAjaran tahun, String konsentrasi);
    Long countPendaftarByTahunAjaranAndLeadsJenjangAndKonsentrasi(TahunAjaran tahun, Jenjang jenjang, String  konsentrasi);
    Long countPendaftarByTahunAjaranAndLeadsJenjang(TahunAjaran tahun, Jenjang jenjang);

    Long countPendaftarByLeadsJenjangAndTahunAjaranAndKonsentrasi(Jenjang jenjang, TahunAjaran tahun, String kelas);
    Long countPendaftarByLeadsJenjangAndTahunAjaran(Jenjang jenjang, TahunAjaran tahun);


    Iterable<Pendaftar> findByLeadsId (String leads);
}

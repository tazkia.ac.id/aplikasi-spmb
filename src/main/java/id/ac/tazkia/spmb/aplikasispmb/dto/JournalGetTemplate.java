package id.ac.tazkia.spmb.aplikasispmb.dto;


import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
public class JournalGetTemplate {
    private String codeTemplate;
    private LocalDate dateTransaction;
    private String description;

    private List<JournalGetTemplateDetail> journalDetailByApiDtoList;

}

package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.Course;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CourseDao extends PagingAndSortingRepository<Course, String >, CrudRepository <Course, String > {
    Page<Course> findByNamaMatakuliahContainingIgnoreCaseOrderByNamaMatakuliahDesc(String namaMatakuliah, Pageable page);
}

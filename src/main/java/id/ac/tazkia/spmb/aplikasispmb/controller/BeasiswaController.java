package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.dao.beasiswa.PendaftarBeasiswaDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.beasiswa.PendaftarBeasiswaFileDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.beasiswa.PendaftarBeasiswaOrangtuaDao;
import id.ac.tazkia.spmb.aplikasispmb.dto.beasiswa.DataBeasiswaDto;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import id.ac.tazkia.spmb.aplikasispmb.entity.beasiswa.PendaftarBeasiswa;
import id.ac.tazkia.spmb.aplikasispmb.entity.beasiswa.PendaftarBeasiswaFile;
import id.ac.tazkia.spmb.aplikasispmb.entity.beasiswa.PendaftarBeasiswaOrangtua;
import id.ac.tazkia.spmb.aplikasispmb.service.BeasiswaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.retrytopic.RetryTopicConfiguration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Controller
public class BeasiswaController {
    private static final Logger logger = LoggerFactory.getLogger(DashboardController.class);

    @Autowired private BeasiswaDao pd;

    @Autowired private UserDao userDao;

    @Autowired private PendaftarBeasiswaDao pendaftarBeasiswaDao;

    @Autowired private PendaftarBeasiswaOrangtuaDao pendaftarBeasiswaOrangtuaDao;

    @Autowired private PendaftarBeasiswaFileDao pendaftarBeasiswaFileDao;

    @Autowired private ProvinsiDao provinsiDao;

    @Autowired private KabupatenKotaDao kabupatenKotaDao;

    @Autowired private PendidikanDao pendidikanDao;

    @Autowired private PekerjaanDao pekerjaanDao;

    @Autowired private PenghasilanDao penghasilanDao;

    @Autowired private BeasiswaService beasiswaService;

    @Value("${upload.folder.beasiswa}")
    private String uploadFile;

    @ModelAttribute("daftarKokab")
    public java.lang.Object daftarKokab(){
        return kabupatenKotaDao.findAll();
    }

    @ModelAttribute("daftarProvinsi")
    public Iterable<Provinsi> daftarProvinsi(){
        return provinsiDao.findAll();
    }

    @ModelAttribute("pendidikanOrtu")
    public Iterable<Pendidikan> pendidikanOrtu(){return pendidikanDao.findAll();}

    @ModelAttribute("pekerjaanOrtu")
    public Iterable<Pekerjaan> pekerjaanOrtu(){return pekerjaanDao.findAll();}

    @ModelAttribute("penghasilanOrtu")
    public Iterable<Penghasilan> penghasilanOrtu(){return penghasilanDao.findAll();}

    //list
    @RequestMapping("/beasiswa/list")
    public void daftarbeasiswa(@RequestParam(required = false)String nama, Model m, Pageable page){
        if(StringUtils.hasText(nama)) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarBeasiswa", pd.findByNamaContainingIgnoreCaseOrderByNama(nama, page));
        } else {
            m.addAttribute("daftarBeasiswa", pd.findAll(page));
        }
    }

    //Hapus Data
    @RequestMapping("/beasiswa/hapus")
    public  String hapus(@RequestParam("id") Beasiswa id ){
        pd.delete(id);
        return "redirect:list";
    }

    //tampikan form
    @RequestMapping(value = "/beasiswa/form", method = RequestMethod.GET)
    public String tampilkanForm(@RequestParam(value = "id", required = false) String id,
                                Model m){
        //defaultnya, isi dengan object baru
        m.addAttribute("beasiswa", new Beasiswa());

        if (id != null && !id.isEmpty()){
            Beasiswa p= pd.findById(id).get();
            if (p != null){
                m.addAttribute("beasiswa", p);
            }
        }
        return "beasiswa/form";
    }

    //simpan
    @PostMapping(value = "/beasiswa/form")
    public String prosesForm(@Valid Beasiswa p, BindingResult errors){
//            if(errors.hasErrors()){
//                return "/beasiswa/form";
//            }

        pd.save(p);
        return "redirect:list";
    }


    @RequestMapping(value = "/beasiswa/aktif", method = RequestMethod.POST)
    public String updateStatusAktif(@RequestParam (required = false) Beasiswa bb){
        bb.setStatus(Status.AKTIF);
        pd.save(bb);
        return "redirect:list";
    }

    @RequestMapping(value = "/beasiswa/nonAktif", method = RequestMethod.POST)
    public String updateStatusnonAktif(@RequestParam (required = false) Beasiswa bb){
        bb.setStatus(Status.NONAKTIF);
        pd.save(bb);
        return "redirect:list";
    }

    // ADMIN BEASISWA
    @GetMapping("/dashboardAdminbeasiswa")
    public String dashboardAdminbeasiswa(Model model, Authentication auth){
        logger.debug("Authentication class : {}", auth.getClass().getName());

        if (auth == null) {
            logger.warn("Current user is null");
        }

        model.addAttribute("jumlahPendaftar", pendaftarBeasiswaDao.countAllBy());
        model.addAttribute("pendaftarYatim", pendaftarBeasiswaDao.countByStatusOrtu("YATIM"));
        model.addAttribute("pendaftarPiatu", pendaftarBeasiswaDao.countByStatusOrtu("PIATU"));
        model.addAttribute("pendaftarYatimPiatu", pendaftarBeasiswaDao.countByStatusOrtu("YATIM_PIATU"));
        model.addAttribute("peraihSertifikatInternasional", pendaftarBeasiswaFileDao.countByPrestasi1IsNotNull());

        model.addAttribute("Top5Provinsi", pendaftarBeasiswaDao.findTop5Provinsi(PageRequest.of(0,5)));
        model.addAttribute("Top5KabupatenKota", pendaftarBeasiswaDao.findTop5Kabupaten(PageRequest.of(0, 5)));

        return "beasiswa/admin/dashboard";
    }

    @GetMapping("/beasiswa/list")
    public String listPendaftarBeasiswa(Model model, @PageableDefault(size = 20)Pageable page, Authentication auth){
        logger.debug("Authentication class : {}", auth.getClass().getName());

        if (auth == null) {
            logger.warn("Current user is null");
        }

        model.addAttribute("listPendaftar", pendaftarBeasiswaDao.findAll(page));

        return "beasiswa/admin/list";
    }

    // PESERTA BEASISWA
    @GetMapping("/dashboardBeasiswa")
    public String dashboardBeasiswa(Model m, Authentication authentication){
        logger.debug("Authentication class : {}", authentication.getClass().getName());

        if (authentication == null) {
            logger.warn("Current user is null");
        }

        String username = ((UserDetails) authentication.getPrincipal()).getUsername();
        User user = userDao.findByUsername(username);
        PendaftarBeasiswa pb = pendaftarBeasiswaDao.findByUser(user);
        if (pb.getNik() == null) {
            return "redirect:beasiswa/detailPendaftar";
        }
        return "dashboardBeasiswa";

    }

    @GetMapping("/beasiswa/detailPendaftar")
    public String String(Model model, Authentication authentication){
        String username = ((UserDetails) authentication.getPrincipal()).getUsername();
        User user = userDao.findByUsername(username);

        model.addAttribute("daftarKokab", daftarKokab());

        DataBeasiswaDto dataBeasiswaDto = new DataBeasiswaDto();
        PendaftarBeasiswa pendaftar = pendaftarBeasiswaDao.findByUser(user);
        PendaftarBeasiswaOrangtua pendaftarOrangTua = pendaftarBeasiswaOrangtuaDao.findByPendaftarBeasiswa(pendaftar);
        PendaftarBeasiswaFile pendaftarBeasiswaFile = pendaftarBeasiswaFileDao.findByPendaftarBeasiswa(pendaftar);

        if (pendaftar != null) {
            dataBeasiswaDto.setId(pendaftar.getId());
            dataBeasiswaDto.setNamaLengkap(pendaftar.getNamaLengkap());
            dataBeasiswaDto.setNoTelpon(pendaftar.getNoTelpon());
            dataBeasiswaDto.setEmail(pendaftar.getEmail());
            dataBeasiswaDto.setLulusan(pendaftar.getLulusan());
            dataBeasiswaDto.setKotaAsalSekolah(pendaftar.getKotaAsalSekolah());
            dataBeasiswaDto.setNamaSekolah(pendaftar.getNamaSekolah());
            dataBeasiswaDto.setTahunLulus(pendaftar.getTahunLulus());
            dataBeasiswaDto.setStatusOrtu(pendaftar.getStatusOrtu());
            dataBeasiswaDto.setNik(pendaftar.getNik());
            dataBeasiswaDto.setTempatLahir(pendaftar.getTempatLahir());
            dataBeasiswaDto.setTanggalLahir(pendaftar.getTanggalLahir());
            dataBeasiswaDto.setNisn(pendaftar.getNisn());
            dataBeasiswaDto.setNpsn(pendaftar.getNpsn());
            dataBeasiswaDto.setAlamat(pendaftar.getAlamat());
            dataBeasiswaDto.setProvinsi(pendaftar.getProvinsi());
            dataBeasiswaDto.setKabupatenKota(pendaftar.getKabupatenKota());
            dataBeasiswaDto.setKecamatan(pendaftar.getKecamatan());
            dataBeasiswaDto.setDesaKelurahan(pendaftar.getDesaKelurahan());
            dataBeasiswaDto.setKodePos(pendaftar.getKodePos());
            dataBeasiswaDto.setNoPendaftaranKip(pendaftar.getNoPendaftaranKip());
            dataBeasiswaDto.setStatusDisabilitas(pendaftar.getStatusDisabilitas());
            dataBeasiswaDto.setDeskripsiDisabilitas(pendaftar.getDeskripsiDisabilitas());
        }
        if (pendaftarOrangTua != null) {
            dataBeasiswaDto.setNamaIbu(pendaftarOrangTua.getNamaIbu());
            dataBeasiswaDto.setAgamaIbu(pendaftarOrangTua.getAgamaIbu());
            dataBeasiswaDto.setPendidikanIbu(pendaftarOrangTua.getPendidikanIbu());
            dataBeasiswaDto.setPekerjaanIbu(pendaftarOrangTua.getPekerjaanIbu());
            dataBeasiswaDto.setStatusIbu(pendaftarOrangTua.getStatusIbu());
            dataBeasiswaDto.setNamaAyah(pendaftarOrangTua.getNamaAyah());
            dataBeasiswaDto.setAgamaAyah(pendaftarOrangTua.getAgamaAyah());
            dataBeasiswaDto.setPendidikanAyah(pendaftarOrangTua.getPendidikanAyah());
            dataBeasiswaDto.setPekerjaanAyah(pendaftarOrangTua.getPekerjaanAyah());
            dataBeasiswaDto.setStatusAyah(pendaftarOrangTua.getStatusAyah());
            dataBeasiswaDto.setAlamatOrangTua(pendaftarOrangTua.getAlamat());
            dataBeasiswaDto.setNoTelponOrangTua(pendaftarOrangTua.getNoTelpon());
            dataBeasiswaDto.setPenghasilanOrangtua(pendaftarOrangTua.getPenghasilanOrangtua());
            dataBeasiswaDto.setJumlahTanggungan(pendaftarOrangTua.getJumlahTanggungan());
        }
        if (pendaftarBeasiswaFile != null) {
            dataBeasiswaDto.setFileId(pendaftarBeasiswaFile.getId());
            dataBeasiswaDto.setSktm(pendaftarBeasiswaFile.getSktm());
            dataBeasiswaDto.setFotoRumah(pendaftarBeasiswaFile.getFotoRumah());
            dataBeasiswaDto.setTagihanListrik(pendaftarBeasiswaFile.getTagihanListrik());
            dataBeasiswaDto.setResume(pendaftarBeasiswaFile.getResume());
            dataBeasiswaDto.setPrestasi1(pendaftarBeasiswaFile.getPrestasi1());
            dataBeasiswaDto.setPrestasi2(pendaftarBeasiswaFile.getPrestasi2());
            dataBeasiswaDto.setPrestasi3(pendaftarBeasiswaFile.getPrestasi3());
            dataBeasiswaDto.setPrestasi4(pendaftarBeasiswaFile.getPrestasi4());
        }

        model.addAttribute("detailPendaftar", dataBeasiswaDto);

        return "beasiswa/pendaftar/detail";
    }

    @PostMapping("/beasiswa/detailPendaftar")
    public String saveDetailBeasiswa(@ModelAttribute DataBeasiswaDto beasiswaDto, Authentication authentication, @RequestParam(required = false) MultipartFile fileSktm,
                                     @RequestParam(required = false) MultipartFile fileTagihanListrik, @RequestParam(required = false) MultipartFile fileFotoRumah, @RequestParam(required = false) MultipartFile fileResume,
                                     @RequestParam(required = false) MultipartFile filePrestasi1, @RequestParam(required = false) MultipartFile filePrestasi2, @RequestParam(required = false) MultipartFile filePrestasi3,
                                     @RequestParam(required = false) MultipartFile filePrestasi4) throws IOException {

        logger.debug("Authentication class : {}", authentication.getClass().getName());

        if(authentication == null){
            logger.warn("Current user is null");
        }

        String username = ((UserDetails)authentication.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }

//        logger.info("Data Detail Pendaftar : {}", beasiswaDto);

        PendaftarBeasiswa pendaftar = pendaftarBeasiswaDao.findById(beasiswaDto.getId()).get();
        BeanUtils.copyProperties(beasiswaDto, pendaftar);
        pendaftarBeasiswaDao.save(pendaftar);

        PendaftarBeasiswaOrangtua beasiswaOrangtua = pendaftarBeasiswaOrangtuaDao.findByPendaftarBeasiswa(pendaftar);

        if (beasiswaOrangtua == null) {
            PendaftarBeasiswaOrangtua bot = new PendaftarBeasiswaOrangtua();
            bot.setPendaftarBeasiswa(pendaftar);
//            BeanUtils.copyProperties(beasiswaDto, bot);
            bot.setNamaIbu(beasiswaDto.getNamaIbu());
            bot.setAgamaIbu(beasiswaDto.getAgamaIbu());
            bot.setPendidikanIbu(beasiswaDto.getPendidikanIbu());
            bot.setPekerjaanIbu(beasiswaDto.getPekerjaanIbu());
            bot.setStatusIbu(beasiswaDto.getStatusIbu());
            bot.setNamaAyah(beasiswaDto.getNamaAyah());
            bot.setAgamaAyah(beasiswaDto.getAgamaAyah());
            bot.setPendidikanAyah(beasiswaDto.getPendidikanAyah());
            bot.setPekerjaanAyah(beasiswaDto.getPekerjaanAyah());
            bot.setStatusAyah(beasiswaDto.getStatusAyah());
            bot.setAlamat(beasiswaDto.getAlamatOrangTua());
            bot.setNoTelpon(beasiswaDto.getNoTelponOrangTua());
            bot.setPenghasilanOrangtua(beasiswaDto.getPenghasilanOrangtua());
            bot.setJumlahTanggungan(beasiswaDto.getJumlahTanggungan());
            pendaftarBeasiswaOrangtuaDao.save(bot);
        }else{
            beasiswaOrangtua.setNamaIbu(beasiswaDto.getNamaIbu());
            beasiswaOrangtua.setAgamaIbu(beasiswaDto.getAgamaIbu());
            beasiswaOrangtua.setPendidikanIbu(beasiswaDto.getPendidikanIbu());
            beasiswaOrangtua.setPekerjaanIbu(beasiswaDto.getPekerjaanIbu());
            beasiswaOrangtua.setStatusIbu(beasiswaDto.getStatusIbu());
            beasiswaOrangtua.setNamaAyah(beasiswaDto.getNamaAyah());
            beasiswaOrangtua.setAgamaAyah(beasiswaDto.getAgamaAyah());
            beasiswaOrangtua.setPendidikanAyah(beasiswaDto.getPendidikanAyah());
            beasiswaOrangtua.setPekerjaanAyah(beasiswaDto.getPekerjaanAyah());
            beasiswaOrangtua.setStatusAyah(beasiswaDto.getStatusAyah());
            beasiswaOrangtua.setAlamat(beasiswaDto.getAlamatOrangTua());
            beasiswaOrangtua.setNoTelpon(beasiswaDto.getNoTelponOrangTua());
            beasiswaOrangtua.setPenghasilanOrangtua(beasiswaDto.getPenghasilanOrangtua());
            beasiswaOrangtua.setJumlahTanggungan(beasiswaDto.getJumlahTanggungan());
            beasiswaOrangtua.setAlamat(beasiswaDto.getAlamatOrangTua());
            beasiswaOrangtua.setNoTelpon(beasiswaDto.getNoTelponOrangTua());
            pendaftarBeasiswaOrangtuaDao.save(beasiswaOrangtua);
        }

        PendaftarBeasiswaFile cekFile = pendaftarBeasiswaFileDao.findByPendaftarBeasiswa(pendaftar);
        if (cekFile == null) {
            PendaftarBeasiswaFile file = new PendaftarBeasiswaFile();
            file.setPendaftarBeasiswa(pendaftar);
            if (!fileSktm.isEmpty()) {
                logger.info("File SKTM Di isi");
                file.setSktm(beasiswaService.saveFile(fileSktm, "SKTM"));
            }
            if (!fileTagihanListrik.isEmpty()) {
                logger.info("File Tagihan Listrik di isi");
                file.setTagihanListrik(beasiswaService.saveFile(fileTagihanListrik, "TagihanListrik"));
            }
            if (!fileFotoRumah.isEmpty()) {
                logger.info("File Foto Rumah Di isi");
                file.setFotoRumah(beasiswaService.saveFile(fileFotoRumah, "FotoRumah"));
            }
            if (!fileResume.isEmpty()) {
                logger.info("File Resume Listrik di isi");
                file.setResume(beasiswaService.saveFile(fileResume, "Resume"));
            }
            if (!filePrestasi1.isEmpty()) {
                logger.info("File Prestasi Internasional Di isi");
                file.setPrestasi1(beasiswaService.saveFile(filePrestasi1, "Prestasi"));
            }
            if (!filePrestasi2.isEmpty()) {
                logger.info("File Prestasi Nasional Listrik di isi");
                file.setPrestasi2(beasiswaService.saveFile(filePrestasi2, "Prestasi"));
            }
            if (!filePrestasi3.isEmpty()) {
                logger.info("File Prestasi Provinsi Di isi");
                file.setPrestasi3(beasiswaService.saveFile(filePrestasi3, "Prestasi"));
            }
            if (!filePrestasi4.isEmpty()) {
                logger.info("File Prestasi Daerah di isi");
                file.setPrestasi4(beasiswaService.saveFile(filePrestasi4, "Prestasi"));
            }
            pendaftarBeasiswaFileDao.save(file);

        }else{
            cekFile.setPendaftarBeasiswa(pendaftar);
            if (!fileSktm.isEmpty()) {
                logger.info("File SKTM Di isi");
                cekFile.setSktm(beasiswaService.saveFile(fileSktm, "SKTM"));
            }
            if (!fileTagihanListrik.isEmpty()) {
                logger.info("File Tagihan Listrik di isi");
                cekFile.setTagihanListrik(beasiswaService.saveFile(fileTagihanListrik, "TagihanListrik"));
            }
            if (!fileFotoRumah.isEmpty()) {
                logger.info("File Foto Rumah Di isi");
                cekFile.setFotoRumah(beasiswaService.saveFile(fileFotoRumah, "FotoRumah"));
            }
            if (!fileResume.isEmpty()) {
                logger.info("File Resume Listrik di isi");
                cekFile.setResume(beasiswaService.saveFile(fileResume, "Resume"));
            }
            if (!filePrestasi1.isEmpty()) {
                logger.info("File Prestasi Internasional Di isi");
                cekFile.setPrestasi1(beasiswaService.saveFile(filePrestasi1, "Prestasi"));
            }
            if (!filePrestasi2.isEmpty()) {
                logger.info("File Prestasi Nasional Listrik di isi");
                cekFile.setPrestasi2(beasiswaService.saveFile(filePrestasi2, "Prestasi"));
            }
            if (!filePrestasi3.isEmpty()) {
                logger.info("File Prestasi Provinsi Di isi");
                cekFile.setPrestasi3(beasiswaService.saveFile(filePrestasi3, "Prestasi"));
            }
            if (!filePrestasi4.isEmpty()) {
                logger.info("File Prestasi Daerah di isi");
                cekFile.setPrestasi4(beasiswaService.saveFile(filePrestasi4, "Prestasi"));
            }
            pendaftarBeasiswaFileDao.save(cekFile);
        }

        return "redirect:detailPendaftar";
    }

    @GetMapping("/beasiswa/{beasiswaFile}/{jenis}/file")
    public ResponseEntity<byte[]> previewFile(@PathVariable PendaftarBeasiswaFile beasiswaFile, @PathVariable String jenis) {
        String lokasiFile = "";
        String namaFile = "";
        switch (jenis){
            case "SKTM":
                namaFile = beasiswaFile.getSktm();
                lokasiFile = uploadFile + File.separator + "SKTM" + File.separator + namaFile;
                break;
            case "TagihanListrik":
                namaFile = beasiswaFile.getTagihanListrik();
                lokasiFile = uploadFile + File.separator + "TagihanListrik" + File.separator + namaFile;
                break;
            case "FotoRumah":
                namaFile = beasiswaFile.getFotoRumah();
                lokasiFile = uploadFile + File.separator + "FotoRumah" + File.separator + namaFile;
                break;
            case "Resume":
                namaFile = beasiswaFile.getResume();
                lokasiFile = uploadFile + File.separator + "Resume" + File.separator + namaFile;
                break;
            case "Prestasi1":
                namaFile = beasiswaFile.getPrestasi1();
                lokasiFile = uploadFile + File.separator + "Prestasi" + File.separator + namaFile;
                break;
            case "Prestasi2":
                namaFile = beasiswaFile.getPrestasi2();
                lokasiFile = uploadFile + File.separator + "Prestasi" + File.separator + namaFile;
                break;
            case "Prestasi3":
                namaFile = beasiswaFile.getPrestasi3();
                lokasiFile = uploadFile + File.separator + "Prestasi" + File.separator + namaFile;
                break;
            case "Prestasi4":
                namaFile = beasiswaFile.getPrestasi4();
                lokasiFile = uploadFile + File.separator + "Prestasi" + File.separator + namaFile;
                break;
        }

        try {
            HttpHeaders headers = new HttpHeaders();
            if (namaFile.toLowerCase().endsWith("jpeg") || namaFile.toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            }else if (namaFile.toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if (namaFile.toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }

}

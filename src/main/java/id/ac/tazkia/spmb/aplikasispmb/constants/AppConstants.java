package id.ac.tazkia.spmb.aplikasispmb.constants;

public interface AppConstants {
    String ID_TAHUN_AJARAN ="001";
    String ROLE_PENDAFTAR ="pendaftar";
    String ROLE_BEASISWA = "beasiswa";
    String ROLE_ADMIN_BEASISWA = "adminbeasiswa";

    String ROLE_EDU ="educonsultans";
    String JENIS_BIAYA_PENDAFTARAN = "001";
    String JENIS_BIAYA_DAFTAR_ULANG = "002";
    String JENIS_BIAYA_ASRAMA = "003";
    String JENIS_BIAYA_ASRAMA_HAFIZ = "006";
    String JENIS_BIAYA_UKT = "004";
    String JENIS_BIAYA_WISUDA = "005";
    String JENIS_BIAYA_ALQURAN = "007";

}

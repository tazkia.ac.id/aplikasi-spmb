package id.ac.tazkia.spmb.aplikasispmb.entity;

import java.time.LocalDate;

import org.hibernate.annotations.GenericGenerator;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Entity @Getter @Setter
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class PendaftarDetail {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_pendaftar")
    private Pendaftar pendaftar;

    private String foto;

    @Column(nullable = false)
    @NotNull
    @NotEmpty
    private String tempatLahir;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull
    @Column(columnDefinition = "DATE")
    private LocalDate tanggalLahir;

    @NotNull
    @NotEmpty
    private String agama;

    @NotNull
    @NotEmpty
    private String jenisKelamin;

    @NotNull
    @NotEmpty
    @Size(max = 2)
    private String golonganDarah;

    @NotNull
    @NotEmpty
    private String noKtp;

    @NotNull
    @NotEmpty
    private String alamatRumah;

    @NotNull
    @NotEmpty
    private String negara;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_provinsi")
    private Provinsi provinsi;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_kokab")
    private KabupatenKota kokab;

    @NotNull
    @NotEmpty
    private String kodePos;

    private String nisn;

    @NotNull
    @NotEmpty
    private String jurusanSekolah;

    @NotNull
    @NotEmpty
    private String tahunLulus;

    @NotNull
    @NotEmpty
    private String statusSipil;

    @NotNull
    @NotEmpty
    private String rencanaBiaya;

    private String nim;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(columnDefinition = "DATE")
    @NotNull
    private LocalDate updateTime = LocalDate.now();


}

package id.ac.tazkia.spmb.aplikasispmb.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.tazkia.spmb.aplikasispmb.dao.CourseDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.DosenDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.PesertaCourseDao;
import id.ac.tazkia.spmb.aplikasispmb.dto.*;
import id.ac.tazkia.spmb.aplikasispmb.entity.Course;
import id.ac.tazkia.spmb.aplikasispmb.entity.Dosen;
import id.ac.tazkia.spmb.aplikasispmb.entity.PesertaCourse;
import id.ac.tazkia.spmb.aplikasispmb.entity.RunningNumber;
import id.ac.tazkia.spmb.aplikasispmb.service.NotifikasiService;
import id.ac.tazkia.spmb.aplikasispmb.service.RunningNumberService;
import org.flywaydb.core.internal.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import reactor.core.publisher.Mono;

import jakarta.annotation.PostConstruct;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import java.io.File;
import java.io.Reader;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class SitInController {
    private static final Logger logger = LoggerFactory.getLogger(SitInController.class);

    @Value("${upload.folder.dosen}")
    private String uploadFolder;

    @Value("${upload.folder.cover}")
    private String uploadCover;

    @Autowired
    private DosenDao dosenDao;
    @Autowired
    private CourseDao courseDao;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private PesertaCourseDao pesertaCourseDao;
    @Autowired
    private NotifikasiService notifikasiService;


    @GetMapping("/sit-in/dosen/list")
    public void ListDosen(@RequestParam(value = "id", required = false) String id,
                          @RequestParam(required = false)String namaDosen, Model m,
                          Pageable page){
        if(StringUtils.hasText(namaDosen)) {
            m.addAttribute("nama", namaDosen);
            m.addAttribute("listDosen", dosenDao.findByNamaDosenContainingIgnoreCaseOrderByNamaDosenDesc(namaDosen, page));
        } else {
            m.addAttribute("listDosen", dosenDao.findAll(page));
        }

//Edit
        //defaultnya, isi dengan object baru
        m.addAttribute("dosen", new Dosen());

        if (id != null && !id.isEmpty()){
            Dosen p= dosenDao.findById(id).get();
            if (p != null){
                m.addAttribute("dosen", p);
            }
        }
    }

    @PostMapping("/sit-in/dosen/list")
    public String saveDosen(@ModelAttribute @Valid Dosen dosen, BindingResult errors,
                            MultipartFile foto){
        try {
//           persiapan lokasi upload

            String idPeserta = dosen.getNamaDosen();
            String lokasiUpload = uploadFolder + File.separator + idPeserta;
            logger.debug("Lokasi upload : {}", lokasiUpload);
            new File(lokasiUpload).mkdirs();

            if (foto == null || foto.isEmpty()) {
                logger.info("File berkas kosong, tidak diproses");
                return idPeserta;
            }

            String namaFile = foto.getName();
            String jenisFile = foto.getContentType();
            String namaAsli = foto.getOriginalFilename();
            Long ukuran = foto.getSize();

            logger.debug("Nama File : {}", namaFile);
            logger.debug("Jenis File : {}", jenisFile);
            logger.debug("Nama Asli File : {}", namaAsli);
            logger.debug("Ukuran File : {}", ukuran);

            //memisahkan extensi
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }

            String idFile = UUID.randomUUID().toString();
            dosen.setFoto(idFile + "." + extension);
            File tujuan = new File(lokasiUpload + File.separator + dosen.getFoto());
            foto.transferTo(tujuan);
            logger.debug("File sudah dicopy ke : {}", tujuan.getAbsolutePath());
            dosenDao.save(dosen);

        } catch (Exception er) {
            logger.error(er.getMessage(), er);
        }


        return "redirect:/sit-in/dosen/list";
    }

    @GetMapping("/fotoDosen/{dosen}/foto/")
    public ResponseEntity<byte[]> tampilkanFoto(@PathVariable Dosen dosen) throws Exception {

        String lokasiFile = uploadFolder + File.separator + dosen.getNamaDosen()
                + File.separator + dosen.getFoto();
        logger.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (dosen.getFoto().toLowerCase().endsWith("jpeg") || dosen.getFoto().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (dosen.getFoto().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            logger.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


//CourseController
    @ModelAttribute("daftarDosen")
    public Iterable<Dosen> daftarDosen(){
    return dosenDao.findAll();
    }

    @GetMapping("/sit-in/course/list")
    public void listCourse(@RequestParam(required = false)String namaMatakuliah, Model m,
                           Pageable page){
        if(StringUtils.hasText(namaMatakuliah)) {
            m.addAttribute("namaMatakuliah", namaMatakuliah);
            m.addAttribute("listCourse", courseDao.findByNamaMatakuliahContainingIgnoreCaseOrderByNamaMatakuliahDesc(namaMatakuliah, page));
        } else {
            m.addAttribute("listCourse", courseDao.findAll(page));
        }
    }

    @GetMapping("/sit-in/course/form")
    public void formCourse(@RequestParam(value = "id", required = false) String id, Model m){
        //defaultnya, isi dengan object baru
        m.addAttribute("course", new Course());

        if (id != null && !id.isEmpty()){
            Course p= courseDao.findById(id).get();
            if (p != null){
                m.addAttribute("course", p);
            }
        }

        List<GetJadwalMatkul> getJadwalMatkuls = GetJadwalMatkul();
        m.addAttribute("listCourse", getJadwalMatkuls);



    }


    @PostMapping("/sit-in/course/form")
    public String fungsiFormCourse(@ModelAttribute @Valid Course course, BindingResult errors,
                           MultipartFile cover){
        try {
//           persiapan lokasi upload

            String idPeserta = course.getNamaMatakuliah();
            String lokasiUpload = uploadCover + File.separator + idPeserta;
            logger.debug("Lokasi upload : {}", lokasiUpload);
            new File(lokasiUpload).mkdirs();

            if (cover == null || cover.isEmpty()) {
                logger.info("File berkas kosong, tidak diproses");
                return idPeserta;
            }

            String namaFile = cover.getName();
            String jenisFile = cover.getContentType();
            String namaAsli = cover.getOriginalFilename();
            Long ukuran = cover.getSize();

            logger.debug("Nama File : {}", namaFile);
            logger.debug("Jenis File : {}", jenisFile);
            logger.debug("Nama Asli File : {}", namaAsli);
            logger.debug("Ukuran File : {}", ukuran);

            //memisahkan extensi
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }

            String idFile = UUID.randomUUID().toString();
            course.setCover(idFile + "." + extension);
            File tujuan = new File(lokasiUpload + File.separator + course.getCover());
            cover.transferTo(tujuan);
            logger.debug("File sudah dicopy ke : {}", tujuan.getAbsolutePath());

            course.setHarga(new BigDecimal(0));

            courseDao.save(course);

        } catch (Exception er) {
            logger.error(er.getMessage(), er);
        }


        return "redirect:/sit-in/course/list";
    }

    @GetMapping("/fotoCover/{course}/cover/")
    public ResponseEntity<byte[]> tampilkanCover(@PathVariable Course course) throws Exception {

        String lokasiFile = uploadCover + File.separator + course.getNamaMatakuliah()
                + File.separator + course.getCover();
        logger.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (course.getCover().toLowerCase().endsWith("jpeg") || course.getCover().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (course.getCover().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            logger.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }



    @GetMapping("/sitinprogram")
    public void indexSitIn(@RequestParam(required = false)String namaMatakuliah, Model m,
                           @PageableDefault(size = 9, sort = "namaMatakuliah", direction = Sort.Direction.DESC) Pageable page){
        if(StringUtils.hasText(namaMatakuliah)) {
            m.addAttribute("namaMatakuliah", namaMatakuliah);
            m.addAttribute("listCourse", courseDao.findByNamaMatakuliahContainingIgnoreCaseOrderByNamaMatakuliahDesc(namaMatakuliah, page));
        } else {
            m.addAttribute("listCourse", courseDao.findAll(page));
        }
    }

    @GetMapping("/sitinprogram-detail")
    public void indexSitInDetail(@RequestParam(value = "id", required = false) String id, Model m){
        m.addAttribute("pesertaCourse", new PesertaCourse());

        if (id != null && !id.isEmpty()){
            Course p= courseDao.findById(id).get();
            if (p != null){
                m.addAttribute("course", p);
            }
        }
    }

    @GetMapping("/sitin_sukses")
    public void sitinSukses(){}


    @PostMapping("/sitinprogram-detail")
    public  String savePesertaSitin(@ModelAttribute @Valid PesertaCourse pc, BindingResult errors, RedirectAttributes redirectAttributes){

        DateFormat dateFormat = new SimpleDateFormat("yy");
        String formattedDate = dateFormat.format(Calendar.getInstance().getTime());
        DateFormat dateFormatBln = new SimpleDateFormat("MM");
        String formattedDateBln = dateFormatBln.format(Calendar.getInstance().getTime());
        String formatNomor = "PC" + formattedDate + formattedDateBln ;

        pc.setNomor(generateNomorPeserta(formatNomor));
        pc.setStatus("Menunggu");
        pc.setPembayaran("Gratis");
        pesertaCourseDao.save(pc);
        redirectAttributes.addFlashAttribute("user", pc.getNama());
        return "redirect:/sitin_sukses";

    }

    @GetMapping("/sit-in/peserta-course/list")
    public void listPesertaCourse(@RequestParam(required = false)String nama, Model m,
                           Pageable page){
        if(StringUtils.hasText(nama)) {
            m.addAttribute("nama", nama);
            m.addAttribute("listPesertaCourse", pesertaCourseDao.findByNamaContainingIgnoreCaseOrderByNamaDesc(nama, page));
        } else {
            m.addAttribute("listPesertaCourse", pesertaCourseDao.findAll(page));
        }
    }


    @Autowired
    private RunningNumberService runningNumberService;

    public String generateNomorPeserta(String formatNomor) {
        RunningNumber terbaru = runningNumberService.generate(formatNomor);

        logger.debug("Format NIM : {}", formatNomor);
        logger.debug("Nomer Terakhir : {}", terbaru.getNomerTerakhir());

        String nomorNim = formatNomor + String.format("%03d", terbaru.getNomerTerakhir());
        logger.info("Nomor Nim : {}", nomorNim);

        return nomorNim;
    }



//Api Dari Smile

    WebClient webClientSmile = WebClient.builder()
            .baseUrl("https://smile.tazkia.ac.id")
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .build();

    public List<GetKelasDto> apiKelas(){
                return webClientSmile.get()
                .uri("/api/getKelas")
                .retrieve().bodyToFlux(GetKelasDto.class)
                .collectList()
                .block();
    }

    @GetMapping("/api/getJadwalMatkul")
    @ResponseBody
    public List<GetJadwalMatkul> GetJadwalMatkul(){
                return webClientSmile.get()
                .uri("/api/getDetailJadwal")
                .retrieve().bodyToFlux(GetJadwalMatkul.class)
                .collectList()
                .block();

    }

    public List<GetDosen> GetDosen(){
                return webClientSmile.get()
                .uri("/api/apiGetKaryawan")
                .retrieve().bodyToFlux(GetDosen.class)
                .collectList()
                .block();

    }

    @GetMapping ("/api/getPesertaCourse")
    @ResponseBody
    @Transactional
    public List<PesertaCourseDto> getPesertaCourse(){
        List<PesertaCourse> pesertaCourse = pesertaCourseDao.findByStatus("Menunggu");
        List<PesertaCourseDto> pesertaCourseDtos = new ArrayList<>();

        for (PesertaCourse pc : pesertaCourse) {
            PesertaCourseDto pcd = new PesertaCourseDto();
            pcd.setNomor(pc.getNomor());
            pcd.setNama(pc.getNama());
            pcd.setEmail(pc.getEmail());
            pcd.setNoHp(pc.getNoHp());
            pcd.setIdElearning(pc.getCourse().getIdElearning());
            pcd.setStatus(pc.getStatus());
            pesertaCourseDtos.add(pcd);

//            pesertaCourseDao.updatePesertaCourse(pc.getId());

            notifikasiService.pesertaCourse(pcd);
        }

        return pesertaCourseDtos;
    }

    private WebClient webClient;
    @Value("${feeder.elearning}") private String feederUrl;

    @PostConstruct
    public void inisalisaiWebClient(){
        webClient = WebClient.builder().
                baseUrl(feederUrl).
                defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE).
                build();
    }


    public List<PesertaCourseDto> postPeserta(){
        List<PesertaCourse> pesertaCourse = pesertaCourseDao.findByStatus("Menunggu");
        List<PesertaCourseDto> pesertaCourseDtos = new ArrayList<>();

        for (PesertaCourse pc : pesertaCourse) {
            PesertaCourseDto pcd = new PesertaCourseDto();
            pcd.setNomor(pc.getNomor());
            pcd.setNama(pc.getNama());
            pcd.setEmail(pc.getEmail());
            pcd.setNoHp(pc.getNoHp());
            pcd.setIdElearning(pc.getCourse().getIdElearning());
            pcd.setStatus(pc.getStatus());
            pesertaCourseDtos.add(pcd);


            notifikasiService.pesertaCourse(pcd);
            pesertaCourseDao.updatePesertaCourse(pc.getId());

        }
        try {
            String jsonRequest = objectMapper.writeValueAsString(pesertaCourseDtos);
            System.out.println(jsonRequest);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        NimMahasiswaSmileResponse response = webClient.post()
                .header("Authorization", "Bearer MY_SECRET_TOKEN")
                .accept(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromObject(pesertaCourseDtos))
                .retrieve().bodyToMono(NimMahasiswaSmileResponse.class)
                .block();

        if (response == null) {
            return null;
        }
        if (response == null) {
            System.out.println("Gagal : " + response);
            return null;
        }
        System.out.println("Respon : " + response);

        System.out.println(pesertaCourseDtos);
        return pesertaCourseDtos;

    }


    @PostMapping("/sitin/migrasi")
    @Transactional
    public String migrasiElearnning(){
        postPeserta();

        return "redirect:/sit-in/peserta-course/list";
    }

}

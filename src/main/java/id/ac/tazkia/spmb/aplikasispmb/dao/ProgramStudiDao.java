package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.dto.ApiPendaftarDto;
import id.ac.tazkia.spmb.aplikasispmb.entity.Institut;
import id.ac.tazkia.spmb.aplikasispmb.entity.Jenjang;
import id.ac.tazkia.spmb.aplikasispmb.entity.ProgramStudi;
import id.ac.tazkia.spmb.aplikasispmb.entity.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProgramStudiDao extends PagingAndSortingRepository<ProgramStudi, String>, CrudRepository<ProgramStudi, String> {
    Page<ProgramStudi> findByNamaContainingIgnoreCaseOrderByNama(String nama, Pageable page);

    List<ProgramStudi> findByJenjangAndInstitutAndStatus(Jenjang jenjang, Institut institut, Status status);

    @Query("select p from ProgramStudi p where p.id >= :id and p.id <= :dua")
    List<ProgramStudi> cariProdi(@Param("id")String id,@Param("dua")String dua);

    @Query("select p from ProgramStudi p where p.id <= :id and p.id != :tidak and p.id != :tidak2")
    List<ProgramStudi> cariProdiS1(@Param("id")String id,@Param("tidak")String tidak,@Param("tidak2")String tidak2);

    @Query("select p from ProgramStudi p where p.id = :id")
    List<ProgramStudi> cariProdiD3(@Param("id")String id);

    @Query(value = "select a.nama,coalesce(jumlah,0) as jumlah from program_studi as a left join\n" +
            "(select e.id,e.nama,count(b.id)as jumlah from tagihan as a\n" +
            "inner join pendaftar as b on a.id_pendaftar = b.id\n" +
            "inner join pembayaran as c on a.id = c.id_tagihan\n" +
            "inner join tahun_ajaran as d on b.id_tahun = d.id\n" +
            "inner join program_studi as e on b.id_program_studi = e.id\n" +
            "where id_jenisbiaya = '002' and lunas = true and d.aktif = 'AKTIF' group by b.id_program_studi) as b\n" +
            "on a.id = b.id where kode_fakultas is not null and a.id <> '005'\n" +
            "order by coalesce(jumlah,0) desc", nativeQuery = true)
    List<ApiPendaftarDto> hitung();



    @Query(value = "select nama,count(id)as jumlah from\n" +
            "(select d.nama,b.id,d.id as tahun from tagihan as a\n" +
            "inner join pendaftar as b on a.id_pendaftar = b.id\n" +
            "inner join pembayaran as c on a.id = c.id_tagihan\n" +
            "inner join tahun_ajaran as d on b.id_tahun = d.id\n" +
            "inner join program_studi as e on b.id_program_studi = e.id\n" +
            "where id_jenisbiaya = '002' and lunas = true and d.aktif in ('NONAKTIF','AKTIF') \n" +
            "and left(d.nama,4) >= year(DATE_SUB(now(), INTERVAL 3 YEAR)) group by b.id) as a group by tahun order by nama", nativeQuery = true)
    List<ApiPendaftarDto> pendaftarPertahun();
}

package id.ac.tazkia.spmb.aplikasispmb.dao.beasiswa;

import id.ac.tazkia.spmb.aplikasispmb.entity.beasiswa.PendaftarBeasiswa;
import id.ac.tazkia.spmb.aplikasispmb.entity.beasiswa.PendaftarBeasiswaFile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PendaftarBeasiswaFileDao extends PagingAndSortingRepository<PendaftarBeasiswaFile, String>, CrudRepository<PendaftarBeasiswaFile, String> {

    PendaftarBeasiswaFile findByPendaftarBeasiswa(PendaftarBeasiswa pendaftarBeasiswa);

    Integer countByPrestasi1IsNotNull();

}

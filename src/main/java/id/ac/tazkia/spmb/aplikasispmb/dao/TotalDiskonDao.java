package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.TotalDiskon;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TotalDiskonDao extends PagingAndSortingRepository<TotalDiskon, String >, CrudRepository<TotalDiskon, String > {
    TotalDiskon findByBulan(String bulan);

    @Query(value = "select sum(total) from total_diskon;", nativeQuery = true)
    Long countTotalDiskonsBy();
}

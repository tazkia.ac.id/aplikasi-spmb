package id.ac.tazkia.spmb.aplikasispmb.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Entity @Getter
@Setter
public class Pendaftar {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @OneToOne
    @JoinColumn(name = "id_leads")
    private Leads leads;

    @NotNull
    @NotEmpty
    private String nomorRegistrasi;

    @ManyToOne
    @JoinColumn(name = "id_program_studi")
    private ProgramStudi programStudi;

    @Column(nullable = false)
    private String konsentrasi;

    @ManyToOne @JoinColumn(name = "kota_asal_sekolah")
    private KabupatenKota idKabupatenKota;

    @Column(nullable = false)
    @NotNull
    @NotEmpty
    private String namaAsalSekolah;

//    @Column(nullable = false)
    private String perekomendasi;

//    @Column(nullable = false)
    private String namaPerekomendasi;

    @OneToOne
    @JoinColumn(name = "id_tahun")
    private TahunAjaran tahunAjaran;

    @OneToOne
    @JoinColumn(name = "id_referal")
    private Referal referal;

    @OneToOne(mappedBy = "pendaftar")
    @JsonBackReference
    private PendaftarDetail pendaftarDetail;

    @OneToOne(mappedBy = "pendaftar")
    @JsonBackReference
    private JadwalTest jadwalTest;

    @Column(columnDefinition="LONGTEXT")
    private String alasanMemilihProdi;

    private BigDecimal uangPangkal;

    private String keteranganTagihan;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate expiredDate;

    @OneToOne
    @JoinColumn(name = "user_update")
    private User userUpdate;

    @Column(columnDefinition = "DATE")
    private LocalDate tglUpdate;

    @ManyToOne
    @JoinColumn(name = "user_fu")
    private Followup followup;

    private String status;
    private String note;
    private String notePic;
}

package id.ac.tazkia.spmb.aplikasispmb.dto;

import lombok.Data;

@Data
public class ApiRekapPendaftarDto {

    private String nama;

    private Integer jumlah;

}

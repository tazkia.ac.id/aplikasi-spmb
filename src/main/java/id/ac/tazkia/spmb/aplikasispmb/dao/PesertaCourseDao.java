package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.PesertaCourse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigInteger;
import java.util.List;

public interface PesertaCourseDao extends PagingAndSortingRepository<PesertaCourse, String >, CrudRepository<PesertaCourse, String > {
    Page<PesertaCourse> findByNamaContainingIgnoreCaseOrderByNamaDesc(String nama, Pageable page);

    List<PesertaCourse> findByStatus(String status);

    @Modifying
    @Query(value = "update peserta_course set status = 'Berhasil' where id = ?1", nativeQuery = true)
    void updatePesertaCourse(String  id);
}

package id.ac.tazkia.spmb.aplikasispmb.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "s_user")
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    private String username;
    private Boolean active;

    @NotNull
    @ManyToOne @JoinColumn(name = "id_role")
    private Role role;
}


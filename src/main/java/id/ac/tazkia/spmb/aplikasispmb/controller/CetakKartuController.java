package id.ac.tazkia.spmb.aplikasispmb.controller;

import fr.opensagres.xdocreport.converter.ConverterTypeTo;
import fr.opensagres.xdocreport.converter.Options;
import fr.opensagres.xdocreport.core.document.DocumentKind;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

@Controller
public class CetakKartuController {
    private static final Logger logger = LoggerFactory.getLogger(CetakKartuController.class);

    @Value("classpath:kartuUjian.odt")
    private Resource templateKartuUjian;
    @Value("${upload.folder}")
    private String uploadFolder;
    @Autowired private UserDao userDao;
    @Autowired private LeadsDao leadsDao;
    @Autowired private PendaftarDao pendaftarDao;
    @Autowired private JadwalTestDao jadwalTestDao;
    @Autowired private PendaftarDetailDao pendaftarDetailDao;
    @Autowired private PembayaranController pembayaranController;

    @GetMapping("/kartuUjian/form")
    public void formCetakKartu(Model model, Authentication currentUser){
        logger.debug("Authentication class : {}", currentUser.getClass().getName());

        if (currentUser == null) {
            logger.warn("Current user is null");
        }

        String username = ((UserDetails) currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if (u == null) {
            logger.warn("Username {} not found in database ", username);
        }

        Leads leads = leadsDao.findByUser(u);
        Pendaftar pendaftar = pendaftarDao.findByLeads(leads);
        if (pendaftar != null) {
            model.addAttribute("pendaftar", pendaftar);
        }
        JadwalTest jt = jadwalTestDao.findByPendaftar(pendaftar);
        model.addAttribute("cetakKartu", jt);
        if (jt != null) {
            model.addAttribute("harinya",jt.getTanggalTest().getDayOfWeek());
        }

//Cek Pembayaran
        Pembayaran pembayaran = pembayaranController.cekPembayaran(pendaftar);
        model.addAttribute("pembayaran", pembayaran);
    }

    @GetMapping("/cetakKartu/{pendaftar}/foto/")
    public ResponseEntity<byte[]> tampilkanFoto(@PathVariable Pendaftar pendaftar) throws Exception {
        PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pendaftar);

        String lokasiFile = uploadFolder + File.separator + pendaftar.getId()
                + File.separator + pendaftarDetail.getFoto();
        logger.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (pendaftarDetail.getFoto().toLowerCase().endsWith("jpeg") || pendaftarDetail.getFoto().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (pendaftarDetail.getFoto().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            logger.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    @GetMapping("/kartu")
    public void kartuUjian(@RequestParam(name = "id") JadwalTest jadwalTest,
                           HttpServletResponse response){
        try {
            // 0. Setup converter
            Options options = Options.getFrom(DocumentKind.ODT).to(ConverterTypeTo.PDF);

            // 1. Load template dari file
            InputStream in = templateKartuUjian.getInputStream();

            // 2. Inisialisasi template engine, menentukan sintaks penulisan variabel
            IXDocReport report = XDocReportRegistry.getRegistry().
                    loadReport(in, TemplateEngineKind.Freemarker);

            // 3. Context object, untuk mengisi variabel

            IContext ctx = report.createContext();
            ctx.put("nomor", jadwalTest.getPendaftar().getNomorRegistrasi());
            ctx.put("nama", jadwalTest.getPendaftar().getLeads().getNama());
            ctx.put("jenjang", jadwalTest.getPendaftar().getLeads().getJenjang());
            ctx.put("programStudi", jadwalTest.getPendaftar().getProgramStudi().getNama());

            if (jadwalTest.getJenisTest() == JenisTest.JPA) {
                ctx.put("jenisTest", "Jalur Potensi Akademik (JPA)");
            } else {
                ctx.put("jenisTest", "Test Potensi Akademik (TPA)");
            }

            ctx.put("tanggalTest", jadwalTest.getTanggalTest().getDayOfWeek() + "," + jadwalTest.getTanggalTest());

            response.setHeader("Content-Disposition", "attachment;filename=kartu-ujian.pdf");
            OutputStream out = response.getOutputStream();
            report.convert(ctx, options, out);
            out.flush();
        } catch (Exception err){
            logger.error(err.getMessage(), err);
        }
    }


    @Value("${upload.folder.raport}")
    private String fileFolder;

    @GetMapping("/raport/{raport}/raport/")
    public ResponseEntity<byte[]> tampilkanBerkas(@PathVariable JadwalTest raport) throws Exception {
        JadwalTest jt = jadwalTestDao.findById(raport.getId()).get();

        System.out.println("Jadwal Test : " + jt.getPendaftar().getNomorRegistrasi());
        String lokasiFile = fileFolder + File.separator + jt.getPendaftar().getNomorRegistrasi()
                + File.separator + jt.getFileRaport();
        logger.debug("Lokasi raport bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (raport.getFileRaport().toLowerCase().endsWith("jpeg") || raport.getFileRaport().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (raport.getFileRaport().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if (raport.getFileRaport().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            logger.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}

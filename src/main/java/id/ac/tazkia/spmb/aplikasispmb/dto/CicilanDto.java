package id.ac.tazkia.spmb.aplikasispmb.dto;

import id.ac.tazkia.spmb.aplikasispmb.entity.Tagihan;
import lombok.Data;

import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class CicilanDto {
    private String id;

//    @NotNull
    private Tagihan tagihan;

    @NotNull
    private String keterangan;
    private String keteranganDis;

    private String cicilan;

    private BigDecimal nilaiKaryawan;

    private BigDecimal c1x1;

    private BigDecimal c2x1;
    private BigDecimal c2x2;

    private  BigDecimal c3x1;
    private  BigDecimal c3x2;
    private  BigDecimal c3x3;

    private  BigDecimal c4x1;
    private  BigDecimal c4x2;
    private  BigDecimal c4x3;
    private  BigDecimal c4x4;


    private BigDecimal perbulan;

}

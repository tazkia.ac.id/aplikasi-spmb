package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.RunningNumber;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import jakarta.persistence.LockModeType;

public interface RunningNumberDao extends PagingAndSortingRepository<RunningNumber, Long>, CrudRepository<RunningNumber, Long> {
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    public RunningNumber findByNama(String nama);
}

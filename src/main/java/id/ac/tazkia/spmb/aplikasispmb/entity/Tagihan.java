package id.ac.tazkia.spmb.aplikasispmb.entity;

import id.ac.tazkia.spmb.aplikasispmb.constants.ZahirExport;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

@Entity @Data
public class Tagihan {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_pendaftar")
    private Pendaftar pendaftar;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_jenisbiaya")
    private JenisBiaya jenisBiaya;

    @NotNull @NotEmpty
    private String nomorTagihan;

    @NotNull @Column(columnDefinition = "DATE")
    private LocalDate tanggalTagihan;

    @NotNull @NotEmpty
    private String keterangan;

    @NotNull @Min(0)
    private BigDecimal nilai;

    @NotNull
    private Boolean lunas = Boolean.FALSE;

    private String cicilan;

    @Column(columnDefinition = "DATE")
    private LocalDate tanggalJatuhTempo;

    @Min(0)
    private BigDecimal totalTagihan;

    @Min(0)
    private BigDecimal sisaTagihan;

    @Enumerated(EnumType.STRING)
    private ZahirExport zahirExport = ZahirExport.NEW;

}

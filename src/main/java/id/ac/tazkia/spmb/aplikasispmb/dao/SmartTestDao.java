package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.SmartTest;
import id.ac.tazkia.spmb.aplikasispmb.entity.TahunAjaran;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface SmartTestDao extends PagingAndSortingRepository<SmartTest, String>, CrudRepository<SmartTest, String> {

    Page<SmartTest> findAllByTahunAjaran(TahunAjaran tahunAjaran, Pageable pageable);
    Page<SmartTest> findByAsalSekolahAndTahunAjaranContainingIgnoreCase(String asalSekolah,TahunAjaran tahunAjaran, Pageable pageable);

    @Query(value = "select * from smart_test where nama like :nama and id_tahun = :tahunAjaran;", nativeQuery = true)
    Page<SmartTest> findByNamaSt (@Param("nama")String nama,TahunAjaran tahunAjaran, Pageable pageable);
    Page<SmartTest> findAllByNamaContainingIgnoreCaseAndTahunAjaran(String nama, TahunAjaran ta, Pageable pageable);

    Page<SmartTest> findByTahunAjaranAndNamaContainingIgnoreCaseAndAsalSekolahContainingIgnoreCase(TahunAjaran tahunAjaran,String nama, String sekolah,  Pageable pageable);
    Page<SmartTest> findByNamaContainingIgnoreCaseOrAsalSekolahContainingIgnoreCaseAndTahunAjaran(String nama, String sekolah,TahunAjaran tahunAjaran, Pageable pageable);




}

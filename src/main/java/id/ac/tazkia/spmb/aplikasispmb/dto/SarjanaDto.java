package id.ac.tazkia.spmb.aplikasispmb.dto;

import id.ac.tazkia.spmb.aplikasispmb.entity.JenisTest;
import id.ac.tazkia.spmb.aplikasispmb.entity.KabupatenKota;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Column;
import java.time.LocalDate;

@Data
public class SarjanaDto {
    private String id;
    private String nama;
    private String email;
    private String noHp;
    private String password;
    private String nomorRegistrasi;
    private KabupatenKota idKabupatenKota;
    private String namaAsalSekolah;
    private String programStudi;
    private JenisTest jenisTest;
    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalTest;
    private String kode;
    private String konsentrasi;
    private String idSubscribe;
    private String lulusan;

}

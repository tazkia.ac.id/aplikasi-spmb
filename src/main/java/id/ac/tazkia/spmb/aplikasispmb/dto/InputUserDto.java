package id.ac.tazkia.spmb.aplikasispmb.dto;

import id.ac.tazkia.spmb.aplikasispmb.entity.Role;
import lombok.Data;

@Data
public class InputUserDto {

    private String id;
    private String username;
    private String password;
    private Role role;
    private Boolean status;

}

package id.ac.tazkia.spmb.aplikasispmb.dto;

public interface ApiPendaftarDto {

    String getNama();

    Integer getJumlah();

}

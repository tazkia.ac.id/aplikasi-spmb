package id.ac.tazkia.spmb.aplikasispmb.service;

import id.ac.tazkia.spmb.aplikasispmb.dao.beasiswa.PendaftarBeasiswaFileDao;
import id.ac.tazkia.spmb.aplikasispmb.entity.beasiswa.PendaftarBeasiswa;
import id.ac.tazkia.spmb.aplikasispmb.entity.beasiswa.PendaftarBeasiswaFile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

@Service @Slf4j
public class BeasiswaService {

    @Value("${upload.folder.beasiswa}")
    private String uploadFile;

    @Autowired private PendaftarBeasiswaFileDao pendaftarBeasiswaFileDao;

    public void uploadFileBeasiswa(PendaftarBeasiswa pendaftarBeasiswa, MultipartFile fileSktm, MultipartFile fileTagihanListrik,
                                   MultipartFile fileFotoRumah, MultipartFile fileResume, MultipartFile filePrestasi1, MultipartFile filePrestasi2,
                                   MultipartFile filePrestasi3, MultipartFile filePrestasi4) throws IOException {

        String lokasiSktm = uploadFile + File.separator + "SKTM";
        String lokasiTagihanListrik = uploadFile + File.separator + "TagihanListrik";
        String lokasiFotoRumah = uploadFile + File.separator + "FotoRumah";
        String lokasiResume = uploadFile + File.separator + "Resume";
        String lokasiPrestasi = uploadFile + File.separator + "Prestasi";

        PendaftarBeasiswaFile cekFile = pendaftarBeasiswaFileDao.findByPendaftarBeasiswa(pendaftarBeasiswa);
        if (cekFile == null) {
            PendaftarBeasiswaFile file = new PendaftarBeasiswaFile();
            file.setPendaftarBeasiswa(pendaftarBeasiswa);
//            file.setSktm(saveFile(fileSktm, lokasiSktm));
//            file.setTagihanListrik(saveFile(fileTagihanListrik, lokasiTagihanListrik));
//            file.setFotoRumah(saveFile(fileFotoRumah, lokasiFotoRumah));
//            file.setResume(saveFile(fileResume, lokasiResume));
//            file.setPrestasi1(saveFile(filePrestasi1, lokasiPrestasi));
//            file.setPrestasi2(saveFile(filePrestasi2, lokasiPrestasi));
//            file.setPrestasi3(saveFile(filePrestasi3, lokasiPrestasi));
//            file.setPrestasi4(saveFile(filePrestasi4, lokasiPrestasi));

            pendaftarBeasiswaFileDao.save(file);
        }else{

            cekFile.setPendaftarBeasiswa(pendaftarBeasiswa);
//            cekFile.setSktm(saveFile(fileSktm, lokasiSktm));
//            cekFile.setTagihanListrik(saveFile(fileTagihanListrik, lokasiTagihanListrik));
//            cekFile.setFotoRumah(saveFile(fileFotoRumah, lokasiFotoRumah));
//            cekFile.setResume(saveFile(fileResume, lokasiResume));
//            cekFile.setPrestasi1(saveFile(filePrestasi1, lokasiPrestasi));
//            cekFile.setPrestasi2(saveFile(filePrestasi2, lokasiPrestasi));
//            cekFile.setPrestasi3(saveFile(filePrestasi3, lokasiPrestasi));
//            cekFile.setPrestasi4(saveFile(filePrestasi4, lokasiPrestasi));

            pendaftarBeasiswaFileDao.save(cekFile);
        }

    }

    public String saveFile(MultipartFile file, String jenis) throws IOException {
        String namaFile = file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();

        log.debug("Nama File : {}", namaFile);
        log.debug("Jenis File : {}", jenisFile);
        log.debug("Nama Asli File : {}", namaAsli);
        log.debug("Ukuran File : {}", ukuran);

        String lokasiFile = "";
        if ("SKTM".equals(jenis)) {
            lokasiFile = uploadFile + File.separator + "SKTM";
        }
        if ("TagihanListrik".equals(jenis)) {
            lokasiFile = uploadFile + File.separator + "TagihanListrik";
        }
        if ("FotoRumah".equals(jenis)) {
            lokasiFile = uploadFile + File.separator + "FotoRumah";
        }
        if ("Resume".equals(jenis)) {
            lokasiFile = uploadFile + File.separator + "Resume";
        }
        if ("Prestasi".equals(jenis)) {
            lokasiFile = uploadFile + File.separator + "Prestasi";
        }

        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }

        String idFile = UUID.randomUUID().toString();
        log.debug("Lokasi upload : {}", lokasiFile);
        new File(lokasiFile).mkdirs();
        String doneFile = idFile + "." + extension;
        File tujuan = new File(lokasiFile + File.separator + doneFile);
        file.transferTo(tujuan);
        log.debug("File sudah dicopy ke : {}", tujuan.getAbsolutePath());

        return doneFile;
    }

}

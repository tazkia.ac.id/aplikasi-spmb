package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.Grade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;

public interface GradeDao extends PagingAndSortingRepository<Grade, String >, CrudRepository<Grade, String > {
    Page<Grade> findByNamaContainingIgnoreCaseOrderByNamaDesc(String nama, Pageable page);
    Grade findTopByNilaiMinimumLessThanOrderByNilaiMinimumDesc(BigDecimal nilai);
}

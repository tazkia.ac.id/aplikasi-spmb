package id.ac.tazkia.spmb.aplikasispmb.dto;

import lombok.Data;

@Data
public class NimMahasiswaSmileResponse {
    private String massage;
    private String code;
}

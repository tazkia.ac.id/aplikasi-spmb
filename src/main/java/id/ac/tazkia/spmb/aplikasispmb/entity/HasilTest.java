package id.ac.tazkia.spmb.aplikasispmb.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity @Data
public class HasilTest {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @ManyToOne @JoinColumn (name="id_jadwal")
    private JadwalTest jadwalTest;

    @NotNull
    private BigDecimal nilai;

    @ManyToOne @JoinColumn (name="id_grade")
    private Grade grade;

    @ManyToOne @JoinColumn (name="id_periode")
    private Periode periode;

    private String keterangan;

    @ManyToOne @JoinColumn (name="user_insert")
    private User user;

    @NotNull
    private LocalDateTime tanggalInsert = LocalDateTime.now();



}

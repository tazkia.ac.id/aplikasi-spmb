package id.ac.tazkia.spmb.aplikasispmb.dao.beasiswa;

import id.ac.tazkia.spmb.aplikasispmb.entity.User;
import id.ac.tazkia.spmb.aplikasispmb.entity.beasiswa.PendaftarBeasiswa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PendaftarBeasiswaDao extends PagingAndSortingRepository<PendaftarBeasiswa, String>, CrudRepository<PendaftarBeasiswa, String> {

    PendaftarBeasiswa findByEmail(String email);

    PendaftarBeasiswa findByUser(User user);

    Integer countByStatusOrtu(String statusOrtu);

    Integer countAllBy();

    Page<PendaftarBeasiswa> findAllByOrderByIdAsc(Pageable pageable);

    @Query("SELECT pb.provinsi.nama, COUNT(pb) FROM PendaftarBeasiswa pb GROUP BY pb.provinsi ORDER BY COUNT(pb) DESC")
    List<Object[]> findTop5Provinsi(Pageable pageable);

    @Query("SELECT pb.kabupatenKota.nama, COUNT(pb) FROM PendaftarBeasiswa pb GROUP BY pb.kabupatenKota ORDER BY COUNT(pb) DESC")
    List<Object[]> findTop5Kabupaten(Pageable pageable);

}

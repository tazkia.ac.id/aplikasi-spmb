package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface HasilTestDao extends PagingAndSortingRepository<HasilTest, String>, CrudRepository <HasilTest, String> {
    HasilTest findByJadwalTest(JadwalTest jadwalTest);

    Page<HasilTest> findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarLeadsNamaContainingIgnoreCase(TahunAjaran ta, String nama, Pageable pageable);
    Page<HasilTest> findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarLeadsNamaContainingIgnoreCaseAndJadwalTestPendaftarLeadsJenjangAndJadwalTestPendaftarKonsentrasiOrderByTanggalInsertDesc(TahunAjaran ta, String nama,Jenjang jenjang, String konsentrasi, Pageable pageable);
    Page<HasilTest> findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarLeadsNamaContainingIgnoreCaseAndJadwalTestPendaftarLeadsJenjangOrderByTanggalInsertDesc(TahunAjaran ta, String nama,Jenjang jenjang, Pageable pageable);
    Page<HasilTest> findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarLeadsNamaContainingIgnoreCaseAndJadwalTestPendaftarFollowupAndJadwalTestPendaftarLeadsJenjangAndJadwalTestPendaftarKonsentrasiOrderByTanggalInsertDesc(TahunAjaran ta, String nama, Followup fl,Jenjang jenjang, String konsentrasi, Pageable pageable);
    Page<HasilTest> findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarLeadsNamaContainingIgnoreCaseAndJadwalTestPendaftarFollowupAndJadwalTestPendaftarLeadsJenjangOrderByTanggalInsertDesc(TahunAjaran ta, String nama, Followup fl,Jenjang jenjang, Pageable pageable);
    Page<HasilTest> findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarFollowupAndJadwalTestPendaftarLeadsJenjangAndJadwalTestPendaftarKonsentrasiOrderByTanggalInsertDesc(TahunAjaran ta, Followup fl, Jenjang jenjang, String konsentrasi, Pageable pageable);
    Page<HasilTest> findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarFollowupAndJadwalTestPendaftarLeadsJenjangOrderByTanggalInsertDesc(TahunAjaran ta, Followup fl, Jenjang jenjang, Pageable pageable);
    Page<HasilTest> findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarLeadsJenjangAndJadwalTestPendaftarKonsentrasiOrderByTanggalInsertDesc(TahunAjaran ta,Jenjang jenjang, String konsentrasi, Pageable pageable);
    Page<HasilTest> findAllByJadwalTestPendaftarTahunAjaranAndJadwalTestPendaftarLeadsJenjangOrderByTanggalInsertDesc(TahunAjaran ta,Jenjang jenjang,Pageable pageable);
    Page<HasilTest> findAllByJadwalTestPendaftarTahunAjaran(TahunAjaran ta, Pageable pageable);
    Iterable<HasilTest> findAllByJadwalTestPendaftarTahunAjaranAktif(Status ta);
    Iterable<HasilTest> findAllByJadwalTestPendaftarTahunAjaranAktifAndJadwalTestPendaftar(Status ta, Pendaftar p);

}

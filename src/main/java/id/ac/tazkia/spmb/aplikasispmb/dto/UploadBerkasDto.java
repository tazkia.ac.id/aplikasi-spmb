package id.ac.tazkia.spmb.aplikasispmb.dto;

import id.ac.tazkia.spmb.aplikasispmb.entity.JenisBerkas;
import lombok.Data;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotNull;

@Data
public class UploadBerkasDto {


    @NotNull
    private String pendaftar;

    @Enumerated(EnumType.STRING)
    private JenisBerkas jenisBerkas1;

    @NotNull
    private String fileBerkas1;

    @Enumerated(EnumType.STRING)
    private JenisBerkas jenisBerkas2;

    @NotNull
    private String fileBerkas2;

    @Enumerated(EnumType.STRING)
    private JenisBerkas jenisBerkas3;

    @NotNull
    private String fileBerkas3;

    @Enumerated(EnumType.STRING)
    private JenisBerkas jenisBerkas4;

    @NotNull
    private String fileBerkas4;

    @Enumerated(EnumType.STRING)
    private JenisBerkas jenisBerkas5;

    @NotNull
    private String fileBerkas5;

    private String alasan;

}

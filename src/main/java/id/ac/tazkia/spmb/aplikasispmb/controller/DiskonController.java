package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.dao.DiskonDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.TotalDiskonDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.UserDao;
import id.ac.tazkia.spmb.aplikasispmb.entity.Diskon;
import id.ac.tazkia.spmb.aplikasispmb.entity.TotalDiskon;
import id.ac.tazkia.spmb.aplikasispmb.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.validation.Valid;
import java.time.LocalDateTime;

@Controller
public class DiskonController {
    private static final Logger logger = LoggerFactory.getLogger(DiskonController.class);


    @Autowired private DiskonDao diskonDao;
    @Autowired private UserDao userDao;
    @Autowired private TotalDiskonDao totalDiskonDao;

    @GetMapping("/diskon/list")
    public void listReferal(Model m, Pageable page) {
        m.addAttribute("daftarDiskon", diskonDao.findAll(page));
    }



    @GetMapping("/diskon/form")
    public String  referalForm(@RequestParam(value = "id", required = false) String id,
                               Model m){
        //defaultnya, isi dengan object baru
        m.addAttribute("diskon", new Diskon());

        if (id != null && !id.isEmpty()){
            Diskon d= diskonDao.findById(id).get();
            if (d != null){
                m.addAttribute("diskon", d);
            }
        }
        return "diskon/form";
    }


    @PostMapping("/diskon/form")
    public String prosesForm(@Valid Diskon d, BindingResult errors, Authentication currentUser){
        logger.debug("Authentication class : {}",currentUser.getClass().getName());

        if(currentUser == null){
            logger.warn("Current user is null");
        }

        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }

        d.setUser(u);
        d.setTanggalInsert(LocalDateTime.now());
        diskonDao.save(d);
        return "redirect:/diskon/list";
    }

    @GetMapping("/diskon/listPengeluaran")
    public void listPengeluaran(Model m, Pageable page) {
        m.addAttribute("daftarDiskon", totalDiskonDao.findAll(page));
        m.addAttribute("totalAkhir", totalDiskonDao.countTotalDiskonsBy());
    }

}

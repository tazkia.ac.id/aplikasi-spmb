package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.dto.UploadError;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Controller
public class NilaiBiayaController {
    @Autowired
    private NilaiBiayaDao nilaiBiayaDao;
    @Autowired
    private JenisBiayaDao jenisBiayaDao;
    @Autowired
    private GradeDao gradeDao;
    @Autowired
    private ProgramStudiDao programStudiDao;
    @Autowired
    private PeriodeDao periodeDao;
    @Autowired
    private UserDao userDao;


    private static final Logger LOGGER = LoggerFactory.getLogger(NilaiBiayaController.class);

    @GetMapping("/nilaibiaya/list")
    public void daftarNilaiBiaya(@RequestParam(required = false) JenisBiaya jenisBiaya,
                                 @RequestParam(required = false) Periode periode,
                                 @RequestParam(required = false) ProgramStudi prodi,
                                 Model m, Pageable page){
        if(jenisBiaya != null && periode != null && prodi != null) {
            m.addAttribute("prodi", prodi);
            m.addAttribute("jenisBiaya", jenisBiaya);
            m.addAttribute("periode", periode);
            m.addAttribute("daftarNilai", nilaiBiayaDao.findByProgramStudiAndJenisBiayaAndPeriode(prodi,jenisBiaya,periode, page));
        } else if(jenisBiaya != null && periode != null && prodi == null) {
            m.addAttribute("jenisBiaya", jenisBiaya);
            m.addAttribute("periode", periode);
            m.addAttribute("daftarNilai", nilaiBiayaDao.findByJenisBiayaAndPeriode(jenisBiaya,periode, page));
        } else if(jenisBiaya != null && periode == null && prodi != null) {
            m.addAttribute("prodi", prodi);
            m.addAttribute("jenisBiaya", jenisBiaya);
            m.addAttribute("daftarNilai", nilaiBiayaDao.findByJenisBiayaAndProgramStudi(jenisBiaya,prodi, page));
        } else if(jenisBiaya == null && periode != null && prodi != null) {
            m.addAttribute("prodi", prodi);
            m.addAttribute("periode", periode);
            m.addAttribute("daftarNilai", nilaiBiayaDao.findByPeriodeAndProgramStudi(periode,prodi, page));
        } else if(jenisBiaya == null && periode == null && prodi != null) {
            m.addAttribute("prodi", prodi);
            m.addAttribute("daftarNilai", nilaiBiayaDao.findByProgramStudi(prodi, page));
        } else if(jenisBiaya == null && periode != null && prodi == null) {
            m.addAttribute("periode", periode);
            m.addAttribute("daftarNilai", nilaiBiayaDao.findByPeriode(periode, page));
        } else if(jenisBiaya != null && periode == null && prodi == null) {
            m.addAttribute("jenisBiaya", jenisBiaya);
            m.addAttribute("daftarNilai", nilaiBiayaDao.findByJenisBiaya(jenisBiaya, page));
        } else {
            m.addAttribute("daftarNilai", nilaiBiayaDao.findAll(page));
        }
    }

    @ModelAttribute("daftarJenis")
    public Iterable<JenisBiaya> daftarJenis(){
        return jenisBiayaDao.findAll();
    }
    @ModelAttribute("daftarGrade")
    public Iterable<Grade> daftarGrade(){
        return gradeDao.findAll();
    }
    @ModelAttribute("daftarProdi")
    public Iterable<ProgramStudi> daftarProdi(){
        return programStudiDao.findAll();
    }
    @ModelAttribute("daftarPeriode")
    public Iterable<Periode> daftarPeriode(){
        return periodeDao.findAll();
    }


    @GetMapping("/nilaibiaya/form")
    public String form(Model model, Authentication currentUser, @RequestParam(required = false)String id){

        model.addAttribute("nilai", new NilaiBiaya());
        LOGGER.debug("Authentication class : {}", currentUser.getClass().getName());
        if (currentUser == null) {
            LOGGER.warn("Current user is null");
        }
        String username = ((UserDetails) currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        LOGGER.debug("User ID : {}", u.getId());
        if (u == null) {
            LOGGER.warn("Username {} not found in database ", username);
        }
        if (id != null && !id.isEmpty()) {
            NilaiBiaya nilaiBiaya = nilaiBiayaDao.findById(id).get();
            if (nilaiBiaya != null) {
                nilaiBiaya.setUserEdit(u);
                nilaiBiaya.setTanggalEdit(LocalDateTime.now());
                model.addAttribute("nilai", nilaiBiaya);
            }
        }
        return "nilaibiaya/form";
    }


    @RequestMapping(value = "/nilaibiaya/form", method = RequestMethod.POST)
    public String prosesNilai(@Valid NilaiBiaya nilaiBiaya,
                              BindingResult error, @RequestParam(required = false) NilaiBiaya id,
                              Authentication currentUser){
        LOGGER.debug("Authentication class : {}", currentUser.getClass().getName());
        if (currentUser == null) {
            LOGGER.warn("Current user is null");
        }
        String username = ((UserDetails) currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        LOGGER.debug("User ID : {}", u.getId());
        if (u == null) {
            LOGGER.warn("Username {} not found in database ", username);
        }
        if (id != null){
            nilaiBiaya.setTanggalEdit(LocalDateTime.now());
            nilaiBiaya.setUserEdit(u);
        }
        nilaiBiayaDao.save(nilaiBiaya);
        return "redirect:/nilaibiaya/list";
    }

    @GetMapping("/nilaibiaya/csv")
    public void rekapPembayaranCsv(HttpServletResponse response) throws Exception {
        String filename = "Template_Biaya.csv";
        response.setHeader("Content-Disposition", "attachment;filename="+filename);
        response.setContentType("text/csv");
        response.getWriter().println("No,Id Program Studi, Program Studi,Nominal");


        Iterable<ProgramStudi> programStudis = programStudiDao.findAll();

        Integer baris = 0;
        for (ProgramStudi p : programStudis) {
            baris++;
            response.getWriter().print(baris);
            response.getWriter().print(",");
            response.getWriter().print(p.getId());
            response.getWriter().print(",");
            response.getWriter().print(p.getNama());
            response.getWriter().print(",");
            response.getWriter().print("0");
            response.getWriter().println();
        }

        response.getWriter().flush();
    }

//
    @PostMapping("/nilaibiaya/list")
    public String processFormUpload(@RequestParam JenisBiaya jenisBiaya, Periode periode,Grade grade,
                                    @RequestParam(required = false) Boolean pakaiHeader,
                                    MultipartFile fileNilai,
                                    RedirectAttributes redirectAttrs, Authentication currentUser){
        LOGGER.debug("Authentication class : {}",currentUser.getClass().getName());

        if(currentUser == null){
            LOGGER.warn("Current user is null");
        }

        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        LOGGER.debug("User ID : {}", u.getId());
        if(u == null){
            LOGGER.warn("Username {} not found in database ", username);
        }

        LOGGER.info("Jenis Biaya : {}",jenisBiaya);
        LOGGER.info("Periode : {}",periode);
        LOGGER.info("Pakai Header : {}",pakaiHeader);
        LOGGER.info("Nama File : {}",fileNilai.getName());
        LOGGER.info("Ukuran File : {}",fileNilai.getSize());

        List<UploadError> errors = new ArrayList<>();
        Integer baris = 0;

        if(jenisBiaya == null){
            errors.add(new UploadError(baris, "Jenis biaya harus diisi", ""));
            redirectAttrs
                    .addFlashAttribute("jumlahBaris", 0)
                    .addFlashAttribute("jumlahSukses", 0)
                    .addFlashAttribute("jumlahError", errors.size())
                    .addFlashAttribute("errors", errors);
            return "redirect:hasil";
        }
        if(periode == null){
            errors.add(new UploadError(baris, "Periode harus diisi", ""));
            redirectAttrs
                    .addFlashAttribute("jumlahBaris", 0)
                    .addFlashAttribute("jumlahSukses", 0)
                    .addFlashAttribute("jumlahError", errors.size())
                    .addFlashAttribute("errors", errors);
            return "redirect:hasil";
        }
        if(grade == null){
            errors.add(new UploadError(baris, "Grade harus diisi", ""));
            redirectAttrs
                    .addFlashAttribute("jumlahBaris", 0)
                    .addFlashAttribute("jumlahSukses", 0)
                    .addFlashAttribute("jumlahError", errors.size())
                    .addFlashAttribute("errors", errors);
            return "redirect:hasil";
        }
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(fileNilai.getInputStream()));
            String content;

            if((pakaiHeader != null && pakaiHeader)){
                content = reader.readLine();
            }

            while ((content = reader.readLine()) != null) {
                baris++;
                String[] data = content.split(",");
                if (data.length != 4) {
                    errors.add(new UploadError(baris, "Format data salah", content));
                    continue;
                }

                // save nilai biaya
                NilaiBiaya nilaiBiaya = new NilaiBiaya();
                nilaiBiaya.setJenisBiaya(jenisBiaya);
                nilaiBiaya.setGrade(grade);
                nilaiBiaya.setPeriode(periode);

                System.out.println("Prodi : "+ data[1]);

                if (data[1].equals("1") == true){
                    ProgramStudi prodi = new ProgramStudi();
                    prodi.setId("001");
                    nilaiBiaya.setProgramStudi(prodi);
                }
                 else if (data[1].equals("2") == true){
                    ProgramStudi prodi = new ProgramStudi();
                    prodi.setId("002");
                    nilaiBiaya.setProgramStudi(prodi);
                }else if (data[1].equals("3") == true){
                    ProgramStudi prodi = new ProgramStudi();
                    prodi.setId("003");
                    nilaiBiaya.setProgramStudi(prodi);
                }else if (data[1].equals("4") == true){
                    ProgramStudi prodi = new ProgramStudi();
                    prodi.setId("004");
                    nilaiBiaya.setProgramStudi(prodi);
                }else if (data[1].equals("5") == true){
                    ProgramStudi prodi = new ProgramStudi();
                    prodi.setId("005");
                    nilaiBiaya.setProgramStudi(prodi);
                }else if (data[1].equals("6") == true){
                    ProgramStudi prodi = new ProgramStudi();
                    prodi.setId("006");
                    nilaiBiaya.setProgramStudi(prodi);
                }else if (data[1].equals("7") == true){
                    ProgramStudi prodi = new ProgramStudi();
                    prodi.setId("007");
                    nilaiBiaya.setProgramStudi(prodi);
                }else if (data[1].equals("8") == true){
                    ProgramStudi prodi = new ProgramStudi();
                    prodi.setId("008");
                    nilaiBiaya.setProgramStudi(prodi);
                }else if (data[1].equals("9") == true){
                    ProgramStudi prodi = new ProgramStudi();
                    prodi.setId("009");
                    nilaiBiaya.setProgramStudi(prodi);
                }else if (data[1].equals("10") == true){
                    ProgramStudi prodi = new ProgramStudi();
                    prodi.setId("010");
                    nilaiBiaya.setProgramStudi(prodi);
                }


                nilaiBiaya.setNilai(new BigDecimal (data[3]));

                nilaiBiayaDao.save(nilaiBiaya);



            }
        } catch (Exception err){
            LOGGER.warn(err.getMessage(), err);
            errors.add(new UploadError(0, "Format file salah", ""));
        }

        redirectAttrs
                .addFlashAttribute("jumlahBaris", baris)
                .addFlashAttribute("jumlahSukses", baris - errors.size())
                .addFlashAttribute("jumlahError", errors.size())
                .addFlashAttribute("errors", errors);

        return "redirect:hasil";
    }
    @GetMapping("/nilaibiaya/hasil")
    public void hasilFormUpload(){}

}

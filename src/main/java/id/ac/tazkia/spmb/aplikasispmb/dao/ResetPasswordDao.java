package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.ResetPassword;
import id.ac.tazkia.spmb.aplikasispmb.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ResetPasswordDao extends PagingAndSortingRepository<ResetPassword,String>, CrudRepository<ResetPassword,String> {
    ResetPassword findByUser(User u);

    ResetPassword findByCode(String code);
}

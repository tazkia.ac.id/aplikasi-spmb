package id.ac.tazkia.spmb.aplikasispmb.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import jakarta.validation.constraints.NotNull;

@Entity @Data
public class ProgramStudi {


    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(nullable = false)
    private String nama;

    @NotEmpty
    @NotNull
    private String kodeBiaya;

    @NotEmpty
    @NotNull
    private String kodeSimak;

    private String kodeFakultas;

    @Enumerated(EnumType.STRING)
    private Jenjang jenjang;

    @ManyToOne
    @JoinColumn(name = "id_institusi")
    private Institut institut;

    @Enumerated(EnumType.STRING)
    private Status status;

}

package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.constants.ZahirExport;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface PembayaranDao extends PagingAndSortingRepository<Pembayaran, String>, CrudRepository <Pembayaran, String> {
    Pembayaran findByTagihan(Tagihan tagihan);

    Pembayaran findDistinctByTagihan(Tagihan tagihan);
    Pembayaran findTopByTagihanOrderByTagihanDesc(Tagihan tagihan);
    Pembayaran findByTagihanAndZahirExport(Tagihan tagihan, ZahirExport zahirExport);
    Page<Pembayaran> findByTagihan(Tagihan tagihan, Pageable pageable);

    Page<Pembayaran> findByTagihanPendaftarLeadsJenjangAndTagihanJenisBiayaAndTagihanPendaftarLeadsNamaContainingIgnoreCaseAndTagihanPendaftarTahunAjaran(Jenjang jenjang,JenisBiaya jenisBiaya, String pendaftar, TahunAjaran tahunAjaran,Pageable page);
    Page<Pembayaran> findByTagihanJenisBiayaAndTagihanPendaftarLeadsNamaContainingIgnoreCaseAndTagihanPendaftarTahunAjaran(JenisBiaya jenisBiaya, String pendaftar, TahunAjaran tahunAjaran,Pageable page);
    Page<Pembayaran> findByTagihanJenisBiayaAndTagihanPendaftarTahunAjaran(JenisBiaya jenisBiaya, TahunAjaran tahunAjaran ,Pageable page);
    Page<Pembayaran> findByTagihanPendaftarLeadsJenjangAndTagihanJenisBiayaAndTagihanPendaftarTahunAjaran(Jenjang jenjang,JenisBiaya jenisBiaya, TahunAjaran tahunAjaran ,Pageable page);
    Page<Pembayaran> findByTagihanPendaftarLeadsJenjangAndTagihanPendaftarLeadsNamaContainingIgnoreCaseAndTagihanPendaftarTahunAjaran(Jenjang jenjang,String  pendaftar, TahunAjaran tahunAjaran ,Pageable page);
    Page<Pembayaran> findByTagihanPendaftarLeadsJenjangAndTagihanPendaftarTahunAjaran(Jenjang jenjang, TahunAjaran tahunAjaran ,Pageable page);
    Page<Pembayaran> findByTagihanPendaftarLeadsNamaContainingIgnoreCaseAndTagihanPendaftarTahunAjaran(String pendaftar, TahunAjaran tahunAjaran, Pageable page);
    Page<Pembayaran> findByTagihanPendaftarTahunAjaran(TahunAjaran tahunAjaran, Pageable pageable);
    Page<Pembayaran> findByTagihanPendaftarLeadsJenjangAndTagihanPendaftarLeadsNamaContainingIgnoreCase(Jenjang jenjang,String pendaftar, Pageable page);
    Page<Pembayaran> findByTagihanPendaftarLeadsJenjang(Jenjang jenjang, Pageable page);

    Iterable<Pembayaran> findByTagihanJenisBiayaAndTagihanPendaftarStatusNotAndWaktuPembayaranBetweenOrderByWaktuPembayaran(JenisBiaya jenis,String Status, LocalDateTime localDateTime, LocalDateTime localDateTime1);

    @Query("select p from Pembayaran p " +
            "inner join p.tagihan t " +
            "where t.jenisBiaya.id = ?1 and p.waktuPembayaran between ?2 and ?3")
    List<Pembayaran> findTagihanIdsByJenisBiayaAndWaktuPembayaran(String jenisBiaya, LocalDateTime startDate, LocalDateTime endDate);

    Iterable<Pembayaran> findByTagihanJenisBiaya(JenisBiaya jb);

    @Query(value = "select * from\n" +
            "(select t.id, p.id_tagihan, p.id_bank, p.bukti_pembayaran, p.waktu_pembayaran, p.cara_pembayaran, p.nilai, p.referensi, p.zahir_export from tagihan t \n" +
            "inner join pembayaran p on p.id_tagihan = t.id\n" +
            "inner join pendaftar pd on pd.id = t.id_pendaftar\n" +
            "inner join leads l on l.id = pd.id_leads\n" +
            "inner join  bank b on b.id = p.id_bank\n" +
            "where t.id_jenisbiaya = 002  and t.lunas = true and pd.id_tahun = :tahun and l.jenjang = 'S1' \n" +
            "group by t.id_pendaftar) aa", nativeQuery = true)
    Iterable<Pembayaran>findByTagihanS1One(String  tahun);

    @Query(value = "select * from\n" +
            "(select t.id, p.id_tagihan, p.id_bank, p.bukti_pembayaran, p.waktu_pembayaran, p.cara_pembayaran, p.nilai, p.referensi, p.zahir_export from tagihan t \n" +
            "inner join pembayaran p on p.id_tagihan = t.id\n" +
            "inner join pendaftar pd on pd.id = t.id_pendaftar\n" +
            "inner join leads l on l.id = pd.id_leads\n" +
            "inner join  bank b on b.id = p.id_bank\n" +
            "where t.id_jenisbiaya = 002  and t.lunas = true and pd.id_tahun = :tahun and l.jenjang = 'S2' \n" +
            "group by t.id_pendaftar) aa", nativeQuery = true)
    Iterable<Pembayaran>findByTagihanS2One(String tahun);


    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjang(JenisBiaya jenisBiaya, TahunAjaran ta, Jenjang j);
    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarKonsentrasi(JenisBiaya jenisBiaya, TahunAjaran ta, Jenjang j, String kelas);
    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudi(JenisBiaya jenisBiaya, TahunAjaran ta, Jenjang j, ProgramStudi prodi);
    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(JenisBiaya jenisBiaya, TahunAjaran ta, Jenjang j, ProgramStudi prodi, String konsentrasi);

    @Query(value="select count(*) as jumlah from  (select count(t.id) from tagihan t " +
            "inner join pendaftar p on p.id = t.id_pendaftar" +
            " inner join jadwal_test j on j.id_pendaftar = p.id " +
            " inner join hasil_test h on h.id_jadwal = j.id " +
            " inner join leads l on l.id = p.id_leads where t.id_jenisbiaya = 002  and  p.id_tahun = :tahun " +
            "and l.jenjang = :jenjang and t.lunas = true  group by t.id_pendaftar) aa", nativeQuery = true)
    Long hitungDUJen(String tahun, String jenjang);

    @Query(value="select count(*) as jumlah from  (select count(t.id) from tagihan t " +
            "inner join pendaftar p on p.id = t.id_pendaftar" +
            " inner join jadwal_test j on j.id_pendaftar = p.id " +
            " inner join hasil_test h on h.id_jadwal = j.id " +
            " inner join leads l on l.id = p.id_leads where t.id_jenisbiaya = 002  and  p.id_tahun = :tahun " +
            "and l.jenjang = 'S1' and p.konsentrasi = :kelas and t.lunas = true  group by t.id_pendaftar) aa", nativeQuery = true)
    Long hitungDUJenByKelas(String tahun, String kelas);

    @Query(value = "select count(*) as jumlah from  (select count(t.id) from tagihan t inner join pendaftar p on p.id = t.id_pendaftar " +
            " inner join jadwal_test j on j.id_pendaftar = p.id " +
            " inner join hasil_test h on h.id_jadwal = j.id " +
            " inner join leads l on l.id = p.id_leads where t.id_jenisbiaya = 001  and  p.id_tahun = :tahun " +
            " and l.jenjang = :jenjang  and t.lunas = true   group by t.id_pendaftar) aa", nativeQuery = true)
    Long hitungYangSudahTest(String tahun, String jenjang);

    @Query(value = "select count(*) as jumlah from  (select count(t.id) from tagihan t inner join pendaftar p on p.id = t.id_pendaftar " +
            " inner join jadwal_test j on j.id_pendaftar = p.id " +
            " inner join hasil_test h on h.id_jadwal = j.id " +
            " inner join leads l on l.id = p.id_leads where t.id_jenisbiaya = 001  and  p.id_tahun = :tahun " +
            " and l.jenjang = 'S1' and l.kelas = :kelas and t.lunas = true   group by t.id_pendaftar) aa", nativeQuery = true)
    Long hitungYangSudahTestByKelas(String tahun, String kelas);

    @Query(value="select count(*) as jumlah from  (select count(t.id) from tagihan t " +
            "inner join pendaftar p on p.id = t.id_pendaftar" +
            " inner join jadwal_test j on j.id_pendaftar = p.id " +
            " inner join hasil_test h on h.id_jadwal = j.id " +
            " inner join leads l on l.id = p.id_leads where t.id_jenisbiaya = 002  and  p.id_tahun = :tahun" +
            " and p.id_program_studi = :prodi  " +
            " and t.lunas = true  group by t.id_pendaftar) aa", nativeQuery = true)
    Long hitungDUJenisTest(String tahun,  String prodi);

    @Query(value="select count(*) as jumlah from  (select count(t.id) from tagihan t " +
            "inner join pendaftar p on p.id = t.id_pendaftar" +
            " inner join jadwal_test j on j.id_pendaftar = p.id " +
            " inner join hasil_test h on h.id_jadwal = j.id " +
            " inner join leads l on l.id = p.id_leads where t.id_jenisbiaya = 002  and  p.id_tahun = :tahun" +
            " and p.id_program_studi = :prodi and p.konsentrasi = :konsentrasi  " +
            " and t.lunas = true  group by t.id_pendaftar) aa", nativeQuery = true)
    Long hitungDUJenisTestKonsentrasi(String tahun,  String prodi, String konsentrasi);

    @Query(value="select count(*) as jumlah from  (select count(t.id) from tagihan t " +
            "inner join pendaftar p on p.id = t.id_pendaftar" +
            " inner join jadwal_test j on j.id_pendaftar = p.id " +
            " inner join hasil_test h on h.id_jadwal = j.id " +
            " inner join leads l on l.id = p.id_leads where t.id_jenisbiaya = 002  and  p.id_tahun = :tahun and l.jenjang = :jenjang" +
            " and t.lunas = true  group by t.id_pendaftar) aa", nativeQuery = true)
    Long hitungDUTotalJenjang(String tahun, String jenjang);

    @Query(value="select count(*) as jumlah from  (select count(t.id) from tagihan t " +
            "inner join pendaftar p on p.id = t.id_pendaftar" +
            " inner join jadwal_test j on j.id_pendaftar = p.id " +
            " inner join hasil_test h on h.id_jadwal = j.id " +
            " inner join leads l on l.id = p.id_leads where t.id_jenisbiaya = 002  and  p.id_tahun = :tahun and l.jenjang = :jenjang" +
            " and t.lunas = true and p.konsentrasi = :konsentrasi  group by t.id_pendaftar) aa", nativeQuery = true)
    Long hitungDUTotalJenjangKonsentrasi(String tahun, String jenjang, String konsentrasi);

    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudi(JenisBiaya jb, TahunAjaran ta, String pe, ProgramStudi pr);
    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarProgramStudiAndTagihanPendaftarKonsentrasi(JenisBiaya jb,
                                                                                                                                                                         TahunAjaran ta, String pe, ProgramStudi pr, String konsentrasi);
    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarLeadsJenjang(JenisBiaya jb, TahunAjaran ta, String pe, Jenjang jenjang);
    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarPendaftarDetailJenisKelaminAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarKonsentrasi(JenisBiaya jb, TahunAjaran ta, String pe, Jenjang jenjang, String konsentrasi);


    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTest(JenisBiaya jb, TahunAjaran ta,ProgramStudi pr, JenisTest jenisTest);
    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudiAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarKonsentrasi(JenisBiaya jb, TahunAjaran ta,ProgramStudi pr, JenisTest jenisTest, String konsentrasi);
    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarLeadsJenjang(JenisBiaya jb, TahunAjaran ta, JenisTest jenisTest, Jenjang jenjang);
    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarJadwalTestJenisTestAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarKonsentrasi(JenisBiaya jb, TahunAjaran ta, JenisTest jenisTest, Jenjang jenjang, String konsentrasi);
    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarLeadsJenjangAndTagihanPendaftarJadwalTestJenisTest(JenisBiaya jb, TahunAjaran ta,Jenjang jenjang, JenisTest jenisTest);

    Long countByTagihanJenisBiayaAndTagihanPendaftarTahunAjaranAndTagihanPendaftarProgramStudi(JenisBiaya jb, TahunAjaran ta,ProgramStudi pr);


    Page<Pembayaran> findByTagihanPendaftar(Pendaftar pendaftar, Pageable pageable);


    @Query(value = "select p.nomor_registrasi,l.nama,l.email,l.no_hp,pr.nama as npprodi,t.cicilan, SUM(pem.nilai) as nilai,coalesce(t.total_tagihan,'-'), p.keterangan_tagihan,p.id from pembayaran pem\n" +
            "inner join tagihan t on t.id = pem.id_tagihan\n" +
            "inner join pendaftar p on p.id = t.id_pendaftar\n" +
            "inner join program_studi pr on pr.id = p.id_program_studi \n" +
            "inner join leads l on l.id = p.id_leads\n" +
            "inner join jadwal_test j on j.id_pendaftar = p.id\n" +
            "inner join hasil_test h on h.id_jadwal = j.id\n" +
            "where p.id_tahun = :tahun and t.id_jenisbiaya = '002' and p.status != :status group by p.nomor_registrasi", nativeQuery = true)
    Iterable<Object[]>findByAllPembayaranDU(String tahun, String status);

    List<Pembayaran> findByTagihanZahirExportAndZahirExport(ZahirExport zahirExport, ZahirExport zahir);

}

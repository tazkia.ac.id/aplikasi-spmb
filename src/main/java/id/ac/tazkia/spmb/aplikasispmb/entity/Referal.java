package id.ac.tazkia.spmb.aplikasispmb.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity @Data
public class Referal {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    private String nama;

    @NotNull
    private String kodeReferal;

    @NotNull @Min(0)
    private BigDecimal nominal;

    @Column(nullable = false)
    private String keterangan;

    @ManyToOne
    @JoinColumn(name = "user_insert")
    private User userInsert;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime tanggalInsert;

    @NotNull
    private Boolean status = Boolean.TRUE;

    @ManyToOne
    @JoinColumn(name = "prodi")
    private ProgramStudi prodi;

    private String email;


}

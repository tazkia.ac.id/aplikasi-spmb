package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.constants.AppConstants;
import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.dto.ValidasiTagihan;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Controller
public class TelemarketingController {
    private static final Logger logger = LoggerFactory.getLogger(TelemarketingController.class);


    @Autowired private PendaftarDao pendaftarDao;
    @Autowired private TahunAjaranDao tahunAjaranDao;
    @Autowired private JadwalTestDao jadwalTestDao;
    @Autowired private TagihanDao tagihanDao;
    @Autowired private UploadBerkasDao uploadBerkasDao;
    @Autowired private HasilTestDao hasilTestDao;
    @Autowired private PendaftarDetailDao pendaftarDetailDao;
    @Autowired private PendaftarDetailPascaDao pendaftarDetailPascaDao;
    @Autowired private FollowupDao followupDao;
    @Autowired private  UserDao userDao;
    @Autowired private  SubscribeDao subscribeDao;

    @GetMapping("/telemarketing/listReguler")
    public void listReguler(@RequestParam(required = false)String search, String user, String status, Model m,
        @PageableDefault(size = 10, sort = "nomorRegistrasi", direction = Sort.Direction.DESC) Pageable page, Authentication currentUser){

        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        m.addAttribute("user", u);
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }

        if (AppConstants.ROLE_EDU.equals(u.getRole().getId())) {
            Followup fu = followupDao.findByUser(u);
            user = fu.getId();
            m.addAttribute("UserEdu", user);
        }

        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
        Iterable<Pendaftar> cekUSer = pendaftarDao.findByTahunAjaranAktif(ta.getAktif());
        for (Pendaftar pe : cekUSer) {
            if (pe.getUserUpdate() != null) {
                m.addAttribute("username", pe.getUserUpdate());
            }
        }


        Iterable<Followup> fu = followupDao.findByStatus(Status.AKTIF);
        m.addAttribute("listFu", fu);

        Page<Pendaftar> pendaftar = pendaftarDao.findByTahunAjaranAktifAndKonsentrasiAndLeadsJenjang(Status.AKTIF,"Reguler", Jenjang.S1,page);

        if(StringUtils.hasText(search) && !StringUtils.hasText(user) && !StringUtils.hasText(status)) {
            Page<Pendaftar> ps = pendaftarDao.findByNomorRegistrasiContainingIgnoreCaseOrLeadsNamaContainingIgnoreCaseAndTahunAjaranAndKonsentrasiAndLeadsJenjangOrderByNomorRegistrasiDesc(search,search,ta,"Reguler",Jenjang.S1,page);
            m.addAttribute("search", search);
            m.addAttribute("daftarPendaftar", ps);

            List<JadwalTest> jadwalTests = new ArrayList<>();
            for (Pendaftar pe : ps) {
                JadwalTest jadwalTest = jadwalTestDao.findByPendaftar(pe);
                if (jadwalTest != null) {
                    jadwalTests.add(jadwalTest);
                }
            }
            if (jadwalTests != null) {
                m.addAttribute("jadwalTest", jadwalTests);
            }

            List<HasilTest> hasilTests = new ArrayList<>();
            for (JadwalTest jt : jadwalTests) {
                HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
                if (hasilTest != null) {
                    hasilTests.add(hasilTest);
                }
            }
            if (hasilTests != null) {
                m.addAttribute("hasilTest", hasilTests);
            }

            List<ValidasiTagihan> pendaftarDetails = new ArrayList<>();
            for (Pendaftar pe : ps) {
                PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetail != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetail == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetails.add(validasiTagihan);

            }
            m.addAttribute("detail", pendaftarDetails);


            JenisBiaya jenisBiaya = new JenisBiaya();
            jenisBiaya.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
            if (jenisBiaya == null) {
                logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());

            }
            List<ValidasiTagihan> tagihans = new ArrayList<>();
            for (Pendaftar pe : ps) {
                Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiaya(pe, jenisBiaya);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (tagihan != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (tagihan == null){
                    validasiTagihan.setStatus("Kosong");
                }
                tagihans.add(validasiTagihan);
            }

            m.addAttribute("tagihan", tagihans);

            List<Berkas> berkas = new  ArrayList<>();

            List<Berkas> berkas1 = uploadBerkasDao.findPendaftar();
//            berkas.add(berkas1);


            m.addAttribute("berkas", berkas1);


        }else if(!StringUtils.hasText(search) && StringUtils.hasText(user)) {
            m.addAttribute("user", user);
            Followup fl = followupDao.findById(user).get();
            Page<Pendaftar> ps= pendaftarDao.findByFollowupAndTahunAjaranAndKonsentrasiAndLeadsJenjangOrderByNomorRegistrasiDesc(fl,ta,"Reguler",Jenjang.S1,page);
            m.addAttribute("daftarPendaftar", ps );

            List<JadwalTest> jadwalTests = new ArrayList<>();
            for (Pendaftar pe : ps) {
                JadwalTest jadwalTest = jadwalTestDao.findByPendaftar(pe);
                if (jadwalTest != null) {
                    jadwalTests.add(jadwalTest);
                }
            }
            if (jadwalTests != null) {
                m.addAttribute("jadwalTest", jadwalTests);
            }

            List<HasilTest> hasilTests = new ArrayList<>();
            for (JadwalTest jt : jadwalTests) {
                HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
                if (hasilTest != null) {
                    hasilTests.add(hasilTest);
                }
            }
            if (hasilTests != null) {
                m.addAttribute("hasilTest", hasilTests);
            }

            List<ValidasiTagihan> pendaftarDetails = new ArrayList<>();
            for (Pendaftar pe : ps) {
                PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetail != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetail == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetails.add(validasiTagihan);

            }
            m.addAttribute("detail", pendaftarDetails);


            JenisBiaya jenisBiaya = new JenisBiaya();
            jenisBiaya.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
            if (jenisBiaya == null) {
                logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());

            }
            List<ValidasiTagihan> tagihans = new ArrayList<>();
            for (Pendaftar pe : ps) {
                Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiaya(pe, jenisBiaya);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (tagihan != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (tagihan == null){
                    validasiTagihan.setStatus("Kosong");
                }
                tagihans.add(validasiTagihan);
            }

            m.addAttribute("tagihan", tagihans);

            List<Berkas> berkas = new  ArrayList<>();

            List<Berkas> berkas1 = uploadBerkasDao.findPendaftar();
//            berkas.add(berkas1);


            m.addAttribute("berkas", berkas1);


        } else if(StringUtils.hasText(search) && StringUtils.hasText(user) && !StringUtils.hasText(status)) {
            m.addAttribute("user", user);
            m.addAttribute("search", search);
            Followup fl = followupDao.findById(user).get();
            Page<Pendaftar> ps= pendaftarDao.findByNomorRegistrasiContainingIgnoreCaseOrLeadsNamaContainingIgnoreCaseAndFollowupAndTahunAjaranAndKonsentrasiAndLeadsJenjangOrderByNomorRegistrasiDesc(search,search, fl,ta,"Reguler",Jenjang.S1,page);
            m.addAttribute("daftarPendaftar", ps );

            List<JadwalTest> jadwalTests = new ArrayList<>();
            for (Pendaftar pe : ps) {
                JadwalTest jadwalTest = jadwalTestDao.findByPendaftar(pe);
                if (jadwalTest != null) {
                    jadwalTests.add(jadwalTest);
                }
            }
            if (jadwalTests != null) {
                m.addAttribute("jadwalTest", jadwalTests);
            }

            List<HasilTest> hasilTests = new ArrayList<>();
            for (JadwalTest jt : jadwalTests) {
                HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
                if (hasilTest != null) {
                    hasilTests.add(hasilTest);
                }
            }
            if (hasilTests != null) {
                m.addAttribute("hasilTest", hasilTests);
            }

            List<ValidasiTagihan> pendaftarDetails = new ArrayList<>();
            for (Pendaftar pe : ps) {
                PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetail != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetail == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetails.add(validasiTagihan);

            }
            m.addAttribute("detail", pendaftarDetails);


            JenisBiaya jenisBiaya = new JenisBiaya();
            jenisBiaya.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
            if (jenisBiaya == null) {
                logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());

            }
            List<ValidasiTagihan> tagihans = new ArrayList<>();
            for (Pendaftar pe : ps) {
                Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiaya(pe, jenisBiaya);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (tagihan != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (tagihan == null){
                    validasiTagihan.setStatus("Kosong");
                }
                tagihans.add(validasiTagihan);
            }

            m.addAttribute("tagihan", tagihans);

            List<Berkas> berkas = new  ArrayList<>();

            List<Berkas> berkas1 = uploadBerkasDao.findPendaftar();
//            berkas.add(berkas1);


            m.addAttribute("berkas", berkas1);


        } else if(StringUtils.hasText(status) && StringUtils.hasText(user) && !StringUtils.hasText(search)) {
            m.addAttribute("user", user);
            m.addAttribute("status", status);
            Followup fl = followupDao.findById(user).get();
            Page<Pendaftar> ps= pendaftarDao.findByStatusContainingIgnoreCaseAndFollowupAndTahunAjaranAndKonsentrasiAndLeadsJenjangOrderByNomorRegistrasiDesc(status, fl,ta,"Reguler",Jenjang.S1,page);
            m.addAttribute("daftarPendaftar", ps );

            List<JadwalTest> jadwalTests = new ArrayList<>();
            for (Pendaftar pe : ps) {
                JadwalTest jadwalTest = jadwalTestDao.findByPendaftar(pe);
                if (jadwalTest != null) {
                    jadwalTests.add(jadwalTest);
                }
            }
            if (jadwalTests != null) {
                m.addAttribute("jadwalTest", jadwalTests);
            }

            List<HasilTest> hasilTests = new ArrayList<>();
            for (JadwalTest jt : jadwalTests) {
                HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
                if (hasilTest != null) {
                    hasilTests.add(hasilTest);
                }
            }
            if (hasilTests != null) {
                m.addAttribute("hasilTest", hasilTests);
            }

            List<ValidasiTagihan> pendaftarDetails = new ArrayList<>();
            for (Pendaftar pe : ps) {
                PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetail != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetail == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetails.add(validasiTagihan);

            }
            m.addAttribute("detail", pendaftarDetails);


            JenisBiaya jenisBiaya = new JenisBiaya();
            jenisBiaya.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
            if (jenisBiaya == null) {
                logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());

            }
            List<ValidasiTagihan> tagihans = new ArrayList<>();
            for (Pendaftar pe : ps) {
                Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiaya(pe, jenisBiaya);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (tagihan != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (tagihan == null){
                    validasiTagihan.setStatus("Kosong");
                }
                tagihans.add(validasiTagihan);
            }

            m.addAttribute("tagihan", tagihans);

            List<Berkas> berkas = new  ArrayList<>();

            List<Berkas> berkas1 = uploadBerkasDao.findPendaftar();
//            berkas.add(berkas1);


            m.addAttribute("berkas", berkas1);


        } else if(StringUtils.hasText(status) && !StringUtils.hasText(user) && !StringUtils.hasText(search)) {
            m.addAttribute("status", status);
            Page<Pendaftar> ps= pendaftarDao.findByStatusContainingIgnoreCaseAndTahunAjaranAndKonsentrasiAndLeadsJenjangOrderByNomorRegistrasiDesc(status,ta,"Reguler",Jenjang.S1,page);
            m.addAttribute("daftarPendaftar", ps );

            List<JadwalTest> jadwalTests = new ArrayList<>();
            for (Pendaftar pe : ps) {
                JadwalTest jadwalTest = jadwalTestDao.findByPendaftar(pe);
                if (jadwalTest != null) {
                    jadwalTests.add(jadwalTest);
                }
            }
            if (jadwalTests != null) {
                m.addAttribute("jadwalTest", jadwalTests);
            }

            List<HasilTest> hasilTests = new ArrayList<>();
            for (JadwalTest jt : jadwalTests) {
                HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
                if (hasilTest != null) {
                    hasilTests.add(hasilTest);
                }
            }
            if (hasilTests != null) {
                m.addAttribute("hasilTest", hasilTests);
            }

            List<ValidasiTagihan> pendaftarDetails = new ArrayList<>();
            for (Pendaftar pe : ps) {
                PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetail != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetail == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetails.add(validasiTagihan);

            }
            m.addAttribute("detail", pendaftarDetails);


            JenisBiaya jenisBiaya = new JenisBiaya();
            jenisBiaya.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
            if (jenisBiaya == null) {
                logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());

            }
            List<ValidasiTagihan> tagihans = new ArrayList<>();
            for (Pendaftar pe : ps) {
                Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiaya(pe, jenisBiaya);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (tagihan != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (tagihan == null){
                    validasiTagihan.setStatus("Kosong");
                }
                tagihans.add(validasiTagihan);
            }

            m.addAttribute("tagihan", tagihans);

            List<Berkas> berkas = new  ArrayList<>();

            List<Berkas> berkas1 = uploadBerkasDao.findPendaftar();
//            berkas.add(berkas1);


            m.addAttribute("berkas", berkas1);


        }  else {
            m.addAttribute("daftarPendaftar", pendaftar);
            List<JadwalTest> jadwalTests = new ArrayList<>();
            for (Pendaftar pe : pendaftar) {
                JadwalTest jadwalTest = jadwalTestDao.findByPendaftar(pe);
                if (jadwalTest != null) {
                    jadwalTests.add(jadwalTest);
                }
            }
            if (jadwalTests != null) {
                m.addAttribute("jadwalTest", jadwalTests);
            }

            List<HasilTest> hasilTests = new ArrayList<>();
            for (JadwalTest jt : jadwalTests) {
                HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
                if (hasilTest != null) {
                    hasilTests.add(hasilTest);
                }
            }
            if (hasilTests != null) {
                m.addAttribute("hasilTest", hasilTests);
            }

            List<ValidasiTagihan> pendaftarDetails = new ArrayList<>();
            for (Pendaftar pe : pendaftar) {
                PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetail != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetail == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetails.add(validasiTagihan);

            }
            m.addAttribute("detail", pendaftarDetails);


            JenisBiaya jenisBiaya = new JenisBiaya();
            jenisBiaya.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
            if (jenisBiaya == null) {
                logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());

            }
            List<ValidasiTagihan> tagihans = new ArrayList<>();
            for (Pendaftar pe : pendaftar) {
                Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiaya(pe, jenisBiaya);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (tagihan != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (tagihan == null){
                    validasiTagihan.setStatus("Kosong");
                }
                tagihans.add(validasiTagihan);
            }

            m.addAttribute("tagihan", tagihans);

            List<Berkas> berkas = new  ArrayList<>();

            List<Berkas> berkas1 = uploadBerkasDao.findPendaftar();
//            berkas.add(berkas1);

//            System.out.println("Berkas :"+ berkas1);
            m.addAttribute("berkas", berkas1);

        }
    }

    @GetMapping("/telemarketing/listKaryawan")
    public void listKaryawan(@RequestParam(required = false)String search,String user, Model m, Authentication auth,
                            @PageableDefault(size = 10, sort = "nomorRegistrasi", direction = Sort.Direction.DESC) Pageable page){

        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);

        Iterable<Pendaftar> cekUSer = pendaftarDao.findByTahunAjaranAktif(ta.getAktif());
        for (Pendaftar pe : cekUSer) {
            if (pe.getUserUpdate() != null) {
                m.addAttribute("username", pe.getUserUpdate());
            }
        }

        String username = ((UserDetails)auth.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);

        if (AppConstants.ROLE_EDU.equals(u.getRole().getId())) {
            Followup fu = followupDao.findByUser(u);
            user = fu.getId();
            m.addAttribute("UserEdu", user);
        }

        Iterable<Followup> fu = followupDao.findByStatus(Status.AKTIF);
        m.addAttribute("listFu", fu);

        Iterable<Subscribe> sc = subscribeDao.findAll();
        m.addAttribute("subscribe", sc);

        Page<Pendaftar> pendaftar = pendaftarDao.findByTahunAjaranAktifAndKonsentrasiAndLeadsJenjang(Status.AKTIF,"Karyawan", Jenjang.S1,page);

        if(StringUtils.hasText(search) && !StringUtils.hasText(user)) {
            Page<Pendaftar> ps= pendaftarDao.findByNomorRegistrasiContainingIgnoreCaseOrLeadsNamaContainingIgnoreCaseAndTahunAjaranAndKonsentrasiAndLeadsJenjangOrderByNomorRegistrasiDesc(search,search,ta,"Karyawan",Jenjang.S1,page);
            m.addAttribute("search", search);
            m.addAttribute("daftarPendaftar", ps );

            List<JadwalTest> jadwalTests = new ArrayList<>();
            for (Pendaftar pe : ps) {
                JadwalTest jadwalTest = jadwalTestDao.findByPendaftar(pe);
                if (jadwalTest != null) {
                    jadwalTests.add(jadwalTest);
                }
            }
            if (jadwalTests != null) {
                m.addAttribute("jadwalTest", jadwalTests);
            }

            List<HasilTest> hasilTests = new ArrayList<>();
            for (JadwalTest jt : jadwalTests) {
                HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
                if (hasilTest != null) {
                    hasilTests.add(hasilTest);
                }
            }
            if (hasilTests != null) {
                m.addAttribute("hasilTest", hasilTests);
            }

            List<ValidasiTagihan> pendaftarDetails = new ArrayList<>();
            for (Pendaftar pe : ps) {
                PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetail != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetail == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetails.add(validasiTagihan);

            }
            m.addAttribute("detail", pendaftarDetails);


            JenisBiaya jenisBiaya = new JenisBiaya();
            jenisBiaya.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
            if (jenisBiaya == null) {
                logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());

            }
            List<ValidasiTagihan> tagihans = new ArrayList<>();
            for (Pendaftar pe : ps) {
                Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiaya(pe, jenisBiaya);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (tagihan != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (tagihan == null){
                    validasiTagihan.setStatus("Kosong");
                }
                tagihans.add(validasiTagihan);
            }

            m.addAttribute("tagihan", tagihans);

            List<Berkas> berkas = new  ArrayList<>();

            List<Berkas> berkas1 = uploadBerkasDao.findPendaftar();
//            berkas.add(berkas1);


            m.addAttribute("berkas", berkas1);


        }else if(!StringUtils.hasText(search) && StringUtils.hasText(user)) {
        m.addAttribute("user", user);
        Followup fl = followupDao.findById(user).get();
        Page<Pendaftar> ps= pendaftarDao.findByFollowupAndTahunAjaranAndKonsentrasiAndLeadsJenjangOrderByNomorRegistrasiDesc(fl,ta,"Karyawan",Jenjang.S1,page);
        m.addAttribute("daftarPendaftar", ps );

        List<JadwalTest> jadwalTests = new ArrayList<>();
        for (Pendaftar pe : ps) {
            JadwalTest jadwalTest = jadwalTestDao.findByPendaftar(pe);
            if (jadwalTest != null) {
                jadwalTests.add(jadwalTest);
            }
        }
        if (jadwalTests != null) {
            m.addAttribute("jadwalTest", jadwalTests);
        }

        List<HasilTest> hasilTests = new ArrayList<>();
        for (JadwalTest jt : jadwalTests) {
            HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
            if (hasilTest != null) {
                hasilTests.add(hasilTest);
            }
        }
        if (hasilTests != null) {
            m.addAttribute("hasilTest", hasilTests);
        }

        List<ValidasiTagihan> pendaftarDetails = new ArrayList<>();
        for (Pendaftar pe : ps) {
            PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pe);
            ValidasiTagihan validasiTagihan = new ValidasiTagihan();
            validasiTagihan.setPendaftar(pe.getId());
            if (pendaftarDetail != null){
                validasiTagihan.setStatus("Ada");
            }
            if (pendaftarDetail == null){
                validasiTagihan.setStatus("Kosong");
            }
            pendaftarDetails.add(validasiTagihan);

        }
        m.addAttribute("detail", pendaftarDetails);


        JenisBiaya jenisBiaya = new JenisBiaya();
        jenisBiaya.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
        if (jenisBiaya == null) {
            logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());

        }
        List<ValidasiTagihan> tagihans = new ArrayList<>();
        for (Pendaftar pe : ps) {
            Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiaya(pe, jenisBiaya);
            ValidasiTagihan validasiTagihan = new ValidasiTagihan();
            validasiTagihan.setPendaftar(pe.getId());
            if (tagihan != null){
                validasiTagihan.setStatus("Ada");
            }
            if (tagihan == null){
                validasiTagihan.setStatus("Kosong");
            }
            tagihans.add(validasiTagihan);
        }

        m.addAttribute("tagihan", tagihans);

        List<Berkas> berkas = new  ArrayList<>();

        List<Berkas> berkas1 = uploadBerkasDao.findPendaftar();
//            berkas.add(berkas1);


        m.addAttribute("berkas", berkas1);


        } else if(StringUtils.hasText(search) && StringUtils.hasText(user)) {
            m.addAttribute("user", user);
            m.addAttribute("search", search);
            Followup fl = followupDao.findById(user).get();
            Page<Pendaftar> ps= pendaftarDao.findByNomorRegistrasiContainingIgnoreCaseOrLeadsNamaContainingIgnoreCaseAndFollowupAndTahunAjaranAndKonsentrasiAndLeadsJenjangOrderByNomorRegistrasiDesc(search,search, fl,ta,"Karyawan",Jenjang.S1,page);
            m.addAttribute("daftarPendaftar", ps );

            List<JadwalTest> jadwalTests = new ArrayList<>();
            for (Pendaftar pe : ps) {
                JadwalTest jadwalTest = jadwalTestDao.findByPendaftar(pe);
                if (jadwalTest != null) {
                    jadwalTests.add(jadwalTest);
                }
            }
            if (jadwalTests != null) {
                m.addAttribute("jadwalTest", jadwalTests);
            }

            List<HasilTest> hasilTests = new ArrayList<>();
            for (JadwalTest jt : jadwalTests) {
                HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
                if (hasilTest != null) {
                    hasilTests.add(hasilTest);
                }
            }
            if (hasilTests != null) {
                m.addAttribute("hasilTest", hasilTests);
            }

            List<ValidasiTagihan> pendaftarDetails = new ArrayList<>();
            for (Pendaftar pe : ps) {
                PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetail != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetail == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetails.add(validasiTagihan);

            }
            m.addAttribute("detail", pendaftarDetails);


            JenisBiaya jenisBiaya = new JenisBiaya();
            jenisBiaya.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
            if (jenisBiaya == null) {
                logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());

            }
            List<ValidasiTagihan> tagihans = new ArrayList<>();
            for (Pendaftar pe : ps) {
                Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiaya(pe, jenisBiaya);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (tagihan != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (tagihan == null){
                    validasiTagihan.setStatus("Kosong");
                }
                tagihans.add(validasiTagihan);
            }

            m.addAttribute("tagihan", tagihans);

            List<Berkas> berkas = new  ArrayList<>();

            List<Berkas> berkas1 = uploadBerkasDao.findPendaftar();
        //            berkas.add(berkas1);


            m.addAttribute("berkas", berkas1);


        } else {
            m.addAttribute("daftarPendaftar", pendaftar);
            List<JadwalTest> jadwalTests = new ArrayList<>();
            for (Pendaftar pe : pendaftar) {
                JadwalTest jadwalTest = jadwalTestDao.findByPendaftar(pe);
                if (jadwalTest != null) {
                    jadwalTests.add(jadwalTest);
                }
            }
            if (jadwalTests != null) {
                m.addAttribute("jadwalTest", jadwalTests);
            }

            List<HasilTest> hasilTests = new ArrayList<>();
            for (JadwalTest jt : jadwalTests) {
                HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
                if (hasilTest != null) {
                    hasilTests.add(hasilTest);
                }
            }
            if (hasilTests != null) {
                m.addAttribute("hasilTest", hasilTests);
            }

            List<ValidasiTagihan> pendaftarDetails = new ArrayList<>();
            for (Pendaftar pe : pendaftar) {
                PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetail != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetail == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetails.add(validasiTagihan);

            }
            m.addAttribute("detail", pendaftarDetails);


            JenisBiaya jenisBiaya = new JenisBiaya();
            jenisBiaya.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
            if (jenisBiaya == null) {
                logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());

            }
            List<ValidasiTagihan> tagihans = new ArrayList<>();
            for (Pendaftar pe : pendaftar) {
                Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiaya(pe, jenisBiaya);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (tagihan != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (tagihan == null){
                    validasiTagihan.setStatus("Kosong");
                }
                tagihans.add(validasiTagihan);
            }

            m.addAttribute("tagihan", tagihans);

            List<Berkas> berkas = new  ArrayList<>();

            List<Berkas> berkas1 = uploadBerkasDao.findPendaftar();
//            berkas.add(berkas1);

//            System.out.println("Berkas :"+ berkas1);
            m.addAttribute("berkas", berkas1);

        }
    }


//    @PostMapping("/telemarketing/update/{id}")
//    @ResponseBody
//    public void updateStatusPendaftar(@PathVariable Pendaftar id,String followup, String status, String note, String notePic, Authentication currentUser){
//        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
//        User u = userDao.findByUsername(username);
//        logger.debug("User ID : {}", u.getId());
//        if(u == null){
//            logger.warn("Username {} not found in database ", username);
//        }
//
//        Pendaftar pendaftar = pendaftarDao.findById(id.getId()).get();
//        pendaftar.setStatus(status);
//        pendaftar.setNote(note);
//        pendaftar.setNotePic(notePic);
//        pendaftar.setUserUpdate(u);
//        pendaftar.setTglUpdate(LocalDate.now());
//        pendaftarDao.save(pendaftar);
//
//    }

    @GetMapping("/telemarketing/listPascasarjana")
    public void listPascasarjana(@RequestParam(required = false)String search, String user, Model m, Authentication auth,
                             @PageableDefault(size = 10, sort = "nomorRegistrasi", direction = Sort.Direction.DESC) Pageable page){

        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);

        String username = ((UserDetails) auth.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);

        if (AppConstants.ROLE_EDU.equals(u.getRole().getId())) {
            Followup fu = followupDao.findByUser(u);
            user = fu.getId();
            m.addAttribute("UserEdu", user);
        }

        Iterable<Pendaftar> cekUSer = pendaftarDao.findByTahunAjaranAktif(ta.getAktif());
        for (Pendaftar pe : cekUSer) {
            if (pe.getUserUpdate() != null) {
                m.addAttribute("username", pe.getUserUpdate());
            }
        }

        Iterable<Followup> fu = followupDao.findByStatus(Status.AKTIF);
        m.addAttribute("listFu", fu);

        Page<Pendaftar> pendaftar = pendaftarDao.findByTahunAjaranAktifAndLeadsJenjang(Status.AKTIF,Jenjang.S2,page);

        if(StringUtils.hasText(search) && !StringUtils.hasText(user)) {
            Page<Pendaftar> ps= pendaftarDao.findByNomorRegistrasiContainingIgnoreCaseOrLeadsNamaContainingIgnoreCaseAndTahunAjaranAndLeadsJenjangOrderByNomorRegistrasi(search,search,ta,Jenjang.S2,page);
            m.addAttribute("search", search);
            m.addAttribute("daftarPendaftar", ps);

            List<JadwalTest> jadwalTests = new ArrayList<>();
            for (Pendaftar pe : ps) {
                JadwalTest jadwalTest = jadwalTestDao.findByPendaftar(pe);
                if (jadwalTest != null) {
                    jadwalTests.add(jadwalTest);
                }
            }
            if (jadwalTests != null) {
                m.addAttribute("jadwalTest", jadwalTests);
            }

            List<HasilTest> hasilTests = new ArrayList<>();
            for (JadwalTest jt : jadwalTests) {
                HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
                if (hasilTest != null) {
                    hasilTests.add(hasilTest);
                }
            }
            if (hasilTests != null) {
                m.addAttribute("hasilTest", hasilTests);
            }

            List<ValidasiTagihan> pendaftarDetails = new ArrayList<>();
            for (Pendaftar pe : ps) {
                PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetail != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetail == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetails.add(validasiTagihan);

            }
            m.addAttribute("detail", pendaftarDetails);

            List<ValidasiTagihan> pendaftarDetailPasca = new ArrayList<>();
            for (Pendaftar pe : ps) {
                PendaftarDetailPasca pendaftarDetailPascas = pendaftarDetailPascaDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetailPascas != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetailPascas == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetailPasca.add(validasiTagihan);

            }
            m.addAttribute("detailS2", pendaftarDetailPasca);



            JenisBiaya jenisBiaya = new JenisBiaya();
            jenisBiaya.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
            if (jenisBiaya == null) {
                logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());

            }
            List<ValidasiTagihan> tagihans = new ArrayList<>();
            for (Pendaftar pe : ps) {
                Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiaya(pe, jenisBiaya);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (tagihan != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (tagihan == null){
                    validasiTagihan.setStatus("Kosong");
                }
                tagihans.add(validasiTagihan);
            }

            m.addAttribute("tagihan", tagihans);

            List<Berkas> berkas = new  ArrayList<>();

            List<Berkas> berkas1 = uploadBerkasDao.findPendaftar();
//            berkas.add(berkas1);


            m.addAttribute("berkas", berkas1);


        } else if(!StringUtils.hasText(search) && StringUtils.hasText(user)) {
            m.addAttribute("user", user);
            Followup fl = followupDao.findById(user).get();
            Page<Pendaftar> ps= pendaftarDao.findByFollowupAndTahunAjaranAndLeadsJenjangOrderByNomorRegistrasi(fl,ta,Jenjang.S2,page);
            m.addAttribute("daftarPendaftar", ps );

            List<JadwalTest> jadwalTests = new ArrayList<>();
            for (Pendaftar pe : ps) {
                JadwalTest jadwalTest = jadwalTestDao.findByPendaftar(pe);
                if (jadwalTest != null) {
                    jadwalTests.add(jadwalTest);
                }
            }
            if (jadwalTests != null) {
                m.addAttribute("jadwalTest", jadwalTests);
            }

            List<HasilTest> hasilTests = new ArrayList<>();
            for (JadwalTest jt : jadwalTests) {
                HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
                if (hasilTest != null) {
                    hasilTests.add(hasilTest);
                }
            }
            if (hasilTests != null) {
                m.addAttribute("hasilTest", hasilTests);
            }

            List<ValidasiTagihan> pendaftarDetails = new ArrayList<>();
            for (Pendaftar pe : ps) {
                PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetail != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetail == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetails.add(validasiTagihan);

            }
            m.addAttribute("detail", pendaftarDetails);


            JenisBiaya jenisBiaya = new JenisBiaya();
            jenisBiaya.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
            if (jenisBiaya == null) {
                logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());

            }
            List<ValidasiTagihan> tagihans = new ArrayList<>();
            for (Pendaftar pe : ps) {
                Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiaya(pe, jenisBiaya);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (tagihan != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (tagihan == null){
                    validasiTagihan.setStatus("Kosong");
                }
                tagihans.add(validasiTagihan);
            }

            m.addAttribute("tagihan", tagihans);

            List<Berkas> berkas = new  ArrayList<>();

            List<Berkas> berkas1 = uploadBerkasDao.findPendaftar();
//            berkas.add(berkas1);


            m.addAttribute("berkas", berkas1);


        } else if(StringUtils.hasText(search) && StringUtils.hasText(user)) {
            m.addAttribute("user", user);
            m.addAttribute("search", search);
            Followup fl = followupDao.findById(user).get();
            Page<Pendaftar> ps= pendaftarDao.findByNomorRegistrasiContainingIgnoreCaseOrLeadsNamaContainingIgnoreCaseAndFollowupAndTahunAjaranAndLeadsJenjangOrderByNomorRegistrasi(search,search, fl,ta,Jenjang.S2,page);
            m.addAttribute("daftarPendaftar", ps );

            List<JadwalTest> jadwalTests = new ArrayList<>();
            for (Pendaftar pe : ps) {
                JadwalTest jadwalTest = jadwalTestDao.findByPendaftar(pe);
                if (jadwalTest != null) {
                    jadwalTests.add(jadwalTest);
                }
            }
            if (jadwalTests != null) {
                m.addAttribute("jadwalTest", jadwalTests);
            }

            List<HasilTest> hasilTests = new ArrayList<>();
            for (JadwalTest jt : jadwalTests) {
                HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
                if (hasilTest != null) {
                    hasilTests.add(hasilTest);
                }
            }
            if (hasilTests != null) {
                m.addAttribute("hasilTest", hasilTests);
            }

            List<ValidasiTagihan> pendaftarDetails = new ArrayList<>();
            for (Pendaftar pe : ps) {
                PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetail != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetail == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetails.add(validasiTagihan);

            }
            m.addAttribute("detail", pendaftarDetails);


            JenisBiaya jenisBiaya = new JenisBiaya();
            jenisBiaya.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
            if (jenisBiaya == null) {
                logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());

            }
            List<ValidasiTagihan> tagihans = new ArrayList<>();
            for (Pendaftar pe : ps) {
                Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiaya(pe, jenisBiaya);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (tagihan != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (tagihan == null){
                    validasiTagihan.setStatus("Kosong");
                }
                tagihans.add(validasiTagihan);
            }

            m.addAttribute("tagihan", tagihans);

            List<Berkas> berkas = new  ArrayList<>();

            List<Berkas> berkas1 = uploadBerkasDao.findPendaftar();
            //            berkas.add(berkas1);


            m.addAttribute("berkas", berkas1);


        } else {
            m.addAttribute("daftarPendaftar", pendaftar);
            List<JadwalTest> jadwalTests = new ArrayList<>();
            for (Pendaftar pe : pendaftar) {
                JadwalTest jadwalTest = jadwalTestDao.findByPendaftar(pe);
                if (jadwalTest != null) {
                    jadwalTests.add(jadwalTest);
                }
            }
            if (jadwalTests != null) {
                m.addAttribute("jadwalTest", jadwalTests);
            }

            List<HasilTest> hasilTests = new ArrayList<>();
            for (JadwalTest jt : jadwalTests) {
                HasilTest hasilTest = hasilTestDao.findByJadwalTest(jt);
                if (hasilTest != null) {
                    hasilTests.add(hasilTest);
                }
            }
            if (hasilTests != null) {
                m.addAttribute("hasilTest", hasilTests);
            }

            List<ValidasiTagihan> pendaftarDetails = new ArrayList<>();
            for (Pendaftar pe : pendaftar) {
                PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetail != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetail == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetails.add(validasiTagihan);

            }
            m.addAttribute("detail", pendaftarDetails);

            List<ValidasiTagihan> pendaftarDetailPasca = new ArrayList<>();
            for (Pendaftar pe : pendaftar) {
                PendaftarDetailPasca pendaftarDetailPascas = pendaftarDetailPascaDao.findByPendaftar(pe);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (pendaftarDetailPascas != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (pendaftarDetailPascas == null){
                    validasiTagihan.setStatus("Kosong");
                }
                pendaftarDetailPasca.add(validasiTagihan);

            }
            m.addAttribute("detailS2", pendaftarDetailPasca);

            JenisBiaya jenisBiaya = new JenisBiaya();
            jenisBiaya.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
            if (jenisBiaya == null) {
                logger.warn("Jenis Biaya dengan id {} tidak ada dalam database", jenisBiaya.getId());

            }
            List<ValidasiTagihan> tagihans = new ArrayList<>();
            for (Pendaftar pe : pendaftar) {
                Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiaya(pe, jenisBiaya);
                ValidasiTagihan validasiTagihan = new ValidasiTagihan();
                validasiTagihan.setPendaftar(pe.getId());
                if (tagihan != null){
                    validasiTagihan.setStatus("Ada");
                }
                if (tagihan == null){
                    validasiTagihan.setStatus("Kosong");
                }
                tagihans.add(validasiTagihan);
            }

            m.addAttribute("tagihan", tagihans);

            List<Berkas> berkas = new  ArrayList<>();

            List<Berkas> berkas1 = uploadBerkasDao.findPendaftar();
//            berkas.add(berkas1);

            m.addAttribute("berkas", berkas1);

        }
    }



    @PostMapping("/telemarketing/update")
    public String updateStatusPendaftar(@RequestParam Pendaftar id, String followup, String note, String notePic, String statusKet, String kelas,
                                        @RequestParam(required = false, defaultValue = "0") int page, RedirectAttributes attributes, Authentication currentUser){
        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.info("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }

        Pendaftar pendaftar = pendaftarDao.findById(id.getId()).get();
        Followup fu = followupDao.findById(followup).get();
        pendaftar.setFollowup(fu);
        pendaftar.setStatus(statusKet);
        pendaftar.setNote(note);
        pendaftar.setNotePic(notePic);
        pendaftar.setUserUpdate(u);
        pendaftar.setTglUpdate(LocalDate.now());
        pendaftarDao.save(pendaftar);
        attributes.addAttribute("page", page);
        if (Jenjang.S1.equals(pendaftar.getLeads().getJenjang())) {
            if (kelas.equals("Reguler")) {
                return "redirect:/telemarketing/listReguler";
            } else {
                return "redirect:/telemarketing/listKaryawan";
            }
        }else{
            return "redirect:/telemarketing/listPascasarjana";
        }
    }

}

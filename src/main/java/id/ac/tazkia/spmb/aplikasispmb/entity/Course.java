package id.ac.tazkia.spmb.aplikasispmb.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity @Data
public class Course {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    private String namaMatakuliah;

    @NotNull
    private String cover;

    @NotNull
    @Column(columnDefinition="TEXT")
    private String deskripsi;

    @NotNull
    @ManyToOne
    @JoinColumn(name="id_dosen")
    private Dosen dosen;

    @NotNull
    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @NotNull
    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @NotNull @Min(0)
    private BigDecimal harga;

    @NotNull
    private String idElearning;



}

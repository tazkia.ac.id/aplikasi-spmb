package id.ac.tazkia.spmb.aplikasispmb.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.dto.*;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import org.apache.commons.lang3.text.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import jakarta.annotation.PostConstruct;

import java.math.BigDecimal;
import java.security.Key;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@Service
public class ApiService {

    @Value("${feeder.url}") private String feederUrl;
    @Value("${feeder.id}") private String id;
    @Value("${feeder.user}") private String user;

    @Autowired
    private TahunAjaranDao tahunAjaranDao;
    @Autowired
    private PendaftarDetailDao pendaftarDetailDao;
    @Autowired
    private PendaftarOrangTuaDao pendaftarOrangTuaDao;
    @Autowired
    private PendaftarDetailPascaDao pendaftarDetailPascaDao;
    @Autowired
    private ObjectMapper objectMapper;

    private WebClient webClient;

    @Autowired
    private TagihanDao tagihanDao;
    @Autowired
    private CicilanDao cicilanDao;
    @Autowired
    private KafkaSender kafkaSender;



    @PostConstruct
    public void inisalisaiWebClient(){
        webClient = WebClient.builder().
                baseUrl(feederUrl).
                defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE).
                build();
    }

    public String insertMahasiswa(String nim){
        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);

        NimMahasiswaSmileDto smileDto = new NimMahasiswaSmileDto();
        smileDto.setId(id);
        smileDto.setUser(user);

        PendaftarDetail pd = pendaftarDetailDao.findByNim(nim);
        PendaftarDetailPasca pdp = pendaftarDetailPascaDao.findByNim(nim);
        JenisBiaya jenisBiaya = new JenisBiaya();
        jenisBiaya.setId("002");
//Cek Pendaftar S1
        if (pd != null && pdp == null ) {
            Iterable<PendaftarDetail> dataPendaftar = pendaftarDetailDao.findByNimNotNullAndPendaftarTahunAjaranAndNim(ta, nim);
            Iterable<PendaftarOrangtua> dataOrangtua = pendaftarOrangTuaDao.findByPendaftarTahunAjaranAndPendaftar(ta, pd.getPendaftar());


            Iterable<Cicilan> cicilans = cicilanDao.cekCicilanTagihan(pd.getPendaftar().getId());

            for (PendaftarDetail p : dataPendaftar) {
                for (PendaftarOrangtua po : dataOrangtua) {

                    smileDto.setNim(p.getNim());
                    smileDto.setNama(WordUtils.capitalize(p.getPendaftar().getLeads().getNama()));
                    smileDto.setAngkatan(p.getPendaftar().getTahunAjaran().getNama().substring(0, 4));
                    smileDto.setProdi(p.getPendaftar().getProgramStudi().getKodeSimak());
                    smileDto.setJenisKelamin(p.getJenisKelamin());
                    smileDto.setTempatLahir(p.getTempatLahir());
                    smileDto.setTanggalLahir(p.getTanggalLahir());
                    smileDto.setKabupaten(p.getKokab().getNama());
                    smileDto.setProvinsi(p.getProvinsi().getNama());
                    smileDto.setNegara(p.getNegara());
                    smileDto.setKewarganegaraan(p.getStatusSipil());
                    smileDto.setNik(p.getNoKtp());
                    smileDto.setNisn(p.getNisn());
                    smileDto.setAlamat(p.getAlamatRumah());
                    smileDto.setIdAgama("1");
                    smileDto.setKewarganegaraan(p.getStatusSipil());

//                    smileDto.setTeleponRumah(po.getNoOrangtua());
                    smileDto.setTelepon(p.getPendaftar().getLeads().getNoHp());
                    smileDto.setEmail(p.getPendaftar().getLeads().getEmail());
                    smileDto.setAyah(WordUtils.capitalize(po.getNamaAyah()));
                    smileDto.setIbu(WordUtils.capitalize(po.getNamaIbu()));
                    smileDto.setJenjang(p.getPendaftar().getLeads().getJenjang().toString());
                    smileDto.setProgram(p.getPendaftar().getKonsentrasi());
                    smileDto.setJenjang(p.getPendaftar().getLeads().getJenjang().toString());
                    smileDto.setKonsentrasi(p.getPendaftar().getKonsentrasi());

                    ArrayList<MigrasiCicilanDto> mccd = new ArrayList<MigrasiCicilanDto>();

                    for (Cicilan cicilan : cicilans){
                        Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiayaAndLunas(pd.getPendaftar(), jenisBiaya, false);
                        MigrasiCicilanDto mcd = new MigrasiCicilanDto();
                        if (tagihan != null && cicilan.getStatus() == true) {
                            mcd.setUrutan(cicilan.getUrutanCicilan());
                            mcd.setKeterangan(cicilan.getKeterangan());
                            mcd.setNominal(cicilan.getNominal());
                            mcd.setTanggalKirim(cicilan.getTanggalKirim());
                            mcd.setStatus(cicilan.getStatus());
                            mcd.setNomorTagihan(tagihan.getNomorTagihan());
                        } else {
                            if (cicilan.getStatus() == false) {
                                mcd.setUrutan(cicilan.getUrutanCicilan());
                                mcd.setKeterangan(cicilan.getKeterangan());
                                mcd.setNominal(cicilan.getNominal());

                                Long urutan = Long.valueOf(cicilan.getUrutanCicilan());
                                LocalDate cc = LocalDate.now();
                                if (cicilan.getTanggalKirim() == null) {
                                        mcd.setTanggalKirim(cc.plusMonths(urutan - 2));
                                } else {
                                    mcd.setTanggalKirim(cicilan.getTanggalKirim());
                                }
                                mcd.setStatus(cicilan.getStatus());
                                mcd.setNomorTagihan(null);
                                //Perbarui Status Cicilan
                                cicilan.setStatus(Boolean.TRUE);
                                cicilanDao.save(cicilan);
                                //
                            }else{
                                mcd = null;
                            }
                        }
                        if (mcd != null) {
                            mccd.add(mcd);
                            smileDto.setCicilan(mccd);
                        }
                    }

                }

                kafkaSender.requestCreateDebitur(DebiturRequest.builder().email(p.getPendaftar().getLeads().getEmail()).nomorDebitur(p.getNim()).nama(p.getPendaftar().getLeads().getNama())
                        .noHp(p.getPendaftar().getLeads().getNoHp()).build());
            }
// Cek Pendaftar S2
        } else if (pdp != null && pd == null ) {

            Iterable<PendaftarDetailPasca> dataPendaftarS2 = pendaftarDetailPascaDao.findByNimNotNullAndPendaftarTahunAjaranAndNim(ta, nim);

            Iterable<Cicilan> cicilans = cicilanDao.cekCicilanTagihan(pdp.getPendaftar().getId());

            for (PendaftarDetailPasca p : dataPendaftarS2) {

                smileDto.setNim(p.getNim());
                smileDto.setNama(WordUtils.capitalize(p.getPendaftar().getLeads().getNama()));
                smileDto.setAngkatan("26");
                smileDto.setProdi(p.getPendaftar().getProgramStudi().getKodeSimak());
                smileDto.setJenisKelamin(p.getJenisKelamin());
                smileDto.setTempatLahir(p.getTempatLahir());
                smileDto.setTanggalLahir(p.getTanggalLahir());
                smileDto.setKabupaten(p.getKokab().getNama());
                smileDto.setProvinsi(p.getProvinsi().getNama());
                smileDto.setNegara(p.getNegara());
                smileDto.setKewarganegaraan(p.getStatusSipil());
                smileDto.setNik(p.getNoKtp());
                smileDto.setIdAgama("1");
                smileDto.setKewarganegaraan(p.getStatusSipil());

                smileDto.setAlamat(p.getAlamatRumah());
//                smileDto.setKodepos(p.getKodePos());
//                smileDto.setNoOrangtua(p.getNoOrangtua());
                smileDto.setTelepon(p.getPendaftar().getLeads().getNoHp());
                smileDto.setEmail(p.getPendaftar().getLeads().getEmail());
                smileDto.setAyah(WordUtils.capitalize(p.getNamaAyah()));
                smileDto.setIbu(WordUtils.capitalize(p.getNamaIbu()));
                smileDto.setJenjang(p.getPendaftar().getLeads().getJenjang().toString());
                smileDto.setProgram(p.getPendaftar().getKonsentrasi());
                smileDto.setJenjang(p.getPendaftar().getLeads().getJenjang().toString());

                ArrayList<MigrasiCicilanDto> mccd = new ArrayList<MigrasiCicilanDto>();

                for (Cicilan cicilan : cicilans){
                    Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiayaAndLunas(pdp.getPendaftar(), jenisBiaya, false);
                    MigrasiCicilanDto mcd = new MigrasiCicilanDto();
                    if (tagihan != null && tagihan.getPendaftar() == cicilan.getPendaftar() && cicilan.getStatus() == true) {
                        mcd.setUrutan(cicilan.getUrutanCicilan());
                        mcd.setKeterangan(cicilan.getKeterangan());
                        mcd.setNominal(cicilan.getNominal());
                        mcd.setTanggalKirim(cicilan.getTanggalKirim());
                        mcd.setStatus(cicilan.getStatus());
                        mcd.setNomorTagihan(tagihan.getNomorTagihan());
                    } else {
                        if (cicilan.getStatus() == false) {
                            mcd.setUrutan(cicilan.getUrutanCicilan());
                            mcd.setKeterangan(cicilan.getKeterangan());
                            mcd.setNominal(cicilan.getNominal());
                            mcd.setTanggalKirim(cicilan.getTanggalKirim());
                            mcd.setStatus(cicilan.getStatus());
                            mcd.setNomorTagihan(null);
                        }else{
                            mcd = null;
                        }
                    }
                    if (mcd != null) {
                        mccd.add(mcd);
                        smileDto.setCicilan(mccd);
                    }
                }


                kafkaSender.requestCreateDebitur(DebiturRequest.builder().email(p.getPendaftar().getLeads().getEmail()).nomorDebitur(p.getNim()).nama(p.getPendaftar().getLeads().getNama())
                        .noHp(p.getPendaftar().getLeads().getNoHp()).build());
            }
        }


        try {
            String jsonRequest = objectMapper.writeValueAsString(smileDto);
            System.out.println(jsonRequest);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }


        NimMahasiswaSmileResponse response = webClient.post()
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(smileDto), NimMahasiswaSmileDto.class)
                .retrieve().bodyToMono(NimMahasiswaSmileResponse.class)
                .block();


        if (response == null) {
            System.out.println("Gagal : " + response);
            return null;
        }
        System.out.println("Respon : " + response);
        return  response.getMassage();
    }


    @Value("${feeder.url.mailerlite}") private String  feederMailerlite;
    @Value("${key.mailerlite}") private String  keyMailerlite;
    @Value("${group.leads}") private String  groupLeads;
    @Value("${group.pendaftar}") private String  groupPendaftar;


    private WebClient webClientMailerlite;

    @PostConstruct
    public void setFeederMailerlite(){
        webClientMailerlite = WebClient.builder().
                baseUrl(feederMailerlite).
                defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE).
                build();
    }

    public String insertSubcriber(Leads leadsDto){
        SubscriberRequest dto = new SubscriberRequest();
        dto.setEmail(leadsDto.getEmail());
        dto.setName(leadsDto.getNama());

        try {
            String jsonRequest = objectMapper.writeValueAsString(dto);
            System.out.println(jsonRequest);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        SubscriberResponse response = webClientMailerlite.post()
                .uri("/subscribers")
                .headers(httpHeaders -> {
                    httpHeaders.add("x-mailerlite-apikey", keyMailerlite);
                })
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(dto), SubscriberRequest.class)
                .retrieve().bodyToMono(SubscriberResponse.class)
                .block();

        if (response == null) {
            return null;
        }
        return  response.getError_code();
    }

    public String insertGroup(Leads leadsDto, String uri){
        SubscriberRequest dto = new SubscriberRequest();
        dto.setEmail(leadsDto.getEmail());
        dto.setName(leadsDto.getNama());

        try {
            String jsonRequest = objectMapper.writeValueAsString(dto);
            System.out.println(jsonRequest);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        SubscriberResponse response = webClientMailerlite.post()
                .uri(uri)
                .headers(httpHeaders -> {
                    httpHeaders.add("x-mailerlite-apikey", keyMailerlite);
                })
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(dto), SubscriberRequest.class)
                .retrieve().bodyToMono(SubscriberResponse.class)
                .block();

        if (response == null) {
            return null;
        }
        return  response.getError_code();
    }


    public String deleteGroup(Leads leads, String uri){
//Delete Group Leads
        SubscriberRequest dto = new SubscriberRequest();
        dto.setEmail(leads.getEmail());
        dto.setName(leads.getNama());
        String email = leads.getEmail();
        System.out.println("Link : "+ uri +"/"+email);

        try {
            String jsonRequest = objectMapper.writeValueAsString(email);
            System.out.println(jsonRequest);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        String response = webClientMailerlite.delete()
                .uri(uri+"/"+email)
                .headers(httpHeaders -> {
                    httpHeaders.add("x-mailerlite-apikey", keyMailerlite);
                    httpHeaders.add("X-MailerLite-ApiDocs", "true");
                    httpHeaders.add("Accept", "application/json");
                })
                .retrieve().bodyToMono(String.class)
                .block();

        return  response;
    }


    //    JOURNAL
//    @Value("${feeder.url.journal}") private String urlJournal;
//    @Value("${journal.code}") private String journalCode;


//    private WebClient webClientJournal;
//
//    @PostConstruct
//    public void inisialisasiWebClient() {
//        webClientJournal = WebClient.builder()
//                .baseUrl(urlJournal)
//                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
//                .build();
//    }
//    public String postJournal(JournalGetTemplate dto){
//        dto.setCodeTemplate(journalCode);
//        dto.setDateTransaction(LocalDate.now());
//
//        try {
//            String jsonRequest = objectMapper.writeValueAsString(dto);
//            System.out.println(jsonRequest);
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }
//
//
//        JournalResponse response = webClientJournal.post()
//                .contentType(MediaType.APPLICATION_JSON)
//                .body(Mono.just(dto), JournalGetTemplate.class)
//                .retrieve().bodyToMono(JournalResponse.class)
//                .block();
//
//        if (response == null) {
//            return null;
//        }
//
//        return  response.getResponseMessage();
//    }
}

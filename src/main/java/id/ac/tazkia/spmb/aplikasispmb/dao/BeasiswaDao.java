package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.Beasiswa;
import id.ac.tazkia.spmb.aplikasispmb.entity.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface BeasiswaDao extends PagingAndSortingRepository<Beasiswa, String>, CrudRepository<Beasiswa, String> {

    Page<Beasiswa> findByNamaContainingIgnoreCaseOrderByNama(String nama, Pageable page);

    List<Beasiswa> findByStatus(Status aktif);
}

package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.Diskon;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface DiskonDao extends PagingAndSortingRepository<Diskon, String>, CrudRepository <Diskon, String> {
    @Query("select p from Diskon p where :tanggal between p.tanggalMulai and p.tanggalSelesai")
    List<Diskon> cariDiskonUntukTanggal(@Param("tanggal") LocalDate tanggal);

}

package id.ac.tazkia.spmb.aplikasispmb.dto;

import id.ac.tazkia.spmb.aplikasispmb.entity.Grade;
import lombok.Data;

import java.util.Date;

@Data
public class SmartTestDto {

    private String nama;
    private String asalSekolah;
    private Grade grade;
    private Date tanggalTest;

}

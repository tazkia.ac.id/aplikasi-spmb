package id.ac.tazkia.spmb.aplikasispmb.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.time.LocalDate;

@Entity @Data
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class PendaftarDetailPasca {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_pendaftar")
    private Pendaftar pendaftar;

    @Column(nullable = false)
    @NotNull
    @NotEmpty
    private String tempatLahir;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull
    @Column(columnDefinition = "DATE")
    private LocalDate tanggalLahir;

    @NotNull
    @NotEmpty
    private String agama;

    @NotNull
    @NotEmpty
    private String jenisKelamin;

    @NotNull
    @NotEmpty
    @Size(max = 2)
    private String golonganDarah;

    @NotNull
    @NotEmpty
    private String noKtp;

    @NotNull
    @NotEmpty
    private String alamatRumah;

    @NotNull
    @NotEmpty
    private String negara;

    @NotNull
    @NotEmpty
    private String statusSipil;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_provinsi")
    private Provinsi provinsi;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_kokab")
    private KabupatenKota kokab;

    @NotNull
    @NotEmpty
    private String kodePos;

    @NotNull
    @NotEmpty
    private String pekerjaan;

    @NotNull
    @NotEmpty
    private String alamatPekerjaan;

    @NotNull
    @NotEmpty
    private String asalPendidikan;

    @NotNull
    @NotEmpty
    private  String sumberBiaya;


    @NotNull
    @NotEmpty
    private  String namaAyah;

    @NotNull
    @NotEmpty
    private  String namaIbu;

    private  String nim;



}

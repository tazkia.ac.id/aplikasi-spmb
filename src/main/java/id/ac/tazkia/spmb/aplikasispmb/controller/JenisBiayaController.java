package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.dao.JenisBiayaDao;
import id.ac.tazkia.spmb.aplikasispmb.entity.JenisBiaya;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.validation.Valid;

@Controller
public class JenisBiayaController {
    @Autowired
    private JenisBiayaDao jenisBiayaDao;

    @RequestMapping("/jenisbiaya/list")
    public void daftarJenisBiaya(@RequestParam(required = false)String nama, Model m, Pageable page){
        if(StringUtils.hasText(nama)) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarBiaya", jenisBiayaDao.findByNamaContainingIgnoreCaseOrderByNama(nama, page));
        } else {
            m.addAttribute("daftarBiaya", jenisBiayaDao.findAll(page));
        }
    }

//Hapus Data
    @RequestMapping("/jenisbiaya/hapus")
    public  String hapus(@RequestParam("id") JenisBiaya id ){
        jenisBiayaDao.delete(id);
        return "redirect:list";
    }
//

//tampikan form
    @RequestMapping(value = "/jenisbiaya/form", method = RequestMethod.GET)
    public String tampilkanForm(@RequestParam(value = "id", required = false) String id,
                                Model m){
        //defaultnya, isi dengan object baru
        m.addAttribute("jenis", new JenisBiaya());

        if (id != null && !id.isEmpty()){
            JenisBiaya p= jenisBiayaDao.findById(id).get();
            if (p != null){
                m.addAttribute("jenis", p);
            }
        }
        return "jenisbiaya/form";
    }
////
//simpan
    @RequestMapping(value = "/jenisbiaya/form", method = RequestMethod.POST)
    public String prosesForm(@Valid JenisBiaya p, BindingResult errors){
        if(errors.hasErrors()){
            return "/jenisbiaya/form";
        }
        jenisBiayaDao.save(p);
        return "redirect:list";
    }

////
}

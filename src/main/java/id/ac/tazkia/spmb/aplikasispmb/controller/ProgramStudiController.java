package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.dao.ProgramStudiDao;
import id.ac.tazkia.spmb.aplikasispmb.dto.ApiPendaftarDto;
import id.ac.tazkia.spmb.aplikasispmb.dto.ApiRekapPendaftarDto;
import id.ac.tazkia.spmb.aplikasispmb.entity.ProgramStudi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ProgramStudiController {
    @Autowired
    private ProgramStudiDao programStudiDao;


    //Hapus Data
    @RequestMapping("/programstudi/hapus")
    public  String hapus(@RequestParam("id") ProgramStudi id ){
        programStudiDao.delete(id);
        return "redirect:list";
    }
    //

    //list programstudi
    @RequestMapping("/programstudi/list")
    public void  daftarProgram(@RequestParam(required = false)String nama, Model m,
                               @PageableDefault(size = 10, sort = "nama", direction = Sort.Direction.ASC) Pageable page){
        if(StringUtils.hasText(nama)) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarProgram", programStudiDao.findByNamaContainingIgnoreCaseOrderByNama(nama, page));
        } else {
            m.addAttribute("daftarProgram", programStudiDao.findAll(page));
        }
    }
    //



    @RequestMapping(value = "/programstudi/form", method = RequestMethod.GET)
    public String leadsUpdateForm(@RequestParam(value = "id", required = false) String id,
                                  Model m){
        //defaultnya, isi dengan object baru
        m.addAttribute("program", new ProgramStudi());

        if (id != null && !id.isEmpty()){
            ProgramStudi programStudi= programStudiDao.findById(id).get();
            if (programStudi != null){
                m.addAttribute("program", programStudi);
            }

        }
        return "/programstudi/form";
    }
    //simpan
    @RequestMapping(value = "/programstudi/form", method = RequestMethod.POST)
    public String prosesForm(@Valid ProgramStudi p, BindingResult errors){
        if(errors.hasErrors()){
            return "/programstudi/form";
        }
        programStudiDao.save(p);
        return "redirect:list";
    }
    ////

    @GetMapping("/api/pendaftarPerprodi")
    @ResponseBody
    public ResponseEntity<List<ApiRekapPendaftarDto>> hitung(){

        List<ApiPendaftarDto> apiPendaftarDtos = programStudiDao.hitung();
        List<ApiRekapPendaftarDto> apiRekapPendaftarDtos = new ArrayList<>();
        for(ApiPendaftarDto apiPendaftarDto : apiPendaftarDtos){
            ApiRekapPendaftarDto apiRekapPendaftarDto = new ApiRekapPendaftarDto();
            apiRekapPendaftarDto.setNama(apiPendaftarDto.getNama());
            apiRekapPendaftarDto.setJumlah(apiPendaftarDto.getJumlah());
            apiRekapPendaftarDtos.add(apiRekapPendaftarDto);
        }

        final HttpHeaders httpHeaders= new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        return new ResponseEntity<>(apiRekapPendaftarDtos, httpHeaders, HttpStatus.OK);
    }

    @GetMapping("/api/pendaftarPertahun")
    @ResponseBody
    public ResponseEntity<List<ApiRekapPendaftarDto>> pendaftarPertahun(){

        List<ApiPendaftarDto> apiPendaftarDtos = programStudiDao.pendaftarPertahun();
        List<ApiRekapPendaftarDto> apiRekapPendaftarDtos = new ArrayList<>();
        for(ApiPendaftarDto apiPendaftarDto : apiPendaftarDtos){
            ApiRekapPendaftarDto apiRekapPendaftarDto = new ApiRekapPendaftarDto();
            apiRekapPendaftarDto.setNama(apiPendaftarDto.getNama());
            apiRekapPendaftarDto.setJumlah(apiPendaftarDto.getJumlah());
            apiRekapPendaftarDtos.add(apiRekapPendaftarDto);
        }

        final HttpHeaders httpHeaders= new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        return new ResponseEntity<>(apiRekapPendaftarDtos, httpHeaders, HttpStatus.OK);
    }
}

package id.ac.tazkia.spmb.aplikasispmb.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DataCourse {
    private String nomorPeserta;
    private String email;
    private String nama;
    private String noHp;
    private String username;
    private String password;
}

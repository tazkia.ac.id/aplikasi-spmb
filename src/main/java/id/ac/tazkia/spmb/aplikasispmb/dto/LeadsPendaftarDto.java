package id.ac.tazkia.spmb.aplikasispmb.dto;

import id.ac.tazkia.spmb.aplikasispmb.entity.Jenjang;
import id.ac.tazkia.spmb.aplikasispmb.entity.Leads;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class LeadsPendaftarDto {

    private String nama;
    private String email;
    private String noHp;
    private Jenjang jenjang;
    private String password;

    private String idKabupatenKota;
    private String namaAsalSekolah;
    private String programStudi;
    private String konsentrasi;
    private String alasanMemilihProdi;

}

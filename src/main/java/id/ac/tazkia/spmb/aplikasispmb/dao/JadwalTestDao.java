package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;

public interface JadwalTestDao extends PagingAndSortingRepository<JadwalTest, String>, CrudRepository<JadwalTest, String> {
    JadwalTest findByPendaftar(Pendaftar pendaftar);

    @Query ("select j from JadwalTest j where j.pendaftar.leads.nama like %:nama% and j.pendaftar.tahunAjaran = :tahun and j.jenisTest != :jenis order by j.tanggalTest DESC")
    Page<JadwalTest> findJadwalTest (@Param("nama") String nama,  @Param("tahun") TahunAjaran tahun,@Param("jenis")JenisTest jenisTest, Pageable page);

    @Query ("select  j from JadwalTest  j where j.pendaftar.tahunAjaran = :tahun and j.jenisTest != :jenis order by j.tanggalTest DESC")
    Page<JadwalTest> findJadwalTestAll (@Param("tahun") TahunAjaran tahun,@Param("jenis")JenisTest jenisTest,Pageable page);

    @Query ("select j from JadwalTest j where j.pendaftar.leads.nama like %:smart% and month(j.tanggalTest) = :bulan and j.pendaftar.tahunAjaran = :tahun and j.jenisTest = :jenis")
    Page<JadwalTest> findJadwalTestSt (@Param("smart") String smart, @Param("bulan") Integer bulan, @Param("tahun") TahunAjaran tahun,@Param("jenis")JenisTest jenisTest, Pageable page);

    @Query ("select  j from JadwalTest  j where month(j.tanggalTest) = :bulan and j.pendaftar.tahunAjaran = :tahun and j.jenisTest = :jenis")
    Page<JadwalTest> findJadwalTestSmart (@Param("bulan") Integer bulan, @Param("tahun") TahunAjaran tahun, @Param("jenis")JenisTest jenisTest, Pageable page);


    Page<JadwalTest> findByPendaftarNomorRegistrasiContainingIgnoreCaseOrPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(String no, String nama, Jenjang jenjang, String konsentrasi, TahunAjaran ta, Pageable pageable);
    Page<JadwalTest> findByJenisTestAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(JenisTest jenis,  Jenjang jenjang, String konsentrasi, TahunAjaran ta, Pageable pageable);
    Page<JadwalTest> findByPendaftarNomorRegistrasiContainingIgnoreCaseOrPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarLeadsJenjangAndPendaftarTahunAjaranOrderByTanggalTestDesc(String no, String nama, Jenjang jenjang, TahunAjaran ta, Pageable pageable);
    Page<JadwalTest> findByPendaftarNomorRegistrasiContainingIgnoreCaseOrPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarFollowupAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(String no, String nama, Followup fl, Jenjang jenjang,String konsentrasi, TahunAjaran ta, Pageable pageable);
    Page<JadwalTest> findByJenisTestAndPendaftarFollowupAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(JenisTest jenis, Followup fl, Jenjang jenjang,String konsentrasi, TahunAjaran ta, Pageable pageable);
    Page<JadwalTest> findByPendaftarNomorRegistrasiContainingIgnoreCaseOrPendaftarLeadsNamaContainingIgnoreCaseAndJenisTestAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(String no, String nama, JenisTest jenis, Jenjang jenjang,String konsentrasi, TahunAjaran ta, Pageable pageable);

    Page<JadwalTest> findByPendaftarNomorRegistrasiContainingIgnoreCaseOrPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarFollowupAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndJenisTestAndPendaftarTahunAjaranOrderByTanggalTestDesc(String no, String nama, Followup fl, Jenjang jenjang,String konsentrasi,JenisTest jenis, TahunAjaran ta, Pageable pageable);

    Page<JadwalTest> findByPendaftarNomorRegistrasiContainingIgnoreCaseOrPendaftarLeadsNamaContainingIgnoreCaseAndPendaftarFollowupAndPendaftarLeadsJenjangAndPendaftarTahunAjaranOrderByTanggalTestDesc(String no, String nama, Followup fl, Jenjang jenjang, TahunAjaran ta, Pageable pageable);
    Page<JadwalTest> findByPendaftarFollowupAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(Followup fl, Jenjang jenjang,String konsentrasi, TahunAjaran ta, Pageable pageable);
    Page<JadwalTest> findByPendaftarFollowupAndPendaftarLeadsJenjangAndPendaftarTahunAjaranOrderByTanggalTestDesc(Followup fl, Jenjang jenjang, TahunAjaran ta, Pageable pageable);
    Page<JadwalTest> findByPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc(Jenjang jenjang,String konsentrasi,TahunAjaran ta, Pageable pageable);
    Page<JadwalTest> findByPendaftarLeadsJenjangAndPendaftarTahunAjaranOrderByTanggalTestDesc(Jenjang jenjang,TahunAjaran ta, Pageable pageable);

    Iterable<JadwalTest> findByPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarTahunAjaranOrderByTanggalTestDesc (Jenjang jenjang,String konsentrasi,TahunAjaran ta);
    Iterable<JadwalTest> findByPendaftarLeadsJenjangAndPendaftarTahunAjaranOrderByTanggalTestDesc (Jenjang jenjang,TahunAjaran ta);






}

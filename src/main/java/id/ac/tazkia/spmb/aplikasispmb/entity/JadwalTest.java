package id.ac.tazkia.spmb.aplikasispmb.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.*;
import java.sql.Blob;
import java.time.LocalDate;

@Entity @Data
public class JadwalTest {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @OneToOne
    @JoinColumn(name = "id_pendaftar")
    private Pendaftar pendaftar;

    @Enumerated(EnumType.STRING)
    private JenisTest jenisTest;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalTest;

    private String fileRaport;

    @OneToOne
    @JoinColumn(name = "id_beasiswa")
    private Beasiswa beasiswa;

    @Column(columnDefinition="TEXT")
    private String soal1;
    @Column(columnDefinition="TEXT")
    private String soal2;
    @Column(columnDefinition="TEXT")
    private String soal3;
    @Column(columnDefinition="TEXT")
    private String soal4;
    @Column(columnDefinition="TEXT")
    private String soal5;
    @Column(columnDefinition="TEXT")
    private String soal6;
    @Column(columnDefinition="TEXT")
    private String soal7;
    @Column(columnDefinition="TEXT")
    private String soal8;
    @Column(columnDefinition="TEXT")
    private String soal9;
    @Column(columnDefinition="TEXT")
    private String soal10;
}

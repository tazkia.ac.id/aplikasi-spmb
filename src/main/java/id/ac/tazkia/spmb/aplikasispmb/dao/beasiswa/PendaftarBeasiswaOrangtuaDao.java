package id.ac.tazkia.spmb.aplikasispmb.dao.beasiswa;

import id.ac.tazkia.spmb.aplikasispmb.entity.beasiswa.PendaftarBeasiswa;
import id.ac.tazkia.spmb.aplikasispmb.entity.beasiswa.PendaftarBeasiswaOrangtua;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PendaftarBeasiswaOrangtuaDao extends PagingAndSortingRepository<PendaftarBeasiswaOrangtua, String>, CrudRepository<PendaftarBeasiswaOrangtua, String> {

    PendaftarBeasiswaOrangtua findByPendaftarBeasiswa(PendaftarBeasiswa pendaftarBeasiswa);

}

package id.ac.tazkia.spmb.aplikasispmb.entity.beasiswa;

import groovyjarjarantlr4.v4.runtime.misc.NotNull;
import id.ac.tazkia.spmb.aplikasispmb.entity.KabupatenKota;
import id.ac.tazkia.spmb.aplikasispmb.entity.Provinsi;
import id.ac.tazkia.spmb.aplikasispmb.entity.User;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Entity @Data
public class PendaftarBeasiswa {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    private String namaLengkap;

    @NotNull
    private String noTelpon;

    @NotNull
    private String email;

    @NotNull
    private String lulusan;

    @NotNull
    private String kotaAsalSekolah;

    @NotNull
    private String namaSekolah;

    private String tahunLulus;

    private String statusOrtu;

    private String nik;

    private String tempatLahir;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalLahir;

    private String nisn;

    private String npsn;

    @Column(columnDefinition = "LONGTEXT")
    private String alamat;

    @ManyToOne
    @JoinColumn(name = "provinsi")
    private Provinsi provinsi;

    @ManyToOne
    @JoinColumn(name = "kabupaten_kota")
    private KabupatenKota kabupatenKota;

    private String kecamatan;

    private String desaKelurahan;

    private String kodePos;

    private String noPendaftaranKip;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

    private String statusDisabilitas;

    @Column(columnDefinition = "LONGTEXT")
    private String deskripsiDisabilitas;

}

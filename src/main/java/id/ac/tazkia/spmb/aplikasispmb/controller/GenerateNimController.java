package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.dto.DetailPendaftarOrangTuaDto;
import id.ac.tazkia.spmb.aplikasispmb.dto.DetailPendaftarPascaDto;
import id.ac.tazkia.spmb.aplikasispmb.dto.MigrasiCicilanDto;
import id.ac.tazkia.spmb.aplikasispmb.dto.NimMahasiswaSmileDto;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import id.ac.tazkia.spmb.aplikasispmb.service.ApiService;
import id.ac.tazkia.spmb.aplikasispmb.service.RunningNumberService;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Controller
public class GenerateNimController {

    private static final Logger logger = LoggerFactory.getLogger(GenerateNimController.class);

    @Autowired
    private PekerjaanDao pekerjaanDao;
    @Autowired
    private PenghasilanDao penghasilanDao;
    @Autowired
    private PendidikanDao pendidikanDao;
    @Autowired
    private ProgramStudiDao programStudiDao;
    @Autowired
    private PendaftarDetailDao pendaftarDetailDao;
    @Autowired
    private PendaftarDetailPascaDao pendaftarDetailPascaDao;
    @Autowired
    private PendaftarOrangTuaDao pendaftarOrangTuaDao;
    @Autowired
    private PembayaranController pembayaranController;
    @Autowired
    private RunningNumberService runningNumberService;
    @Autowired
    private TagihanDao tagihanDao;
    @Autowired
    private CicilanDao cicilanDao;
    @Autowired
    private PendaftarDao pendaftarDao;
    @Autowired
    private LeadsDao leadsDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private TahunAjaranDao tahunAjaranDao;
    @Autowired
    private HasilTestDao hasilTestDao;
    @Autowired
    private PembayaranDao pembayaranDao;
    @Value("${upload.folder}")
    private String uploadFolder;

    @Autowired
    private ApiService apiService;
    @Autowired
    private KabupatenKotaDao kabupatenKotaDao;


    @ModelAttribute("daftarProdiS1")
    public Iterable<ProgramStudi> daftarProdiS1() {
        String id = "007";
        String tidak = "005";
        String tidak2 = "000";
        return programStudiDao.cariProdiS1(id,tidak, tidak2);
    }

    @ModelAttribute("daftarProdiD3")
    public Iterable<ProgramStudi> daftarProdiD3() {
        String id = "012";
        return programStudiDao.cariProdiD3(id);
    }


    @ModelAttribute("daftarProdiS2")
    public Iterable<ProgramStudi> daftarProdiS2() {
        String id = "007";
        String dua = "009";
        return programStudiDao.cariProdi(id, dua);
    }


    @ModelAttribute("pendidikanOrtu")
    public Iterable<Pendidikan> pendidikanOrtu() {
        return pendidikanDao.findAll();
    }

    @ModelAttribute("pekerjaanOrtu")
    public Iterable<Pekerjaan> pekerjaanOrtu() {
        return pekerjaanDao.findAll();
    }

    @ModelAttribute("penghasilanOrtu")
    public Iterable<Penghasilan> penghasilanOrtu() {
        return penghasilanDao.findAll();
    }


    @GetMapping("/generateNim/form")
    public String formGenerateNim(@RequestParam(required = false) Pendaftar pendaftar, Model model,
                                  Authentication currentUser, Pageable page) {
        logger.debug("Authentication class : {}", currentUser.getClass().getName());


        logger.info("Pendaftar a.n {} ditemukan", pendaftar.getLeads().getNama());

        DetailPendaftarOrangTuaDto detailDto = new DetailPendaftarOrangTuaDto();

        PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pendaftar);
        PendaftarOrangtua pendaftarOrangtua = pendaftarOrangTuaDao.findByPendaftar(pendaftar);
        if (pendaftarDetail != null && pendaftarOrangtua != null) {
            detailDto.setId(pendaftarDetail.getId());
            detailDto.setPendaftar(pendaftar);
            detailDto.setFotoPe(pendaftarDetail.getFoto());
            detailDto.setTempatLahir(pendaftarDetail.getTempatLahir());
            detailDto.setTanggalLahir(pendaftarDetail.getTanggalLahir());
            detailDto.setAgama(pendaftarDetail.getAgama());
            detailDto.setJenisKelamin(pendaftarDetail.getJenisKelamin());
            detailDto.setGolonganDarah(pendaftarDetail.getGolonganDarah());
            detailDto.setNoKtp(pendaftarDetail.getNoKtp());
            detailDto.setAlamatRumah(pendaftarDetail.getAlamatRumah());
            detailDto.setNegara(pendaftarDetail.getNegara());
            detailDto.setProvinsi(pendaftarDetail.getProvinsi());
            detailDto.setKokab(pendaftarDetail.getKokab());
            detailDto.setKodePos(pendaftarDetail.getKodePos());
            detailDto.setNisn(pendaftarDetail.getNisn());
            detailDto.setJurusanSekolah(pendaftarDetail.getJurusanSekolah());
            detailDto.setTahunLulus(pendaftarDetail.getTahunLulus());
            detailDto.setStatusSipil(pendaftarDetail.getStatusSipil());
            detailDto.setRencanaBiaya(pendaftarDetail.getRencanaBiaya());

            detailDto.setOrangTua(pendaftarOrangtua.getId());
            detailDto.setNamaAyah(pendaftarOrangtua.getNamaAyah());
            detailDto.setAgamaAyah(pendaftarOrangtua.getAgamaAyah());
            detailDto.setPekerjaanAyah(pendaftarOrangtua.getPekerjaanAyah());
            detailDto.setPendidikanAyah(pendaftarOrangtua.getPendidikanAyah());
            detailDto.setStatusAyah(pendaftarOrangtua.getStatusAyah());

            detailDto.setNamaIbu(pendaftarOrangtua.getNamaIbu());
            detailDto.setAgamaIbu(pendaftarOrangtua.getAgamaIbu());
            detailDto.setPekerjaanIbu(pendaftarOrangtua.getPekerjaanIbu());
            detailDto.setPendidikanIbu(pendaftarOrangtua.getPendidikanIbu());
            detailDto.setStatusIbu(pendaftarOrangtua.getStatusIbu());

            detailDto.setAlamatOrangtua(pendaftarOrangtua.getAlamat());
            detailDto.setNoOrangtua(pendaftarOrangtua.getNoOrangtua());
            detailDto.setPenghasilanOrangtua((pendaftarOrangtua.getPenghasilanOrangtua()));
            detailDto.setJumlahTanggungan(pendaftarOrangtua.getJumlahTanggungan());

            detailDto.setProgramStudi(pendaftarDetail.getPendaftar().getProgramStudi());
            detailDto.setKonsentrasi(pendaftarDetail.getPendaftar().getKonsentrasi());
            detailDto.setLeads(pendaftarDetail.getPendaftar().getLeads());
            detailDto.setLeads(pendaftarDetail.getPendaftar().getLeads());
            detailDto.setNama(pendaftarDetail.getPendaftar().getLeads().getNama());
            detailDto.setNoHp(pendaftarDetail.getPendaftar().getLeads().getNoHp());
            detailDto.setEmail(pendaftarDetail.getPendaftar().getLeads().getEmail());

            detailDto.setNim(pendaftarDetail.getNim());
            detailDto.setProdiLama(pendaftarDetail.getPendaftar().getProgramStudi());

            detailDto.setUpdateTime(pendaftarDetail.getUpdateTime());


            model.addAttribute("detailDto", detailDto);
        } else {
            logger.warn("Data detail & data orangtua dengan pendaftar {} tidak ada dalam database", pendaftar.getLeads().getNama());
        }
        detailDto.setPendaftar(pendaftar);
        model.addAttribute("detailDto", detailDto);

//Cek Pembayaran
        Pembayaran pembayaran = pembayaranController.cekPembayaran(pendaftar);
        model.addAttribute("pembayaran", pembayaran);

        return "generateNim/form";
    }

    @PostMapping("/generateNim/form")
    private String prosesGenerateNim(@ModelAttribute @Valid DetailPendaftarOrangTuaDto dpotd,
                                     @RequestParam(required = false) ProgramStudi prodiLama, RedirectAttributes redirectAttributes) {

        Pendaftar pendaftar = pendaftarDao.findById(dpotd.getPendaftar().getId()).get();
        pendaftar.setProgramStudi(dpotd.getProgramStudi());
        pendaftar.setKonsentrasi(dpotd.getKonsentrasi());
        logger.info("pendaftar nomor registrasi {} berhasil diperbaharui", pendaftar.getNomorRegistrasi());
        pendaftarDao.save(pendaftar);

        Leads leads = leadsDao.findById(dpotd.getLeads().getId()).get();
        leads.setNama(dpotd.getNama());
        leads.setEmail(dpotd.getEmail());
        leads.setNoHp(dpotd.getNoHp());
        leadsDao.save(leads);

        PendaftarDetail pdl = pendaftarDetailDao.findById(dpotd.getId()).get();
        pdl.setPendaftar(pendaftar);
        pdl.setId(dpotd.getId());
        pdl.setNoKtp(dpotd.getNoKtp());
        pdl.setJenisKelamin(dpotd.getJenisKelamin());
        pdl.setGolonganDarah(dpotd.getGolonganDarah());
        pdl.setTempatLahir(dpotd.getTempatLahir());
        pdl.setTanggalLahir(dpotd.getTanggalLahir());
        pdl.setAgama(dpotd.getAgama());
        pdl.setAlamatRumah(dpotd.getAlamatRumah());
        pdl.setProvinsi(dpotd.getProvinsi());
        pdl.setKokab(dpotd.getKokab());
        pdl.setKodePos(dpotd.getKodePos());
        pdl.setNegara(dpotd.getNegara());
        pdl.setStatusSipil(dpotd.getStatusSipil());
        pdl.setTahunLulus(dpotd.getTahunLulus());
        pdl.setJurusanSekolah(dpotd.getJurusanSekolah());
        pdl.setNisn(dpotd.getNisn());
        pdl.setRencanaBiaya(dpotd.getRencanaBiaya());
        pdl.setUpdateTime(LocalDate.now());

        //Format Generate NIM
            if (prodiLama != null) {
                if (prodiLama.getId() != pdl.getPendaftar().getProgramStudi().getId()) {
                    dpotd.setNim(null);
                    pdl.setNim(dpotd.getNim());
//                    dep.setStatus(StatusTagihan.N);
                    System.out.println("SET NIM : " + pdl.getNim());
                    pendaftarDetailDao.save(pdl);
                }
            }

            if (pdl.getNim() == null || !StringUtils.hasText(pdl.getNim())) {
                DateFormat dateFormat = new SimpleDateFormat("yy");
                String formattedDate = dateFormat.format(Calendar.getInstance().getTime());
                String fakultas = pdl.getPendaftar().getProgramStudi().getKodeFakultas();
                String  kodeSimak = pdl.getPendaftar().getProgramStudi().getKodeSimak();
                String formatNim = formattedDate + "1" + fakultas + kodeSimak;
                pdl.setNim(generateNim(formatNim));


            }

        /////
        redirectAttributes.addFlashAttribute("detail", pdl);
        pendaftarDetailDao.save(pdl);
        apiService.insertMahasiswa(pdl.getNim());

        logger.info("Detail dengan nomor registrasi {} berhasil diperbaharui", pdl.getPendaftar().getNomorRegistrasi());

        PendaftarOrangtua pot = pendaftarOrangTuaDao.findById(dpotd.getOrangTua()).get();
        pot.setId(dpotd.getOrangTua());
        pot.setNamaAyah(dpotd.getNamaAyah());
        pot.setAgamaAyah(dpotd.getAgamaAyah());
        pot.setPendidikanAyah(dpotd.getPendidikanAyah());
        pot.setPekerjaanAyah(dpotd.getPekerjaanAyah());
        pot.setStatusAyah(dpotd.getStatusAyah());
        pot.setNamaIbu(dpotd.getNamaIbu());
        pot.setAgamaIbu(dpotd.getAgamaIbu());
        pot.setPendidikanIbu(dpotd.getPendidikanIbu());
        pot.setPekerjaanIbu(dpotd.getPekerjaanIbu());
        pot.setStatusIbu(dpotd.getStatusIbu());
        pot.setAlamat(dpotd.getAlamatOrangtua());
        pot.setNoOrangtua(dpotd.getNoOrangtua());
        pot.setPenghasilanOrangtua(dpotd.getPenghasilanOrangtua());
        pot.setJumlahTanggungan(dpotd.getJumlahTanggungan());
        logger.info("Orang tua dengan nomor registrasi {} berhasil diperbaharui", pot.getPendaftar().getNomorRegistrasi());
        pendaftarOrangTuaDao.save(pot);


        return "redirect:selesai";
    }

    @GetMapping("/generateNim/formS2")
    public String formGenerateNimS2(@RequestParam(required = false)Pendaftar pendaftar,Model model,
                                  Authentication currentUser, Pageable page){
        logger.info("Pendaftar a.n {} ditemukan", pendaftar.getLeads().getNama());

        DetailPendaftarPascaDto dpd = new DetailPendaftarPascaDto();

        PendaftarDetailPasca pd = pendaftarDetailPascaDao.findByPendaftar(pendaftar);
        if (pd != null) {
            dpd.setId(pd.getId());
            dpd.setPendaftar(pd.getPendaftar());
            dpd.setTempatLahir(pd.getTempatLahir());
            dpd.setTanggalLahir(pd.getTanggalLahir());
            dpd.setJenisKelamin(pd.getJenisKelamin());
            dpd.setAgama(pd.getAgama());
            dpd.setGolonganDarah(pd.getGolonganDarah());
            dpd.setNoKtp(pd.getNoKtp());
            dpd.setAlamatRumah(pd.getAlamatRumah());
            dpd.setNegara(pd.getNegara());
            dpd.setStatusSipil(pd.getStatusSipil());
            dpd.setProvinsi(pd.getProvinsi());
            dpd.setKokab(pd.getKokab());
            dpd.setKodePos(pd.getKodePos());
            dpd.setPekerjaan(pd.getPekerjaan());
            dpd.setAlamatPekerjaan(pd.getAlamatPekerjaan());
            dpd.setAsalPendidikan(pd.getAsalPendidikan());
            dpd.setSumberBiaya(pd.getSumberBiaya());
            dpd.setNamaAyah(pd.getNamaAyah());
            dpd.setNamaIbu(pd.getNamaIbu());

            dpd.setLeads(pd.getPendaftar().getLeads());
            dpd.setNama(pd.getPendaftar().getLeads().getNama());
            dpd.setEmail(pd.getPendaftar().getLeads().getEmail());
            dpd.setNoHp(pd.getPendaftar().getLeads().getNoHp());

            dpd.setProgramStudi(pd.getPendaftar().getProgramStudi());
            dpd.setKonsentrasi(pd.getPendaftar().getKonsentrasi());
            dpd.setProdiLama(pd.getPendaftar().getProgramStudi());

            dpd.setNim(pd.getNim());

            model.addAttribute("detailDto", dpd);
        } else {
            logger.warn("Data detail dengan pendaftar {} tidak ada dalam database", pendaftar.getLeads().getNama());
        }
        dpd.setPendaftar(pendaftar);
        dpd.setLeads(pendaftar.getLeads());
        dpd.setNama(pendaftar.getLeads().getNama());
        dpd.setEmail(pendaftar.getLeads().getEmail());
        dpd.setNoHp(pendaftar.getLeads().getNoHp());
        dpd.setProgramStudi(pendaftar.getProgramStudi());
        dpd.setKonsentrasi(pendaftar.getKonsentrasi());
        model.addAttribute("detailDto", dpd);


        return "generateNim/formS2";
    }
    @PostMapping("/generateNim/formS2")
    private String prosesGenerateNimS2(@ModelAttribute @Valid DetailPendaftarPascaDto dpotd,
                                       @RequestParam(required = false) ProgramStudi prodiLama, RedirectAttributes redirectAttributes) {

        Pendaftar pendaftar = pendaftarDao.findById(dpotd.getPendaftar().getId()).get();
        pendaftar.setProgramStudi(dpotd.getProgramStudi());
        pendaftar.setKonsentrasi(dpotd.getKonsentrasi());
        logger.info("pendaftar nomor registrasi {} berhasil diperbaharui", pendaftar.getNomorRegistrasi());
        pendaftarDao.save(pendaftar);

        Leads leads = leadsDao.findById(dpotd.getLeads().getId()).get();
        leads.setNama(dpotd.getNama());
        leads.setEmail(dpotd.getEmail());
        leads.setNoHp(dpotd.getNoHp());
        leadsDao.save(leads);

        if (dpotd.getId() == null || !StringUtils.hasText(dpotd.getId())) {
            PendaftarDetailPasca pd = new PendaftarDetailPasca();
            pd.setPendaftar(dpotd.getPendaftar());
            pd.setTempatLahir(dpotd.getTempatLahir());
            pd.setTanggalLahir(dpotd.getTanggalLahir());
            pd.setJenisKelamin(dpotd.getJenisKelamin());
            pd.setAgama(dpotd.getAgama());
            pd.setGolonganDarah(dpotd.getGolonganDarah());
            pd.setNoKtp(dpotd.getNoKtp());
            pd.setAlamatRumah(dpotd.getAlamatRumah());
            pd.setNegara(dpotd.getNegara());
            pd.setStatusSipil(dpotd.getStatusSipil());
            pd.setProvinsi(dpotd.getProvinsi());
            pd.setKokab(dpotd.getKokab());
            pd.setKodePos(dpotd.getKodePos());
            pd.setPekerjaan(dpotd.getPekerjaan());
            pd.setAlamatPekerjaan(dpotd.getAlamatPekerjaan());
            pd.setAsalPendidikan(dpotd.getAsalPendidikan());
            pd.setSumberBiaya(dpotd.getSumberBiaya());
            pd.setNamaAyah(dpotd.getNamaAyah());
            pd.setNamaIbu(dpotd.getNamaIbu());
            //Format Generate NIM
            if (pd.getNim() == null || !StringUtils.hasText(pd.getNim())) {
                DateFormat dateFormatYY = new SimpleDateFormat("yy");
                String tahun = dateFormatYY.format(Calendar.getInstance().getTime());

                DateFormat dateFormatMM = new SimpleDateFormat("MM");
                String formatBulan = dateFormatMM.format(Calendar.getInstance().getTime());
                Integer cekBulan = Integer.parseInt(formatBulan);

                String fakultas = pd.getPendaftar().getProgramStudi().getKodeFakultas();
                String kodeSimak = pd.getPendaftar().getProgramStudi().getKodeSimak();
                if (cekBulan >= 07 && cekBulan <= 10) {
                    String kode = "2";
                    String formatNim = tahun + kode + fakultas + kodeSimak;
                    pd.setNim(generateNim(formatNim));
                } else if (cekBulan >= 01 && cekBulan <= 03) {
                    String kode = "1";
                    String formatNim = tahun + kode + fakultas + kodeSimak;
                    pd.setNim(generateNim(formatNim));
                }
            }

            /////
            redirectAttributes.addFlashAttribute("detail", pd);
            pendaftarDetailPascaDao.save(pd);
        }  else if (dpotd.getId() != null || StringUtils.hasText(dpotd.getId())){
            PendaftarDetailPasca pdp = pendaftarDetailPascaDao.findByPendaftar(dpotd.getPendaftar());
            pdp.setId(dpotd.getId());
            pdp.setPendaftar(dpotd.getPendaftar());
            pdp.setTempatLahir(dpotd.getTempatLahir());
            pdp.setTanggalLahir(dpotd.getTanggalLahir());
            pdp.setJenisKelamin(dpotd.getJenisKelamin());
            pdp.setAgama(dpotd.getAgama());
            pdp.setGolonganDarah(dpotd.getGolonganDarah());
            pdp.setNoKtp(dpotd.getNoKtp());
            pdp.setAlamatRumah(dpotd.getAlamatRumah());
            pdp.setNegara(dpotd.getNegara());
            pdp.setStatusSipil(dpotd.getStatusSipil());
            pdp.setProvinsi(dpotd.getProvinsi());
            pdp.setKokab(dpotd.getKokab());
            pdp.setKodePos(dpotd.getKodePos());
            pdp.setPekerjaan(dpotd.getPekerjaan());
            pdp.setAlamatPekerjaan(dpotd.getAlamatPekerjaan());
            pdp.setAsalPendidikan(dpotd.getAsalPendidikan());
            pdp.setSumberBiaya(dpotd.getSumberBiaya());
            pdp.setNamaAyah(dpotd.getNamaAyah());
            pdp.setNamaIbu(dpotd.getNamaIbu());
            //Format Generate NIM
            if (prodiLama != null) {
                if (prodiLama.getId() != pdp.getPendaftar().getProgramStudi().getId()) {
                    dpotd.setNim(null);
                    pdp.setNim(dpotd.getNim());
//                    dep.setStatus(StatusTagihan.N);
                    System.out.println("SET NIM : " + pdp.getNim());
                    pendaftarDetailPascaDao.save(pdp);
                }
            }

            if (pdp.getNim() == null || !StringUtils.hasText(pdp.getNim())) {
                DateFormat dateFormatYY = new SimpleDateFormat("yy");
                String tahun = dateFormatYY.format(Calendar.getInstance().getTime());

                DateFormat dateFormatMM = new SimpleDateFormat("MM");
                String formatBulan = dateFormatMM.format(Calendar.getInstance().getTime());
                Integer cekBulan = Integer.parseInt(formatBulan);

                String fakultas = pdp.getPendaftar().getProgramStudi().getKodeFakultas();
                String  kodeSimak = pdp.getPendaftar().getProgramStudi().getKodeSimak();
                if (cekBulan >= 07 && cekBulan <= 10) {
                    String kode = "2";
                    String formatNim = tahun + kode + fakultas + kodeSimak;
                    pdp.setNim(generateNim(formatNim));
                }else if (cekBulan >= 01 && cekBulan <= 03) {
                    String kode = "1";
                    String formatNim = tahun + kode + fakultas + kodeSimak;
                    pdp.setNim(generateNim(formatNim));
                }
            }

            /////
            redirectAttributes.addFlashAttribute("detail", pdp);
            pendaftarDetailPascaDao.save(pdp);
            apiService.insertMahasiswa(pdp.getNim());
            logger.info("Detail dengan nomor registrasi {} berhasil diperbaharui", pdp.getPendaftar().getNomorRegistrasi());
        }

        return "redirect:selesai";
    }

    @GetMapping("/generateNim/selesai")
    public void formSelesai() {
    }


    public String generateNim(String formatNim) {
        RunningNumber terbaru = runningNumberService.generate(formatNim);

        logger.debug("Format NIM : {}", formatNim);
        logger.debug("Nomer Terakhir : {}", terbaru.getNomerTerakhir());

        String nomorNim = formatNim + String.format("%03d", terbaru.getNomerTerakhir());
        logger.info("Nomor Nim : {}", nomorNim);

        return nomorNim;
    }


    @GetMapping("/generateNim/viewNim")
    public void formViewNim(Model model, Authentication currentUser) {
        logger.debug("Authentication class : {}", currentUser.getClass().getName());

        if (currentUser == null) {
            logger.warn("Current user is null");
        }

        String username = ((UserDetails) currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if (u == null) {
            logger.warn("Username {} not found in database ", username);
        }

        Leads leads = leadsDao.findByUser(u);
        Pendaftar pendaftar = pendaftarDao.findByLeads(leads);
        PendaftarDetail pd = pendaftarDetailDao.findByPendaftar(pendaftar);
        PendaftarDetailPasca pdc = pendaftarDetailPascaDao.findByPendaftar(pendaftar);

        if (leads.getJenjang() == Jenjang.S1){
            if (pd != null) {
                model.addAttribute("detail", pd);
            }
        }else if(leads.getJenjang() == Jenjang.S2){
            if (pdc != null) {
                model.addAttribute("detail", pdc);
            }
        }


    }

    @GetMapping("/generateNim/list")
    public void listNim(@RequestParam(required = false)String search, Model m,
                        @PageableDefault(size = 10, sort = "nim", direction = Sort.Direction.ASC)Pageable page){

        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
        if(StringUtils.hasText(search)) {
            Page<PendaftarDetail> ps= pendaftarDetailDao.findByNimNotNullAndPendaftarLeadsNamaContainingIgnoreCaseOrNimContainingIgnoreCaseAndPendaftarTahunAjaranOrderByNimAsc(search,search,ta,page);
            m.addAttribute("search", search);
            m.addAttribute("daftarNim", ps );
        }else {
            m.addAttribute("daftarNim", pendaftarDetailDao.findByNimNotNullAndPendaftarTahunAjaran(ta,page));
        }
    }

    @GetMapping("/generateNim/listS2")
    public void listNimS2(@RequestParam(required = false)String search, Model m,
                        @PageableDefault(size = 10, sort = "nim", direction = Sort.Direction.ASC)Pageable page){

        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
        if(StringUtils.hasText(search)) {
            Page<PendaftarDetailPasca> ps= pendaftarDetailPascaDao.findByNimNotNullAndPendaftarLeadsNamaContainingIgnoreCaseOrNimContainingIgnoreCaseAndPendaftarTahunAjaranOrderByNimAsc(search,search,ta,page);
            m.addAttribute("search", search);
            m.addAttribute("daftarNim", ps );
        }else {
            m.addAttribute("daftarNim", pendaftarDetailPascaDao.findByNimNotNullAndPendaftarTahunAjaran(ta,page));
        }
    }

    @GetMapping("/generateNim/xlsx")
    public void rekapHumasXlsx(HttpServletResponse response) throws Exception {

        String[] columns = {"no","nim","nama","email","no telephone","jenis kelamin","agama","golongan darah","provinsi & kota asal sekolah","nama asal sekolah","tahun lulus","rekomendasi","nama perekomendaasi","program studi",
        "jenjang","konsentrasi","jalur test","grade","nominal DU","tanggal","keterangan","TTL","NISN","Status sipil", "NO KTP", "provinsi","kabupaten/kota", "alamat",
        "rencana biaya","nama ayah","agama ayah","pendidikan ayah","pekerjaan ayah","status", "nama ibu","agama ibu","pendidikan ibu","pekerjaan ibu","status",
        "alamat orang tua", "jumlah sodara","penghasian"};

        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
        List<PendaftarDetail> dataPendaftar = pendaftarDetailDao.findByNimNotNullAndPendaftarTahunAjaran(ta);
        List<PendaftarOrangtua> dataOrangtua = pendaftarOrangTuaDao.findByPendaftarTahunAjaran(ta);
        Iterable<HasilTest> hasilTests = hasilTestDao.findAll();
        JenisBiaya jb = new JenisBiaya();
        jb.setId("002");
        Iterable<Pembayaran> pembayarans = pembayaranDao.findByTagihanJenisBiaya(jb);

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("DetailNimPendaftar");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1 ;
        int baris = 1 ;

        for (PendaftarDetail p : dataPendaftar) {
            if (p.getNim() != null) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(baris++);
                row.createCell(1).setCellValue(p.getNim());
                row.createCell(2).setCellValue(WordUtils.capitalize(p.getPendaftar().getLeads().getNama()));
                row.createCell(3).setCellValue(p.getPendaftar().getLeads().getEmail());
                row.createCell(4).setCellValue(p.getPendaftar().getLeads().getNoHp());
                row.createCell(5).setCellValue(p.getJenisKelamin());
                row.createCell(6).setCellValue(p.getAgama());
                row.createCell(7).setCellValue(p.getGolonganDarah());
                row.createCell(8).setCellValue(p.getPendaftar().getIdKabupatenKota().getProvinsi().getNama()+" - "+p.getPendaftar().getIdKabupatenKota().getNama());
                row.createCell(9).setCellValue(p.getPendaftar().getNamaAsalSekolah());
                row.createCell(10).setCellValue(p.getTahunLulus());
                row.createCell(11).setCellValue(p.getPendaftar().getPerekomendasi());
                row.createCell(12).setCellValue(p.getPendaftar().getNamaPerekomendasi());
                row.createCell(13).setCellValue(p.getPendaftar().getProgramStudi().getNama());
                row.createCell(14).setCellValue(p.getPendaftar().getLeads().getJenjang().toString());
                row.createCell(15).setCellValue(p.getPendaftar().getKonsentrasi());

                for (HasilTest ht : hasilTests) {
                    if (ht.getJadwalTest().getPendaftar() == p.getPendaftar()) {
                        row.createCell(16).setCellValue(ht.getJadwalTest().getJenisTest().toString());
                        row.createCell(17).setCellValue(ht.getGrade().getNama());
                    }
                    for (Pembayaran pb : pembayarans) {
                        if (pb.getTagihan().getPendaftar() == p.getPendaftar()) {
                            row.createCell(18).setCellValue(pb.getNilai().toString());
                            row.createCell(19).setCellValue(pb.getWaktuPembayaran().toString());
                            row.createCell(20).setCellValue(p.getPendaftar().getKeteranganTagihan());
                        }
                    }
                }
                String cekTempat = p.getTempatLahir();
                int stringSize = cekTempat.length();
                if (stringSize < 5) {
                    KabupatenKota kabupatenKota = new KabupatenKota();
                    kabupatenKota.setId(cekTempat);
                    KabupatenKota tempatLahir = kabupatenKotaDao.findById(kabupatenKota.getId()).get();
                    if (tempatLahir != null) {
                        row.createCell(21).setCellValue(tempatLahir.getNama() + ", " + p.getTanggalLahir().format(DateTimeFormatter.ofPattern(("dd-MM-yyyy"))));
                    }
                }else {
                    row.createCell(21).setCellValue(p.getTempatLahir() + ", " + p.getTanggalLahir().format(DateTimeFormatter.ofPattern(("dd-MM-yyyy"))));
                }
                row.createCell(22).setCellValue(p.getNisn());
                row.createCell(23).setCellValue(p.getStatusSipil());
                row.createCell(24).setCellValue(p.getNoKtp());
                row.createCell(25).setCellValue(p.getProvinsi().getNama());
                row.createCell(26).setCellValue(p.getKokab().getNama());
                row.createCell(27).setCellValue(p.getAlamatRumah());
                row.createCell(28).setCellValue(p.getRencanaBiaya());

                for(PendaftarOrangtua po : dataOrangtua) {
                    if (po.getPendaftar() == p.getPendaftar()) {
                        row.createCell(29).setCellValue(po.getNamaAyah());
                        row.createCell(30).setCellValue(po.getAgamaAyah());
                        row.createCell(31).setCellValue(po.getPendidikanAyah().getNama());
                        row.createCell(32).setCellValue(po.getPekerjaanAyah().getNama());
                        row.createCell(33).setCellValue(po.getStatusAyah());
                        row.createCell(34).setCellValue(po.getNamaIbu());
                        row.createCell(35).setCellValue(po.getAgamaIbu());
                        row.createCell(36).setCellValue(po.getPendidikanIbu().getNama());
                        row.createCell(37).setCellValue(po.getPekerjaanIbu().getNama());
                        row.createCell(38).setCellValue(po.getStatusIbu());
                        row.createCell(49).setCellValue(po.getAlamat());
                        row.createCell(40).setCellValue(po.getJumlahTanggungan());
                        row.createCell(41).setCellValue(po.getPenghasilanOrangtua().getNama());

                    }
                }


            }

        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=Detail_Nim_Pendaftar.xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();

    }


    @GetMapping("/generateNimS2/xlsxHms")
    public void rekapHumasXlsxS2(HttpServletResponse response) throws Exception {

        String[] columns = {"no","nim","nama","email","no telephone","jenis kelamin","agama","golongan darah","rekomendasi","nama perekomendaasi","program studi",
                "jenjang","konsentrasi","TTL","Status sipil", "NO KTP", "provinsi","kabupaten/kota", "alamat","asal pendidikan","pekerjaan","alamat pekerjaan",
                "sumber biaya","nama ayah","nama ibu"};

        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
        Iterable<PendaftarDetailPasca> dataPendaftar = pendaftarDetailPascaDao.findByNimNotNullAndPendaftarTahunAjaran(ta);

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("DetailNimPendaftar");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1 ;
        int baris = 1 ;

        for (PendaftarDetailPasca p : dataPendaftar) {
            if (p.getNim() != null) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(baris++);
                row.createCell(1).setCellValue(p.getNim());
                row.createCell(2).setCellValue(WordUtils.capitalize(p.getPendaftar().getLeads().getNama()));
                row.createCell(3).setCellValue(p.getPendaftar().getLeads().getEmail());
                row.createCell(4).setCellValue(p.getPendaftar().getLeads().getNoHp());
                row.createCell(5).setCellValue(p.getJenisKelamin());
                row.createCell(6).setCellValue(p.getAgama());
                row.createCell(7).setCellValue(p.getGolonganDarah());
                row.createCell(8).setCellValue(p.getPendaftar().getPerekomendasi());
                row.createCell(9).setCellValue(p.getPendaftar().getNamaPerekomendasi());
                row.createCell(10).setCellValue(p.getPendaftar().getProgramStudi().getNama());
                row.createCell(11).setCellValue(p.getPendaftar().getLeads().getJenjang().toString());
                row.createCell(12).setCellValue(p.getPendaftar().getKonsentrasi());

                row.createCell(13).setCellValue(p.getTempatLahir()+ ", " + p.getTanggalLahir().format(DateTimeFormatter.ofPattern(("dd-MM-yyyy"))));
                row.createCell(14).setCellValue(p.getStatusSipil());
                row.createCell(15).setCellValue(p.getNoKtp());
                row.createCell(16).setCellValue(p.getProvinsi().getNama());
                row.createCell(17).setCellValue(p.getKokab().getNama());
                row.createCell(18).setCellValue(p.getAlamatRumah());
                row.createCell(19).setCellValue(p.getAsalPendidikan());
                row.createCell(20).setCellValue(p.getPekerjaan());
                row.createCell(21).setCellValue(p.getAlamatPekerjaan());
                row.createCell(22).setCellValue(p.getSumberBiaya());
                row.createCell(23).setCellValue(p.getNamaAyah());
                row.createCell(24).setCellValue(p.getNamaIbu());

            }

        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=Detail_Nim_Pasca.xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();

    }

    @GetMapping("/generateNim/xlsxIt")
    public void rekapHumasXlsxIt(HttpServletResponse response) throws Exception {

        String[] columns = {"no","id","angkatan","id_prodi","id_konsentrasi","nim","nama","status_matrik","id_program","jenis kelamin","id_agama","tampat lahir","tgl lahir","idKel","idKec",
                "idKab","idProv","idNegara","kewarganegaraan","NIK","NISN","namaJalan","rt","rw","dusun", "kodePOs", "jenisTinggal","alatTransport", "teleponRumah",
                "teleponSeluler","emailPribadi","emailTazkia","statusAktif","status","id_ayah","id_ibu","Tanggal Update"};

        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
        Iterable<PendaftarDetail> dataPendaftar = pendaftarDetailDao.findByNimNotNullAndPendaftarTahunAjaran(ta);
        Iterable<PendaftarOrangtua> dataOrangtua = pendaftarOrangTuaDao.findByPendaftarTahunAjaran(ta);

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("DetailNimPendaftar");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1 ;
        int baris = 1 ;

        for (PendaftarDetail p : dataPendaftar) {
            if (p.getNim() != null) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(baris++);
                row.createCell(1).setCellValue(p.getNim());
                row.createCell(2).setCellValue("2020");
                row.createCell(3).setCellValue(p.getPendaftar().getProgramStudi().getKodeSimak());
                row.createCell(4).setCellValue("");
                row.createCell(5).setCellValue(p.getNim());
                row.createCell(6).setCellValue(WordUtils.capitalize(p.getPendaftar().getLeads().getNama()));
                row.createCell(7).setCellValue("");
                row.createCell(8).setCellValue("");
                row.createCell(9).setCellValue(p.getJenisKelamin());
                row.createCell(10).setCellValue(p.getAgama());
                row.createCell(11).setCellValue(p.getTempatLahir());
                row.createCell(12).setCellValue(p.getTanggalLahir().toString());
                row.createCell(13).setCellValue("");
                row.createCell(14).setCellValue("");
                row.createCell(15).setCellValue(p.getKokab().getNama());
                row.createCell(16).setCellValue(p.getKokab().getProvinsi().getNama());
                row.createCell(17).setCellValue("");
                row.createCell(18).setCellValue(p.getStatusSipil());
                row.createCell(19).setCellValue(p.getNoKtp());
                row.createCell(20).setCellValue(p.getNisn());
                row.createCell(21).setCellValue(p.getAlamatRumah());
                row.createCell(22).setCellValue("");
                row.createCell(23).setCellValue("");
                row.createCell(24).setCellValue("");
                row.createCell(25).setCellValue(p.getKodePos());
                row.createCell(26).setCellValue("");
                row.createCell(27).setCellValue("");

                for(PendaftarOrangtua po : dataOrangtua) {
                    if (po.getPendaftar() == p.getPendaftar()) {
                        row.createCell(28).setCellValue(po.getNoOrangtua());
                        row.createCell(29).setCellValue(p.getPendaftar().getLeads().getNoHp());
                        row.createCell(30).setCellValue(p.getPendaftar().getLeads().getEmail());
                        row.createCell(31).setCellValue("");
                        row.createCell(32).setCellValue("");
                        row.createCell(33).setCellValue("");
                        row.createCell(34).setCellValue(po.getNamaAyah());
                        row.createCell(35).setCellValue(po.getNamaIbu());
                        row.createCell(35).setCellValue(p.getUpdateTime().toString());

                    }
                }


            }

        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=Detail_Nim_Smile.xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();

    }

    @GetMapping("/api/mahasiswanim")
    @ResponseBody
    public NimMahasiswaSmileDto nimMahasiswaSmileDtos(@RequestParam String nim){
        TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);

        NimMahasiswaSmileDto nms = new NimMahasiswaSmileDto();

        PendaftarDetail pd = pendaftarDetailDao.findByNim(nim);
        PendaftarDetailPasca pdp = pendaftarDetailPascaDao.findByNim(nim);

        JenisBiaya jenisBiaya = new JenisBiaya();
        jenisBiaya.setId("002");

//Cek Pendaftar S1
        if (pd != null && pdp == null ) {
            Iterable<PendaftarDetail> dataPendaftar = pendaftarDetailDao.findByNimNotNullAndPendaftarTahunAjaranAndNim(ta, nim);
            Iterable<PendaftarOrangtua> dataOrangtua = pendaftarOrangTuaDao.findByPendaftarTahunAjaranAndPendaftar(ta, pd.getPendaftar());


            Iterable<Cicilan> cicilans = cicilanDao.cekCicilanTagihan(pd.getPendaftar().getId());

            for (PendaftarDetail p : dataPendaftar) {
                for (PendaftarOrangtua po : dataOrangtua) {

                    nms.setNim(p.getNim());
                    nms.setNama(WordUtils.capitalize(p.getPendaftar().getLeads().getNama()));
                    nms.setAngkatan(p.getPendaftar().getTahunAjaran().getNama().substring(0, 4));
                    nms.setProdi(p.getPendaftar().getProgramStudi().getKodeSimak());
                    nms.setJenisKelamin(p.getJenisKelamin());
                    nms.setTempatLahir(p.getTempatLahir());
                    nms.setTanggalLahir(p.getTanggalLahir());
                    nms.setKabupaten(p.getKokab().getNama());
                    nms.setProvinsi(p.getProvinsi().getNama());
                    nms.setNegara(p.getNegara());
                    nms.setKewarganegaraan(p.getStatusSipil());
                    nms.setNik(p.getNoKtp());
                    nms.setNisn(p.getNisn());
                    nms.setAlamat(p.getAlamatRumah());
                    nms.setIdAgama("1");
                    nms.setKewarganegaraan(p.getStatusSipil());

//                    nms.setTeleponRumah(po.getNoOrangtua());
                    nms.setTelepon(p.getPendaftar().getLeads().getNoHp());
                    nms.setEmail(p.getPendaftar().getLeads().getEmail());
                    nms.setAyah(WordUtils.capitalize(po.getNamaAyah()));
                    nms.setIbu(WordUtils.capitalize(po.getNamaIbu()));
                    nms.setJenjang(p.getPendaftar().getLeads().getJenjang().toString());
                    nms.setProgram(p.getPendaftar().getKonsentrasi());
                    nms.setJenjang(p.getPendaftar().getLeads().getJenjang().toString());

                    ArrayList<MigrasiCicilanDto> mccd = new ArrayList<MigrasiCicilanDto>();

                    for (Cicilan cicilan : cicilans){
                        Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiayaAndLunas(pd.getPendaftar(), jenisBiaya, false);
                        MigrasiCicilanDto mcd = new MigrasiCicilanDto();
                        if (tagihan != null && tagihan.getPendaftar() == cicilan.getPendaftar()
                                && cicilan.getStatus() == true) {
                            mcd.setUrutan(cicilan.getUrutanCicilan());
                            mcd.setKeterangan(cicilan.getKeterangan());
                            mcd.setNominal(cicilan.getNominal());
                            mcd.setTanggalKirim(cicilan.getTanggalKirim());
                            mcd.setStatus(cicilan.getStatus());
                            mcd.setNomorTagihan(tagihan.getNomorTagihan());
                        } else {
                            if (cicilan.getStatus() == false) {
                                mcd.setUrutan(cicilan.getUrutanCicilan());
                                mcd.setKeterangan(cicilan.getKeterangan());
                                mcd.setNominal(cicilan.getNominal());
                                mcd.setTanggalKirim(cicilan.getTanggalKirim());
                                mcd.setStatus(cicilan.getStatus());
                                mcd.setNomorTagihan(null);
                            }else{
                                mcd = null;
                            }
                        }
                        if (mcd != null) {
                            mccd.add(mcd);
                            nms.setCicilan(mccd);
                        }
                    }

                }
            }
// Cek Pendaftar S2
        } else if (pdp != null && pd == null ) {
            Iterable<PendaftarDetailPasca> dataPendaftarS2 = pendaftarDetailPascaDao.findByNimNotNullAndPendaftarTahunAjaranAndNim(ta, nim);

            Iterable<Cicilan> cicilans = cicilanDao.cekCicilanTagihan(pdp.getPendaftar().getId());

            for (PendaftarDetailPasca p : dataPendaftarS2) {

                nms.setNim(p.getNim());
                nms.setNama(WordUtils.capitalize(p.getPendaftar().getLeads().getNama()));
                nms.setAngkatan("19");
                nms.setProdi(p.getPendaftar().getProgramStudi().getKodeSimak());
                nms.setJenisKelamin(p.getJenisKelamin());
                nms.setTempatLahir(p.getTempatLahir());
                nms.setTanggalLahir(p.getTanggalLahir());
                nms.setKabupaten(p.getKokab().getNama());
                nms.setProvinsi(p.getProvinsi().getNama());
                nms.setNegara(p.getNegara());
                nms.setKewarganegaraan(p.getStatusSipil());
                nms.setNik(p.getNoKtp());
                nms.setIdAgama("1");
                nms.setKewarganegaraan(p.getStatusSipil());

                nms.setAlamat(p.getAlamatRumah());
//                nms.setKodepos(p.getKodePos());
//                nms.setNoOrangtua(p.getNoOrangtua());
                nms.setTelepon(p.getPendaftar().getLeads().getNoHp());
                nms.setEmail(p.getPendaftar().getLeads().getEmail());
                nms.setAyah(WordUtils.capitalize(p.getNamaAyah()));
                nms.setIbu(WordUtils.capitalize(p.getNamaIbu()));
                nms.setJenjang(p.getPendaftar().getLeads().getJenjang().toString());
                nms.setProgram(p.getPendaftar().getKonsentrasi());
                nms.setJenjang(p.getPendaftar().getLeads().getJenjang().toString());

                ArrayList<MigrasiCicilanDto> mccd = new ArrayList<MigrasiCicilanDto>();

                for (Cicilan cicilan : cicilans){
                    Tagihan tagihan = tagihanDao.findByPendaftarAndJenisBiayaAndLunas(pdp.getPendaftar(), jenisBiaya, false);
                    MigrasiCicilanDto mcd = new MigrasiCicilanDto();
                    if (tagihan != null && tagihan.getPendaftar() == cicilan.getPendaftar()
                            && cicilan.getStatus() == true) {
                        mcd.setUrutan(cicilan.getUrutanCicilan());
                        mcd.setKeterangan(cicilan.getKeterangan());
                        mcd.setNominal(cicilan.getNominal());
                        mcd.setTanggalKirim(cicilan.getTanggalKirim());
                        mcd.setStatus(cicilan.getStatus());
                        mcd.setNomorTagihan(tagihan.getNomorTagihan());
                    } else {
                        if (cicilan.getStatus() == false) {
                            mcd.setUrutan(cicilan.getUrutanCicilan());
                            mcd.setKeterangan(cicilan.getKeterangan());
                            mcd.setNominal(cicilan.getNominal());
                            mcd.setTanggalKirim(cicilan.getTanggalKirim());
                            mcd.setStatus(cicilan.getStatus());
                            mcd.setNomorTagihan(null);
                        }else{
                            mcd = null;
                        }
                    }
                    if (mcd != null) {
                        mccd.add(mcd);
                        nms.setCicilan(mccd);
                    }
                }

            }
        }

      return nms;
    }


}

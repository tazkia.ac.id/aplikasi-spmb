package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.dao.GradeDao;
import id.ac.tazkia.spmb.aplikasispmb.entity.Grade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.validation.Valid;

@Controller
public class GradeController {
    @Autowired
    private GradeDao gradeDao;

//list
    @RequestMapping("/grade/list")
    public void daftarGrade(@RequestParam(required = false)String nama, Model m,
                            @PageableDefault(sort = "nama", direction = Sort.Direction.DESC) Pageable page){
        if(StringUtils.hasText(nama)) {
            m.addAttribute("nama", nama);
            m.addAttribute("daftarGrade", gradeDao.findByNamaContainingIgnoreCaseOrderByNamaDesc(nama, page));
        } else {
            m.addAttribute("daftarGrade", gradeDao.findAll(page));
        }
    }
//
//Hapus Data
@RequestMapping("/grade/hapus")
    public  String hapus(@RequestParam("id") Grade id ){
        gradeDao.delete(id);
        return "redirect:list";
    }
//
//tampikan form
    @RequestMapping(value = "/grade/form", method = RequestMethod.GET)
    public String tampilkanForm(@RequestParam(value = "id", required = false) String id,
                                Model m){
        //defaultnya, isi dengan object baru
        m.addAttribute("grade", new Grade());

        if (id != null && !id.isEmpty()){
            Grade p= gradeDao.findById(id).get();
            if (p != null){
                m.addAttribute("grade", p);
            }
        }
        return "grade/form";
    }
////

//simpan
    @RequestMapping(value = "/grade/form", method = RequestMethod.POST)
    public String prosesForm(@Valid Grade p, BindingResult errors){
        if(errors.hasErrors()){
            return "/grade/form";
        }
        gradeDao.save(p);
        return "redirect:list";
    }

////
}

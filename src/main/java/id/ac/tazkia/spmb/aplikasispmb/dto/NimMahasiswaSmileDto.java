package id.ac.tazkia.spmb.aplikasispmb.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Column;
import java.time.LocalDate;
import java.util.ArrayList;

@Data
public class NimMahasiswaSmileDto {

    private String id;
    private String nama;
    private String angkatan;
    private String prodi;
    private String nim;
    private String jenjang;
    private String program;
    private String jenisKelamin;
    private String idAgama;
    private String tempatLahir;
    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalLahir;
    private String kabupaten;
    private String provinsi;
    private String negara;
    private String kewarganegaraan;
    private String nik;
    private String nisn;
    private String alamat;
    private String telepon;
    private String email;
    private String statusAktif = "AKTIF";
    private String user;
    private String ayah;
    private String ibu;
    private String konsentrasi;

    private ArrayList<MigrasiCicilanDto> cicilan;



//Cicilan
//    1. nominal
//    2. Keterangan
//    3. Urutan
//    4. Tanggal kirim

}

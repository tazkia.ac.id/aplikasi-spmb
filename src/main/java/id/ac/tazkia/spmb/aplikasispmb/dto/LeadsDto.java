package id.ac.tazkia.spmb.aplikasispmb.dto;

import id.ac.tazkia.spmb.aplikasispmb.entity.Jenjang;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class LeadsDto {

    private String nama;
    private String email;
    private String noHp;
    private Jenjang jenjang;
    private String kelas;
    private LocalDateTime createDate = LocalDateTime.now();
    private String username;
    private String password;
    private String kode;

}

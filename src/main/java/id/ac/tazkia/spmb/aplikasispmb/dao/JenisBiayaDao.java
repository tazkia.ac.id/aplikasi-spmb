package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.JenisBiaya;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface JenisBiayaDao extends PagingAndSortingRepository<JenisBiaya, String>, CrudRepository<JenisBiaya, String> {
    Page<JenisBiaya> findByNamaContainingIgnoreCaseOrderByNama(String nama, Pageable page);

}

package id.ac.tazkia.spmb.aplikasispmb.service;

import id.ac.tazkia.spmb.aplikasispmb.dao.KabupatenKotaDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.ProgramStudiDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.TahunAjaranDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.UserDao;
import id.ac.tazkia.spmb.aplikasispmb.dto.BaseResponse;
import id.ac.tazkia.spmb.aplikasispmb.dto.LeadsPendaftarDto;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Valid;
import java.util.List;

@RestController
@Slf4j
public class ApiSobatSyariah {

    @Autowired private ProgramStudiDao programStudiDao;
    @Autowired private PendaftarService pendaftarService;
    @Autowired private UserDao userDao;
    @Autowired private KabupatenKotaDao kabupatenKotaDao;
    @Autowired private TahunAjaranDao tahunAjaranDao;

    @GetMapping("/api/program-studi")
    @ResponseBody
    public List<ProgramStudi> listProdi (){
        String id ="009";
        String tidak = "005";
        String tidak2 = "000";

        return programStudiDao.cariProdiS1(id,tidak, tidak2);
    }

    @GetMapping("/api/kabupaten")
    @ResponseBody
    public  Iterable<KabupatenKota> listKabupaten() {
        return kabupatenKotaDao.findAll();
    }

    @PostMapping(value = "/api/input-leads", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public BaseResponse apiLeads (@Valid @RequestBody LeadsPendaftarDto lpDto ){
        TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);
        User cekLeads = userDao.findUsername(tahunAjaran.getId(), lpDto.getEmail());

        if (cekLeads != null) {
            return new  BaseResponse(HttpStatus.ALREADY_REPORTED.getReasonPhrase(),
                    String.valueOf(HttpStatus.ALREADY_REPORTED.value()));
        }else {
            try {
                pendaftarService.prosesLeadsPendaftar(lpDto);
                return new BaseResponse(HttpStatus.CREATED.getReasonPhrase(),
                        String.valueOf(HttpStatus.CREATED.value()));
            }catch (ConstraintViolationException err){
                err.printStackTrace();
                log.info("ConstraintViolationException : {} ", err.getMessage());
                throw err;
            }

        }
    }
}

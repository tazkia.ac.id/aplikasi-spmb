package id.ac.tazkia.spmb.aplikasispmb.entity;


import groovyjarjarantlr4.v4.runtime.misc.NotNull;
import id.ac.tazkia.spmb.aplikasispmb.constants.RecordStatus;
import id.ac.tazkia.spmb.aplikasispmb.constants.StatusRecord;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;


import java.time.LocalDateTime;


@Entity
@Table(name = "table_user")
@Data
public class TableUser {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")    private String id;
    private String username;

    @Enumerated(EnumType.STRING)
    private RecordStatus active;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_role")
    private TableRole role;

    private LocalDateTime waktuMulai;

    private LocalDateTime waktuSelesai;

    private Integer reading;
    private Integer konversiReading;
    private Integer structure;
    private Integer konversiStructure;
    private Integer total;
    private Integer nilaiTotal;

    @Enumerated(EnumType.STRING)
    private StatusRecord status;

}

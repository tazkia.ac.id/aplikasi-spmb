package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.constants.AppConstants;
import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.dto.DetailPendaftarOrangTuaDto;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import jakarta.servlet.http.HttpServletResponse;
import java.io.File;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.UUID;

@Controller
public class DetailPendaftarController {
    private static final Logger logger = LoggerFactory.getLogger(DetailPendaftarController.class);

    @Value("${upload.folder}")
    private String uploadFolder;
    @Autowired private PekerjaanDao pekerjaanDao;
    @Autowired private PenghasilanDao penghasilanDao;
    @Autowired private PendidikanDao pendidikanDao;
    @Autowired private UserDao userDao;
    @Autowired private LeadsDao leadsDao;
    @Autowired private PendaftarDao pendaftarDao;
    @Autowired private PendaftarDetailDao pendaftarDetailDao;
    @Autowired private PendaftarDetailPascaDao pendaftarDetailPascaDao;
    @Autowired private PendaftarOrangTuaDao pendaftarOrangTuaDao;
    @Autowired private PembayaranController pembayaranController;
    @Autowired private PembayaranDao pembayaranDao;
    @Autowired private HasilTestDao hasilTestDao;
    @Autowired private TahunAjaranDao tahunAjaranDao;
    @Autowired private KabupatenKotaDao kabupatenKotaDao;
    @Autowired private ProvinsiDao provinsiDao;
    @Autowired private TagihanDao tagihanDao;
    @Autowired private UploadBerkasDao uploadBerkasDao;

    @ModelAttribute("pendidikanOrtu")
    public Iterable<Pendidikan> pendidikanOrtu(){return pendidikanDao.findAll();}

    @ModelAttribute("pekerjaanOrtu")
    public Iterable<Pekerjaan> pekerjaanOrtu(){return pekerjaanDao.findAll();}

    @ModelAttribute("penghasilanOrtu")
    public Iterable<Penghasilan> penghasilanOrtu(){return penghasilanDao.findAll();}

    @ModelAttribute("daftarKokab")
    public Iterable<KabupatenKota> daftarKokab(){
        return kabupatenKotaDao.findAll();
    }

    @ModelAttribute("daftarProvinsi")
    public Iterable<Provinsi> daftarProvinsi(){
        return provinsiDao.findAll();
    }


    @GetMapping("/detailPendaftar/form")
    public String formDetailPendaftar(Model model,
                                      Authentication currentUser, Pageable page){
        logger.debug("Authentication class : {}",currentUser.getClass().getName());

        if(currentUser == null){
            logger.warn("Current user is null");
        }

        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }

        Leads leads = leadsDao.findByUser(u);
        logger.info("Pendaftar a.n {} ditemukan", leads.getNama());
        Pendaftar pendaftar = pendaftarDao.findByLeads(leads);
        if (pendaftar != null) {
            model.addAttribute("pendaftar", pendaftar);
        }

        DetailPendaftarOrangTuaDto detailDto = new DetailPendaftarOrangTuaDto();

        PendaftarDetail pendaftarDetail = pendaftarDetailDao.findByPendaftar(pendaftar);
        PendaftarOrangtua pendaftarOrangtua = pendaftarOrangTuaDao.findByPendaftar(pendaftar);
        if (pendaftarDetail != null && pendaftarOrangtua != null) {
            detailDto.setId(pendaftarDetail.getId());
            detailDto.setPendaftar(pendaftar);
            detailDto.setFotoPe(pendaftarDetail.getFoto());
            detailDto.setTempatLahir(pendaftarDetail.getTempatLahir());
            detailDto.setTanggalLahir(pendaftarDetail.getTanggalLahir());
            detailDto.setAgama(pendaftarDetail.getAgama());
            detailDto.setJenisKelamin(pendaftarDetail.getJenisKelamin());
            detailDto.setGolonganDarah(pendaftarDetail.getGolonganDarah());
            detailDto.setNoKtp(pendaftarDetail.getNoKtp());
            detailDto.setAlamatRumah(pendaftarDetail.getAlamatRumah());
            detailDto.setNegara(pendaftarDetail.getNegara());
            detailDto.setProvinsi(pendaftarDetail.getProvinsi());
            detailDto.setKokab(pendaftarDetail.getKokab());
            detailDto.setKodePos(pendaftarDetail.getKodePos());
            detailDto.setNisn(pendaftarDetail.getNisn());
            detailDto.setJurusanSekolah(pendaftarDetail.getJurusanSekolah());
            detailDto.setTahunLulus(pendaftarDetail.getTahunLulus());
            detailDto.setStatusSipil(pendaftarDetail.getStatusSipil());
            detailDto.setRencanaBiaya(pendaftarDetail.getRencanaBiaya());
            detailDto.setNim(pendaftarDetail.getNim());

            detailDto.setOrangTua(pendaftarOrangtua.getId());
            detailDto.setNamaAyah(pendaftarOrangtua.getNamaAyah());
            detailDto.setAgamaAyah(pendaftarOrangtua.getAgamaAyah());
            detailDto.setPekerjaanAyah(pendaftarOrangtua.getPekerjaanAyah());
            detailDto.setPendidikanAyah(pendaftarOrangtua.getPendidikanAyah());
            detailDto.setStatusAyah(pendaftarOrangtua.getStatusAyah());

            detailDto.setNamaIbu(pendaftarOrangtua.getNamaIbu());
            detailDto.setAgamaIbu(pendaftarOrangtua.getAgamaIbu());
            detailDto.setPekerjaanIbu(pendaftarOrangtua.getPekerjaanIbu());
            detailDto.setPendidikanIbu(pendaftarOrangtua.getPendidikanIbu());
            detailDto.setStatusIbu(pendaftarOrangtua.getStatusIbu());

            detailDto.setAlamatOrangtua(pendaftarOrangtua.getAlamat());
            detailDto.setNoOrangtua(pendaftarOrangtua.getNoOrangtua());
            detailDto.setPenghasilanOrangtua((pendaftarOrangtua.getPenghasilanOrangtua()));
            detailDto.setJumlahTanggungan(pendaftarOrangtua.getJumlahTanggungan());
            model.addAttribute("detailDto", detailDto);
        } else {
            logger.warn("Data detail & data orangtua dengan pendaftar {} tidak ada dalam database", pendaftar.getLeads().getNama());
        }
        detailDto.setLeads(pendaftar.getLeads());
        detailDto.setNama(pendaftar.getLeads().getNama());
        detailDto.setEmail(pendaftar.getLeads().getEmail());
        detailDto.setNoHp(pendaftar.getLeads().getNoHp());
        model.addAttribute("detailDto", detailDto);

//Cek Pembayaran
        Pembayaran pembayaran = pembayaranController.cekPembayaranDU(pendaftar);
        model.addAttribute("pembayaran", pembayaran);

        return  "detailPendaftar/form";
    }


    @PostMapping("/detailPendaftar/form")
    public String prosesFromDetailPendaftar(DetailPendaftarOrangTuaDto detailDto, Model model, BindingResult errors,
                                            Authentication currentUser, MultipartFile foto) throws Exception {
        logger.debug("Authentication class : {}",currentUser.getClass().getName());

        if(currentUser == null){
            logger.warn("Current user is null");
        }

        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }

        Leads leads = leadsDao.findByUser(u);

        Leads sLeads = leadsDao.findById(detailDto.getLeads().getId()).get();
        sLeads.setNama(detailDto.getNama());
        sLeads.setEmail(detailDto.getEmail());
        sLeads.setNoHp(detailDto.getNoHp());
        leadsDao.save(sLeads);

        logger.info("Pendaftar a.n {} ditemukan", leads.getNama());
        Pendaftar pendaftar = pendaftarDao.findByLeads(leads);


        if (detailDto.getId() == null || detailDto.getId().isEmpty()) {
            PendaftarDetail pendaftarDetail = new PendaftarDetail();
            BeanUtils.copyProperties(detailDto, pendaftarDetail);
            pendaftarDetail.setPendaftar(pendaftar);
            pendaftarDetail.setUpdateTime(LocalDate.now());
            pendaftarDetail.setNim(null);
//Upload Foto
            if (errors.hasErrors()) {
                logger.debug("Error upload foto pendaftar : {}", errors.toString());
                return "redirect:/detailPendaftar/form?error=Invalid&id=" + pendaftar.getNomorRegistrasi();
            }

                String idPeserta = pendaftar.getId();
                String namaFile = foto.getName();
                String jenisFile = foto.getContentType();
                String namaAsli = foto.getOriginalFilename();
                Long ukuran = foto.getSize();

                logger.debug("Nama File : {}", namaFile);
                logger.debug("Jenis File : {}", jenisFile);
                logger.debug("Nama Asli File : {}", namaAsli);
                logger.debug("Ukuran File : {}", ukuran);

                String extension = "";

                int i = namaAsli.lastIndexOf('.');
                int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

                if (i > p) {
                    extension = namaAsli.substring(i + 1);
                }

                String idFile = UUID.randomUUID().toString();
                String lokasiUpload = uploadFolder + File.separator + idPeserta;
                logger.debug("Lokasi upload : {}", lokasiUpload);
                new File(lokasiUpload).mkdirs();
                File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
                foto.transferTo(tujuan);
                logger.debug("File sudah dicopy ke : {}", tujuan.getAbsolutePath());
                pendaftarDetail.setFoto(idFile + "." + extension);

            //
            pendaftarDetail.setId(detailDto.getId());
            pendaftarDetailDao.save(pendaftarDetail);
            logger.info("Detail Pendaftar a.n {} berhasil di simpan", pendaftarDetail.getPendaftar().getLeads().getNama());

///////
            PendaftarOrangtua pendaftarOrangtua = new PendaftarOrangtua();
            BeanUtils.copyProperties(detailDto, pendaftarOrangtua);
            pendaftarOrangtua.setPendaftar(pendaftar);
            pendaftarOrangtua.setAlamat(detailDto.getAlamatOrangtua());
            pendaftarOrangTuaDao.save(pendaftarOrangtua);
            logger.info("Data orangtua dengan pendaftar a.n {} berhasil di simpan", pendaftarDetail.getPendaftar().getLeads().getNama());

        }else{

            if (detailDto.getId() != null || !detailDto.getId().isEmpty()) {
                PendaftarDetail pd = pendaftarDetailDao.findById(detailDto.getId()).get();
                BeanUtils.copyProperties(detailDto, pd);
                pd.setId(detailDto.getId());
                pd.setPendaftar(pendaftar);
                pd.setFoto(detailDto.getFotoPe());
                pd.setUpdateTime(LocalDate.now());
                pd.setNim(null);
                pendaftarDetailDao.save(pd);
            }

            if (detailDto.getOrangTua() != null || !detailDto.getOrangTua().isEmpty()) {
                PendaftarOrangtua pot = pendaftarOrangTuaDao.findById(detailDto.getOrangTua()).get();
                BeanUtils.copyProperties(detailDto, pot);
                pot.setId(detailDto.getOrangTua());
                pot.setPendaftar(pendaftar);
                pot.setAlamat(detailDto.getAlamatOrangtua());
                pendaftarOrangTuaDao.save(pot);
                logger.info("Data Berhasil Di update");
            }

        }

        return "redirect:/jadwalTest/form";
    }

    @GetMapping("/detailPendaftar/showDetail")
    public void showDetail(@RequestParam(required = false)Pendaftar  id, Model m, Pageable pa){
        PendaftarDetail pd = pendaftarDetailDao.findByPendaftar(id);
        PendaftarOrangtua pdo = pendaftarOrangTuaDao.findByPendaftar(id);
        if (pd != null) {
            m.addAttribute("dPendaftar", pd);
            logger.info("Detail pendaftar tersedia didatabase");
        }else{
            logger.debug("Detail {} Tidak ada didatabase",id.getNomorRegistrasi() );
        }

        if (pdo != null) {
            m.addAttribute("dOrangtua", pdo);
            logger.info("Orangtua pendaftar tersedia didatabase");
        }
    }

    @GetMapping("/detail/xlsx")
    public void reportDetail(HttpServletResponse response) throws  Exception{
        String[] columns = {"No","Nomor Registrasi","Nama","Tempat, Tanggal Lahir","Jenis Kelamin","Email","No Telephone","NISN",
                "Status Sipil","NIK","Asal Sekolah","Tahun Lulus","Program Studi","Konsentrasi","Provinsi","Kabupaten/Kota","Alamat","Recana Biaya",
                "Nama Ayah","Agama Ayah","Pendidikan Ayah","Pekerjaan Ayah","Status",
                "Nama Ibu","Agama Ibu", "Pendidikan Ibu","Pekerjaan Ibu","Status","Alamat Orangtua",
                "Jumlah Sodara", "Penghasilan", "Lulusan",
                "Rekomendasi","Nama Perekomendasi", "Program Studi", "Kelas", "Alasan Memilih Prodi","Tanggal Daftar",
                "Nominal Registrasi", "Test", "Nominal Daftar Ulang", "Tanggal DU", "Status", "Catatan", "Catatan PIC", "User FU", "User Update", "Tanggal Update",
                "Sumber", "Kode Voucer","Beasiswa", "Note", "Lulusan","Bekras", "Data Detail"};


        Iterable<PendaftarDetail> dataPendaftar = pendaftarDetailDao.findByPendaftarTahunAjaranAktif(Status.AKTIF);
        Iterable<PendaftarOrangtua> dataPendaftarOrtu = pendaftarOrangTuaDao.findByPendaftarTahunAjaranAktif(Status.AKTIF);

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Pendaftar - "+LocalDate.now());

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1 ;
        int baris = 1 ;
        for (PendaftarDetail p : dataPendaftar) {
            for (PendaftarOrangtua pdo : dataPendaftarOrtu) {
                if (pdo.getPendaftar() == p.getPendaftar() && pdo != null) {
                    Row row = sheet.createRow(rowNum++);
                    row.createCell(0).setCellValue(baris++);
                    row.createCell(1).setCellValue(p.getPendaftar().getNomorRegistrasi());
                    row.createCell(2).setCellValue(WordUtils.capitalize(p.getPendaftar().getLeads().getNama()));
                    row.createCell(3).setCellValue(p.getTempatLahir()+", "+ p.getTanggalLahir().format(DateTimeFormatter.ofPattern(("dd-MM-yyyy"))));
                    row.createCell(4).setCellValue(p.getJenisKelamin());
                    row.createCell(5).setCellValue(p.getPendaftar().getLeads().getEmail());
                    row.createCell(6).setCellValue(p.getPendaftar().getLeads().getNoHp());
                    row.createCell(7).setCellValue(p.getNisn());
                    row.createCell(8).setCellValue(p.getStatusSipil());
                    row.createCell(9).setCellValue(p.getNoKtp());
                    row.createCell(10).setCellValue(p.getPendaftar().getNamaAsalSekolah());
                    row.createCell(11).setCellValue(p.getTahunLulus());
                    row.createCell(12).setCellValue(p.getPendaftar().getProgramStudi().getNama());
                    row.createCell(13).setCellValue(p.getPendaftar().getKonsentrasi());
                    row.createCell(14).setCellValue(p.getProvinsi().getNama());
                    row.createCell(15).setCellValue(p.getKokab().getNama());
                    row.createCell(16).setCellValue(p.getAlamatRumah());
                    row.createCell(17).setCellValue(p.getRencanaBiaya());
                    row.createCell(18).setCellValue(pdo.getNamaAyah());
                    row.createCell(19).setCellValue(pdo.getAgamaAyah());
                    row.createCell(20).setCellValue(pdo.getPendidikanAyah().getNama());
                    row.createCell(21).setCellValue(pdo.getPekerjaanAyah().getNama());
                    row.createCell(22).setCellValue(pdo.getStatusAyah());
                    row.createCell(23).setCellValue(pdo.getNamaIbu());
                    row.createCell(24).setCellValue(pdo.getAgamaIbu());
                    row.createCell(25).setCellValue(pdo.getPendidikanIbu().getNama());
                    row.createCell(26).setCellValue(pdo.getPekerjaanIbu().getNama());
                    row.createCell(27).setCellValue(pdo.getStatusIbu());
                    row.createCell(28).setCellValue(pdo.getAlamat());
                    row.createCell(29).setCellValue(pdo.getJumlahTanggungan());
                    row.createCell(30).setCellValue(pdo.getPenghasilanOrangtua().getNama());
                    if (pdo.getPendaftar().getLeads().getSubscribe() != null) {
                        row.createCell(31).setCellValue(pdo.getPendaftar().getLeads().getSubscribe().getLulusan());
                    } else {
                        row.createCell(31).setCellValue("-");
                    }
                    row.createCell(32).setCellValue(p.getPendaftar().getPerekomendasi());
                    row.createCell(33).setCellValue(p.getPendaftar().getNamaPerekomendasi());
                    row.createCell(34).setCellValue(p.getPendaftar().getProgramStudi().getNama());
                    row.createCell(35).setCellValue(p.getPendaftar().getKonsentrasi());
                    row.createCell(36).setCellValue(p.getPendaftar().getAlasanMemilihProdi());
                    row.createCell(37).setCellValue(p.getPendaftar().getLeads().getCreateDate().toString());

                    TahunAjaran ta = tahunAjaranDao.findByAktif(Status.AKTIF);
                    JenisBiaya jb = new JenisBiaya();
                    jb.setId(AppConstants.JENIS_BIAYA_PENDAFTARAN);
                    Iterable<Tagihan> cekTagihanLunas = tagihanDao.cekTagihanLunas(ta);
                    for (Tagihan cekRegis : cekTagihanLunas) {
                        if (cekRegis.getPendaftar().getId() == p.getId() && cekRegis.getLunas() == true) {
                            row.createCell(38).setCellValue(cekRegis.getNilai().toString());
                        }
                    }

                    Iterable<HasilTest> hasilTest = hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAktifAndJadwalTestPendaftar(Status.AKTIF,p.getPendaftar());

                    for (HasilTest ht : hasilTest) {
                        if (ht != null) {
                            row.createCell(39).setCellValue("Sudah Test + "+ ht.getGrade().getNama());
                        } else if (ht == null) {
                            row.createCell(39).setCellValue("Belum Test");
                        }
                    }

                    if (p.getPendaftar().getUangPangkal() != null) {
                        row.createCell(40).setCellValue(p.getPendaftar().getUangPangkal().toString());
                    } else {
                        row.createCell(40).setCellValue("-");
                    }

                    Tagihan cekTagihanLunasD = tagihanDao.cekTagihanDu(p.getPendaftar());
                    Pembayaran pembayaran = pembayaranDao.findByTagihan(cekTagihanLunasD);
                    if (pembayaran != null) {
                        row.createCell(41).setCellValue(pembayaran.getWaktuPembayaran().toString());
                    } else {
                        row.createCell(41).setCellValue("-");
                    }

                    row.createCell(42).setCellValue(p.getPendaftar().getStatus());
                    row.createCell(43).setCellValue(p.getPendaftar().getNote());
                    row.createCell(44).setCellValue(p.getPendaftar().getNotePic());
                    row.createCell(45).setCellValue(p.getPendaftar().getFollowup().getUser().getUsername());

                    if (p.getPendaftar().getUserUpdate() != null) {
                        row.createCell(46).setCellValue(p.getPendaftar().getUserUpdate().getUsername());
                        row.createCell(47).setCellValue(p.getPendaftar().getTglUpdate().toString());
                    } else {
                        row.createCell(46).setCellValue("-");
                        row.createCell(46).setCellValue("-");
                    }


                    if (p.getPendaftar().getLeads().getSubscribe() != null) {
                        row.createCell(48).setCellValue(p.getPendaftar().getLeads().getSubscribe().getSumber());
                    } else {
                        row.createCell(48).setCellValue("-");
                    }

                    if (p.getPendaftar().getReferal() != null) {
                        row.createCell(49).setCellValue(p.getPendaftar().getReferal().getKodeReferal());
                    } else {
                        row.createCell(49).setCellValue("-");
                    }


                    row.createCell(50).setCellValue(p.getPendaftar().getKeteranganTagihan());
                    row.createCell(51).setCellValue(p.getPendaftar().getNote());
                    if (pdo.getPendaftar().getLeads().getSubscribe() != null) {
                        row.createCell(52).setCellValue(pdo.getPendaftar().getLeads().getSubscribe().getLulusan());
                    } else {
                        row.createCell(52).setCellValue("-");
                    }

                    Iterable<Berkas> berkas = uploadBerkasDao.findByPendaftar(p.getPendaftar());
                    for (Berkas bkr : berkas) {
                        if (bkr != null) {
                            row.createCell(53).setCellValue("Sudah Ada Berkas");
                        } else {
                            row.createCell(53).setCellValue("Belum Ada Berkas");
                        }
                    }

                    Iterable<PendaftarDetail> pd = pendaftarDetailDao.findByPendaftarAndPendaftarTahunAjaranAktif(p.getPendaftar(),Status.AKTIF);
                    for (PendaftarDetail cekDetail : pd ) {
                        if (cekDetail != null) {
                            row.createCell(54).setCellValue("Sudah Isi Detail");
                        } else {
                            row.createCell(54).setCellValue("Belum Isi Detail");
                        }
                    }




                }
            }
        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }



        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition","attachment; filename=\"Pendaftar Detail "+ LocalDate.now() +".xlsx"+"\"");
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    @GetMapping("/pendaftarDuS1/xlsx")
    public void reportDuS1(HttpServletResponse response) throws  Exception{
        String[] columns = {"No","Nomor Registrasi","Nama","Tempat, Tanggal Lahir","Jenis Kelamin","Email","No Telephone","NISN",
                "Status Sipil","NIK","Asal Sekolah","Tahun Lulus","Program Studi","Konsentrasi","Provinsi","Kabupaten/Kota","Alamat","Recana Biaya",
                "Nama Ayah","Agama Ayah","Pendidikan Ayah","Pekerjaan Ayah","Status",
                "Nama Ibu","Agama Ibu", "Pendidikan Ibu","Pekerjaan Ibu","Status","Alamat Orangtua",
                "Jumlah Sodara", "Penghasilan","Jalur Test", "Grade","Periode", "Keterangan DU", "Nominal Du"};


        Iterable<PendaftarDetail> dataPendaftar = pendaftarDetailDao.findByPendaftarTahunAjaranAktifAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarStatusNot(Status.AKTIF, Jenjang.S1,"Reguler","Close Lose - Mengundurkan Diri");
        Iterable<PendaftarOrangtua> dataPendaftarOrtu = pendaftarOrangTuaDao.findByPendaftarTahunAjaranAktifAndPendaftarLeadsJenjang(Status.AKTIF, Jenjang.S1);

        JenisBiaya jb = new JenisBiaya();
        jb.setId("002");

        TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);
        Iterable<Pembayaran> cPembayaran = pembayaranDao.findByTagihanS1One(tahunAjaran.getId());

        Iterable<HasilTest> cHasilTest  = hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAktif(Status.AKTIF);

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Daftar Ulang Sarjana - "+LocalDate.now());

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1 ;
        int baris = 1 ;
        for (Pembayaran pembayaran : cPembayaran) {
            for (PendaftarDetail p : dataPendaftar) {
                for (PendaftarOrangtua pdo : dataPendaftarOrtu) {
                    if (pdo.getPendaftar() == p.getPendaftar() && pdo != null) {
                        if (pembayaran.getTagihan().getPendaftar() == pdo.getPendaftar() && pembayaran.getTagihan().getPendaftar() == p.getPendaftar()) {
                            Row row = sheet.createRow(rowNum++);
                            row.createCell(0).setCellValue(baris++);
                            row.createCell(1).setCellValue(p.getPendaftar().getNomorRegistrasi());
                            row.createCell(2).setCellValue(WordUtils.capitalize(p.getPendaftar().getLeads().getNama()));
                            row.createCell(3).setCellValue(p.getTempatLahir() + ", " + p.getTanggalLahir().format(DateTimeFormatter.ofPattern(("dd-MM-yyyy"))));
                            row.createCell(4).setCellValue(p.getJenisKelamin());
                            row.createCell(5).setCellValue(p.getPendaftar().getLeads().getEmail());
                            row.createCell(6).setCellValue(p.getPendaftar().getLeads().getNoHp());
                            row.createCell(7).setCellValue(p.getNisn());
                            row.createCell(8).setCellValue(p.getStatusSipil());
                            row.createCell(9).setCellValue(p.getNoKtp());
                            row.createCell(10).setCellValue(p.getPendaftar().getNamaAsalSekolah());
                            row.createCell(11).setCellValue(p.getTahunLulus());
                            row.createCell(12).setCellValue(p.getPendaftar().getProgramStudi().getNama());
                            row.createCell(13).setCellValue(p.getPendaftar().getKonsentrasi());
                            row.createCell(14).setCellValue(p.getProvinsi().getNama());
                            row.createCell(15).setCellValue(p.getKokab().getNama());
                            row.createCell(16).setCellValue(p.getAlamatRumah());
                            row.createCell(17).setCellValue(p.getRencanaBiaya());
                            row.createCell(18).setCellValue(pdo.getNamaAyah());
                            row.createCell(19).setCellValue(pdo.getAgamaAyah());
                            row.createCell(20).setCellValue(pdo.getPendidikanAyah().getNama());
                            row.createCell(21).setCellValue(pdo.getPekerjaanAyah().getNama());
                            row.createCell(22).setCellValue(pdo.getStatusAyah());
                            row.createCell(23).setCellValue(pdo.getNamaIbu());
                            row.createCell(24).setCellValue(pdo.getAgamaIbu());
                            row.createCell(25).setCellValue(pdo.getPendidikanIbu().getNama());
                            row.createCell(26).setCellValue(pdo.getPekerjaanIbu().getNama());
                            row.createCell(27).setCellValue(pdo.getStatusIbu());
                            row.createCell(28).setCellValue(pdo.getAlamat());
                            row.createCell(29).setCellValue(pdo.getJumlahTanggungan());
                            row.createCell(30).setCellValue(pdo.getPenghasilanOrangtua().getNama());
                            for (HasilTest h : cHasilTest) {
                                if (h.getJadwalTest().getPendaftar() == p.getPendaftar()) {
                                    row.createCell(31).setCellValue(h.getJadwalTest().getJenisTest().toString());
                                    row.createCell(32).setCellValue(h.getGrade().getNama());
                                    row.createCell(33).setCellValue(h.getPeriode().getNama());
                                    if (p.getPendaftar().getKeteranganTagihan() != null) {
                                        row.createCell(34).setCellValue(p.getPendaftar().getKeteranganTagihan());
                                    } else {
                                        row.createCell(34).setCellValue("-");
                                    }
                                }
                                Locale localeID = new Locale("in", "ID");
                                NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
                                double nilai = pembayaran.getNilai().doubleValue();
                                row.createCell(35).setCellValue(formatRupiah.format((double)nilai));

                            }

                        }
                    }
                }
            }
        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }



        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition","attachment; filename=\"Daftar Ulang Sarjana"+ LocalDate.now() +".xlsx"+"\"");
        workbook.write(response.getOutputStream());
        workbook.close();
    }
    @GetMapping("/pendaftarDuS1KK/xlsx")
    public void reportDuKKS1(HttpServletResponse response) throws  Exception{
        String[] columns = {"No","Nomor Registrasi","Nama","Tempat, Tanggal Lahir","Jenis Kelamin","Email","No Telephone","NISN",
                "Status Sipil","NIK","Asal Sekolah","Tahun Lulus","Program Studi","Konsentrasi","Provinsi","Kabupaten/Kota","Alamat","Recana Biaya",
                "Nama Ayah","Agama Ayah","Pendidikan Ayah","Pekerjaan Ayah","Status",
                "Nama Ibu","Agama Ibu", "Pendidikan Ibu","Pekerjaan Ibu","Status","Alamat Orangtua",
                "Jumlah Sodara", "Penghasilan","Jalur Test", "Grade","Periode", "Keterangan DU", "Nominal Du"};


        Iterable<PendaftarDetail> dataPendaftar = pendaftarDetailDao.findByPendaftarTahunAjaranAktifAndPendaftarLeadsJenjangAndPendaftarKonsentrasiAndPendaftarStatusNot(Status.AKTIF, Jenjang.S1,"Karyawan","Close Lose - Mengundurkan Diri");
        Iterable<PendaftarOrangtua> dataPendaftarOrtu = pendaftarOrangTuaDao.findByPendaftarTahunAjaranAktifAndPendaftarLeadsJenjang(Status.AKTIF, Jenjang.S1);

        JenisBiaya jb = new JenisBiaya();
        jb.setId("002");

        TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);
        Iterable<Pembayaran> cPembayaran = pembayaranDao.findByTagihanS1One(tahunAjaran.getId());

        Iterable<HasilTest> cHasilTest  = hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAktif(Status.AKTIF);

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Daftar Ulang Sarjana - "+LocalDate.now());

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1 ;
        int baris = 1 ;
        for (Pembayaran pembayaran : cPembayaran) {
            for (PendaftarDetail p : dataPendaftar) {
                for (PendaftarOrangtua pdo : dataPendaftarOrtu) {
                    if (pdo.getPendaftar() == p.getPendaftar() && pdo != null) {
                        if (pembayaran.getTagihan().getPendaftar() == pdo.getPendaftar() && pembayaran.getTagihan().getPendaftar() == p.getPendaftar()) {
                            Row row = sheet.createRow(rowNum++);
                            row.createCell(0).setCellValue(baris++);
                            row.createCell(1).setCellValue(p.getPendaftar().getNomorRegistrasi());
                            row.createCell(2).setCellValue(WordUtils.capitalize(p.getPendaftar().getLeads().getNama()));
                            row.createCell(3).setCellValue(p.getTempatLahir() + ", " + p.getTanggalLahir().format(DateTimeFormatter.ofPattern(("dd-MM-yyyy"))));
                            row.createCell(4).setCellValue(p.getJenisKelamin());
                            row.createCell(5).setCellValue(p.getPendaftar().getLeads().getEmail());
                            row.createCell(6).setCellValue(p.getPendaftar().getLeads().getNoHp());
                            row.createCell(7).setCellValue(p.getNisn());
                            row.createCell(8).setCellValue(p.getStatusSipil());
                            row.createCell(9).setCellValue(p.getNoKtp());
                            row.createCell(10).setCellValue(p.getPendaftar().getNamaAsalSekolah());
                            row.createCell(11).setCellValue(p.getTahunLulus());
                            row.createCell(12).setCellValue(p.getPendaftar().getProgramStudi().getNama());
                            row.createCell(13).setCellValue(p.getPendaftar().getKonsentrasi());
                            row.createCell(14).setCellValue(p.getProvinsi().getNama());
                            row.createCell(15).setCellValue(p.getKokab().getNama());
                            row.createCell(16).setCellValue(p.getAlamatRumah());
                            row.createCell(17).setCellValue(p.getRencanaBiaya());
                            row.createCell(18).setCellValue(pdo.getNamaAyah());
                            row.createCell(19).setCellValue(pdo.getAgamaAyah());
                            row.createCell(20).setCellValue(pdo.getPendidikanAyah().getNama());
                            row.createCell(21).setCellValue(pdo.getPekerjaanAyah().getNama());
                            row.createCell(22).setCellValue(pdo.getStatusAyah());
                            row.createCell(23).setCellValue(pdo.getNamaIbu());
                            row.createCell(24).setCellValue(pdo.getAgamaIbu());
                            row.createCell(25).setCellValue(pdo.getPendidikanIbu().getNama());
                            row.createCell(26).setCellValue(pdo.getPekerjaanIbu().getNama());
                            row.createCell(27).setCellValue(pdo.getStatusIbu());
                            row.createCell(28).setCellValue(pdo.getAlamat());
                            row.createCell(29).setCellValue(pdo.getJumlahTanggungan());
                            row.createCell(30).setCellValue(pdo.getPenghasilanOrangtua().getNama());
                            for (HasilTest h : cHasilTest) {
                                if (h.getJadwalTest().getPendaftar() == p.getPendaftar()) {
                                    row.createCell(31).setCellValue(h.getJadwalTest().getJenisTest().toString());
                                    row.createCell(32).setCellValue(h.getGrade().getNama());
                                    row.createCell(33).setCellValue(h.getPeriode().getNama());
                                    if (p.getPendaftar().getKeteranganTagihan() != null) {
                                        row.createCell(34).setCellValue(p.getPendaftar().getKeteranganTagihan());
                                    } else {
                                        row.createCell(34).setCellValue("-");
                                    }
                                }
                                Locale localeID = new Locale("in", "ID");
                                NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
                                double nilai = pembayaran.getNilai().doubleValue();
                                row.createCell(35).setCellValue(formatRupiah.format((double)nilai));

                            }

                        }
                    }
                }
            }
        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }



        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition","attachment; filename=\"Daftar Ulang Sarjana"+ LocalDate.now() +".xlsx"+"\"");
        workbook.write(response.getOutputStream());
        workbook.close();
    }
//Pascasarjana
    @GetMapping("/pendaftarDuS2/xlsx")
    public void reportDuS2(HttpServletResponse response) throws  Exception{
        String[] columns = {"No","Nomor Registrasi","Nama", "Tempat, Tanggal Lahir","Jenis Kelamin","Email","No Telephone","Jenis Kelamin","Golongan Darah","No Ktp",
                "Status Sipil","Negara","Provinsi","Kabupaten/Kota","Alamat","Kode Pos","Pekerjaan","Alamat Pekerjaan","Asal Pendidikan",
                "Program Studi","Konsentrasi","Sumber Biaya",
                "Nama Ayah","Nama Ibu","Jalur Test", "Grade","Periode", "Keterangan DU", "Nominal Du"};


        Iterable<PendaftarDetailPasca> dataPendaftar = pendaftarDetailPascaDao.findByPendaftarTahunAjaranAktifAndPendaftarLeadsJenjang(Status.AKTIF, Jenjang.S2);

        JenisBiaya jb = new JenisBiaya();
        jb.setId("002");
        TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);
        Iterable<Pembayaran> cPembayaran = pembayaranDao.findByTagihanS2One(tahunAjaran.getId());

        Iterable<HasilTest> hasilTests  = hasilTestDao.findAllByJadwalTestPendaftarTahunAjaranAktif(Status.AKTIF);


        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Daftar Ulang Pascasrjana "+LocalDate.now());

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1 ;
        int baris = 1 ;
        for (PendaftarDetailPasca p : dataPendaftar) {
            for (Pembayaran pembayaran : cPembayaran) {
                if (pembayaran.getTagihan().getPendaftar() == p.getPendaftar()) {
                    Row row = sheet.createRow(rowNum++);
                    row.createCell(0).setCellValue(baris++);
                    row.createCell(1).setCellValue(p.getPendaftar().getNomorRegistrasi());
                    row.createCell(2).setCellValue(WordUtils.capitalize(p.getPendaftar().getLeads().getNama()));
                    row.createCell(3).setCellValue(p.getTempatLahir() + ", " + p.getTanggalLahir().format(DateTimeFormatter.ofPattern(("dd-MM-yyyy"))));
                    row.createCell(4).setCellValue(p.getJenisKelamin());
                    row.createCell(5).setCellValue(p.getPendaftar().getLeads().getEmail());
                    row.createCell(6).setCellValue(p.getPendaftar().getLeads().getNoHp());
                    row.createCell(7).setCellValue(p.getJenisKelamin());
                    row.createCell(8).setCellValue(p.getGolonganDarah());
                    row.createCell(9).setCellValue(p.getNoKtp());
                    row.createCell(10).setCellValue(p.getStatusSipil());
                    row.createCell(11).setCellValue(p.getNegara());
                    row.createCell(12).setCellValue(p.getKokab().getProvinsi().getNama());
                    row.createCell(13).setCellValue(p.getKokab().getNama());
                    row.createCell(14).setCellValue(p.getAlamatRumah());
                    row.createCell(15).setCellValue(p.getKodePos());
                    row.createCell(16).setCellValue(p.getPekerjaan());
                    row.createCell(17).setCellValue(p.getAlamatPekerjaan());
                    row.createCell(18).setCellValue(p.getAsalPendidikan());
                    row.createCell(19).setCellValue(p.getPendaftar().getProgramStudi().getNama());
                    row.createCell(20).setCellValue(p.getPendaftar().getKonsentrasi());
                    row.createCell(21).setCellValue(p.getSumberBiaya());
                    row.createCell(22).setCellValue(p.getNamaAyah());
                    row.createCell(23).setCellValue(p.getNamaIbu());
                    for (HasilTest h : hasilTests) {
                        if (h.getJadwalTest().getPendaftar() == p.getPendaftar()) {
                            row.createCell(24).setCellValue(h.getJadwalTest().getJenisTest().toString());
                            row.createCell(25).setCellValue(h.getGrade().getNama());
                            row.createCell(26).setCellValue(h.getPeriode().getNama());
                        }
                    }
                    if (p.getPendaftar().getKeteranganTagihan() != null) {
                        row.createCell(27).setCellValue(p.getPendaftar().getKeteranganTagihan());
                    } else {
                        row.createCell(27).setCellValue("-");
                    }
                    Locale localeID = new Locale("in", "ID");
                    NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
                    double nilai = pembayaran.getNilai().doubleValue();
                    row.createCell(28).setCellValue(formatRupiah.format((double)nilai));                }
            }
        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }



        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition","attachment; filename=\"Daftar Ulang Pascasarjana - "+ LocalDate.now() +".xlsx"+"\"");
        workbook.write(response.getOutputStream());
        workbook.close();
    }



    @GetMapping("/detail/xlsxS2")
    public void reportDetailS2(HttpServletResponse response) throws  Exception{
        String[] columns = {"No","Nomor Registrasi","Nama","Tempat, Tanggal Lahir","Email","No Telephone","Jenis Kelamin","Golongan Darah","No Ktp",
                "Status Sipil","Negara","Provinsi","Kabupaten/Kota","Alamat","Kode Pos","Pekerjaan","Alamat Pekerjaan","Asal Pendidikan",
                "Program Studi","Konsentrasi","Sumber Biaya",
                "Nama Ayah","Nama Ibu"};


        Iterable<PendaftarDetailPasca> dataPendaftar = pendaftarDetailPascaDao.findByPendaftarTahunAjaranAktif(Status.AKTIF);

            Workbook workbook = new XSSFWorkbook();
            Sheet sheet = workbook.createSheet("Pendaftar Pascasrjana 2020");

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setFontHeightInPoints((short) 12);
            headerFont.setColor(IndexedColors.BLACK.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            Row headerRow = sheet.createRow(0);

            for (int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
                cell.setCellStyle(headerCellStyle);
            }

            int rowNum = 1 ;
            int baris = 1 ;
        for (PendaftarDetailPasca p : dataPendaftar) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(baris++);
            row.createCell(1).setCellValue(p.getPendaftar().getNomorRegistrasi());
            row.createCell(2).setCellValue(WordUtils.capitalize(p.getPendaftar().getLeads().getNama()));
            row.createCell(3).setCellValue(p.getTempatLahir()+", "+ p.getTanggalLahir().format(DateTimeFormatter.ofPattern(("dd-MM-yyyy"))));
            row.createCell(4).setCellValue(p.getPendaftar().getLeads().getEmail());
            row.createCell(5).setCellValue(p.getPendaftar().getLeads().getNoHp());
            row.createCell(6).setCellValue(p.getJenisKelamin());
            row.createCell(7).setCellValue(p.getGolonganDarah());
            row.createCell(8).setCellValue(p.getNoKtp());
            row.createCell(9).setCellValue(p.getStatusSipil());
            row.createCell(10).setCellValue(p.getNegara());
            row.createCell(11).setCellValue(p.getKokab().getProvinsi().getNama());
            row.createCell(12).setCellValue(p.getKokab().getNama());
            row.createCell(13).setCellValue(p.getAlamatRumah());
            row.createCell(14).setCellValue(p.getKodePos());
            row.createCell(15).setCellValue(p.getPekerjaan());
            row.createCell(16).setCellValue(p.getAlamatPekerjaan());
            row.createCell(17).setCellValue(p.getAsalPendidikan());
            row.createCell(18).setCellValue(p.getPendaftar().getProgramStudi().getNama());
            row.createCell(19).setCellValue(p.getPendaftar().getKonsentrasi());
            row.createCell(20).setCellValue(p.getSumberBiaya());
            row.createCell(21).setCellValue(p.getNamaAyah());
            row.createCell(22).setCellValue(p.getNamaIbu());
        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }



        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition","attachment; filename=\"Pendaftar Detail Pascasarjana - "+ LocalDate.now() +".xlsx"+"\"");
        workbook.write(response.getOutputStream());
        workbook.close();
    }


    @GetMapping("/detailPendaftar/formS2")
    public String formDetailPasca(Model model, Authentication currentUser) {
        logger.debug("Authentication class : {}", currentUser.getClass().getName());

        if (currentUser == null) {
            logger.warn("Current user is null");
        }

        String username = ((UserDetails) currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if (u == null) {
            logger.warn("Username {} not found in database ", username);
        }

        Leads leads = leadsDao.findByUser(u);
        logger.info("Pendaftar a.n {} ditemukan", leads.getNama());
        Pendaftar pendaftar = pendaftarDao.findByLeads(leads);
        if (pendaftar != null) {
            model.addAttribute("pendaftar", pendaftar);
        }


        PendaftarDetailPasca pendaftarDetailPascaDone = pendaftarDetailPascaDao.findByPendaftar(pendaftar);
        if (pendaftarDetailPascaDone != null) {
            model.addAttribute("pendaftarDetailPasca", pendaftarDetailPascaDone);
        } else {
            logger.warn("Data detail  dengan pendaftar {} tidak ada dalam database", pendaftar.getLeads().getNama());
            model.addAttribute("pendaftarDetailPasca", new PendaftarDetailPasca());
        }

//Cek Pembayaran
        Pembayaran pembayaran = pembayaranController.cekPembayaran(pendaftar);
        model.addAttribute("pembayaran", pembayaran);

        return "detailPendaftar/formS2";
    }


    @PostMapping("/detailPendaftar/formS2")
    public String prosesFromDetailPendaftarPasca(PendaftarDetailPasca pendaftarDetailPasca, Model model, BindingResult errors,
                                            Authentication currentUser, MultipartFile foto) throws Exception {
        logger.debug("Authentication class : {}",currentUser.getClass().getName());

        if(currentUser == null){
            logger.warn("Current user is null");
        }

        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }

        Leads leads = leadsDao.findByUser(u);
        logger.info("Pendaftar a.n {} ditemukan", leads.getNama());
        Pendaftar pendaftar = pendaftarDao.findByLeads(leads);
        pendaftarDetailPasca.setPendaftar(pendaftar);

        pendaftarDetailPascaDao.save(pendaftarDetailPasca);
        logger.info("Detail Pendaftar a.n {} berhasil di simpan", pendaftarDetailPasca.getPendaftar().getLeads().getNama());
//////
        return "redirect:/jadwalTest/form";
    }


    @GetMapping("/detailPendaftar/showDetailS2")
    public void showDetailPascaSarjana(@RequestParam(required = false)Pendaftar  id, Model m, Pageable pa){
        PendaftarDetailPasca pd = pendaftarDetailPascaDao.findByPendaftar(id);
        if (pd != null) {
            m.addAttribute("dPendaftar", pd);
            logger.info("Detail pendaftar tersedia didatabase");
        }else{
            logger.debug("Detail {} Tidak ada didatabase",id.getNomorRegistrasi() );
        }
    }
}

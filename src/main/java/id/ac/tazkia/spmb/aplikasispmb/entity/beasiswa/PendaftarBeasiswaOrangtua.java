package id.ac.tazkia.spmb.aplikasispmb.entity.beasiswa;

import id.ac.tazkia.spmb.aplikasispmb.entity.Pekerjaan;
import id.ac.tazkia.spmb.aplikasispmb.entity.Pendidikan;
import id.ac.tazkia.spmb.aplikasispmb.entity.Penghasilan;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Data
public class PendaftarBeasiswaOrangtua {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_pendaftar_beasiswa")
    private PendaftarBeasiswa pendaftarBeasiswa;

    @NotEmpty
    private String namaIbu;
    
    @NotEmpty
    private String agamaIbu;
    
    @ManyToOne @JoinColumn(name = "pendidikan_ibu")
    private Pendidikan pendidikanIbu;
    
    @ManyToOne @JoinColumn(name = "pekerjaan_ibu")
    private Pekerjaan pekerjaanIbu;

    private String statusIbu;

    private String namaAyah;

    private String agamaAyah;

    @ManyToOne
    @JoinColumn(name = "pendidikan_ayah")
    private Pendidikan pendidikanAyah;

    @ManyToOne
    @JoinColumn(name = "pekerjaan_ayah")
    private Pekerjaan pekerjaanAyah;

    private String statusAyah;

    @Column(columnDefinition = "LONGTEXT")
    private String alamat;

    private String noTelpon;

    @ManyToOne
    @JoinColumn(name = "id_penghasilan")
    private Penghasilan penghasilanOrangtua;

    private Integer jumlahTanggungan;

}

package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.dao.*;
import id.ac.tazkia.spmb.aplikasispmb.dto.UploadBerkasDto;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Controller
public class UploadBerkasController {
    private static final Logger logger = LoggerFactory.getLogger(UploadBerkasController.class);

    @Autowired private UserDao userDao;
    @Autowired private PendaftarDao pendaftarDao;
    @Autowired private LeadsDao leadsDao;
    @Autowired private UploadBerkasDao uploadBerkasDao;
    @Autowired private PembayaranController pembayaranController;
    @Autowired private PendaftarDetailDao pendaftarDetailDao;
    @Autowired private PendaftarDetailPascaDao  pendaftarDetailPascaDao;

    //FORM NIMKO
    @Value("classpath:sample/Form NIRM.xlsx")
    private Resource formNimko;

    @Value("${upload.folder.berkas}")
    private String uploadFolder;

    @GetMapping("/formNimko")
    public void downloadContohFileNirm(HttpServletResponse response) throws Exception {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=Form-NIRM.xlsx");
        FileCopyUtils.copy(formNimko.getInputStream(), response.getOutputStream());
        response.getOutputStream().flush();
    }
//

    @GetMapping("/uploadBerkas/form")
    public void formUploadBerkas (Model model, Authentication currentUser, Pageable page){

        UploadBerkasDto uploadBerkasDto = new UploadBerkasDto();
        model.addAttribute("uploadBerkasDto", uploadBerkasDto);

        logger.debug("Authentication class : {}",currentUser.getClass().getName());

        if(currentUser == null){
            logger.warn("Current user is null");
            return;
        }

        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
            return;
        }

        Leads l = leadsDao.findByUser(u);
        Pendaftar p = pendaftarDao.findByLeads(l);
        logger.debug("Nama Pendaftar : "+p.getLeads().getNama());
        if(p == null){
            logger.warn("Pendaftar not found for username {} ", username);
            return;
        }
        model.addAttribute("pendaftar", p);

        List<Berkas> berkas = uploadBerkasDao.findByPendaftar(p);
        for (Berkas berkas1 : berkas) {
            if (berkas1 != null) {
                model.addAttribute("berkas", berkas1);
            }
        }
        model.addAttribute("listBerkas", uploadBerkasDao.findByPendaftarOrderByJenisBerkas(p, page));

        PendaftarDetail pd = pendaftarDetailDao.findByPendaftar(p);
        System.out.printf("detail" + pd);
        model.addAttribute("detail", pd);
        PendaftarDetailPasca pdp = pendaftarDetailPascaDao.findByPendaftar(p);
        System.out.printf("detailPasca" + pdp);
        model.addAttribute("detailPasca", pdp);

//Cek Pembayaran
        Pembayaran pembayaran = pembayaranController.cekPembayaranDU(p);
        model.addAttribute("pembayaran", pembayaran);


    }

    @PostMapping("/uploadBerkas/form")
    public String  prosesUploadBerkas(@ModelAttribute @Valid UploadBerkasDto berkasDto, BindingResult errors,
                                      MultipartFile fileBerkas1, MultipartFile fileBerkas2, MultipartFile fileBerkas3
            , MultipartFile fileBerkas4, MultipartFile fileBerkas5
            , Model model, JenisBerkas jenisBerkas, Authentication currentUser) throws Exception{
        logger.debug("Authentication class : {}",currentUser.getClass().getName());

        if(currentUser == null){
            logger.warn("Current user is null");
        }

        String username = ((UserDetails)currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsername(username);
        logger.debug("User ID : {}", u.getId());
        if(u == null){
            logger.warn("Username {} not found in database ", username);
        }

        Leads l = leadsDao.findByUser(u);
        Pendaftar pendaftar = pendaftarDao.findByLeads(l);
        logger.debug("Nama Pendaftar : "+pendaftar.getLeads().getNama());
        if(pendaftar == null){
            logger.warn("Pendaftar not found for username {} ", username);
        }
        model.addAttribute("pendaftar", pendaftar);

        if (errors.hasErrors()) {
            logger.debug("Error upload bukti pembayaran : {}", errors.toString());
        }

        berkasDto.setAlasan("Waiting");
// persiapan lokasi upload

        String idPeserta = pendaftar.getLeads().getNama();
        String lokasiUpload = uploadFolder + File.separator + idPeserta;
        logger.debug("Lokasi upload : {}", lokasiUpload);
        new File(lokasiUpload).mkdirs();

// Proses upload berkas.
        simpanBerkas(fileBerkas1, pendaftar, berkasDto, lokasiUpload, berkasDto.getJenisBerkas1());
        simpanBerkas(fileBerkas2, pendaftar, berkasDto, lokasiUpload, berkasDto.getJenisBerkas2());
        simpanBerkas(fileBerkas3, pendaftar, berkasDto, lokasiUpload, berkasDto.getJenisBerkas3());
        simpanBerkas(fileBerkas4, pendaftar, berkasDto, lokasiUpload, berkasDto.getJenisBerkas4());
        simpanBerkas(fileBerkas5, pendaftar, berkasDto, lokasiUpload, berkasDto.getJenisBerkas5());


        return "redirect:/uploadBerkas/form";
    }

    // Fungsi Upload Berkas
    private void simpanBerkas(MultipartFile berkasFile, Pendaftar pendaftar, UploadBerkasDto berkasDto, String lokasiUpload, JenisBerkas jenisBerkas){
        try {
            if (berkasFile == null || berkasFile.isEmpty()) {
                logger.info("File berkas kosong, tidak diproses");
                return;
            }

            Berkas berkas = new Berkas();
            berkas.setPendaftar(pendaftar);
            berkas.setJenisBerkas(berkasDto.getJenisBerkas1());

            String namaFile = berkasFile.getName();
            String jenisFile = berkasFile.getContentType();
            String namaAsli = berkasFile.getOriginalFilename();
            Long ukuran = berkasFile.getSize();

            logger.debug("Nama File : {}", namaFile);
            logger.debug("Jenis File : {}", jenisFile);
            logger.debug("Nama Asli File : {}", namaAsli);
            logger.debug("Ukuran File : {}", ukuran);

            //memisahkan extensi
            String extension = "";

            int i = namaAsli.lastIndexOf('.');
            int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

            if (i > p) {
                extension = namaAsli.substring(i + 1);
            }

            String idFile = UUID.randomUUID().toString();
            berkas.setFileBerkas(idFile + "." + extension);
            File tujuan = new File(lokasiUpload + File.separator + berkas.getFileBerkas());
            berkasFile.transferTo(tujuan);
            logger.debug("File sudah dicopy ke : {}", tujuan.getAbsolutePath());
            berkas.setJenisBerkas(jenisBerkas);
            uploadBerkasDao.save(berkas);
        } catch (Exception er) {
            logger.error(er.getMessage(), er);
        }

    }

    @GetMapping("/berkas/{berkas}/berkas/")
    public ResponseEntity<byte[]> tampilkanBerkas(@PathVariable Berkas berkas) throws Exception {
        String lokasiFile = uploadFolder + File.separator + berkas.getPendaftar().getLeads().getNama()
                + File.separator + berkas.getFileBerkas();
        logger.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (berkas.getFileBerkas().toLowerCase().endsWith("jpeg") || berkas.getFileBerkas().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (berkas.getFileBerkas().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            }else if (berkas.getFileBerkas().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            }else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            logger.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @RequestMapping("/file/{fileBerkas}")
    public void downloadNirm( HttpServletRequest request,
                                     HttpServletResponse response,
                                     @RequestParam Berkas berkas,
                                     @PathVariable("fileBerkas") String fileBerkas)
    {
        //If user is not authorized - he should be thrown out from here itself

        //Authorized user will download the file
        String lokasi = uploadFolder+File.separator+berkas.getPendaftar().getLeads().getNama();
        String dataDirectory = request.getServletContext().getRealPath(lokasi);
        Path file = Paths.get(lokasi, fileBerkas);
        System.out.println(file);
        if (Files.exists(file))
        {
            response.setContentType("application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.addHeader("Content-Disposition", "attachment; filename="+fileBerkas);
            try
            {
                Files.copy(file, response.getOutputStream());
                response.getOutputStream().flush();
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    @GetMapping("/uploadBerkas/listAdmin")
    public ModelMap viewBerkas(@RequestParam Pendaftar pendaftar, Pageable page){
        ModelMap mm = new ModelMap();
        mm.addAttribute("daftarBerkas",uploadBerkasDao.findByPendaftarOrderByJenisBerkas(pendaftar, page));
        mm.addAttribute("pendaftar", pendaftar);
        return mm;
    }


    @PostMapping("/uploadBerkas/reject")
    public String rejectBerkas(String alasan, Berkas berkas){
        berkas.setAlasan(alasan);
        berkas.setStatus(Status.REJECTED);
        uploadBerkasDao.save(berkas);

        return "redirect:/uploadBerkas/listAdmin?pendaftar="+berkas.getPendaftar().getId();
    }

    @PostMapping("/uploadBerkas/approved")
    public String approvedBerkas(@RequestParam(required = false) Berkas berkas){
        berkas.setStatus(Status.APPROVED);
        berkas.setAlasan("Approved");
        uploadBerkasDao.save(berkas);

        return "redirect:/uploadBerkas/listAdmin?pendaftar="+berkas.getPendaftar().getId();
    }

    @PostMapping("/uploadBerkas/update")
    public String updateBerkas(@RequestParam(required = false) Berkas berkas,MultipartFile fileBerkas, JenisBerkas jenisBerkas) throws IOException {

        String namaFile = fileBerkas.getName();
        String jenisFile = fileBerkas.getContentType();
        String namaAsli = fileBerkas.getOriginalFilename();
        Long ukuran = fileBerkas.getSize();

        logger.debug("Nama File : {}", namaFile);
        logger.debug("Jenis File : {}", jenisFile);
        logger.debug("Nama Asli File : {}", namaAsli);
        logger.debug("Ukuran File : {}", ukuran);

        //memisahkan extensi
        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idPeserta = berkas.getPendaftar().getLeads().getNama();
        String lokasiUpload = uploadFolder + File.separator + idPeserta;
        logger.debug("Lokasi upload : {}", lokasiUpload);

        String idFile = UUID.randomUUID().toString();
        berkas.setFileBerkas(idFile + "." + extension);
        File tujuan = new File(lokasiUpload + File.separator + berkas.getFileBerkas());
        fileBerkas.transferTo(tujuan);
        logger.debug("File sudah dicopy ke : {}", tujuan.getAbsolutePath());

        berkas.setJenisBerkas(jenisBerkas);
        berkas.setStatus(Status.WAITING);
        berkas.setAlasan(null);
        uploadBerkasDao.save(berkas);

        return "redirect:/uploadBerkas/form";
    }

    @Value("classpath:sample/Panduan_Upload_Berkas.pdf")
    private Resource panduanUpload;

    @GetMapping("/berkas/panduanUpload")
    public void downloadBrosurPei(HttpServletResponse response) throws Exception {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=Penduan_Upload_Berkas.pdf");
        FileCopyUtils.copy(panduanUpload.getInputStream(), response.getOutputStream());
        response.getOutputStream().flush();
    }

}

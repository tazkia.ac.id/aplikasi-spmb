package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.Followup;
import id.ac.tazkia.spmb.aplikasispmb.entity.Institut;
import id.ac.tazkia.spmb.aplikasispmb.entity.Status;
import id.ac.tazkia.spmb.aplikasispmb.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface FollowupDao extends PagingAndSortingRepository<Followup,String>, CrudRepository <Followup,String> {

//    @Query(value = "select * from followup where status = 'AKTIF' order by jumlah ASC , id limit 1", nativeQuery = true)
//    Followup cariUserFu ();

    @Query("select fu from Followup fu where fu.status = 'AKTIF' and fu.s1 = 'AKTIF' and fu.institut=:institut order by fu.jumlah ASC , fu.id limit 1")
    Followup cariUserFuS1(@Param("institut") Institut institut);

    @Query(value = "select * from followup where status = 'AKTIF' and s2 = 'AKTIF' order by jumlah ASC , id limit 1", nativeQuery = true)
    Followup cariUserFuS2 ();

    @Query(value = "select * from followup where status = 'AKTIF' and s1 = 'AKTIF' order by jumlah DESC , id limit 1", nativeQuery = true)
    Followup cariUserFuS1Terbaru ();
    @Query(value = "select * from followup where status = 'AKTIF' and kk = 'AKTIF' order by jumlah ASC , id limit 1", nativeQuery = true)
    Followup cariUserFuKk ();
    Page<Followup> findByNamaContainingIgnoreCaseOrderByNama(String nama, Pageable page);

    Followup findByUser(User user);

    Iterable<Followup> findByStatus(Status status);

    Iterable<Followup> findByStatusAndInstitut(Status status, Institut institut);
}

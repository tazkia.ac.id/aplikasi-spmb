package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.constants.AppConstants;
import id.ac.tazkia.spmb.aplikasispmb.dao.RoleDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.UserDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.UserPasswordDao;
import id.ac.tazkia.spmb.aplikasispmb.dto.InputUserDto;
import id.ac.tazkia.spmb.aplikasispmb.entity.User;
import id.ac.tazkia.spmb.aplikasispmb.entity.UserPassword;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
public class UserController {

    @Autowired private UserDao userDao;

    @Autowired private UserPasswordDao passwordDao;

    @Autowired private RoleDao roleDao;

    @Autowired private PasswordEncoder passwordEncoder;

    @GetMapping("/user/list")
    public String listUser(Model model, @PageableDefault(size = 20) Pageable pageable) {

        model.addAttribute("listUser", userDao.findByRoleIdNotIn(List.of(AppConstants.ROLE_PENDAFTAR, AppConstants.ROLE_EDU), pageable));

        return "user/list";
    }

    @GetMapping("/user/add")
    public String addUser(Model model, @RequestParam(required = false) String id) {

        model.addAttribute("listRole", roleDao.findByIdNotIn(List.of(AppConstants.ROLE_PENDAFTAR, AppConstants.ROLE_BEASISWA, AppConstants.ROLE_EDU)));

        model.addAttribute("user", new InputUserDto());
        if (id != null && !id.isEmpty()) {
            User user = userDao.findById(id).get();
            if (user != null) {
                InputUserDto userDto = new InputUserDto();
                userDto.setId(user.getId());
                userDto.setUsername(user.getUsername());
                userDto.setRole(user.getRole());
                userDto.setStatus(user.getActive());
                model.addAttribute("user", userDto);
                if (Boolean.FALSE.equals(user.getActive())) {
                    user.setActive(false);
                }
            }
        }
        return "user/form";
    }

    @PostMapping("/user/add")
    public String addUser(@Valid InputUserDto userDto, RedirectAttributes attributes) {
        log.info("Cek id user : {}", userDto.getId());
        if (userDto.getId().isEmpty()) {
            log.info("Membuat user baru");
            User cekExistUser = userDao.findByUsername(userDto.getUsername());
            if (cekExistUser != null) {
                attributes.addFlashAttribute("error", "Username sudah digunakan!");
                return "redirect:/user/add";
            }

            User newUser = new User();
            newUser.setUsername(userDto.getUsername());
            newUser.setActive(userDto.getStatus());
            newUser.setRole(userDto.getRole());
            userDao.save(newUser);
            UserPassword up = new UserPassword();
            up.setPassword(passwordEncoder.encode(userDto.getPassword()));
            up.setUser(newUser);
            passwordDao.save(up);

        }else{
            log.info("Mengubah user yang sudah ada");
            Optional<User> getUserExist = userDao.findById(userDto.getId());
            if (getUserExist.isPresent()) {
                User user = getUserExist.get();
                log.info("Mengubah user : {}", user);
                user.setUsername(userDto.getUsername());
                user.setActive(userDto.getStatus());
                user.setRole(userDto.getRole());
                userDao.save(user);
                UserPassword up = passwordDao.findByUser(user);
                up.setPassword(passwordEncoder.encode(userDto.getPassword()));
                passwordDao.save(up);
            }
        }

        return "redirect:/user/list";
    }

}

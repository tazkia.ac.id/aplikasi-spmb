package id.ac.tazkia.spmb.aplikasispmb.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Leads {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull @NotEmpty
    private String nama;

    @NotNull @NotEmpty
    private String email;

    @NotNull @NotEmpty
    private String noHp;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Jenjang jenjang;

    @NotNull
    private String kelas;

    @OneToOne
    @JoinColumn(name = "id_user")
    private User user;

    @OneToOne
    @JoinColumn(name = "id_tahun")
    private TahunAjaran tahunAjaran;

    @NotNull
    private LocalDateTime createDate = LocalDateTime.now();

    @OneToOne
    @JoinColumn(name = "id_referal")
    private Referal kode;

    @OneToOne
    @JoinColumn(name = "id_subscribe")
    private Subscribe subscribe;

    @ManyToOne
    @JoinColumn(name = "id_institut")
    private Institut institut;

}

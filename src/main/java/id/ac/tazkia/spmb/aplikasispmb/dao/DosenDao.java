package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.Dosen;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface DosenDao extends PagingAndSortingRepository<Dosen, String >, CrudRepository<Dosen, String > {
    Page<Dosen> findByNamaDosenContainingIgnoreCaseOrderByNamaDosenDesc(String namaDosen, Pageable page);
}

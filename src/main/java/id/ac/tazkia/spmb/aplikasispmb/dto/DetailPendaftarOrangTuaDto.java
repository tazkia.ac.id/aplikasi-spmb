package id.ac.tazkia.spmb.aplikasispmb.dto;

import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Column;
import java.time.LocalDate;

@Data
public class DetailPendaftarOrangTuaDto {

    private String id;
    private Pendaftar pendaftar;
    private String fotoPe;
    private String noKtp;
    private String jenisKelamin;
    private String tempatLahir;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(columnDefinition = "DATE")
    private LocalDate tanggalLahir;

    private String golonganDarah;
    private String agama;
    private String alamatRumah;
    private Provinsi provinsi;
    private KabupatenKota kokab;
    private String kodePos;
    private String negara;
    private String statusSipil;

    private String tahunLulus;
    private String jurusanSekolah;
    private String nisn;
    private String rencanaBiaya;
//
    private String orangTua;
    private String namaAyah;
    private String agamaAyah;
    private Pendidikan pendidikanAyah;
    private Pekerjaan pekerjaanAyah;
    private String statusAyah;

    private String namaIbu;
    private String agamaIbu;
    private Pendidikan pendidikanIbu;
    private Pekerjaan pekerjaanIbu;
    private String statusIbu;

    private String alamatOrangtua;
    private String noOrangtua;
    private Penghasilan penghasilanOrangtua;
    private String jumlahTanggungan;

    private ProgramStudi programStudi;
    private String  konsentrasi;

    private Leads leads;
    private String nama;
    private String email;
    private String noHp;

    private String nim;

    private ProgramStudi prodiLama;

    private LocalDate updateTime = LocalDate.now();


}

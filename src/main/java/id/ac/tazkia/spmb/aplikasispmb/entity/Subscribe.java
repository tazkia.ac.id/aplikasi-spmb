package id.ac.tazkia.spmb.aplikasispmb.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity @Data
public class Subscribe {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull @NotEmpty
    private String nama;

    @NotNull @NotEmpty
    private String email;

    @NotNull @NotEmpty
    private String noWa;

    private String asalKota;

    @NotEmpty @NotEmpty
    private String asalSekolah;

    @NotEmpty @NotNull
    private String status;

    @NotNull
    private LocalDateTime tglInsert = LocalDateTime.now();

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_fu")
    private Followup followup;

    private LocalDate tglUpdate;

    @ManyToOne
    @JoinColumn(name = "user_update")
    private User userUpdate;

    private Integer hubungi;
    private String sumber;
    private String medium;

    @ManyToOne
    @JoinColumn(name = "id_institusi")
    private Institut institut;

    private String kelas;

    private String tahunLulus;
    private String kelasSma;

    @OneToOne
    @JoinColumn(name = "id_tahunajaran")
    private TahunAjaran tahunAjaran;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Jenjang jenjang;

    private String noteEc;
    private String lulusan;
}

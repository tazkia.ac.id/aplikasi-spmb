package id.ac.tazkia.spmb.aplikasispmb.controller;

import id.ac.tazkia.spmb.aplikasispmb.constants.AppConstants;
import id.ac.tazkia.spmb.aplikasispmb.dao.FollowupDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.RoleDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.UserDao;
import id.ac.tazkia.spmb.aplikasispmb.dao.UserPasswordDao;
import id.ac.tazkia.spmb.aplikasispmb.dto.FollowupDto;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@Controller
public class FollowupController {

    @Autowired
    private FollowupDao followupDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private UserPasswordDao userPasswordDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private PasswordEncoder passwordEncoder;



    @GetMapping("/followup/form")
    public String formEduCon(@RequestParam(value = "id", required = false) String id,
                           Model m){
        //defaultnya, isi dengan object baru
        m.addAttribute("followup", new FollowupDto());

        if (id != null && !id.isEmpty()){
            Followup p= followupDao.findById(id).get();
            if (p != null){
                m.addAttribute("followup", p);
            }
        }
        return "followup/form";
    }

    @PostMapping("followup/form")
    public String prosesFromEdu(@Valid FollowupDto followupDto){
        User u = new User();
        u.setUsername(followupDto.getUsername());
        u.setActive(true);
        Role role = roleDao.findById(AppConstants.ROLE_EDU).get();
        u.setRole(role);
        userDao.save(u);
        UserPassword up = new UserPassword();
        up.setUser(u);
        up.setPassword(passwordEncoder.encode(followupDto.getPasword()));
        userPasswordDao.save(up);

        System.out.println(followupDto);
        Followup fu = new Followup();
        fu.setNama(followupDto.getNama());
        fu.setNoWhatsapp(followupDto.getNoWhatsapp());

        Followup cFu = followupDao.cariUserFuS1Terbaru();
        fu.setJumlah(cFu.getJumlah());
        fu.setUser(u);

        fu.setStatus(Status.AKTIF);

        if (followupDto.getS1() != null) {
            fu.setS1(Status.AKTIF);
        } else {
            fu.setS1(Status.NONAKTIF);
        }
        if (followupDto.getS2() != null) {
            fu.setS2(Status.AKTIF);
        } else {
            fu.setS2(Status.NONAKTIF);
        }
        if (followupDto.getKk() != null) {
            fu.setKk(Status.AKTIF);
        } else {
            fu.setKk(Status.NONAKTIF);
        }
        followupDao.save(fu);

        return "redirect:/followup/list";


    }



    @GetMapping("/followup/list")
    public void listEduCons (@RequestParam(required = false)String nama, Model m, Pageable page){
        if(StringUtils.hasText(nama)) {
            m.addAttribute("nama", nama);
            m.addAttribute("listFu", followupDao.findByNamaContainingIgnoreCaseOrderByNama(nama, page));
        } else {
            m.addAttribute("listFu", followupDao.findAll(page));
        }

        Iterable<Followup> sc = followupDao.findAll();
        m.addAttribute("followup", sc);
    }

    @RequestMapping(value = "/followup/aktif", method = RequestMethod.POST)
    public String updateStatusAktif (@RequestParam (required = false) Followup fu){
        Followup followup = followupDao.cariUserFuS1Terbaru();
        fu.setJumlah(followup.getJumlah());
        System.out.println(fu);
        fu.setStatus(Status.AKTIF);
        System.out.println("Status Baru : " + fu.getStatus());
        followupDao.save(fu);

        User user = userDao.findById(fu.getUser().getId()).get();
        user.setActive(true);
        userDao.save(user);
        return "redirect:list";

    }

    @RequestMapping(value = "/followup/nonAktif", method = RequestMethod.POST)
    public String updateStatusNonAktif (@RequestParam (required = false) Followup fu){
        System.out.println(fu);
        fu.setStatus(Status.NONAKTIF);
        System.out.println("Status Baru : " + fu.getStatus());
        followupDao.save(fu);

        User user = userDao.findById(fu.getUser().getId()).get();
        user.setActive(false);
        userDao.save(user);
        return "redirect:list";

    }

    @RequestMapping(value = "/followup/aktifS1", method = RequestMethod.POST)
    public String updateStatusAktifS1 (@RequestParam (required = false) Followup fu){
        Followup followup = followupDao.cariUserFuS1Terbaru();
        fu.setJumlah(followup.getJumlah());
        System.out.println(fu);
        fu.setS1(Status.AKTIF);
        followupDao.save(fu);
        return "redirect:list";
    }

    @RequestMapping(value = "/followup/nonAktifS1", method = RequestMethod.POST)
    public String updateStatusNonAktifS1 (@RequestParam (required = false) Followup fu){
        System.out.println(fu);
        fu.setS1(Status.NONAKTIF);
        followupDao.save(fu);
        return "redirect:list";
    }
    @RequestMapping(value = "/followup/aktifS2", method = RequestMethod.POST)
    public String updateStatusAktifS2 (@RequestParam (required = false) Followup fu){
        Followup followup = followupDao.cariUserFuS1Terbaru();
        fu.setJumlah(followup.getJumlah());
        System.out.println(fu);
        fu.setS2(Status.AKTIF);
        followupDao.save(fu);
        return "redirect:list";
    }

    @RequestMapping(value = "/followup/nonAktifS2", method = RequestMethod.POST)
    public String updateStatusNonAktifS2 (@RequestParam (required = false) Followup fu){
        System.out.println(fu);
        fu.setS2(Status.NONAKTIF);
        followupDao.save(fu);
        return "redirect:list";
    }

    @RequestMapping(value = "/followup/aktifKk", method = RequestMethod.POST)
    public String updateStatusAktifKk(@RequestParam (required = false) Followup fu){
        Followup followup = followupDao.cariUserFuS1Terbaru();
        fu.setJumlah(followup.getJumlah());
        System.out.println(fu);
        fu.setKk(Status.AKTIF);
        followupDao.save(fu);
        return "redirect:list";
    }

    @RequestMapping(value = "/followup/nonAktifKk", method = RequestMethod.POST)
    public String updateStatusNonAktifKk (@RequestParam (required = false) Followup fu){
        System.out.println(fu);
        fu.setKk(Status.NONAKTIF);
        followupDao.save(fu);
        return "redirect:list";
    }

    @PostMapping("/kategori/update/{id}")
    @ResponseBody
    public void updateStatus (@PathVariable Followup id,@RequestParam String kategori){
        Followup followup = followupDao.findById(id.getId()).get();
        followup.setKategori(kategori);
        followupDao.save(id);

    }
}

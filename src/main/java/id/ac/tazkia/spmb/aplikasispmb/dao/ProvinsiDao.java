package id.ac.tazkia.spmb.aplikasispmb.dao;

import id.ac.tazkia.spmb.aplikasispmb.entity.Provinsi;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ProvinsiDao extends PagingAndSortingRepository<Provinsi, String>, CrudRepository<Provinsi, String> {
    List<Provinsi> findByNamaContainingIgnoreCaseOrderByNama(String nama);
}

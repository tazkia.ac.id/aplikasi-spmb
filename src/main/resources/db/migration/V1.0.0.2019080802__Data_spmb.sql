INSERT INTO s_permission (id, permission_value, permission_label) VALUES
  ('editmaster', 'EDIT_MASTER', 'Edit Master'),
  ('viewmaster', 'VIEW_MASTER', 'View Master'),
  ('editpendaftar', 'EDIT_PENDAFTAR', 'Edit Peserta'),
  ('viewpendaftar', 'VIEW_PENDAFTAR', 'View Peserta'),
  ('editkeuangan', 'EDIT_KEUANGAN', 'Edit Keuangan'),
  ('viewkeuangan', 'VIEW_KEUANGAN', 'View Keuangan'),
  ('editakademik', 'EDIT_AKADEMIK', 'Edit Akademik'),
  ('viewakademik', 'VIEW_AKADEMIK', 'View Akademik');

INSERT INTO s_role (id, description, name) VALUES
  ('pendaftar', 'PENDAFTAR', 'Peserta'),
  ('humas', 'HUMAS', 'Humas'),
  ('keuangan', 'KEUANGAN', 'Keuangan'),
  ('akademik', 'AKADEMIK', 'Akademik');

INSERT INTO s_role_permission (id_role, id_permission) VALUES
  ('pendaftar', 'viewpendaftar'),
  ('pendaftar', 'editpendaftar'),
  ('humas', 'viewmaster'),
  ('humas', 'editmaster'),
  ('keuangan', 'viewkeuangan'),
  ('keuangan', 'editkeuangan'),
  ('akademik', 'viewakademik'),
  ('akademik', 'editakademik');

insert into program_studi VALUES ('001','Ekonomi Syariah (S1)',	'PSES-001'	,'03');
insert into program_studi VALUES ('002','Akuntansi Syariah (S1)',	'PSAS-001'	,'02');
insert into program_studi VALUES ('003','Manajemen Bisnis Syariah (S1)',	'PSBM-001'	,'01');
insert into program_studi VALUES ('004','Hukum Ekonomi Syariah (S1)',	'PSHES-001'	,'04');
insert into program_studi VALUES ('005','FTUI (S1)',	'PSES-001'	,'02');
insert into program_studi VALUES ('006','Pendidikan Ekonomi Syariah (S1)',	'PSTI-001'	,'08');
insert into program_studi VALUES ('007','Manajemen Bisnis Syariah (S2)',	'PSMES-001'	,'06');
insert into program_studi VALUES ('008','Manajemen Harta Syariah (S2)',	'PSMES-001'	,'06');
insert into program_studi VALUES ('009','Akuntansi Syariah (S2)',	'PSMES-001'	,'06');
insert into program_studi VALUES ('010','Manajemen Keuangan Mikro Syariah (D3)',	'PMKMS-001'	,'05');


INSERT INTO grade VALUES ('001','Grade A','70.00');
INSERT INTO grade VALUES ('002','Grade B','29.00');
INSERT INTO grade VALUES ('003','Grade C','0.00');

INSERT INTO tahun_ajaran VALUES ('001','2020/2021','AKTIF' );

insert into pekerjaan VALUES ('01', 'Tidak Bekerja');
insert into pekerjaan VALUES ('02', 'Pensiun/Almarhum');
insert into pekerjaan VALUES ('03', 'PNS');
insert into pekerjaan VALUES ('04', 'TNI/POLISI');
insert into pekerjaan VALUES ('05', 'Guru/Dosen');
insert into pekerjaan VALUES ('06', 'Pegawai Swasta');
insert into pekerjaan VALUES ('07', 'Pengusaha/Wiraswasta');
insert into pekerjaan VALUES ('08', 'Pengacara/Hakim/Jaksa/Notaris');
insert into pekerjaan VALUES ('09', 'Seniman/Pelukis/Artis/Sejenis');
insert into pekerjaan VALUES ('10', 'Dokter/Bidan/Perawat');
insert into pekerjaan VALUES ('11', 'Pilot/Pramugari');
insert into pekerjaan VALUES ('12', 'Pedagang');
insert into pekerjaan VALUES ('13', 'Petani/Peternak');
insert into pekerjaan VALUES ('14', 'Nelayan');
insert into pekerjaan VALUES ('15', 'Buruh (Tani/Pabrik/Bangunan)');
insert into pekerjaan VALUES ('16', 'Sopir/Masinis/Kondektur');
insert into pekerjaan VALUES ('17', 'Politikus');
insert into pekerjaan VALUES ('18', 'Lainnya');

insert into penghasilan values ('1','Dibawah Rp.1.000.000');
insert into penghasilan values ('2','Rp.1.000.000 - Rp.2.000.000');
insert into penghasilan values ('3','Rp.2.000.001 - Rp.4.000.000');
insert into penghasilan values ('4','Rp.4.000.001 - Rp.6.000.000');
insert into penghasilan values ('5','Diatas Rp.6.000.000');

insert into pendidikan values ('0','Tidak Berpendidikan Formal');
insert into pendidikan values ('1','<= SLTA');
insert into pendidikan values ('2','Diploma');
insert into pendidikan values ('3','S1');
insert into pendidikan values ('4','S2');
insert into pendidikan values ('5','S3');

INSERT INTO periode VALUES ('001','Batch 1 2020-2021','2019-10-01','2020-01-31');
INSERT INTO periode VALUES ('002','Batch 2 2020-2021','2020-02-01','2020-04-30');
INSERT INTO periode VALUES ('003','Batch 3 2020-2021','2020-05-01','2020-08-31');

INSERT INTO jenis_biaya (id, nama) VALUES ('001', 'Pendaftaran');
INSERT INTO jenis_biaya (id, nama) VALUES ('002', 'Daftar Ulang');
INSERT INTO jenis_biaya (id, nama) VALUES ('003', 'Asrama');

insert into bank (id, kode_bank, nama_bank, nomor_rekening, nama_rekening) values
  ('bnisy001', '009', 'BNI Syariah','1234567890', 'Yayasan Tazkia Cendekia');

insert into bank (id, kode_bank, nama_bank, nomor_rekening, nama_rekening) values
  ('bsm001', '451', 'BSM', '9876543210', 'STEI Tazkia');

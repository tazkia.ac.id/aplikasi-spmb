package id.ac.tazkia.spmb.aplikasispmb;

import id.ac.tazkia.spmb.aplikasispmb.dao.TotalDiskonDao;
import id.ac.tazkia.spmb.aplikasispmb.entity.*;
import id.ac.tazkia.spmb.aplikasispmb.service.ApiService;
import id.ac.tazkia.spmb.aplikasispmb.service.TagihanService;
import io.netty.handler.codec.socksx.v5.Socks5PasswordAuthRequestDecoder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AplikasiSpmbApplicationTests {

	@Autowired
	private ApiService apiService;

	@Test
	public void contextxLoads() {
		apiService.insertMahasiswa("2120405051");
	}

	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private TagihanService tagihanService;
	private Socks5PasswordAuthRequestDecoder passwordAuthRequestDecoder;

	@Test
	public void  setTagihanService(){
		ProgramStudi pr = new ProgramStudi();
		pr.setId("001");
		Pendaftar p = new Pendaftar();
		p.setProgramStudi(pr);
		System.out.println(tagihanService.hitungBiayaPendaftaran(p));
	}
	@Test
	public void	generatePassword() {
		System.out.println("afifa123 : " + passwordEncoder.encode("afifa123"));
		System.out.println("22september2001 : " + passwordEncoder.encode("22september2001"));
		System.out.println("170100 : " + passwordEncoder.encode("170100"));
		System.out.println("123mar : " + passwordEncoder.encode("123mar"));


//		Integer selesai = 48;
//		for (	Integer mulai = 2; mulai<= selesai; mulai++) {
//			System.out.println(mulai);
//		}
	}

	@Autowired private TotalDiskonDao totalDiskonDao;
	@Test
	public void hitungTotalDiskon(){
		Date date = new Date();
		SimpleDateFormat formatter= new SimpleDateFormat("MM");
		String bulan = formatter.format(date);
		System.out.println("date = " + bulan );
		TotalDiskon cekBulan = totalDiskonDao.findByBulan(bulan);
		TotalDiskon td = new TotalDiskon();
		if (cekBulan == null) {
			System.out.println("Bulan " + bulan + " belum tersedia");
		} else {
			System.out.println("Bulan " + bulan + " sudah tersedia");
		}
	}
	@Value("${group.pendaftar}") private String groupPendaftar;
	@Value("${group.leads}") private String groupLeads;
	@Test
	public void testMailerlite(){
		Leads leadsDto = new Leads();
		leadsDto.setEmail("haffizhafis@gmail.com");
		leadsDto.setNama("Test Leads Masuk Ga");
//		apiService.insertSubcriber(leadsDto);
//		apiService.insertGroup(leadsDto, groupLeads);


		apiService.insertGroup(leadsDto, groupPendaftar);
		apiService.deleteGroup(leadsDto,groupLeads);
	}

}
